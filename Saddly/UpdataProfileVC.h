//
//  UpdataProfileVC.h
//  Saddly
//
//  Created by Sai krishna on 7/15/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoaderClass.h"
#import "WebServicesMethods.h"
#import "ConnectivityManager.h"
#import "MCLocalization.h"
#import "UIView+Toast.h"
#import "Validate.h"
#import "UpdateProfileOTP.h"

@interface UpdataProfileVC : UIViewController  <NSURLSessionDelegate,UITextFieldDelegate>{
    
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    NSString * Ipaddress,*name;
    NSString * latitude, * longitude,*a123,*methodname;
    
    
    NSUInteger namecount , numbercount,differcount1,differcount2;
    NSString* Names, * Numbers, * otpFromServer;
    
    NSDictionary * profileDict;
    
}




@property (strong, nonatomic) IBOutlet UITextField *TxtUserName;
@property (strong, nonatomic) IBOutlet UITextField *TxtEmailid;
@property (strong, nonatomic) IBOutlet UITextField *TxtMobNum;
@property (strong, nonatomic) IBOutlet UIButton * btnDoneOutlet;



- (IBAction)btnNameEdit:(id)sender;
- (IBAction)btnEmailEdit:(id)sender;


- (IBAction)Done:(id)sender;

@end


