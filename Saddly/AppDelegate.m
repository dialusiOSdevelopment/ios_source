//
//  AppDelegate.m
//  Saddly
//
//  Created by Sai krishna on 1/18/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "AppDelegate.h"
#import <GooglePlus/GooglePlus.h>

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

/** Google Analytics configuration constants **/
//static NSString *const kGaPropertyId = @"UA-93500474-1"; // Placeholder property ID.
//static NSString *const kTrackingPreferenceKey = @"allowTracking";
//static BOOL const kGaDryRun = NO;
//static int const kGaDispatchPeriod = 30;



@interface AppDelegate ()

- (void)initializeGoogleAnalytics;

@end




@implementation AppDelegate

NSString *const kGCMMessageIDKey = @"gcm.message_id";


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    // TODO: Replace the tracker-id with your app one from https://www.google.com/analytics/web/
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-93499939-1"];
    
    // Provide unhandled exceptions reports.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Enable Remarketing, Demographics & Interests reports. Requires the libAdIdAccess library
    // and the AdSupport framework.
    // https://developers.google.com/analytics/devguides/collection/ios/display-features
    tracker.allowIDFACollection = YES;
    
    ///
    [self DemoScreens];
    
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    [GPPSignIn sharedInstance].clientID = @"798402490965-kl9r540uuab90on631bc000168haie6u.apps.googleusercontent.com";
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

    
//    [[UILabel appearance] setFont:[UIFont fontWithName:@"MyriadProRegular" size:17]];
//    [[UILabel appearance] setFont:[UIFont fontWithName:@"JFFlatregular" size:17]];

    
    NSDictionary * languageURLPairs = @{
                                        @"en":[[NSBundle mainBundle] URLForResource:@"en.json" withExtension:nil],
                                        @"ru":[[NSBundle mainBundle] URLForResource:@"ru.json" withExtension:nil],
                                        };
    
    [MCLocalization loadFromLanguageURLPairs:languageURLPairs defaultLanguage:@"en"];
    
    [MCLocalization sharedInstance].noKeyPlaceholder = @"[No '{key}' in '{language}']";
    
    
    NSLog(@"DidFinishLaunchingWithOptions");
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
    } else {
        // iOS 8 or later
        // [START register_for_notifications]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
            }];
            
            // For iOS 10 display notification (sent via APNS)
            [UNUserNotificationCenter currentNotificationCenter].delegate = self;
            // For iOS 10 data message (sent via FCM)
            [FIRMessaging messaging].remoteMessageDelegate = self;
#endif
        }
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        // [END register_for_notifications]
    }
    
    // [START configure_firebase]
    
    [FIRApp configure];
    
    // [END configure_firebase]
    // [START add_token_refresh_observer]
    // Add observer for InstanceID token refresh callback.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
    // [END add_token_refresh_observer]
    
 //   [self initializeGoogleAnalytics];


    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    [[FIRMessaging messaging] disconnect];
    NSLog(@"Disconnected from FCM");
    
    
    [NSTimer scheduledTimerWithTimeInterval: 60 * 60 * 3
                                     target: self
                                   selector:@selector(onTick:)
                                   userInfo: nil repeats:NO];
    
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}



-(void)onTick:(NSTimer *)timer {
    
    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
    
    NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
    [loginName removeObjectForKey:@"userName"];
    
    NSUserDefaults * logOTPMobNumDefaults = [NSUserDefaults standardUserDefaults];
    [logOTPMobNumDefaults removeObjectForKey:@"logOTPMobNumDefaults"];
    
//    NSUserDefaults * contactsinvitedDefaults = [NSUserDefaults standardUserDefaults];
//    [contactsinvitedDefaults removeObjectForKey:@"contactsinvitedDefaults"];
//    
    NSUserDefaults * Notification = [NSUserDefaults standardUserDefaults];
    [Notification removeObjectForKey:@"NotificationDefault"];

    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UINavigationController * login  = (UINavigationController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"navigation"];
    self.window.rootViewController = login;
    
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}




- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//Demo Screen
-(void)DemoScreens
{
    
    NSUserDefaults * demoScreenDefaults = [NSUserDefaults standardUserDefaults];
    
    if(![[demoScreenDefaults valueForKey:@"demoScreenDefaults"] isEqualToString:@"YES"])
    {

        [demoScreenDefaults setObject:@"NO" forKey:@"demoScreenDefaults"];
        
    }
    
}


// Appdelegate shared Instance.
+(AppDelegate*)sharedAppdelegate
{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    NSUserDefaults * Notification = [NSUserDefaults standardUserDefaults];
    [Notification setObject:userInfo forKey:@"NotificationDefault"];
    

    // Print full message.
    NSLog(@"userInfo 4: %@", userInfo);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // Print message ID.
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    NSUserDefaults * Notification = [NSUserDefaults standardUserDefaults];
    [Notification setObject:userInfo forKey:@"NotificationDefault"];
    
    
    // Print full message.
    NSLog(@"userInfo 1: %@", userInfo);
    
    completionHandler(UIBackgroundFetchResultNewData);
}
// [END receive_message]

// [START ios_10_message_handling]
// Receive displayed notifications for iOS 10 devices.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    // Print message ID.
    NSDictionary *userInfo = notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    NSUserDefaults * Notification = [NSUserDefaults standardUserDefaults];
    [Notification setObject:userInfo forKey:@"NotificationDefault"];
    

    // Print full message.
    NSLog(@"userInfo 2: %@", userInfo);
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionNone);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    NSUserDefaults * Notification = [NSUserDefaults standardUserDefaults];
    [Notification setObject:userInfo forKey:@"NotificationDefault"];
    

    // Print full message.
    NSLog(@"userInfo 3: %@", userInfo);
    
    completionHandler();
}
#endif
// [END ios_10_message_handling]

// [START ios_10_data_message_handling]
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
// Receive data message on iOS 10 devices while app is in the foreground.
- (void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    // Print full message
    NSLog(@"%@", remoteMessage.appData);
}
#endif
// [END ios_10_data_message_handling]

// [START refresh_token]
- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    
    NSUserDefaults * TokenDefaults = [NSUserDefaults standardUserDefaults];
    [TokenDefaults setObject:refreshedToken forKey:@"TokenDefault"];
    
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}
// [END refresh_token]

// [START connect_to_fcm]
- (void)connectToFcm {
    // Won't connect since there is no token
    if (![[FIRInstanceID instanceID] token]) {
        return;
    }
    
    // Disconnect previous FCM connection if it exists.
    [[FIRMessaging messaging] disconnect];
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}
// [END connect_to_fcm]

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Unable to register for remote notifications: %@", error);
}

// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
// If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
// the InstanceID token.
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"APNs token retrieved: %@", deviceToken);
    
    // With swizzling disabled you must set the APNs token here.
    // [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
}

// [START connect_on_active]
- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self connectToFcm];
    [FBSDKAppEvents activateApp];
    
//    [GAI sharedInstance].optOut =
//    ![[NSUserDefaults standardUserDefaults] boolForKey:kTrackingPreferenceKey];
//

}

//
//- (void)initializeGoogleAnalytics {
//    
//    [[GAI sharedInstance] setDispatchInterval:kGaDispatchPeriod];
//    [[GAI sharedInstance] setDryRun:kGaDryRun];
//    self.tracker = [[GAI sharedInstance] trackerWithTrackingId:kGaPropertyId];
//}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ];
    // Add any custom logic here.
    return handled;
}
@end

@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}
@end

// [END connect_on_active]

//// [START disconnect_from_fcm]
//- (void)applicationDidEnterBackground:(UIApplication *)application {
//    [[FIRMessaging messaging] disconnect];
//    NSLog(@"Disconnected from FCM");
//}
//// [END disconnect_from_fcm]


