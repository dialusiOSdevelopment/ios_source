//
//  RechargeHistoryCell.h
//  DialuzApp
//
//  Created by Sai krishna on 10/22/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RechargeHistoryCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblRCStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblRCRespMsg;
@property (strong, nonatomic) IBOutlet UILabel *lblRCAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblRCMobNum;


@property (strong, nonatomic)  NSString * RCResMsg;
@property (strong, nonatomic)  NSString * RCOperator;
@property (strong, nonatomic)  NSString * RCOrderId;
@property (strong, nonatomic)  NSString * RCDateTime;


@property (strong, nonatomic)  NSString * RCAmountFromCard;
@property (strong, nonatomic)  NSString * RCAmountFromWallet;
@property (strong, nonatomic)  NSString * RCRefundedAmount;



@property (strong, nonatomic) IBOutlet UIImageView *imgRC;

@end
