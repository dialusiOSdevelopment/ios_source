//
//  SendMoneyVC.m
//  Saddly
//
//  Created by Sai krishna on 1/20/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "SendMoneyVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface SendMoneyVC ()

@end


@implementation SendMoneyVC


- (void)viewDidLoad {
    
    
    //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Send Money Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //

    
    
    _lblWalletBalName.adjustsFontSizeToFitWidth=YES;
    _lblInsuffWalBal.adjustsFontSizeToFitWidth=YES;
    _lblCharLeft.adjustsFontSizeToFitWidth=YES;
    _lblToName.adjustsFontSizeToFitWidth=YES;
    _lblCharLeftName.adjustsFontSizeToFitWidth=YES;
    _lblAmountName.adjustsFontSizeToFitWidth=YES;
    _lblComments.adjustsFontSizeToFitWidth=YES;
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
       
    }
    
    
    self.title = [MCLocalization stringForKey:@"transferCOuponvalue"];
    _lblInsuffWalBal.hidden = YES;
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    // Showing Balance
    NSString * actualCash = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"actualCash"]];
    NSString * bonusCash  = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"bonusCash"]];
    
    
    NSString * actualCashStr, * bonusCashStr;
    
    // Actual & bonus Cash
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ ﷼"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ ﷼"], bonusCash];
    } else {
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ SAR"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ SAR"], bonusCash];
    }
    
    _lblActualCash.text = actualCashStr;
    _lblBonusCash.text = bonusCashStr;
    
    _lblActualCash.adjustsFontSizeToFitWidth = YES;
    _lblBonusCash.adjustsFontSizeToFitWidth = YES;
    

    
    NSString * name = [[[[[[NSUserDefaults standardUserDefaults] valueForKey:@"reciverInfo"]objectForKey:@"name"]stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    
    if (name.length >0) {
        _Nameofreciever.text=name;
    } else {
        _Nameofreciever.hidden=YES;
        
    }
    


    
    NSUserDefaults * sendMoneyDetailsDefaults = [NSUserDefaults standardUserDefaults];
    [sendMoneyDetailsDefaults removeObjectForKey:@"sendMoneyDetailsDefaults"];
    
    
    
    NSUserDefaults * numQRCodeDefaults = [NSUserDefaults standardUserDefaults];
    _txtReceiptentNum.text = [numQRCodeDefaults stringForKey:@"numQRCodeDefaults"];
    
    //Names in English/Arabic
    _lblWalletBalName.text = [MCLocalization stringForKey:@"walletBal"];
    _lblToName.text = [MCLocalization stringForKey:@"to"];
    _lblAmountName.text = [MCLocalization stringForKey:@"Amount"];
    _lblCharLeftName.text = [MCLocalization stringForKey:@"charLeft"];
    _txtReceiptentNum.placeholder=[MCLocalization stringForKey:@"enterrecipient"];
    _txtEnterAmount.placeholder = [MCLocalization stringForKey:@"EnterAmount"];
    _lblInsuffWalBal.text = [MCLocalization stringForKey:@"insuffWalPlsAddToWallet"];
    _lblComments.text = [MCLocalization stringForKey:@"comments"];
    _txtComments.placeholder = [MCLocalization stringForKey:@"enterCommOptional"];
    
    
    [_btnContactOutlet setTitle:[MCLocalization stringForKey:@"pickfrmContact"] forState:UIControlStateNormal];
    [_btnSendNowOutlet setTitle:[MCLocalization stringForKey:@"transferCOuponvalue"] forState:UIControlStateNormal];
    
    
    _btnSendNowOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnSendNowOutlet.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btnSendNowOutlet.titleLabel.numberOfLines = 2;

    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        _lblCurrentBal.text = [[NSString stringWithFormat:@"%@",[profileDict valueForKey:@"totalCash"]] stringByAppendingString:@" ﷼"];
        
    } else {
        
        _lblCurrentBal.text = [[NSString stringWithFormat:@"%@",[profileDict valueForKey:@"totalCash"]] stringByAppendingString:@" SAR"];
    }
    
    NSUserDefaults * mobNumContactsDefaults = [NSUserDefaults standardUserDefaults];
    [mobNumContactsDefaults removeObjectForKey:@"mobNumContactsDefaults"];
    
    NSUserDefaults * txtRecepNumDefaults = [NSUserDefaults standardUserDefaults];
    [txtRecepNumDefaults removeObjectForKey:@"txtRecepNumDefaults"];
    
    
    // Returning Keyboard For Number Pad.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي" style:UIBarButtonItemStyleDone target:self action:@selector(removingKeyboardByTap)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    _txtEnterAmount.inputAccessoryView = keyboardDoneButtonView;
    _txtReceiptentNum.inputAccessoryView = keyboardDoneButtonView;
    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    [super viewDidLoad];
    
    if ([_txtReceiptentNum.text length] >0) {
        [self RecieverInfo];
    }
    // Do any additional setup after loading the view.
}



-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
//    if ([view isKindOfClass:[UIButton class]])
//    {
//        UIButton *lbl = (UIButton *)view;
//        
//         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
//        
//        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
//
//
//        
//        
//    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// Displaying Mobile Number in the text field.
-(void)viewDidAppear:(BOOL)animated{
    
    _lblWalletBalName.adjustsFontSizeToFitWidth=YES;
    _lblInsuffWalBal.adjustsFontSizeToFitWidth=YES;
    _lblCharLeft.adjustsFontSizeToFitWidth=YES;
    _lblToName.adjustsFontSizeToFitWidth=YES;
    _lblCharLeftName.adjustsFontSizeToFitWidth=YES;
    _lblAmountName.adjustsFontSizeToFitWidth=YES;
    _lblComments.adjustsFontSizeToFitWidth=YES;
    
    _lblInsuffWalBal.hidden = YES;
    
    
    // Showing Balance
    NSString * actualCash = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"actualCash"]];
    NSString * bonusCash  = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"bonusCash"]];
    
    
    NSString * actualCashStr1, * bonusCashStr;
    
    // Actual & bonus Cash
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        actualCashStr1 = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ ﷼"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ ﷼"], bonusCash];
    } else {
        actualCashStr1 = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ SAR"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ SAR"], bonusCash];
    }
    
    _lblActualCash.text = actualCashStr1;
    _lblBonusCash.text = bonusCashStr;
    
    _lblActualCash.adjustsFontSizeToFitWidth = YES;
    _lblBonusCash.adjustsFontSizeToFitWidth = YES;
    

    
    NSUserDefaults * mobNumContactsDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * numQRCodeDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * txtRecepNumDefaults = [NSUserDefaults standardUserDefaults];
    
    
    
    NSUserDefaults * amountSentDefaults = [NSUserDefaults standardUserDefaults];
    [amountSentDefaults removeObjectForKey:@"amountSentDefaults"];
    
    
//    if (userEnteredNum.length == 9) {
//        _txtReceiptentNum.text = userEnteredNum;
//    }
    
    if ([[numQRCodeDefaults stringForKey:@"numQRCodeDefaults"] length] > 1) {
        _txtReceiptentNum.text = [[numQRCodeDefaults stringForKey:@"numQRCodeDefaults"]stringByReplacingOccurrencesOfString:@"966" withString:@""];
    }
    
    else if ([[mobNumContactsDefaults stringForKey:@"mobNumContactsDefaults"]length] > 1) {
        
        _txtReceiptentNum.text = [[mobNumContactsDefaults stringForKey:@"mobNumContactsDefaults"] stringByReplacingOccurrencesOfString:@"+966" withString:@""];
        
        [self RecieverInfo];
        [LoaderClass showLoader:self.view];
        
    }
    
    else if ([[txtRecepNumDefaults stringForKey:@"txtRecepNumDefaults"]length] > 1) {
        _txtReceiptentNum.text = [[txtRecepNumDefaults stringForKey:@"txtRecepNumDefaults"] stringByReplacingOccurrencesOfString:@"+966" withString:@""];
    }
    
    [numQRCodeDefaults removeObjectForKey:@"numQRCodeDefaults"];
    [mobNumContactsDefaults removeObjectForKey:@"mobNumContactsDefaults"];
    [txtRecepNumDefaults removeObjectForKey:@"txtRecepNumDefaults"];

    
    NSString * actualCashStr = [profileDict valueForKey:@"actualCash"];
    
    if ([_txtEnterAmount.text intValue] > [actualCashStr intValue]) {
        [_btnSendNowOutlet setTitle:[MCLocalization stringForKey:@"add/sendmoney"] forState:UIControlStateNormal];
        _lblInsuffWalBal.hidden = NO;
        _btnSendNowOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
        _btnSendNowOutlet.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _btnSendNowOutlet.titleLabel.numberOfLines = 2;
        
        
    } else {
        
        [_btnSendNowOutlet setTitle:[MCLocalization stringForKey:@"transferCOuponvalue"] forState:UIControlStateNormal];
        _lblInsuffWalBal.hidden = YES;
        _btnSendNowOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
        _btnSendNowOutlet.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _btnSendNowOutlet.titleLabel.numberOfLines = 2;
        
    }
}




// Returning to Next Text fields
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtReceiptentNum) {
        [self.txtEnterAmount becomeFirstResponder];
    } else if (textField == self.txtEnterAmount) {
        [self.txtComments becomeFirstResponder];
    } else if (textField == self.txtComments) {
        [self.txtComments resignFirstResponder];
    }
    
    return YES;
}



-(void)editingChanged:(UITextField * )sender {
    userEnteredNum = sender.text;
}


-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}



// Calling a User Existing or not Method, When Text field end editing
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    
    if ([textField isEqual:_txtReceiptentNum]) {
        
        [self RecieverInfo];
        
    }
    
}

//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:_txtEnterAmount]) {
        
        [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        
        if([self isNumeric:string]){
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 5;
        } else  {
            return FALSE;
        }
    } else if ([textField isEqual:_txtReceiptentNum]) {
        
        if([self isNumeric:string]){
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 9;
        } else  {
            return FALSE;
        }
    } else if ([textField isEqual:_txtComments]) {
        
        if([string length] == 0) {
            if([_txtComments.text length] != 0)
            {
                return YES;
            }
        } else if([[_txtComments text] length] > 69) {
            return NO;
        }
        [textField addTarget:self
                      action:@selector(textFieldDidChange:)
            forControlEvents:UIControlEventEditingChanged];
        
    }
    return YES;
}


// Entering only numbers not a special Characters
-(BOOL)isNumeric:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}



- (void)textFieldDidChange:(NSNotification *)notification {
    
    // For Displaying the Button as send money or Add/Send money
    // When sent amount > Actual cash
    NSString * actualCashStr = [profileDict valueForKey:@"actualCash"];
    
    if ([_txtEnterAmount.text intValue] > [actualCashStr intValue]) {
        
        [_btnSendNowOutlet setTitle:[MCLocalization stringForKey:@"add/sendmoney"] forState:UIControlStateNormal];
        _lblInsuffWalBal.hidden = NO;
        _btnSendNowOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
        _btnSendNowOutlet.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _btnSendNowOutlet.titleLabel.numberOfLines = 2;

    } else {
        
        [_btnSendNowOutlet setTitle:[MCLocalization stringForKey:@"transferCOuponvalue"] forState:UIControlStateNormal];
        _lblInsuffWalBal.hidden = YES;
        _btnSendNowOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
        _btnSendNowOutlet.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _btnSendNowOutlet.titleLabel.numberOfLines = 2;


    }
    
    // Characters left in the Text field (_txtComments)
    _lblCharLeft.text = [NSString stringWithFormat:@"%lu",70-_txtComments.text.length];
    
}







//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}


// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}



-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];

    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"sendMoneyWebServicesMethod"]) {
                    [self sendMoneyWebServicesMethod];
                }
                else if ([methodname isEqualToString:@"checkTokenStatusWebServices"]) {
                    [self checkTokenStatusWebServices];
                }

            
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//






-(void)sendMoneyWebServicesMethod{
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    

    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    
    
    NSString * usermob =[profileDict valueForKey:@"user_mob"];
    NSString * target_mob= [@"966" stringByAppendingString:_txtReceiptentNum.text];
    NSString * comments = _txtComments.text;
    
    
    [LoaderClass showLoader:self.view];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/addMoney/insertPayMoneyDetails1"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
//Request:user_mob,target_mob,amount,comments,ipAddress,login_status,unique_id,accessToken,from
    


    NSDictionary * sendMoneyDetailsDict =  @{@"user_mob":usermob,
                                           @"target_mob":target_mob,
                                           @"amount":_txtEnterAmount.text,
                                           @"login_status":@"iphone_SA",
                                           @"comments":comments,
                                           @"ipAddress":Ipaddress,
                                           @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                           @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                          };
    
    NSLog(@"Posting sendMoneyDetailsDict is %@",sendMoneyDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:sendMoneyDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *deserr;
            
            NSDictionary *    Kill = [NSJSONSerialization
                                        JSONObjectWithData:data
                                        options:kNilOptions
                                        error:&deserr];
            
            NSDictionary * Killer = [Kill valueForKey:@"transfermoneystatus"];
            
            NSLog(@"Send Money Response is: %@",Killer);
            
            [LoaderClass removeLoader:self.view];
            
            // Saving Send Money Details Dictionary to NSUserDefaults.
            NSUserDefaults *sendMoneyDetailsDefaults = [NSUserDefaults standardUserDefaults];
            NSData * CardTrData = [NSKeyedArchiver archivedDataWithRootObject:Killer];
            [sendMoneyDetailsDefaults setObject:CardTrData forKey:@"sendMoneyDetailsDefaults"];
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
             
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"sendMoneyWebServicesMethod";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                }
                
                else if ([[Killer valueForKey:@"response_message"] isEqualToString:@"Sent"]) {
                    
                    SendMoneySuccessVC * sendMnySuccess = [self.storyboard instantiateViewControllerWithIdentifier:@"SendMoneySuccessVC"];
                    [self.navigationController pushViewController:sendMnySuccess animated:YES];
                    
                    
                } else if ([[Killer valueForKey:@"response_message"] isEqualToString:@"Insufficient Wallet balance"]) {
                    
                    
                    NSString * reqAmount;
                    
                    if ([[profileDict valueForKey:@"actualCash"] integerValue] < [_txtEnterAmount.text integerValue]) {
                        reqAmount = [NSString stringWithFormat:@"%ld", [_txtEnterAmount.text integerValue] - [[profileDict valueForKey:@"actualCash"] integerValue]];
                    } else {
                        reqAmount = _txtEnterAmount.text;
                    }
                    
                    
                    NSUserDefaults * amountSentDefaults = [NSUserDefaults standardUserDefaults];
                    [amountSentDefaults setObject:reqAmount forKey:@"amountSentDefaults"];
                    
                    NSUserDefaults * totalAmountSentDefaults = [NSUserDefaults standardUserDefaults];
                    [totalAmountSentDefaults setObject:_txtEnterAmount.text forKey:@"totalAmountSentDefaults"] ;
                    
                    
                    InsufficientWalBalVC * inSufWallet = [self.storyboard instantiateViewControllerWithIdentifier:@"InsufficientWalBalVC"];
                    [self.navigationController pushViewController:inSufWallet  animated:YES];
                    
                } else if ([[Killer valueForKey:@"response_message"] isEqualToString:@"notsent"]) {
                    
                    
                    alertTitle = [MCLocalization stringForKey:@"failure"];
                    alertMessage = [MCLocalization stringForKey:@"plsTryAgain"];
                    [self alertMethod];
                    
                } else if ([[Killer valueForKey:@"response_message"] isEqualToString:@"account doesn't exist"]) {
                    
                    _Nameofreciever.hidden = YES;
                    _Nameofreciever.text = @"";
                    _txtReceiptentNum.text = @"";
                    
                    toastMsg = [[[MCLocalization stringForKey:@"noAccMobNum"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
                    [self toastMessagemethod];
                    
                } else if ([[Killer valueForKey:@"response_message"] isEqualToString:@"User Mobile Number and Target Mobile Number is Same"]) {
                    
                    
                    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
                        
                        alertMessage = @"لا يمكنك ارسال قيمة الكوبون لنفس رقم الجوال يرجى المحاولة مرة ثانية";
                        
                    } else {
                        
                        alertMessage = @"You Can't Send the Coupon Value to the Same Mobile Number. Please try Again..";
                    }
                    
                    [self alertMethod];
                    
                    
                } else {
                    
                    toastMsg = [[[MCLocalization stringForKey:@"wentWrong"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
                    [self toastMessagemethod];
                    
                    [NSTimer scheduledTimerWithTimeInterval: 3.0
                                                     target: self
                                                   selector:@selector(onTick:)
                                                   userInfo: nil repeats:NO];
                    
                }
                
                NSUserDefaults * numQRCodeDefaults = [NSUserDefaults standardUserDefaults];
                [numQRCodeDefaults removeObjectForKey:@"numQRCodeDefaults"];
                

            }
          
        });
     
     }];
            
    [dataTask resume];

}



-(void)insertPayMoneyServicesResponse:(id)response{

    NSLog(@"%@",response);
}



-(void)onTick:(NSTimer *)timer {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}



// Webservices for Paymeny Gateway
-(void)RecieverInfo {
    
    NSString*uid = [[@"966" stringByAppendingString:_txtReceiptentNum.text] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    
    
    NSString * userId =[profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    [LoaderClass showLoader:self.view];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/profile/getuserprofileDetails"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    NSDictionary * receiverInfoDict =     @{@"username":uid,
                                            @"ipAddress":Ipaddress,
                                            @"from":@"iPhone",
                                            @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                            @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                            };
    
    NSLog(@"Posting receiverInfoDict is %@",receiverInfoDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:receiverInfoDict options:NSJSONWritingPrettyPrinted error:nil];
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            NSLog(@"resSrt: %@", resSrt);
            NSError *deserr;
            
            
            
            [LoaderClass removeLoader:self.view];
            
            NSDictionary*Response=[NSJSONSerialization
                                   JSONObjectWithData:data
                                   options:kNilOptions
                                   error:&deserr];
            
            
            NSLog(@"response is %@", Response);
            
            
            NSLog(@"Recieverinfo response is :%@",Response);
            
            
            _Nameofreciever.hidden=NO;

            NSDictionary*RecRes=[Response valueForKey:@"profileStatus"];
            
            [LoaderClass removeLoader:self.view];
            
            NSString*Name;
            
            NSLog(@"Name is %@",Name);
            
            NSString * abcdxyz = [NSString stringWithFormat:@"%@",[RecRes valueForKey:@"user_name"]];
            
            
            if ([[RecRes valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[RecRes valueForKey:@"user_name"] isEqual:[NSNull null]] || [[RecRes valueForKey:@"user_name"] isEqualToString:@"(null)"] || abcdxyz.length < 1) {
                    
                    Name = @"<null>";
                    
                    alertTitle=[MCLocalization stringForKey:@"noAccMobNum"];
                    alertMessage= [MCLocalization stringForKey:@"plsTryAgain"];
                    [self alertMethod];
                    
                    _txtReceiptentNum.text  = @"";
                    _Nameofreciever.hidden = YES;
                    _Nameofreciever.text = @"";
                    
                } else {
                    
                    Name = [NSString stringWithFormat:@"%@",[[[[RecRes valueForKey:@"user_name"] stringByRemovingPercentEncoding]componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]]  componentsJoinedByString:@" "]];
                    
                    _Nameofreciever.text= Name;
                    
                }

            }
        });
    }];
    
    [dataTask resume];
   
    
}






- (IBAction)btnContacts:(id)sender {
    
    UserContactsVC * contacts = [self.storyboard instantiateViewControllerWithIdentifier:@"UserContactsVC"];
    [self.navigationController pushViewController:contacts animated:YES];
}



- (IBAction)btnBonusCashInfo:(id)sender {
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        alertMessage = @"فقط يمكنك الحصول عليها عند الشحن بميلغ  100 ريال او اكثر";
        
    } else {
        
        alertMessage = @"Can be Redeemed only with 100SAR and above Recharges";
        
    }
    
    [self alertMethod];
    
}


- (IBAction)btnInfo:(id)sender {
    
    NSString * actualCash = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"actualCash"]];
    
    NSString * bonusCash  = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"bonusCash"]];
    
    
    NSString * actualCashStr, * bonusCashStr;
    
    // Actual & bonus Cash Displaying on Alert
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ ﷼"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ ﷼"], bonusCash];
        
    } else {
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ SAR"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ SAR"], bonusCash];
    }
    
    alert = [UIAlertController alertControllerWithTitle:@"" message:[[[[actualCashStr stringByAppendingString:@"\n"] stringByAppendingString:bonusCashStr] stringByAppendingString:@"\n\n"] stringByAppendingString:[MCLocalization stringForKey:@"actualCashMoneyTransfer"]] preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"] style:UIAlertActionStyleDefault  handler:^(UIAlertAction * action) {
        
        [self presentedViewController];
    }];
    
    [alert addAction:okButton];
    alert.view.tintColor = [UIColor colorWithRed:0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    
    [self presentViewController:alert animated:YES completion:nil];
}


// Adding +50,+100,+200 to textfield
- (IBAction)btnAddAmount:(id)sender {
    
    
    if ([_txtEnterAmount.text intValue] < 99800) {
        
        if ([sender tag] == 0) {
            int result = [_txtEnterAmount.text intValue] + 50;
            _txtEnterAmount.text = [NSString stringWithFormat:@"%d", result];
        } else if ([sender tag] == 1) {
            int result = [_txtEnterAmount.text intValue] + 100;
            _txtEnterAmount.text = [NSString stringWithFormat:@"%d", result];
        } else if ([sender tag] == 2) {
            int result = [_txtEnterAmount.text intValue] + 200;
            _txtEnterAmount.text = [NSString stringWithFormat:@"%d", result];
        }
        
    } else  if ([_txtEnterAmount.text intValue] < 99900) {
        
        if ([sender tag] == 0) {
            int result = [_txtEnterAmount.text intValue] + 50;
            _txtEnterAmount.text = [NSString stringWithFormat:@"%d", result];
        } else if ([sender tag] == 1) {
            int result = [_txtEnterAmount.text intValue] + 100;
            _txtEnterAmount.text = [NSString stringWithFormat:@"%d", result];
        }
        
    } else  if ([_txtEnterAmount.text intValue] < 99950) {
        
        if ([sender tag] == 0) {
            int result = [_txtEnterAmount.text intValue] + 50;
            _txtEnterAmount.text = [NSString stringWithFormat:@"%d", result];
        }
    }
    
    
    NSString * actualCashStr = [profileDict valueForKey:@"actualCash"];
    
    if ([_txtEnterAmount.text intValue] > [actualCashStr intValue]) {
        [_btnSendNowOutlet setTitle:[MCLocalization stringForKey:@"add/sendmoney"] forState:UIControlStateNormal];
        _lblInsuffWalBal.hidden = NO;
        _btnSendNowOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
        _btnSendNowOutlet.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _btnSendNowOutlet.titleLabel.numberOfLines = 2;
        

    } else {
        
        [_btnSendNowOutlet setTitle:[MCLocalization stringForKey:@"transferCOuponvalue"] forState:UIControlStateNormal];
        _lblInsuffWalBal.hidden = YES;
        _btnSendNowOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
        _btnSendNowOutlet.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _btnSendNowOutlet.titleLabel.numberOfLines = 2;

    }
    
    
}


- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString * phoneRegex = @"^((5))[0-9]{8}$";
    NSPredicate * phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}


- (IBAction)btnSendNow:(id)sender {
    
    // Recipient Mobile Number
    if ([_txtReceiptentNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg = [MCLocalization stringForKey:@"pleaseEnterMobile"];
        [self toastMessagemethod];
    } else if (![Validate isValidMobileNumber:[_txtReceiptentNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg = [MCLocalization stringForKey:@"mobilevalidate"];
        [self toastMessagemethod];
    } else if (![self validatePhone:[_txtReceiptentNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg = [MCLocalization stringForKey:@"startWith5"];
        [self toastMessagemethod];
    } else if ([_txtEnterAmount.text isEqualToString:@""]) {
        toastMsg = [MCLocalization stringForKey:@"plsenteramount"];
        [self toastMessagemethod];
    } else if ([_txtEnterAmount.text intValue] <= 0) {
        toastMsg = [MCLocalization stringForKey:@"amountGreater0"];
        [self toastMessagemethod];
    } else {
        [self sendMoneyWebServicesMethod];
    }
}



-(void)viewWillDisappear:(BOOL)animated{
    
    NSUserDefaults * txtRecepNumDefaults = [NSUserDefaults standardUserDefaults];
    [txtRecepNumDefaults setObject:_txtReceiptentNum.text forKey:@"txtRecepNumDefaults"];
    
}




@end
