//
//  MenuVC.m
//  Saddly
//
//  Created by Sai krishna on 1/19/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "MenuVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface MenuVC ()

@end

@implementation MenuVC

- (void)viewDidLoad {
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
    }

    
    self.title = [MCLocalization stringForKey:@"account"];
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    [_btnRechargeOutlet setTitle:[MCLocalization stringForKey:@"recharge"] forState:UIControlStateNormal];
    [_btnWalletOutlet setTitle:[MCLocalization stringForKey:@"SlidearrWllt"] forState:UIControlStateNormal];
    [_btnprofileOutlet setTitle:[MCLocalization stringForKey:@"Slidearrprof"] forState:UIControlStateNormal];
    [_btnSendMoneyOutlet setTitle:[MCLocalization stringForKey:@"transferCOuponvalue"] forState:UIControlStateNormal];
    [_btnSendFreeSmsOutlet setTitle:[MCLocalization stringForKey:@"SlidearrSMS"] forState:UIControlStateNormal];
    [_btnInviteFrndsOutlet setTitle:[MCLocalization stringForKey:@"inviteFrnds"] forState:UIControlStateNormal];
    [_btnShareWithFrndsOutlet setTitle:[MCLocalization stringForKey:@"SlidearrSHare"] forState:UIControlStateNormal];
    [_btnContactUsOutlet setTitle:[MCLocalization stringForKey:@"Slifdearrcont"] forState:UIControlStateNormal];
    [_btnAbooutUsOutlet setTitle:[MCLocalization stringForKey:@"SlidearrAbtus"] forState:UIControlStateNormal];
    [_btnSelUrLanguageOutlet setTitle:[MCLocalization stringForKey:@"SlideSelectLanguage"] forState:UIControlStateNormal];
    [_btnlogoutOutlet setTitle:[MCLocalization stringForKey:@"Slidearrlgt"] forState:UIControlStateNormal];
    
    [_HelpName setTitle:[MCLocalization stringForKey:@"help"] forState:UIControlStateNormal];
    
    [_Support setTitle:[MCLocalization stringForKey:@"Support"] forState:UIControlStateNormal];

    
    _btnRechargeOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnWalletOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnprofileOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnSendMoneyOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnSendFreeSmsOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnInviteFrndsOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnShareWithFrndsOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnContactUsOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnAbooutUsOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnSelUrLanguageOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnlogoutOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnShareWithFrndsOutlet.titleLabel.adjustsFontSizeToFitWidth=YES;
    
    
    
    _lblUserName.text = [[[[profileDict valueForKey:@"user_name"] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    _lblUserEmail.text = [profileDict valueForKey:@"user_email_id"];
    _lblMobNum.text = [profileDict valueForKey:@"user_mob"];
    _lblUserName.adjustsFontSizeToFitWidth = YES;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 

-(void)viewDidAppear:(BOOL)animated {

    
    _lblUserName.text = [[[[profileDict valueForKey:@"user_name"] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    _lblUserEmail.text = [profileDict valueForKey:@"user_email_id"];
    _lblMobNum.text = [profileDict valueForKey:@"user_mob"];
    _lblUserName.adjustsFontSizeToFitWidth = YES;
    
    
//    // Getting Image URL
//    NSString * imgLocation = [profileDict valueForKey:[@"user_image" stringByReplacingOccurrencesOfString:@" " withString:@""]];
//    
//    if ([imgLocation isEqual:[NSNull null]] || [imgLocation isEqualToString:@"null"] || imgLocation.length < 1){
//
    
        if ([[profileDict valueForKey:@"gender"] isEqual:[NSNull null]] || [[profileDict valueForKey:@"gender"] isEqualToString:@"null"]){
            
            _imgProfileImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"appicon_green.png"]];

        } else if ([[profileDict valueForKey:@"gender"] isEqualToString:@"male"]) {
            
            _imgProfileImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"man.png"]];

        } else if ([[profileDict valueForKey:@"gender"] isEqualToString:@"female"]) {
            
            _imgProfileImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"female.png"]];
            
        } else {
        
            _imgProfileImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"appicon_green.png"]];

        }
    
//    } else {
//    
//        url = @"http://www.dialus.com/english/images/www/saddly/user_images/";
//        appendedUrl = [url stringByAppendingPathComponent:imgLocation];
//        
//        _imgProfileImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[appendedUrl stringByReplacingOccurrencesOfString:@"+" withString:@"%20"]]]];
//    }
    
    _imgProfileImage.layer.cornerRadius = self.imgProfileImage.frame.size.width / 2;
    _imgProfileImage.clipsToBounds = YES;
    _imgProfileImage.layer.masksToBounds = YES;
    
}




-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
        [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}



- (IBAction)btnRecharge:(id)sender {
    
    OperatorsVC * operator = [self.storyboard instantiateViewControllerWithIdentifier:@"OperatorsVC"];
    operator.hidesBottomBarWhenPushed = YES;
    
    NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
    [btnRcDefaults setObject:@"100" forKey:@"btnRcDefaults"];
    
    [self.navigationController pushViewController:operator animated:YES];
    
}

- (IBAction)btnWallet:(id)sender {
    
    WalletVC * wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"WalletVC"];
    wallet.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:wallet animated:YES];
    
}

- (IBAction)btnprofile:(id)sender {
    
    MyProfileVC * profile = [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileVC"];
    profile.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:profile animated:YES];
   
}

- (IBAction)btnSendMoney:(id)sender {
    
    PayOrSendVC * payOrSend = [self.storyboard instantiateViewControllerWithIdentifier:@"PayOrSendVC"];
    payOrSend.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:payOrSend animated:YES];
    
}

- (IBAction)btnSendFreeSms:(id)sender {
    
    FreeSMSVC * sms = [self.storyboard instantiateViewControllerWithIdentifier:@"FreeSMSVC"];
    sms.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:sms animated:YES];
    
}






- (IBAction)btnShareWithFnds:(id)sender {
    
    ShareViewController * shareVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareViewController"];
    shareVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:shareVC animated:YES];
    
}



- (IBAction)Help:(id)sender {

    HelpVC * help = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpVC"];
    help.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:help animated:YES];
    
}



- (IBAction)btnContactUs:(id)sender {
    
    ContactUSDetailVC * contactUs = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUSDetailVC"];
    contactUs.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:contactUs animated:YES];
    
}

- (IBAction)btnAbooutUs:(id)sender {
    
    AboutUsVC * aboutUs = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsVC"];
    aboutUs.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:aboutUs animated:YES];
    
}

- (IBAction)btnSelUrLanguage:(id)sender {
    
    ChooseLanguage * language = [self.storyboard instantiateViewControllerWithIdentifier:@"ChooseLanguage"];
    language.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:language animated:YES];
    
}
-(void)logoutservices {
    
    [LoaderClass showLoader:self.view];
    
    NSString * userId =[profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    NSString * UEmail= [profileDict valueForKey:@"user_email_id"];
    NSString*mob= [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
        
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url123 = [NSURL URLWithString:@"https://m.app.saddly.com/profile/logoutDetails"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url123];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * logoutDetailsDict =  @{@"usermob":mob,
                                          @"user_emaiId":UEmail,
                                          @"ipAddress":Ipaddress,
                                          @"from":@"iPhone",
                                          @"accessToken":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                          @"uniqueid":[uuidDefaults stringForKey:@"uuidDefaults"],
                                          @"db":[MCLocalization stringForKey:@"DBvalue"],
                                        };
    
    NSLog(@"Posting logoutDetailsDict is %@",logoutDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:logoutDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSLog(@"No data returned from server, error ocurred: %@", error);
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
    
    
    
            NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
            NSLog(@"resSrt: %@", resSrt);
            
            [LoaderClass removeLoader:self.view];
    
    
        });

    }];

    [dataTask resume];


}






- (IBAction)btnlogout:(id)sender {
    
    [self logoutservices];
    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
    
    NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
    [loginName removeObjectForKey:@"userName"];
    
    NSUserDefaults * logOTPMobNumDefaults = [NSUserDefaults standardUserDefaults];
    [logOTPMobNumDefaults removeObjectForKey:@"logOTPMobNumDefaults"];
    
//    NSUserDefaults * contactsinvitedDefaults = [NSUserDefaults standardUserDefaults];
//    [contactsinvitedDefaults removeObjectForKey:@"contactsinvitedDefaults"];
//    
    NSUserDefaults * Notification = [NSUserDefaults standardUserDefaults];    
    [Notification removeObjectForKey:@"NotificationDefault"];

    
    LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:login animated:YES];
    

    
}
- (IBAction)Support:(id)sender {
    
    WriteUsVC * aboutUs = [self.storyboard instantiateViewControllerWithIdentifier:@"WriteUsVC"];
    aboutUs.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:aboutUs animated:YES];
}
@end
