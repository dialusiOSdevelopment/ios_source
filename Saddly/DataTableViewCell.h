//
//  DataTableViewCell.h
//  DialuzApp
//
//  Created by Sai krishna on 11/10/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblData;
@property (strong, nonatomic) IBOutlet UIButton *btnDataOutlet;

@end
