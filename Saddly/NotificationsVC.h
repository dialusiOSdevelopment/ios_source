//
//  NotificationsVC.h
//  Saddly
//
//  Created by Sai krishna on 2/23/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "UIView+Toast.h"
#import "ConnectivityManager.h"
#import "WebServicesMethods.h"
#import "LoaderClass.h"
#import "SendMoneyVC.h"
#import "OperatorsVC.h"
#import "NotiTableViewCell.h"
#import "WalletVC.h"

@interface NotificationsVC : UIViewController  <UITableViewDataSource, UITableViewDelegate, NSURLSessionDelegate>{
    
    UILabel * noDataLabel;
    NSString * bodyAbc, * titleAbc,*a123,*methodname;
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle,* alertMessage, * toastMsg;

    NSUInteger height;
    
    NSDictionary * profileDict;
    
    NSArray * notiCount, * notiDateArray, * notiMsgArray, * notiTypeArray;
    NSString * str;
    
    
}


@property (strong, nonatomic) IBOutlet UITableView *tblNotifications;



@end
