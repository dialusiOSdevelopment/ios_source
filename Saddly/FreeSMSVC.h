//
//  FreeSMSVC.h
//  DialuzApp
//
//  Created by gandhi on 28/06/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServicesMethods.h"
#import "UIView+Toast.h"
#import "Validate.h"
#import "MCLocalization.h"
#import "ConnectivityManager.h"
#import "UserContactsVC.h"
#import "LoaderClass.h"
#import "UserSendSMSCell.h"
#import "LoginViewController.h"

#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"



@interface FreeSMSVC : UIViewController<UITextFieldDelegate, UITextViewDelegate,UITableViewDataSource,UITableViewDelegate, NSURLSessionDelegate>{
    
    NSDictionary  * smsCountDetails, * smsHistDetails;
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg,*a123,*methodname;
    
    NSArray * message, * mobile, * smsDate;
    NSUInteger height;
    
        NSDictionary * profileDict;
    
    NSMutableDictionary * freeSmsDetails;
}

@property (strong, nonatomic) IBOutlet UITextField * txtMobNum;
@property (strong, nonatomic) IBOutlet UITextView * txtViewMsg;
@property (strong, nonatomic) IBOutlet UILabel *charactersLeftMsg;
@property (strong, nonatomic) IBOutlet UILabel *dailySmsCount;

- (IBAction)btnContacts:(id)sender;
- (IBAction)btnFreeSmsSend:(id)sender;

//Names
@property (strong, nonatomic) IBOutlet UILabel *msgsTodayName;
@property (strong, nonatomic) IBOutlet UILabel *mobNumName;
@property (strong, nonatomic) IBOutlet UILabel *msgName;
@property (strong, nonatomic) IBOutlet UILabel *charLeftName;
@property (strong, nonatomic) IBOutlet UIButton *sendName;
@property (strong, nonatomic) IBOutlet UIButton *btnMsgToFrndsName;



@property (strong, nonatomic) IBOutlet UITableView *tblSMSHistory;


@end
