//
//  SocialMediaViewController.h
//  Saddly
//
//  Created by Kishan Gandhi on 21/03/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import <CoreMotion/CoreMotion.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <GooglePlus/GooglePlus.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>

@interface SocialMediaViewController : UIViewController {
    
    
}


//@interface MyViewController : UIViewController <GPPSignInDelegate>



- (IBAction)Twitter:(id)sender;

- (IBAction)Googleplus:(id)sender;


@property (weak, nonatomic) NSDictionary * socialMediaDict;

@property (weak, nonatomic) IBOutlet FBSDKLikeControl *likeButton;
@property (weak, nonatomic) IBOutlet FBSDKShareButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *btnTwitterFollowOutlet;
@property (weak, nonatomic) IBOutlet  UIButton *btnGooglePlusShareOutlet;



- (IBAction)btnInstagramFollow:(id)sender;




@end
