//
//  ForgotPwdOTPVC.h
//  Saddly
//
//  Created by Sai krishna on 8/6/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "LoaderClass.h"
#import "UIView+Toast.h"
#import "ConnectivityManager.h"
#import "Validate.h"
#import "LoginViewController.h"


@interface ForgotPwdOTPVC : UIViewController <NSURLSessionDelegate, UITextFieldDelegate> {
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    
     NSString * methodname, * a123;
    
}

@property (strong, nonatomic) NSString * forOTPPwd;
@property (strong, nonatomic) NSString * forgotMobnum;
@property (strong, nonatomic) NSString * forgotPwd;

@property (strong, nonatomic) IBOutlet UILabel *lblGreetings;
@property (strong, nonatomic) IBOutlet UILabel *lblEnterOTPName;
@property (strong, nonatomic) IBOutlet UITextField *txtEnterOTP;
@property (strong, nonatomic) IBOutlet UITextView *txtViewMsg;



@property (strong, nonatomic) IBOutlet UIButton *btnSubmitOTPOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnShowPwdOutlet;

- (IBAction)btnShowPwd:(id)sender;

- (IBAction)btnSubmitOTP:(id)sender;


@end
