//
//  RechargeConfirmationVC.m
//  DialuzApp
//
//  Created by Sai krishna on 10/31/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import "RechargeConfirmationVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);



@interface RechargeConfirmationVC ()

@end

@implementation RechargeConfirmationVC

- (void)viewDidLoad {
    
    _Namewalletamount.adjustsFontSizeToFitWidth=YES;
    _Namerechargeamount.adjustsFontSizeToFitWidth=YES;
    _Nameremainingamount.adjustsFontSizeToFitWidth=YES;
    _lblRemainingBalAmount.adjustsFontSizeToFitWidth=YES;
    _lblUncheckWallet.adjustsFontSizeToFitWidth=YES;
    _lblRemainingBalAmount.adjustsFontSizeToFitWidth=YES;
    
    _Usedialuswalletamount.adjustsFontSizeToFitWidth=YES;
    _otherPaymentModesName.adjustsFontSizeToFitWidth=YES;
    _fromOtherPaymentModes.adjustsFontSizeToFitWidth=YES;
    _fromOtherPaymentModes.adjustsFontSizeToFitWidth=YES;
    _lbltotAmountToBePaid.adjustsFontSizeToFitWidth=YES;
    _Checkbox.titleLabel.adjustsFontSizeToFitWidth=YES;
    
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
        
    }
    
    NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
        
        self.title = [MCLocalization stringForKey:@"RechargeConfirmation"];
        
    } else {
        
        self.title = [MCLocalization stringForKey:@"purConfirmation"];
        
    }
    
    
    NSUserDefaults * rechargeAmountFromTextField = [NSUserDefaults standardUserDefaults];
    
    _Namewalletamount.text=[MCLocalization stringForKey:@"WalletAmount"] ;
    _Namerechargeamount.text=[MCLocalization stringForKey:@"payBalAmount"] ;
    _Nameremainingamount.text=[MCLocalization stringForKey:@"RemainingWallet"] ;
    _otherPaymentModesName.text=[MCLocalization stringForKey:@"OtherPaymentModes"];
    _lblUncheckWallet.text = [MCLocalization stringForKey:@"uncheckOtherPayment"];
    _fromOtherPaymentModes.text = [MCLocalization stringForKey:@"frmOhterPayModes"];
    _lbltotAmountToBePaid.text = [[MCLocalization stringForKey:@"totAmountTobepaid"] stringByAppendingString:[rechargeAmountFromTextField stringForKey:@"rechargeAmountFromTextField"]];
    
    
    
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    [_Namepaynow setTitle:[MCLocalization stringForKey:@"Paynow"] forState:UIControlStateNormal];
    [_Checkbox setTitle:[MCLocalization stringForKey:@"UsewalletDialus"] forState:UIControlStateNormal];
    [_NamecreditcardDebtcard setTitle:[MCLocalization stringForKey:@"CREDIT"] forState:UIControlStateNormal];
    [_NameSadad setTitle:[MCLocalization stringForKey:@"sadad"] forState:UIControlStateNormal];
    
    [_DebtATmName setTitle:[MCLocalization stringForKey:@"DEBIT/ATMCARD"] forState:UIControlStateNormal];
    
    NSUserDefaults *WalletDefaults = [NSUserDefaults standardUserDefaults];
    
    Walletdetails = [NSKeyedUnarchiver unarchiveObjectWithData:[WalletDefaults objectForKey:@"WalletDefaults"]];
    
    
    NSLog(@"WaletDeatilsare%@",Walletdetails);
    
    
    
    
    _WalletAmount.text=[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"totalCash"]];
    
    _payBalAmount.text =[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"amtHasToPayFrmCard"]];
    
    _Usedialuswalletamount.text =[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"rechargeAmount"]];
    
    _lblRemainingBalAmount.text =[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"remwalletamt"]];
    
    
    if ([_WalletAmount.text isEqualToString:@"0"]) {
        
        _lblUncheckWallet.hidden = YES;
        
        [_Checkbox setSelected:YES];
        [_Checkbox setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateSelected];
        _Usedialuswalletamount.text= @"0";
        _lblRemainingBalAmount.text = @"0";
        
        
        
    }
    
    else if ([[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"rechargeAmount"]] intValue] <100 && [[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"frmActulaCash"]] isEqualToString:@"0"]) {
        
        
        
        _lblUncheckWallet.hidden = YES;
        
        [_Checkbox setSelected:YES];
        [_Checkbox setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateSelected];
        _Usedialuswalletamount.text= @"0";
        _lblRemainingBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"remwalletamt"]];
        
    }
    
    else if ([[rechargeAmountFromTextField stringForKey:@"rechargeAmountFromTextField"] intValue] > [[Walletdetails valueForKey:@"totalCash"]  intValue]) {
        
        [_Checkbox setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateNormal];
        
        _Usedialuswalletamount.text= [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"totalCash"]];
        
        _otherPaymentModesName.hidden = NO;
        _NamecreditcardDebtcard.hidden = NO;
        _DebtATmName.hidden=NO;
        _NameSadad.hidden = NO;
        _btnoperatorLogos.hidden = NO;
        contactButton.hidden = NO;
        
        _Namerechargeamount.hidden = NO;
        _payBalAmount.hidden = NO;
        _fromOtherPaymentModes.hidden = NO;
        
    }
    
    ////
    else if ([[Walletdetails valueForKey:@"rechargeAmount"]  intValue]<100 &&[[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"frmBonusCash"]] isEqualToString:@"0"]) {
        
        if ([[rechargeAmountFromTextField stringForKey:@"rechargeAmountFromTextField"]intValue]>[[Walletdetails valueForKey:@"frmActulaCash"]intValue]) {
            
            [_Checkbox setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateNormal];
            
            _Usedialuswalletamount.text= [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"actualCash"]];
            
            _payBalAmount.text =[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"amtHasToPayFrmCard"]];
            
            _lblRemainingBalAmount.text =[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"bonusCash"]];
            
            
            _otherPaymentModesName.hidden = NO;
            _NamecreditcardDebtcard.hidden = NO;
            _DebtATmName.hidden=NO;
            _NameSadad.hidden = NO;
            _btnoperatorLogos.hidden = NO;
            contactButton.hidden = NO;
            
            _Namerechargeamount.hidden = NO;
            _payBalAmount.hidden = NO;
            _fromOtherPaymentModes.hidden = NO;
            
            
        } else{
            
            
            [_Checkbox setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateNormal];
            
            _Usedialuswalletamount.text= [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"rechargeAmount"]];
            
            _otherPaymentModesName.hidden = YES;
            _NamecreditcardDebtcard.hidden = YES;
            _DebtATmName.hidden=YES;
            _btnoperatorLogos.hidden = YES;
            _NameSadad.hidden = YES;
            contactButton.hidden = YES;
            
            _Namerechargeamount.hidden = YES;
            _payBalAmount.hidden = YES;
            _fromOtherPaymentModes.hidden = YES;
            
        }
    }
    
    else {
        
        [_Checkbox setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateNormal];
        
        _Usedialuswalletamount.text= [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"rechargeAmount"]];
        
        _otherPaymentModesName.hidden = YES;
        _NamecreditcardDebtcard.hidden = YES;
        _DebtATmName.hidden=YES;
        _btnoperatorLogos.hidden = YES;
        _NameSadad.hidden = YES;
        contactButton.hidden = YES;
        
        _Namerechargeamount.hidden = YES;
        _payBalAmount.hidden = YES;
        _fromOtherPaymentModes.hidden = YES;
        
    }
    
    
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    _Namewalletamount.adjustsFontSizeToFitWidth=YES;
    _Namerechargeamount.adjustsFontSizeToFitWidth=YES;
    _Nameremainingamount.adjustsFontSizeToFitWidth=YES;
    _lblRemainingBalAmount.adjustsFontSizeToFitWidth=YES;
    _lblUncheckWallet.adjustsFontSizeToFitWidth=YES;
    _lblRemainingBalAmount.adjustsFontSizeToFitWidth=YES;
    
    _Usedialuswalletamount.adjustsFontSizeToFitWidth=YES;
    _otherPaymentModesName.adjustsFontSizeToFitWidth=YES;
    _fromOtherPaymentModes.adjustsFontSizeToFitWidth=YES;
    _fromOtherPaymentModes.adjustsFontSizeToFitWidth=YES;
    _lbltotAmountToBePaid.adjustsFontSizeToFitWidth=YES;
    _Checkbox.titleLabel.adjustsFontSizeToFitWidth=YES;
    
    
    UIImage* needHelp = [UIImage imageNamed:@"sadad-logo.png"];
    CGRect frameimg = CGRectMake(0, 0, 50, 30);
    contactButton = [[UIButton alloc] initWithFrame:frameimg];
    [contactButton setBackgroundImage:needHelp forState:UIControlStateNormal];
    [contactButton setShowsTouchWhenHighlighted:NO];
    contactButton.hidden = YES;
    
    UIBarButtonItem * sadadButton = [[UIBarButtonItem alloc] initWithCustomView:contactButton];
    self.navigationItem.rightBarButtonItem = sadadButton;
    
}

-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
        [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
        
        
        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




          
            

-(void)getQucikPayDetails{
    NSString*db;
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){

        db=@"AR";
    } else {
        db=@"SA";

    }
    
    NSUserDefaults * Couponcode = [NSUserDefaults standardUserDefaults];
    NSString*couponCode;
    
    if ([couponCode length]<1) {
        couponCode=@"NIL";
        
    }
    else{
        couponCode = [Couponcode stringForKey:@"Couponecodenum"];
        
    }
    
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*ipAddress=[ipaddressdefault stringForKey:@"ipaddressdefault"];
    
    NSUserDefaults * plandefaults = [NSUserDefaults standardUserDefaults];
    NSString*rechargePlan= [plandefaults stringForKey:@"plandefaults"];
    

    NSUserDefaults * rcMobNum = [NSUserDefaults standardUserDefaults];
    NSString*rechargemobileno= [@"966" stringByAppendingString:[rcMobNum stringForKey:@"rcMobNum"]];

    NSString*rechargeamount = _payBalAmount.text;
    
    
    
    NSString * fromBonusCash=[Walletdetails valueForKey:@"frmBonusCash"];
    
    
    NSString * fromActualCash=[Walletdetails valueForKey:@"frmActulaCash"];
    
    //float abc=10.3;
    
    
    NSString*email;
    
    if ([[profileDict valueForKey:@"user_email_id"] isEqual:[NSNull null]]) {
        email=@"info@saddly.com";
    }else{
        
        email=[[profileDict valueForKey:@"user_email_id"]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
       
    }
    
    
    NSString*mobileNo=[profileDict valueForKey:@"user_mob"];
    NSString*username=[profileDict valueForKey:@"user_name"];
    
    //stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
    NSString* operator=   [selectedOperator stringForKey:@"selectedOperator"];
    
    NSUserDefaults * serviceIdDefaults = [NSUserDefaults standardUserDefaults];
    NSString*serviceId=[serviceIdDefaults stringForKey:@"serviceIdDefaults"];
    
    
    ///
    
    
    [LoaderClass showLoader:self.view];
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/insertQuickPayDetails1"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
 
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    //         NSString * userPwd=@"585858";
    
    NSLog(@"%@",userId);
    NSLog(@"%@",userPwd);
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];

    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    NSDictionary * loginDetailsDict =  @{@"db":db,
                                         @"couponCode":couponCode,
                                         @"ipAddress":ipAddress,
                                         @"rechargePlan":rechargePlan,
                                         @"rechargemobileno":rechargemobileno,
                                         @"rechargeamount":rechargeamount,
                                         @"fromBonusCash":fromBonusCash,
                                         
                                         @"fromActualCash":fromActualCash,
                                         @"email":email,
                                         @"userMobileNo":mobileNo,
                                         @"from":@"iphone",
                                         @"userName":username,
                                         @"operator":operator,
                                         @"access_token":accessToken,
                                         @"unique_id":uniqueid,
                                         @"serviceId":serviceId,
                                         
                                         };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*Response = [NSJSONSerialization
                                     
                                     JSONObjectWithData:data
                                     
                                     options:kNilOptions
                                     
                                     error:&deserr];
            
            
            
            
            
          str =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            if ([[Response valueForKey:@"quickPayRechargeDetails"] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"getQucikPayDetails";
                
                [self checkTokenStatusWebServices];
                
                
            }else if ([[Response valueForKey:@"quickPayRechargeDetails"] isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{
            
            NSLog(@"Killer Response is%@",[Response valueForKey:@"quickPayRechargeDetails"]);
            
            NSLog(@"str Response is%@",str);
        
            
            NSLog(@"got response=%@", [Response valueForKey:@"quickPayRechargeDetails"] );

            
        str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        
        NSUserDefaults * trasactionid = [NSUserDefaults standardUserDefaults];
        
        [trasactionid setObject:[Response valueForKey:@"quickPayRechargeDetails"] forKey:@"trasactionid"];
        
        NSLog(@"Response is%@",str);

    PayFromWalletVC * payfromwallet = [self.storyboard instantiateViewControllerWithIdentifier:@"PayFromWalletVC"];
    
    [self.navigationController pushViewController:payfromwallet animated:YES];
    
    
            }
        });
        
        
        
    }];
    
    
    [dataTask resume];
    
    
    
    
}




//  #pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}



- (IBAction)SadadSelected:(id)sender {
    
    otherPaymentModesDefaults = [NSUserDefaults standardUserDefaults];
    [otherPaymentModesDefaults setObject:[MCLocalization stringForKey:@"sadad"] forKey:@"otherPaymentModesDefaults"];
    
    
    NSUserDefaults * amountFromPayBalAmountDefaults = [NSUserDefaults standardUserDefaults];
    [amountFromPayBalAmountDefaults setObject:_payBalAmount.text forKey:@"amountFromPayBalAmountDefaults"];
    
    NSLog(@"Amount is %@",_payBalAmount.text);
    
    
    NSString * fromBonusCash;
    NSString * fromActualCash;
    
    if([_Checkbox isSelected]==YES) {
        
        fromBonusCash = @"0";
        fromActualCash = @"0";
        
    } else {
        
        fromBonusCash=[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"frmBonusCash"]];
        fromActualCash=[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"frmActulaCash"]];
        
    }
    
    
    
    NSUserDefaults*actualcashDefaults=[NSUserDefaults standardUserDefaults];
    [actualcashDefaults setObject:fromActualCash forKey:@"fromActualCash"];
    
    
    NSUserDefaults*BonuscashDefaults=[NSUserDefaults standardUserDefaults];
    [BonuscashDefaults setObject:fromBonusCash forKey:@"fromBonusCash"];
    
    
    if([_Checkbox isSelected]==YES) {
        
        Paidamount=[Walletdetails valueForKey:@"rechargeAmount"];
        
    } else {
        
        Paidamount=[Walletdetails valueForKey:@"amtHasToPayFrmCard"];
        
        
    }
    NSUserDefaults*paidamountdefaults=[NSUserDefaults standardUserDefaults];
    [paidamountdefaults setObject:Paidamount forKey:@"Paidamount"];
    
    
    NSUserDefaults * sadadefaults = [NSUserDefaults standardUserDefaults];
    [sadadefaults setObject:@"100" forKey:@"sadadefaults"];
    
    //    alertTitle =   [MCLocalization stringForKey:@"sadadNotSupporting"];
    //    alertMessage =  [MCLocalization stringForKey:@"chooseCredit/Debit"];
    //    [self alertMethod];
    
    PayfromCard * PayFromCard = [self.storyboard instantiateViewControllerWithIdentifier:@"PayfromCard"];
    [self.navigationController pushViewController:PayFromCard animated:YES];
    
}



- (IBAction)Creditordebitselected:(id)sender {
    
    
    
    otherPaymentModesDefaults = [NSUserDefaults standardUserDefaults];
    [otherPaymentModesDefaults setObject:[MCLocalization stringForKey:@"CREDIT"] forKey:@"otherPaymentModesDefaults"];
    
    
    NSUserDefaults * sadadefaults = [NSUserDefaults standardUserDefaults];
    
    
    [sadadefaults setObject:@"200" forKey:@"sadadefaults"];
    
    Paymentmethod = [MCLocalization stringForKey:@"CREDIT"];
    
    NSUserDefaults * amountFromPayBalAmountDefaults = [NSUserDefaults standardUserDefaults];
    [amountFromPayBalAmountDefaults setObject:_payBalAmount.text forKey:@"amountFromPayBalAmountDefaults"];
    
    NSLog(@"Amount is %@",_payBalAmount.text);
    
    
    [self CardTRansactiondetails];
    
}

- (IBAction)DebitAtmcard:(id)sender {
    
    
    
    otherPaymentModesDefaults = [NSUserDefaults standardUserDefaults];
    [otherPaymentModesDefaults setObject:[MCLocalization stringForKey:@"DEBIT/ATMCARD"] forKey:@"otherPaymentModesDefaults"];

    
    NSUserDefaults * sadadefaults = [NSUserDefaults standardUserDefaults];
    
    
    [sadadefaults setObject:@"300" forKey:@"sadadefaults"];
    
    
    Paymentmethod = [MCLocalization stringForKey:@"DEBIT/ATMCARD"];
    
    NSUserDefaults * amountFromPayBalAmountDefaults = [NSUserDefaults standardUserDefaults];
    [amountFromPayBalAmountDefaults setObject:_payBalAmount.text forKey:@"amountFromPayBalAmountDefaults"];
    
    NSLog(@"Amount is %@",_payBalAmount.text);
    
    [self CardTRansactiondetails];
    
    
}


- (IBAction)Checkbox:(id)sender {
    
    
    NSUserDefaults * rechargeAmountFromTextField = [NSUserDefaults standardUserDefaults];
    
    if ([_WalletAmount.text isEqualToString:@"0"]) {
        
        _lblUncheckWallet.hidden = YES;
        
        [_Checkbox setSelected:NO];
        [_Checkbox setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateSelected];
        _Usedialuswalletamount.text= @"0";
        _lblRemainingBalAmount.text = @"0";
        
    }
    
    else if ([[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"rechargeAmount"]] intValue] <100 && [[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"frmActulaCash"]] isEqualToString:@"0"]) {
        
        _lblUncheckWallet.hidden = YES;
        
        [_Checkbox setSelected:YES];
        [_Checkbox setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateSelected];
        _Usedialuswalletamount.text= @"0";
        _lblRemainingBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"remwalletamt"]];
        
    }
    
    if ([[rechargeAmountFromTextField stringForKey:@"rechargeAmountFromTextField"] intValue]>[[Walletdetails valueForKey:@"frmActulaCash"]intValue] &&[[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"frmBonusCash"]] isEqualToString:@"0"]) {
        
        [_Checkbox setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateNormal];
        
        
        if([_Checkbox isSelected]==YES) {
            
            
            [_Checkbox setSelected:NO];
            [_Checkbox setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateSelected];
            _Usedialuswalletamount.text= [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"totalCash"]];
            _payBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"amtHasToPayFrmCard"]];
            _lblRemainingBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"remwalletamt"]];
            
            
            _lblUncheckWallet.hidden = NO;
            _btnoperatorLogos.hidden = NO;
            _NamecreditcardDebtcard.hidden = NO;
            _DebtATmName.hidden=NO;
            _NameSadad.hidden = NO;
            contactButton.hidden = NO;
            
            _Namerechargeamount.hidden = NO;
            _payBalAmount.hidden = NO;
            _fromOtherPaymentModes.hidden = NO;
            
        } else {
            
            [_Checkbox setSelected:YES];
            
            
            [_Checkbox setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateSelected];
            
            _payBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"rechargeAmount"]];
            _lblRemainingBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"totalCash"]];
            
            _Usedialuswalletamount.text= @"0";
            
            _lblUncheckWallet.hidden = YES;
            _otherPaymentModesName.hidden = NO;
            _NamecreditcardDebtcard.hidden = NO;
            _DebtATmName.hidden=NO;
            _NameSadad.hidden = NO;
            _btnoperatorLogos.hidden = NO;
            contactButton.hidden = NO;
            
            _Namerechargeamount.hidden = NO;
            _payBalAmount.hidden = NO;
            _fromOtherPaymentModes.hidden = NO;
            
        }
        
    }
    //////
    else if ([[Walletdetails valueForKey:@"rechargeAmount"]  intValue]<100 &&[[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"frmBonusCash"]] isEqualToString:@"0"]) {
        
        if ([[rechargeAmountFromTextField stringForKey:@"rechargeAmountFromTextField"]intValue]>[[Walletdetails valueForKey:@"frmActulaCash"]intValue]) {
            
            [_Checkbox setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateNormal];
            
            
            
            if([_Checkbox isSelected]==YES) {
                
                
                [_Checkbox setSelected:NO];
                [_Checkbox setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateSelected];
                _Usedialuswalletamount.text= [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"actualCash"]];
                _payBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"amtHasToPayFrmCard"]];
                _lblRemainingBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"bonusCash"]];
                
                
                _lblUncheckWallet.hidden = NO;
                _btnoperatorLogos.hidden = NO;
                _NamecreditcardDebtcard.hidden = NO;
                _DebtATmName.hidden=NO;
                _NameSadad.hidden = NO;
                contactButton.hidden = NO;
                
                _Namerechargeamount.hidden = NO;
                _payBalAmount.hidden = NO;
                _fromOtherPaymentModes.hidden = NO;
                
            } else {
                
                [_Checkbox setSelected:YES];
                
                
                [_Checkbox setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateSelected];
                
                _payBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"rechargeAmount"]];
                _lblRemainingBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"totalCash"]];
                
                _Usedialuswalletamount.text= @"0";
                
                _lblUncheckWallet.hidden = YES;
                _otherPaymentModesName.hidden = NO;
                _NamecreditcardDebtcard.hidden = NO;
                _DebtATmName.hidden=NO;
                _NameSadad.hidden = NO;
                _btnoperatorLogos.hidden = NO;
                contactButton.hidden = NO;
                
                _Namerechargeamount.hidden = NO;
                _payBalAmount.hidden = NO;
                _fromOtherPaymentModes.hidden = NO;
                
            }
            
        }
        
        else{
            
            if([_Checkbox isSelected]==YES) {
                
                
                [_Checkbox setSelected:NO];
                [_Checkbox setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateSelected];
                _Usedialuswalletamount.text= [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"totalCash"]];
                _payBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"amtHasToPayFrmCard"]];
                _lblRemainingBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"remwalletamt"]];
                
                
                _lblUncheckWallet.hidden = YES;
                _btnoperatorLogos.hidden = NO;
                _NamecreditcardDebtcard.hidden = YES;
                _DebtATmName.hidden=YES;
                _NameSadad.hidden = YES;
                contactButton.hidden = YES;
                _otherPaymentModesName.hidden=YES;
                _Namerechargeamount.hidden = YES;
                _payBalAmount.hidden = YES;
                _fromOtherPaymentModes.hidden = YES;
                
            } else {
                
                [_Checkbox setSelected:YES];
                
                
                [_Checkbox setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateSelected];
                
                _payBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"rechargeAmount"]];
                _lblRemainingBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"totalCash"]];
                
                _Usedialuswalletamount.text= @"0";
                
                _lblUncheckWallet.hidden = NO;
                _otherPaymentModesName.hidden = NO;
                _NamecreditcardDebtcard.hidden = NO;
                _DebtATmName.hidden=NO;
                _NameSadad.hidden = NO;
                _btnoperatorLogos.hidden = NO;
                contactButton.hidden = NO;
                
                _Namerechargeamount.hidden = NO;
                _payBalAmount.hidden = NO;
                _fromOtherPaymentModes.hidden = NO;
                
            }
            
        }
    }
    else if ([[rechargeAmountFromTextField stringForKey:@"rechargeAmountFromTextField"] intValue]>[[Walletdetails valueForKey:@"totalCash"]  intValue]) {
        
        
        [_Checkbox setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateNormal];
        
        if([_Checkbox isSelected]==YES) {
            
            
            [_Checkbox setSelected:NO];
            [_Checkbox setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateSelected];
            _Usedialuswalletamount.text= [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"totalCash"]];
            _payBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"amtHasToPayFrmCard"]];
            _lblRemainingBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"remwalletamt"]];
            
            
            _lblUncheckWallet.hidden = NO;
            _btnoperatorLogos.hidden = NO;
            _NamecreditcardDebtcard.hidden = NO;
            _DebtATmName.hidden=NO;
            _NameSadad.hidden = NO;
            contactButton.hidden = NO;
            
            _Namerechargeamount.hidden = NO;
            _payBalAmount.hidden = NO;
            _fromOtherPaymentModes.hidden = NO;
            
        } else {
            
            [_Checkbox setSelected:YES];
            
            
            [_Checkbox setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateSelected];
            
            _payBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"rechargeAmount"]];
            _lblRemainingBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"totalCash"]];
            
            _Usedialuswalletamount.text= @"0";
            
            _lblUncheckWallet.hidden = YES;
            _otherPaymentModesName.hidden = NO;
            _NamecreditcardDebtcard.hidden = NO;
            _DebtATmName.hidden=NO;
            _NameSadad.hidden = NO;
            _btnoperatorLogos.hidden = NO;
            contactButton.hidden = NO;
            
            _Namerechargeamount.hidden = NO;
            _payBalAmount.hidden = NO;
            _fromOtherPaymentModes.hidden = NO;
            
        }
    } else {
        
        [_Checkbox setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateNormal];
        
        if([_Checkbox isSelected]==YES)
        {
            
            
            [_Checkbox setSelected:NO];
            [_Checkbox setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateSelected];
            
            _Usedialuswalletamount.text= [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"rechargeAmount"]];
            
            _lblRemainingBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"remwalletamt"]];
            
            
            _lblUncheckWallet.hidden = NO;
            _otherPaymentModesName.hidden = YES;
            _NamecreditcardDebtcard.hidden = YES;
            _DebtATmName.hidden=YES;
            _NameSadad.hidden = YES;
            _btnoperatorLogos.hidden = YES;
            contactButton.hidden = YES;
            
            _Namerechargeamount.hidden = YES;
            _payBalAmount.hidden = YES;
            _fromOtherPaymentModes.hidden = YES;
            
        } else{
            
            
            [_Checkbox setSelected:YES];
            [_Checkbox setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateSelected];
            
            _lblRemainingBalAmount.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"totalCash"]];
            
            
            _Usedialuswalletamount.text= @"0";
            _lblUncheckWallet.hidden = YES;
            _otherPaymentModesName.hidden = NO;
            _NamecreditcardDebtcard.hidden = NO;
            _DebtATmName.hidden=NO;
            _NameSadad.hidden = NO;
            _btnoperatorLogos.hidden = NO;
            contactButton.hidden = NO;
            
            _Namerechargeamount.hidden = NO;
            _payBalAmount.hidden = NO;
            _fromOtherPaymentModes.hidden = NO;
        }
    }

    
}




-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str1  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str1);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                
                if ([methodname isEqualToString:@"getQucikPayDetails"]) {
                    
                    [self getQucikPayDetails];
                    
                }
                else if ([methodname isEqualToString:@"CardTRansactiondetails"]) {
                    
                    [self CardTRansactiondetails];
                    
                }
                
                
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//







-(void)CardTRansactiondetails{
    
    
    NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
    NSString* operator=   [selectedOperator stringForKey:@"selectedOperator"];
    
    NSLog(@"Jaffa fello%@",operator);
    
    NSString* userName= [[[[profileDict valueForKey:@"user_name"] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
   
    NSLog(@"Jaffa fello%@",userName);
 
    NSString* usermob=  [profileDict valueForKey:@"user_mob"];
    
    NSLog(@"Jaffa fello%@",usermob);
    
    
    NSString*email=[[profileDict valueForKey:@"user_email_id"]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSLog(@"Jaffa fello%@",email);
    
    NSUserDefaults * rcMobNum = [NSUserDefaults standardUserDefaults];
    NSString*rechargemobileno= [@"966" stringByAppendingString:[rcMobNum stringForKey:@"rcMobNum"]];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*ipAddress= [ipaddressdefault objectForKey:@"ipaddressdefault"];
    

    NSString* rechargeamount=[Walletdetails valueForKey:@"rechargeAmount"];
    

    NSUserDefaults * plandefaults = [NSUserDefaults standardUserDefaults];
    
    NSString*rechargePlan=[plandefaults stringForKey:@"plandefaults"];
    
    NSUserDefaults * serviceIdDefaults = [NSUserDefaults standardUserDefaults];
    NSString*serviceId=[serviceIdDefaults stringForKey:@"serviceIdDefaults"];

    NSString * fromBonusCash;
    NSString * fromActualCash;
    
  
    
    if([_Checkbox isSelected]==YES) {
        
        fromBonusCash = @"0";
        fromActualCash = @"0";
            
    } else {
        
        fromBonusCash=[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"frmBonusCash"]];
        fromActualCash=[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"frmActulaCash"]];
        
    }
    
    
    if([_Checkbox isSelected]==YES) {
        
        Paidamount=[Walletdetails valueForKey:@"rechargeAmount"];
        
    } else {
        
        
        if ([[Walletdetails valueForKey:@"rechargeAmount"]  intValue]<100 &&[[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"frmBonusCash"]] isEqualToString:@"0"]) {
            
            if ([[Walletdetails valueForKey:@"rechargeAmount"] intValue]>[[Walletdetails valueForKey:@"frmActulaCash"]intValue]) {
                
                NSUserDefaults * amountFromPayBalAmountDefaults = [NSUserDefaults standardUserDefaults];
                
                Paidamount = [amountFromPayBalAmountDefaults stringForKey:@"amountFromPayBalAmountDefaults"];
                
            } else {
                
                Paidamount=[Walletdetails valueForKey:@"amtHasToPayFrmCard"];
                
            }
            
        } else {
            
            Paidamount=[Walletdetails valueForKey:@"amtHasToPayFrmCard"];
            
        }

    }
    
    
    
    NSString * from = @"iPhone";
    
    
    NSString * db = [MCLocalization stringForKey:@"DBvalue"];
    
    NSUserDefaults * Couponcode = [NSUserDefaults standardUserDefaults];
    NSString*couponCode;
    
    if (couponCode.length>1) {
        couponCode= [Couponcode stringForKey:@"Couponecodenum"];
        
    } else{
        
        couponCode=@"Nil";
    }

    
    
    [LoaderClass showLoader:self.view];
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/insertRechargethrCardPayDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    

    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    //         NSString * userPwd=@"585858";
    
    NSLog(@"%@",userId);
    NSLog(@"%@",userPwd);
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];

    
    
    NSDictionary * loginDetailsDict =  @{@"operator":operator,
                                         @"user_name":userName,
                                         @"user_mob":usermob,
                                         @"user_emailId":email,
                                         @"recharge_mob":rechargemobileno,
                                         @"ipAddress":ipAddress,
                                         @"recharge_amount":rechargeamount,
                                         @"payment_method":Paymentmethod,
                                         @"rechargeplan":rechargePlan,
                                         @"service_id":serviceId,
                                         @"fromBonusCash":fromBonusCash,
                                         @"fromActualCash":fromActualCash,
                                         @"paidAmount":Paidamount,
                                         @"from":from,
                                         @"db":db,
                                         @"couponCode":couponCode,
                                         @"access_token":accessToken,
                                         @"unique_id":uniqueid,
                                         @"service_id":serviceId,
                                         @"sadad_username":@"",
                                         
                                         };
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            NSError *deserr;
            
         
            NSDictionary*Killer = [NSJSONSerialization
                                     
                                     JSONObjectWithData:data
                                     
                                     options:kNilOptions
                                     
                                     error:&deserr];
            
       
            str =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Killer Response is%@",Killer);
            
            NSLog(@"str Response is%@",str);
            
            
        
        [LoaderClass removeLoader:self.view];
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"getQucikPayDetails";
                
                [self checkTokenStatusWebServices];
                
                
            }else if ([[Killer valueForKey:@"response_message"]isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{
        
        // Saving profile Details Dictionary to NSUserDefaults.
        NSUserDefaults *CardTrDefaults = [NSUserDefaults standardUserDefaults];
        NSData * CardTrData = [NSKeyedArchiver archivedDataWithRootObject:Killer];
        [CardTrDefaults setObject:CardTrData forKey:@"CardTrDefaults"];
        
        PayfromCard * PayFromCard = [self.storyboard instantiateViewControllerWithIdentifier:@"PayfromCard"];
        [self.navigationController pushViewController:PayFromCard animated:YES];
            }
            
        });
        
        
        
    }];
    
    
    [dataTask resume];
    
    
    
    
}



- (IBAction)Paynowselected:(id)sender {
    
    NSString*ac=[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"frmActulaCash"]];
    
    NSUInteger rc=[[Walletdetails valueForKey:@"rechargeAmount"]intValue];
    
    
    NSUInteger bc=[[Walletdetails valueForKey:@"frmBonusCash"]intValue];
    
    
    if (![_Checkbox isSelected]) {
        
        NSUserDefaults * rechargeAmountFromTextField = [NSUserDefaults standardUserDefaults];
        
        if ([[rechargeAmountFromTextField stringForKey:@"rechargeAmountFromTextField"] intValue]>[[Walletdetails valueForKey:@"totalCash"] intValue]) {
            
            NSLog(@"Hello");
            
        } else if ([[Walletdetails valueForKey:@"rechargeAmount"]  intValue]<100 &&[[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"frmBonusCash"]] isEqualToString:@"0"]) {
            
            
            NSUserDefaults * rechargeAmountFromTextField = [NSUserDefaults standardUserDefaults];
            
            if ([[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"bonusCash"]] isEqualToString:@"0"]) {
                
                NSLog(@"Killer");
                [self getQucikPayDetails];
                
            } else {
                
                if ([[rechargeAmountFromTextField stringForKey:@"rechargeAmountFromTextField"]intValue]>[[Walletdetails valueForKey:@"frmActulaCash"]intValue]) {
                    
                    NSLog(@"Hello");
                    
                }  else {
                    
                    [self getQucikPayDetails];
                    
                }
            }
            
        } else {
            
            [self getQucikPayDetails];
        }
        
    } else if ([ac isEqualToString:@"0"] && bc > 0 && rc < 100) {
        
        
        alertMessage=@"Bonus cash is applicable for the recharges above 100 only";
        alertTitle=@"Please pay through other payment modes";
        [self alertMethod];
        
    } else if ([[Walletdetails valueForKey:@"rechargeAmount"]  intValue]<100 &&[[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"frmBonusCash"]] isEqualToString:@"0"]) {
        
        
        NSUserDefaults * rechargeAmountFromTextField = [NSUserDefaults standardUserDefaults];
        
        if ([[rechargeAmountFromTextField stringForKey:@"rechargeAmountFromTextField"]intValue]>[[Walletdetails valueForKey:@"frmActulaCash"]intValue]) {
            
            NSLog(@"Hello");
    
        }
        
    } else {
        NSLog(@"HI");
    }
    
}


@end
