//
//  WalletVC.h
//  Saddly
//
//  Created by Sai krishna on 1/20/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentDetailsVC.h"
#import "WebServicesMethods.h"
#import "ConnectivityManager.h"
#import "MCLocalization.h"
#import "UIView+Toast.h"
#import "LoaderClass.h"
#import "SadadViewController.h"


#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"




@interface WalletVC : UIViewController<UITextFieldDelegate,UIPopoverPresentationControllerDelegate, NSURLSessionDelegate>{
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle,* alertMessage, * toastMsg;
    NSDictionary * paymentGatewayDetails, * profileDict;
    UIButton *button;
    
    NSString * str;
    UITextField * txtCouponCode;
    NSString*cpncode;NSDictionary*Cpncoderesponse;
    
    UIButton * sadadButton;
    
    NSString * a123, * methodname;
    

}


@property (strong, nonatomic) IBOutlet UILabel *lblActualCash;
@property (strong, nonatomic) IBOutlet UILabel *lblBonusCash;

- (IBAction)btnBonusCashInfo:(id)sender;




@property (strong, nonatomic) IBOutlet UITextField *enterAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblOffer;
@property (strong, nonatomic) IBOutlet UILabel *lblOfferTwo;
@property (strong, nonatomic) IBOutlet UITextView *noOffer;
@property (strong, nonatomic) IBOutlet UILabel *lblOne;
@property (strong, nonatomic) IBOutlet UILabel *lblTwo;

@property (strong, nonatomic) IBOutlet UILabel *lblCurrentBal;
@property (strong, nonatomic) IBOutlet UITextField *txtPayment;

//Names
@property (strong, nonatomic) IBOutlet UILabel *walletBalName;
@property (strong, nonatomic) IBOutlet UILabel *amountName;
@property (strong, nonatomic) IBOutlet UILabel *payWIthName;
@property (strong, nonatomic) IBOutlet UIButton *addMoneyName;
@property (strong, nonatomic) IBOutlet UILabel *offersForYouName;
@property (strong, nonatomic) IBOutlet UITextField *enterAmountName;


@property (strong, nonatomic) IBOutlet UILabel *lblCouponCodeSuccess;

@property (strong, nonatomic) IBOutlet UIButton *btnCouponCodeOutlet;
- (IBAction)btnCouponCode:(id)sender;


- (IBAction)btnInfo:(id)sender;

- (IBAction)addAmount:(id)sender;
- (IBAction)selectpaymentMethod:(id)sender;
- (IBAction)btnAddMoney:(id)sender;


@end
