//
//  AboutUsVC.h
//  DialuzApp
//
//  Created by Sai krishna on 8/25/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "LoaderClass.h"
#import "ConnectivityManager.h"

@interface AboutUsVC : UIViewController 

@property (strong, nonatomic) IBOutlet UIImageView *ImgAboutus;


@end
