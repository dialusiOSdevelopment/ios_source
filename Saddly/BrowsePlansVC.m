//
//  BrowsePlansVC.m
//  DialuzApp
//
//  Created by Sai krishna on 11/10/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import "BrowsePlansVC.h"
#import "PlansCollectionViewCell.h"

@interface BrowsePlansVC ()

@end


@implementation BrowsePlansVC


- (void)viewDidLoad {
    
    
    
    [self PlansWebServicesMethod];
    
}




// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}





// For BrowsePlans History
-(void)PlansWebServicesMethod{
    
    
    NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
    NSString * operator = [selectedOperator stringForKey:@"selectedOperator"];
    NSLog(@"operator is %@",operator);
    
    
    NSString*UEmail= @"Guest";
    NSString*uMob=   @"Guest";
    
                      
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/getRechargePlansDetails1"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    
    
//    
//    NSString * userId = [profileDict valueForKey:@"user_mob"];
//    
//    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
//    
//    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
//    
//    
//    NSLog(@"%@",userId);
//    NSLog(@"%@",userPwd);
//    
//    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
//    
//    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
//    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
//    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
//    
//    
//    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
//    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
//    
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    
    
    NSDictionary * OpDict =  @{
                               @"operator":operator,
                               
                               @"user_mob":uMob,
                               @"planType":_operType,
                               @"user_emailId":UEmail,
                               @"user_mob":uMob,
                               @"ipAddress":Ipaddress,
                               @"access_token":[accessTokenDefaults valueForKey:@"accessTokenDefaults"],
                               @"unique_id":uniqueid,
                               @"from":@"iphone",
                               
                               };
    
    
    
    NSLog(@"Posting browsePlansDict is %@",OpDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:OpDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            
            NSDictionary*    Response = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            NSString*  resSrt  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Killer Response is%@",Response );
            
            NSLog(@"resSrt Response is%@",resSrt );
            
            [LoaderClass removeLoader:self.view];
            
            
            
            voiceDetails = Response;
            voiceResMsg = [voiceDetails valueForKey:@"response_message"];
            
            if ([voiceResMsg containsObject:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                

            } else {
            
                
                if (voiceDetails.count >0) {
                    
                    voicePlans =  [voiceDetails valueForKey:@"service_name"];
                    voiceAmount = [voiceDetails  valueForKey:@"price"];
                    voiceArrServiceId = [voiceDetails  valueForKey:@"service_id"];
                    
                    
                    height= 47.0f;
                    [self.collecOperator reloadData];
                    
                    
                } else {
                    
                    UILabel * noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.collecOperator.bounds.size.width, self.collecOperator.bounds.size.height)];
                    noDataLabel.textColor        = [UIColor blackColor];
                    noDataLabel.textAlignment    = NSTextAlignmentCenter;
                    self.collecOperator.backgroundView = noDataLabel;
                    noDataLabel.numberOfLines = 2;
                    
                    
                    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
                        
                        noDataLabel.text = @"عفوا !! لاتتوفر باقات صوتيه لهذا المزود . فضلاً اختر باقة بيانات";
                        
                    } else {
                        
                        noDataLabel.text  = @"Sorry!! No Voice Plans Are Found For This Operator Please Check The Data Plans";
                    }
                    
                    
                    height = 0;
                }
                
                
                [self.collecOperator reloadData];
                
            }
            
        });
        
    }];
    
    
    [dataTask resume];
    
    
    
    
    
}

//
//-(void) checkTokenStatusWebServices {
//    
//    
//    NSUserDefaults * guestDefaults = [NSUserDefaults standardUserDefaults];
//    
//    
//    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
//    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
//    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
//    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
//    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
//    
//    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
//    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
//    
//    
//    
//    // Create the URLSession on the default configuration
//    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    
//    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
//    
//    
//    // Setup the request with URL
//    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
//    
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    
//    
//    request.HTTPMethod = @"POST";
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    
//    NSString * userId;
//    
//    if ([[guestDefaults stringForKey:@"guestDefaults"] isEqualToString:@"guestDefaults"]) {
//        
//        userId = @"Guest";
//        
//    } else {
//        
//        NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
//        
//        
//        userId = [profileDict valueForKey:@"user_mob"];
//        NSString * userPwd = [pddefaults stringForKey:@"pddd"];
//        
//        NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
//        
//        NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
//        NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
//        NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
//        
//        
//        NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
//        [request setValue:authValue forHTTPHeaderField:@"Authorization"];
//        
//        
//
//    }
//    
//       NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
//                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
//                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
//                                       @"user_mob":userId,
//                                       @"from":@"iPhone",
//                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
//                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
//                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
//                                       @"token_type":@"refresh_token",
//                                       };
//    
//    
//    NSLog(@"Posting Check token status is %@",checkTokenDict);
//    
//    
//    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
//    
//    
//    [request setHTTPBody:postdata];
//    
//    
//    // Create dataTask
//    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        
//        if (!data) {
//            
//            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
//            NSLog(@"%@",userErrorText);
//            return;
//        }
//        
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//            
//            NSError *deserr;
//            
//            NSDictionary*    Killer = [NSJSONSerialization
//                                       JSONObjectWithData:data
//                                       options:kNilOptions
//                                       error:&deserr];
//            
//            
//            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
//            
//            
//            NSLog(@"Killer Response is%@",Killer);
//            NSLog(@"str Response is%@",str);
//            
//            
//            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
//            
//            
//            
//            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
//                
//                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
//                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
//                
//                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
//                [loginName removeObjectForKey:@"userName"];
//                
//                
//                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
//                [self.navigationController pushViewController:login animated:YES];
//                
//            } else  {
//                
//                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
//                
//                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
//                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
//                
//                
//                if ([methodname isEqualToString:@"sendMoneyWebServicesMethod"]) {
//                    [self sendMoneyWebServicesMethod];
//                }
//                
//            }
//        });
//        
//    }];
//    
//    
//    [dataTask resume];
//    ////////////
//    
//}
////






#pragma mark --
#pragma mark - UICollectionView Delegate Methods


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    NSInteger count = [voicePlans count];
    return count;
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    PlansCollectionViewCell *cell = (PlansCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    if (cell == nil) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"PlansCollectionViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    cell.imgOperator.image = _operatorImage;
    cell.lblPlan.text = [NSString stringWithFormat:@"%@", [voicePlans objectAtIndex:indexPath.row]];
    cell.lblAmount.text =  [[NSString stringWithFormat:@"%@", [voiceAmount objectAtIndex:indexPath.row]] stringByAppendingString:@" SAR"];
    
    
    
    NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
    
    NSString * operator;
    
    operator = [selectedOperator stringForKey:@"selectedOperator"];
    
    if ([operator isEqualToString:@"iTunes"]) {
        
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"itunes123.png"]];
        
        cell.lblAmount.textColor=[UIColor blackColor];
        cell.lblPlan.textColor=[UIColor blackColor];
        
    }else if ([operator isEqualToString:@"Xbox"]){
        
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"xbox123.png"]];
        
        cell.lblAmount.textColor=[UIColor blackColor];
        cell.lblPlan.textColor=[UIColor blackColor];
        
    }else if ([operator isEqualToString:@"Playstation"]){
        
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"playstation123.png"]];
        
        cell.lblAmount.textColor=[UIColor blackColor];
        cell.lblPlan.textColor=[UIColor blackColor];
        
    } else if ([operator isEqualToString:@"Zain"]){
        
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zain123.png"]];
        
        cell.lblAmount.textColor=[UIColor blackColor];
        cell.lblPlan.textColor=[UIColor blackColor];
        
        
    }else if ([operator isEqualToString:@"Sawa"]){
        
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sawa123.png"]];
        
        cell.lblAmount.textColor=[UIColor blackColor];
        cell.lblPlan.textColor=[UIColor blackColor];
        
    }
    else if ([operator isEqualToString:@"Virgin"]){
        
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"virgin123.png"]];
        
        cell.lblAmount.textColor=[UIColor blackColor];
        cell.lblPlan.textColor=[UIColor blackColor];
        
    }
    else if ([operator isEqualToString:@"Friendi"]){
        
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"friendi123.png"]];
        
        cell.lblAmount.textColor=[UIColor blackColor];
        cell.lblPlan.textColor=[UIColor blackColor];
        
    }
    else if ([operator isEqualToString:@"Jawwy"]){
        
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"jawwwy123.png"]];
        
        cell.lblAmount.textColor=[UIColor blackColor];
        cell.lblPlan.textColor=[UIColor blackColor];
        
    }
    else if ([operator isEqualToString:@"Quicknet"]){
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"quicknet123.png"]];
        
        cell.lblAmount.textColor=[UIColor blackColor];
        cell.lblPlan.textColor=[UIColor blackColor];
        
    }
    else if ([operator isEqualToString:@"Mobily"]){
        
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mobily123.png"]];
        
        cell.lblAmount.textColor=[UIColor blackColor];
        cell.lblPlan.textColor=[UIColor blackColor];
    }
    else if ([operator isEqualToString:@"lebara"]){
        
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lebera123.png"]];
        
        cell.lblAmount.textColor=[UIColor blackColor];
        cell.lblPlan.textColor=[UIColor blackColor];
        
    }
    
    
    
    cell.lblPlan.adjustsFontSizeToFitWidth = YES;
    cell.lblAmount.adjustsFontSizeToFitWidth = YES;
    
    
    NSUserDefaults * dataDefaults = [NSUserDefaults standardUserDefaults];
    [dataDefaults setObject:cell.lblPlan.text forKey:@"dataDefaults"];
    
    _collecOperator.alwaysBounceVertical = NO;
    return cell;
    
}





- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    

    selectedAmount  = [voiceAmount objectAtIndex:indexPath.row];
    Planname=[voicePlans objectAtIndex:indexPath.row];
    serviceidname = [voiceArrServiceId objectAtIndex:indexPath.row];

    NSUserDefaults * planAmountDefaults = [NSUserDefaults standardUserDefaults];
    [planAmountDefaults setObject:selectedAmount forKey:@"planAmountDefaults"];

    NSUserDefaults * plandefaults = [NSUserDefaults standardUserDefaults];
    [plandefaults setObject:Planname forKey:@"plandefaults"];

    NSUserDefaults * serviceIdDefaults = [NSUserDefaults standardUserDefaults];
    [serviceIdDefaults setObject:serviceidname forKey:@"serviceIdDefaults"];

    [self.navigationController popViewControllerAnimated:YES];

}

//
//

@end




//
//
//- (void)viewDidLoad {
//    
//    
//    
////    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
////        
////        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
////        
////    } else {
////        
////        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
////        
////    }
//    
//    
//    NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
//    
//     if ([[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
//
//    self.title = [MCLocalization stringForKey:@"plans"];
//    
//    [_segment setTitle:[MCLocalization stringForKey:@"Voice"]  forSegmentAtIndex:0];
//    [_segment setTitle:[MCLocalization stringForKey:@"Data"]  forSegmentAtIndex:1];
//    
//     }else{
//     
//         self.title = [MCLocalization stringForKey:@"giftCards"];
//         
//         [_segment setTitle:[MCLocalization stringForKey:@"US"]  forSegmentAtIndex:0];
//         [_segment setTitle:[MCLocalization stringForKey:@"UK"]  forSegmentAtIndex:1];
//
//     
//     }
//    
//    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor darkGrayColor]} forState:UIControlStateNormal];
//    
//    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateSelected];
//    
//    [_segment setTintColor:[UIColor clearColor]];
//    
//    
//    // Creating new layer for selection
//    bottomBorder = [CALayer layer];
//    bottomBorder.borderColor = [UIColor whiteColor].CGColor;
//    bottomBorder.borderWidth = 2;
//    
//    //  Calculating frame
//    CGFloat width            = self.segment.frame.size.width/2;
//    CGFloat x                = self.segment.selectedSegmentIndex * width;
//    CGFloat y                = self.segment.frame.size.height - bottomBorder.borderWidth;
//    bottomBorder.frame       = CGRectMake(x, y,width, bottomBorder.borderWidth);
//    
//    // Adding Bottom line  to segment
//    [self.segment.layer addSublayer:bottomBorder];
//    
//    
//    
//    
//    NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
//    
//    NSString * operator;
//    
//    operator = [selectedOperator stringForKey:@"selectedOperator"];
//    
//    
//    if ([operator isEqualToString:@"Sawa"]) {
//        
//        [_segment setWidth:0.01 forSegmentAtIndex:1];
//        [_segment setEnabled:NO forSegmentAtIndex:1];
//
//        
//        
//        //  Calculating frame
//        CGFloat width            = self.segment.frame.size.width;
//        CGFloat x                = self.segment.selectedSegmentIndex * width;
//        CGFloat y                = self.segment.frame.size.height - bottomBorder.borderWidth;
//        bottomBorder.frame       = CGRectMake(x, y,width, bottomBorder.borderWidth);
//        
//        // Adding Bottom line  to segment
//        [self.segment.layer addSublayer:bottomBorder];
//        
//        
//        self.viewVoice.hidden = NO;
//        self.viewData.hidden = YES;
//        [self rcPlansWebServicesMethod];
//        [self.tblVoice reloadData];
//
//        
//    } else if ([operator isEqualToString:@"Quicknet"]) {
//        
//        [_segment setWidth:0.01 forSegmentAtIndex:0];
//        [_segment setEnabled:NO forSegmentAtIndex:0];
//        
//        
//        
//        //  Calculating frame
//        CGFloat width            = self.segment.frame.size.width;
//        bottomBorder.frame       = CGRectMake(self.segment.frame.origin.x, self.segment.frame.origin.y , width - 20.0, bottomBorder.borderWidth);
//        
//        // Adding Bottom line  to segment
//        [self.segment.layer addSublayer:bottomBorder];
//        
//        
//        self.viewVoice.hidden = YES;
//        self.viewData.hidden = NO;
//        [self rcPlansWebServicesMethod];
//        [self.tblData reloadData];
//        
//        
//    } else {
//        
//        self.viewVoice.hidden = NO;
//        self.viewData.hidden = YES;
//        [self rcPlansWebServicesMethod];
//        [self.tblVoice reloadData];
//    }
//    
//    
//    
//
//
//    [super viewDidLoad];
//    // Do any additional setup after loading the view.
//}
//
////
////-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
////{
////    if ([view isKindOfClass:[UILabel class]])
////    {
////        UILabel *lbl = (UILabel *)view;
////        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
////    }
////    
////    if ([view isKindOfClass:[UIButton class]])
////    {
////        UIButton *lbl = (UIButton *)view;
////        
////         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
////        
////        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
////
////        
////    }
////    
////    if ([view isKindOfClass:[UITextField class]])
////    {
////        UITextField *lbl = (UITextField *)view;
////        
////        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
////        
////        
////    }
////    
////    
////    if (isSubViews)
////    {
////        for (UIView *sview in view.subviews)
////        {
////            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
////        }
////    }
////}
//
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
///*
//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}
//*/
//
//// Segmented Control for Voice & Data
//- (IBAction)segmentValueChanged:(UISegmentedControl *)sender {
//    
//    
//    //  Calculating frame
//    CGFloat width            = self.segment.frame.size.width/2;
//    CGFloat x                = self.segment.selectedSegmentIndex * width;
//    CGFloat y                = self.segment.frame.size.height - bottomBorder.borderWidth;
//    bottomBorder.frame       = CGRectMake(x, y, width, bottomBorder.borderWidth);
//    
//    
//    switch (sender.selectedSegmentIndex) {
//        //VOICE
//        case 0:
//        self.viewVoice.hidden = NO;
//        self.viewData.hidden = YES;
//        [self rcPlansWebServicesMethod];
//        [self.tblVoice reloadData];
//        break;
//        //DATA
//        case 1:
//        self.viewVoice.hidden = YES;
//        self.viewData.hidden = NO;
//        [self rcPlansWebServicesMethod];
//        [self.tblData reloadData];
//        break;
//        //DEFAULT
//        default:
//        break;
//    }
//    
//    // Adding Bottom line to segment
//    [self.segment.layer addSublayer:bottomBorder];
//
//}
//
//
//
//-(void)dataPlansResponse:(id)response{
//    
//    [LoaderClass removeLoader:self.view];
//    
//    
//    NSLog(@"data Plans Response %@",response);
//    dataDetails = [response objectForKey:@"List"];
//    
//    
//    if (dataDetails.count > 0) {
//        
//        dataPlans =  [dataDetails valueForKey:@"service_name"];
//        dataAmount = [dataDetails  valueForKey:@"price"];
//        dataArrServiceId = [dataDetails  valueForKey:@"service_id"];
//        
//        height= 47.0f;
//        [self.tblData reloadData];
//        
//    } else {
//        
//        UILabel * noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tblData.bounds.size.width, self.tblData.bounds.size.height)];
//        noDataLabel.textColor        = [UIColor whiteColor];
//        noDataLabel.textAlignment    = NSTextAlignmentCenter;
//        self.tblData.backgroundView = noDataLabel;
////        noDataLabel.adjustsFontSizeToFitWidth = YES;
//        noDataLabel.numberOfLines = 2;
//        
//
//        
//       
//        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
//            
//               noDataLabel.text = @"عفوا !! لاتتوفر باقات بيانات لهذا المزود . فضلاً اختر من الباقات الصوتيه";
//            
//        } else {
//            
//            noDataLabel.text  = @"Sorry!! No Data Plans Are Found For This Operator Please Check Voice Plans";
//        }
//            
//        
//        self.tblData.tableFooterView = [[UIView alloc] init];
//        height = 0;
//    }
//    
//    
//    if (_segment.selectedSegmentIndex == 0) {
//        [self.tblVoice reloadData];
//    } else{
//        [self.tblData reloadData];
//    }
//    
//    }
//
//
//
//
//
//#pragma mark --
//#pragma mark - UITableView Delegate Methods
//
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    
//        if (_segment.selectedSegmentIndex == 0) {
//            NSInteger count = [voicePlans count];
//            return count;
//        } else {
//            NSInteger count = [dataPlans count];
//            return count;
//        }
//
//}
//
//-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    
//    
//    if (_segment.selectedSegmentIndex == 0) {
//        
//        NSString * cellIdentifier=@"Cell";
//        VoiceTableViewCell * cell= (VoiceTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
//        if (cell == nil) {
//            NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"VoiceTableViewCell" owner:self options:nil];
//            cell = [nib objectAtIndex:0];
//        }
//        cell.lblVoice.text = [NSString stringWithFormat:@"%@", [voicePlans objectAtIndex:indexPath.row]];
//        NSString * money =  [NSString stringWithFormat:@"%@", [voiceAmount objectAtIndex:indexPath.row]];
//        [cell.btnVoiceOutlet setTitle:money forState:UIControlStateNormal];
//        cell.backgroundColor =  [UIColor clearColor];
//        
//        cell.lblVoice.textColor = [UIColor whiteColor];
//        [cell.btnVoiceOutlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        cell.lblVoice.adjustsFontSizeToFitWidth = YES;
//        
//        
//        NSUserDefaults * voiceDefaults = [NSUserDefaults standardUserDefaults];
//        [voiceDefaults setObject:cell.lblVoice.text forKey:@"voiceDefaults"];
//        
//        _tblVoice.alwaysBounceVertical = NO;
//        self.tblVoice.tableFooterView = [[UIView alloc] init];
//        return cell;
//        
//    } else {
//        
//        NSString * cellIdentifier=@"Cell";
//        DataTableViewCell * cell= (DataTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
//        if (cell == nil) {
//            NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"DataTableViewCell" owner:self options:nil];
//            cell = [nib objectAtIndex:0];
//        }
//        cell.lblData.text = [NSString stringWithFormat:@"%@", [dataPlans objectAtIndex:indexPath.row]];
//        NSString * money =  [NSString stringWithFormat:@"%@", [dataAmount objectAtIndex:indexPath.row]];
//        cell.backgroundColor =  [UIColor clearColor];
//        [cell.btnDataOutlet setTitle:money forState:UIControlStateNormal];
//        
//        cell.lblData.textColor = [UIColor whiteColor];
//        [cell.btnDataOutlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        cell.lblData.adjustsFontSizeToFitWidth = YES;
//        
//        
//        NSUserDefaults * dataDefaults = [NSUserDefaults standardUserDefaults];
//        [dataDefaults setObject:cell.lblData.text forKey:@"dataDefaults"];
//        
//        _tblData.alwaysBounceVertical = NO;
//        self.tblData.tableFooterView = [[UIView alloc] init];
//        return cell;
//    }
//    
//}
//
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    if (_segment.selectedSegmentIndex == 0) {
//        selectedAmount  = [voiceAmount objectAtIndex:indexPath.row];
//        Planname=[voicePlans objectAtIndex:indexPath.row];
//        serviceidname = [voiceArrServiceId objectAtIndex:indexPath.row];
//     } else {
//         selectedAmount = [dataAmount objectAtIndex:indexPath.row];
//         Planname=[dataPlans objectAtIndex:indexPath.row];
//         serviceidname = [dataArrServiceId objectAtIndex:indexPath.row];
//    }
//    
//    
//    NSUserDefaults * planAmountDefaults = [NSUserDefaults standardUserDefaults];
//    [planAmountDefaults setObject:selectedAmount forKey:@"planAmountDefaults"];
//    
//    NSUserDefaults * plandefaults = [NSUserDefaults standardUserDefaults];
//    [plandefaults setObject:Planname forKey:@"plandefaults"];
//
//    NSUserDefaults * serviceIdDefaults = [NSUserDefaults standardUserDefaults];
//    [serviceIdDefaults setObject:serviceidname forKey:@"serviceIdDefaults"];
//    
//    [self.navigationController popViewControllerAnimated:YES];
//
//}
//
//
//
//
//
//
//
//@end
