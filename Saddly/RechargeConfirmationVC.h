//
//  RechargeConfirmationVC.h
//  DialuzApp
//
//  Created by Sai krishna on 10/31/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServicesMethods.h"
#import "ConnectivityManager.h"
#import "MobileRechargeVC.h"
#import "PayFromWalletVC.h"
#import "PayfromCard.h"
#import "LoaderClass.h"



@interface RechargeConfirmationVC : UIViewController <NSURLSessionDelegate>{

    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle,* alertMessage;
    NSDictionary * Walletdetails,*profileDict;
    NSDictionary * responseDict, * SDKresponsedict;
    
    NSString  *str;
    NSString*Paidamount;
    
    NSString*Paymentmethod;
    
    UIButton * contactButton;

    NSUserDefaults * otherPaymentModesDefaults;
    
    NSString * methodname, * a123;
    

}

//names

@property (strong, nonatomic) IBOutlet UILabel *Namewalletamount;
@property (strong, nonatomic) IBOutlet UILabel *Namerechargeamount;

@property (strong, nonatomic) IBOutlet UILabel *Nameremainingamount;
@property (strong, nonatomic) IBOutlet UIButton *Namepaynow;
@property (strong, nonatomic) IBOutlet UIButton *NamecreditcardDebtcard;
@property (strong, nonatomic) IBOutlet UIButton *NameSadad;
@property (strong, nonatomic) IBOutlet UILabel *lblRemainingBalAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblUncheckWallet;
@property (strong, nonatomic) IBOutlet UIButton *DebtATmName;

@property (strong, nonatomic) IBOutlet UIButton *btnoperatorLogos;


//actions
@property (strong, nonatomic) IBOutlet UILabel *Remainingwalletamount;

- (IBAction)SadadSelected:(id)sender;
- (IBAction)Creditordebitselected:(id)sender;
- (IBAction)DebitAtmcard:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *payBalAmount;
@property (strong, nonatomic) IBOutlet UILabel *WalletAmount;
@property (strong, nonatomic) IBOutlet UIButton *Checkbox;
- (IBAction)Checkbox:(id)sender;

- (IBAction)Paynowselected:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *Usedialuswalletamount;
@property (strong, nonatomic) IBOutlet UILabel *otherPaymentModesName;
@property (strong, nonatomic) IBOutlet UILabel *lbltotAmountToBePaid;
@property (strong, nonatomic) IBOutlet UILabel *fromOtherPaymentModes;

@end
