//
//  RegistrationVC.h
//  Saddly
//
//  Created by Sai krishna on 1/19/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WebServicesMethods.h"
#import "UIView+Toast.h"
#import "LoginViewController.h"
#import "MCLocalization.h"
#import "AppDelegate.h"
#import "ConnectivityManager.h"
#import "LoaderClass.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <netdb.h>
#import "RegisterWithOTP.h"
#import "LoginViewController.h"



#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"



@interface RegistrationVC : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, NSURLSessionDelegate> {
    
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    NSString * gender, * Ipaddress, * imgSelected;
    NSString* db;
    
    UIImagePickerController * picImg;
    
    NSDictionary *loginDetails;
    NSUInteger namecount , numbercount,differcount1,differcount2;
    
    NSString* Names;
    NSString* Numbers;

    CLLocationManager *locationManager;
    NSString * latitude, * longitude;

    NSString * methodname, * a123;
    
}



@property (nonatomic, strong) NSMutableArray * FinalContact, * Finalnumber;
@property (nonatomic, strong) NSMutableArray * finalcontacts;

@property (strong, nonatomic) NSMutableArray * groupOfContacts;
@property (strong, nonatomic) NSMutableArray * contactNameArray;
@property (strong, nonatomic) NSMutableArray * phoneNumberArray;



// Name
@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UILabel *lblSelUrGender;

// Select City
@property (strong, nonatomic) IBOutlet UITextField *txtCity;


// Mobile Number
@property (strong, nonatomic) IBOutlet UITextField *txtMobNum;

// profile Image
@property (strong, nonatomic) IBOutlet UIImageView *profilePicImg;

// Email Id
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;

// passwords
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtConfirmPassword;

@property (strong, nonatomic) IBOutlet UITextField *txtReferralCode;

@property (strong, nonatomic) IBOutlet UIButton *btnCheckboxTCOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnRegOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnMaleOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnFemaleOutlet;

@property (strong, nonatomic) IBOutlet UIButton *btnEnterShowPwdOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnConfirmShowPwdOutlet;

@property (strong, nonatomic) IBOutlet UILabel *lblmobNumBackground;

- (IBAction)btnMale:(id)sender;
- (IBAction)btnFemale:(id)sender;

- (IBAction)btnCheckboxTC:(id)sender;
- (IBAction)btnRegister:(id)sender;

- (IBAction)btnEnterShowPwd:(id)sender;
- (IBAction)btnConfirmShowPwd:(id)sender;



@end
