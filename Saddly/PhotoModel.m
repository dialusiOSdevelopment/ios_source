//
//  PhotoModel.m
//  CoverFlowLayoutDemo
//
//  Created by Yuriy Romanchenko on 3/13/15.
//  Copyright (c) 2015 solomidSF. All rights reserved.
//

#import "PhotoModel.h"

@implementation PhotoModel

#pragma mark - Init

+ (instancetype)modelWithImageNamed:(NSString *)imageNamed
                        description:(NSString *)description
                              Title:(NSString *)Title
                         Buttonname:(NSString *)ButtonName

{
    return [[self alloc] initWithImageNamed:imageNamed
                                description:description
                                      Title:Title
                                 Buttonname:ButtonName
            
            
            ];
}

- (instancetype)initWithImageNamed:(NSString *)imageNamed
                       description:(NSString *)description
                             Title:(NSString *)Title
                        Buttonname:(NSString *)ButtonName
{
    if (self = [super init]) {
        _image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[imageNamed stringByReplacingOccurrencesOfString:@"+" withString:@"%20"]]]];
        
        _imageDescription = description;
        _imageButton=ButtonName;
        _imageTitle=Title;
    }
    
    return self;
}

@end
