//
//  TransactionDetailsVC.m
//  Saddly
//
//  Created by Sai krishna on 5/6/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "TransactionDetailsVC.h"

@interface TransactionDetailsVC ()

@end

@implementation TransactionDetailsVC

- (void)viewDidLoad {
    
    
    if ([_tagStr isEqualToString:@"1000"]) {
        
        
        _imgOperator.image = _txnImg;
        _lblStatus.text = _txnStatus;
        _lblAmount.text = _txnAmount;
        _lblMobNum.text = _txnMobNum;
        _lblResMsg.text = _txnResMsg;
        _lblOperator.text = _txnOperator;
        _lblOrderId.text = _txnOrderId;
        _lblTransactedOn.text = _txnDate;
        _lblMobRc.text = [_txnRefundedAmount stringByAppendingString:@" SAR"];
        
        _lblWallet.text = [[[MCLocalization stringForKey:@"SlidearrWllt"] stringByAppendingString:@" - "] stringByAppendingString:_txnAmtFrmWallet];
        
       

        _btnInvoiceOutlet.hidden = NO;
        _lblPaidUsingName.hidden = NO;
        _lblWallet.hidden = NO;
        _lblOperator.hidden = NO;
        _lblSeperator.hidden = NO;
        _lblSaudiName.hidden = NO;
        
        _viewWallet.hidden = YES;
        _viewRefund.hidden = NO;
        
        
        if ([_txnAmtFrmCard isEqualToString:@"0"]) {
            
            _lblCard.hidden = YES;
            
        } else {
            
            _lblCard.hidden = NO;
            _lblCard.text = [[[MCLocalization stringForKey:@"card"] stringByAppendingString:@" - "] stringByAppendingString:_txnAmtFrmCard];
        }
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        NSMutableAttributedString *myString1;
        
        
        
        if ([_lblResMsg.text isEqualToString:[MCLocalization stringForKey:@"failure"]]) {
            
            _lblResMsg.textColor = [UIColor redColor];
            _btnInvoiceOutlet.hidden = YES;
            attachment.image = [UIImage imageNamed:@"wrong.png"];
            
            
            if ([_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"giftcard"]]) {
                
                _btnRepeatOutlet.enabled = NO;
                
            } else {
                
                _btnRepeatOutlet.enabled = YES;
                
            }
            
            
            
        } else {
            
            _lblResMsg.textColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
            _btnInvoiceOutlet.hidden = NO;
            attachment.image = [UIImage imageNamed:@"right.png"];
            
            
            if ([_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"giftcard"]]) {
                
                _btnRepeatOutlet.enabled = NO;
                
            } else {
                
                _btnRepeatOutlet.enabled = YES;
                
            }
            
            
          
        }
        
        
        
        CGFloat offsetY = -3.0;
        attachment.bounds = CGRectMake(0, offsetY, attachment.image.size.width, attachment.image.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:@""];
        [myString appendAttributedString:attachmentString];
        myString1 = [[NSMutableAttributedString alloc] initWithString:_lblResMsg.text];
        [myString appendAttributedString:myString1];
        _lblResMsg.textAlignment=NSTextAlignmentRight;
        _lblResMsg.attributedText=myString;
        
        
        
        if ([_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"refund"]]) {
            
            _btnInvoiceOutlet.hidden = YES;
            _btnRepeatOutlet.hidden = YES;
            _viewRefund.hidden = NO;
            
        } else {
            
            _btnInvoiceOutlet.hidden = NO;
            _btnRepeatOutlet.hidden = NO;
            _viewRefund.hidden = YES;
            
        }
        
        
    } else {
        
        _imgOperator.image = _txnImg;
        _lblStatus.text = _txnStatus;
        _lblAmount.text = _txnAmount;
        _lblMobNum.text = _txnMobNum;
        _lblResMsg.text = _txnResMsg;
        _lblOperator.text = _txnOperator;
        _lblOrderId.text = _txnOrderId;
        _lblTransactedOn.text = _txnDate;
        _lblWallet.text = _txnAmount;
        
        
        _walLblOrderId.text = _txnOrderId;
        _walLblTransactedOn.text = _txnDate;
        
        
        
        _viewWallet.hidden = NO;
        _viewRefund.hidden = YES;
        
        _lblPaidUsingName.hidden = YES;
        _lblWallet.hidden = YES;
        _lblOperator.hidden = YES;
        _lblSeperator.hidden = YES;
        _lblSaudiName.hidden = YES;
        _lblCard.hidden = YES;
        
        
        _lblTransactedOnName.hidden = YES;
        _lblSeperator.hidden = YES;

        _lblOrderId.hidden = YES;
        _lblTransactedOn.hidden = YES;
        
        _btnRepeatOutlet.hidden = YES;
        _btnInvoiceOutlet.hidden = YES;
        
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        NSMutableAttributedString *myString1;
        
        
        if ([_lblResMsg.text isEqualToString:[MCLocalization stringForKey:@"failure"]]) {
            
            _lblResMsg.textColor = [UIColor redColor];
            attachment.image = [UIImage imageNamed:@"wrong.png"];
            
        } else {
            
            _lblResMsg.textColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
            attachment.image = [UIImage imageNamed:@"right.png"];
            
        }
        
        
        CGFloat offsetY = -3.0;
        attachment.bounds = CGRectMake(0, offsetY, attachment.image.size.width, attachment.image.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:@""];
        [myString appendAttributedString:attachmentString];
        myString1 = [[NSMutableAttributedString alloc] initWithString:_lblResMsg.text];
        [myString appendAttributedString:myString1];
        _lblResMsg.textAlignment=NSTextAlignmentRight;
        _lblResMsg.attributedText=myString;
        

        
    }
    
    
    
    
    
    
    self.imgOperator.layer.masksToBounds = YES;
    self.imgOperator.contentMode = UIViewContentModeScaleToFill;
   
    
    // Setting Vertical line.
    UIView *borderBottom = [[UIView alloc] initWithFrame:CGRectMake(0.0, 100.0, self.view.frame.size.width, 1.5)];
    borderBottom.backgroundColor = [UIColor blackColor];
    [self.view addSubview:borderBottom];

    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 



-(void)viewDidAppear:(BOOL)animated {
    
    
    self.title = [MCLocalization stringForKey:@"details"];
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
   
    
    
    _lblSaudiName.text = [MCLocalization stringForKey:@"saudi"];
    _lblPaidUsingName.text = [MCLocalization stringForKey:@"paidusing"];
    _lblOrderIdName.text = [MCLocalization stringForKey:@"orderId"];
    _lblTransactedOnName.text = [MCLocalization stringForKey:@"transactedon"];
    
    _walLblOrderIdName.text = [MCLocalization stringForKey:@"orderId"];
    _walLblTransactedOnName.text = [MCLocalization stringForKey:@"transactedon"];
    
    _lblRefundReceivedName.text = [MCLocalization stringForKey:@"refundreceived"];
    _lblMobRcName.text = [MCLocalization stringForKey:@"mobileRc"];
    
    
    [_btnRepeatOutlet setTitle:[MCLocalization stringForKey:@"repeat"] forState:UIControlStateNormal];
    [_btnInvoiceOutlet setTitle:[MCLocalization stringForKey:@"invoice"] forState:UIControlStateNormal];
    
    [_walBtnRepeatOutlet setTitle:[MCLocalization stringForKey:@"repeat"] forState:UIControlStateNormal];


   
    
    
    
    _lblStatus.adjustsFontSizeToFitWidth = YES;
    _lblAmount.adjustsFontSizeToFitWidth = YES;
    _lblMobNum.adjustsFontSizeToFitWidth = YES;
    _lblResMsg.adjustsFontSizeToFitWidth = YES;
    _lblOperator.adjustsFontSizeToFitWidth = YES;
    _lblOrderId.adjustsFontSizeToFitWidth = YES;
    _lblTransactedOn.adjustsFontSizeToFitWidth = YES;

    
    _walLblOrderIdName.adjustsFontSizeToFitWidth = YES;
    _walLblTransactedOnName.adjustsFontSizeToFitWidth = YES;

    _walLblOrderId.adjustsFontSizeToFitWidth = YES;
    _walLblTransactedOn.adjustsFontSizeToFitWidth = YES;


    _lblRefundReceivedName.adjustsFontSizeToFitWidth = YES;
    _lblMobRc.adjustsFontSizeToFitWidth = YES;
    _lblMobRcName.adjustsFontSizeToFitWidth = YES;
}





- (IBAction)btnRepeat:(id)sender {
    
    
    if ([_tagStr isEqualToString:@"1000"]) {
        
        
        MobileRechargeVC * mobileRc = [self.storyboard instantiateViewControllerWithIdentifier:@"MobileRechargeVC"];
        mobileRc.hidesBottomBarWhenPushed = YES;
        
        NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
        [btnRcDefaults setObject:@"100" forKey:@"btnRcDefaults"];
        

        mobileRc.operName = _txnOperator;
        mobileRc.operImage = _txnImg;
        
        
        [self.navigationController pushViewController:mobileRc animated:YES];
        
    } else {

        
        
        if ([_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"moneyAddedWallet"]] || [_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"addiCash"]] || [_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"SlidearrWllt"]] || [_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"bonucash"]]) {
            
            
            WalletVC * wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"WalletVC"];
            wallet.hidesBottomBarWhenPushed = YES;
            
            [self.navigationController pushViewController:wallet animated:YES];
            
            
        } else if ([_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"senMon"]] || [_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"receivedMoney"]]) {
            
            
            SendMoneyVC * sendMoney = [self.storyboard instantiateViewControllerWithIdentifier:@"SendMoneyVC"];
            sendMoney.hidesBottomBarWhenPushed = YES;
            
            [self.navigationController pushViewController:sendMoney animated:YES];
            
            
        } else {
            
            [self presentedViewController];
        }
        
    }
    
}



- (IBAction)btnInvoice:(id)sender {
    
    InvoiceDetailsVC * invoice = [self.storyboard instantiateViewControllerWithIdentifier:@"InvoiceDetailsVC"];
    invoice.hidesBottomBarWhenPushed = YES;
    
    
    
    invoice.inOrderId = _lblOrderId.text;
    invoice.inDate = _lblTransactedOn.text;
    
    
    invoice.inOperator = _lblOperator.text;
    invoice.inRcMobNum = _txnMobNum;
    invoice.inRcAmount = _lblAmount.text;

    invoice.inValueFromWallet = _txnAmtFrmWallet;
    invoice.inValueFromCard = _txnAmtFrmCard;
    invoice.inTotal = _lblAmount.text;
   

    
    [self.navigationController pushViewController:invoice animated:YES];

    
}




- (IBAction)walBtnRepeat:(id)sender {
    
    
    
    if ([_tagStr isEqualToString:@"1000"]) {
        
        
        MobileRechargeVC * mobileRc = [self.storyboard instantiateViewControllerWithIdentifier:@"MobileRechargeVC"];
        mobileRc.hidesBottomBarWhenPushed = YES;
        
        NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
        [btnRcDefaults setObject:@"100" forKey:@"btnRcDefaults"];
        
        
        mobileRc.operName = _txnOperator;
        mobileRc.operImage = _txnImg;
        
        
        [self.navigationController pushViewController:mobileRc animated:YES];
        
    } else {
                
        
        if ([_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"moneyAddedWallet"]] || [_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"addiCash"]] || [_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"SlidearrWllt"]] || [_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"bonucash"]]) {
            
            
            WalletVC * wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"WalletVC"];
            wallet.hidesBottomBarWhenPushed = YES;
            
            [self.navigationController pushViewController:wallet animated:YES];
            
            
        } else if ([_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"senMon"]] || [_lblStatus.text isEqualToString:[MCLocalization stringForKey:@"receivedMoney"]]) {
            
            
            SendMoneyVC * sendMoney = [self.storyboard instantiateViewControllerWithIdentifier:@"SendMoneyVC"];
            sendMoney.hidesBottomBarWhenPushed = YES;
            
            [self.navigationController pushViewController:sendMoney animated:YES];
            
            
        } else {
            
            [self presentedViewController];
        }
        
    }

    
}



@end
