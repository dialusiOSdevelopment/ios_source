//
//  PageContentViewController.m
//  Saddly
//
//  Created by Sai krishna on 2/7/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "PageContentViewController.h"

@interface PageContentViewController ()

@end

@implementation PageContentViewController

- (void)viewDidLoad {
    
    
    self.imgBackground.image = [UIImage imageNamed:self.imageFile];
    self.lblScreenData.text = self.titleText;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 

@end
