//
//  PayFromWalletVC.m
//  DialuzApp
//
//  Created by Sai krishna on 11/11/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import "PayFromWalletVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface PayFromWalletVC ()

@end

@implementation PayFromWalletVC

- (void)viewDidLoad {
    
 
    //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Pay From Wallet Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //

    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    _Namemobilenumber.adjustsFontSizeToFitWidth=YES;
    _NameOperator.adjustsFontSizeToFitWidth=YES;
    _NameRechargeplan.adjustsFontSizeToFitWidth=YES;
    _nameRechargeAmount.adjustsFontSizeToFitWidth=YES;
    _fromWalletName.adjustsFontSizeToFitWidth=YES;
    _NameRemainingWallet.adjustsFontSizeToFitWidth=YES;
    _btnProceedToPayOutlet.titleLabel.adjustsFontSizeToFitWidth=YES;
    _mobilenumberTxt.adjustsFontSizeToFitWidth=YES;
    _OperatorTxt.adjustsFontSizeToFitWidth=YES;
    _PlanTxt.adjustsFontSizeToFitWidth=YES;
    _AmountFromwalletTxt.adjustsFontSizeToFitWidth=YES;
    _lblRcAmount.adjustsFontSizeToFitWidth=YES;
    _lblRemainingWallet.adjustsFontSizeToFitWidth=YES;
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        //  [myView setDefaultFontFamily:@"JFFlat-Regular" andSubViews:YES andColor:nil];
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
    }
    
    
    // Setting Vertical line.
    UIView *borderBottom = [[UIView alloc] initWithFrame:CGRectMake(0.0, 100.0, self.view.frame.size.width, 1.5)];
    borderBottom.backgroundColor = [UIColor blackColor];
    [self.view addSubview:borderBottom];

    
    
    self.title = [MCLocalization stringForKey:@"Payfromwallet"] ;
    
    _Namemobilenumber.text= [MCLocalization stringForKey:@"numbername"];
    _NameRechargeplan.text=[MCLocalization stringForKey:@"planname"];
    _fromWalletName.text=[MCLocalization stringForKey:@"FromWall"] ;
    _nameRechargeAmount.text = [MCLocalization stringForKey:@"Amount12"];
    _NameRemainingWallet.text = [MCLocalization stringForKey:@"walletBal"];
    
    _lblFromName.text = [MCLocalization stringForKey:@"from"];
    _lblWalletName.text = [MCLocalization stringForKey:@"SlidearrWllt"];;
    
    
    NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
    
        _NameOperator.text=[MCLocalization stringForKey:@"operator"] ;

    } else {
        
        _NameOperator.text=[MCLocalization stringForKey:@"store"] ;

    }
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSUserDefaults * rcMobNum = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * rcAmount = [NSUserDefaults standardUserDefaults];
    _lblRcAmount.text = [rcAmount stringForKey:@"rcAmount"];
    
    [_btnProceedToPayOutlet setTitle:[[MCLocalization stringForKey:@"proceedpay"] stringByAppendingString:_lblRcAmount.text] forState:UIControlStateNormal];
    
        
    NSString*mobiletxt=[NSString stringWithFormat:@"%@",[rcMobNum stringForKey:@"rcMobNum"]];
    _mobilenumberTxt.text=mobiletxt;
        
    NSUserDefaults * rcOperator = [NSUserDefaults standardUserDefaults];
    NSString*operator=[NSString stringWithFormat:@"%@",[rcOperator stringForKey:@"rcOperator"]];
    
    NSLog(@"operator is: %@",operator);
    
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        if ([operator isEqualToString:@"Zain"]) {
            _imgOperator.image = [UIImage imageNamed:@"Zainar.png"];
        } else if ([operator isEqualToString:@"Friendi"]) {
            _imgOperator.image = [UIImage imageNamed:@"Friendiar.png"];
        } else if ([operator isEqualToString:@"Jawwy"]) {
            _imgOperator.image = [UIImage imageNamed:@"Jawwyar.png"];
        } else if ([operator isEqualToString:@"Mobily"]) {
            _imgOperator.image = [UIImage imageNamed:@"Mobilyar.png"];
        } else if ([operator isEqualToString:@"STC"]) {
            _imgOperator.image = [UIImage imageNamed:@"STC_round.png"];
        } else if ([operator isEqualToString:@"Virgin"]) {
            _imgOperator.image = [UIImage imageNamed:@"Virginar.png"];
        } else if ([operator isEqualToString:@"Sawa"]) {
            _imgOperator.image = [UIImage imageNamed:@"Sawaar.png"];
        } else if ([operator isEqualToString:@"Quicknet"]) {
            _imgOperator.image = [UIImage imageNamed:@"Quicknetar.png"];
        } else if ([operator isEqualToString:@"Lebara"]) {
            _imgOperator.image = [UIImage imageNamed:@"Leberaar.png"];
        }else if ([operator isEqualToString:@"iTunes"]) {
            _imgOperator.image = [UIImage imageNamed:@"itunsar.png"];
        }else if ([operator isEqualToString:@"Xbox"]) {
            _imgOperator.image = [UIImage imageNamed:@"xbox-cardar.png"];
        } else if ([operator isEqualToString:@"Playstation"]) {
            _imgOperator.image = [UIImage imageNamed:@"ps-cardar.png"];
        } else {
            _imgOperator.image = [UIImage imageNamed:@"appicon_green.png"];
        }
        
    } else {
        
        if ([operator isEqualToString:@"Zain"]) {
            _imgOperator.image = [UIImage imageNamed:@"zainen.png"];
        } else if ([operator isEqualToString:@"Friendi"]) {
            _imgOperator.image = [UIImage imageNamed:@"friendien.png"];
        } else if ([operator isEqualToString:@"Jawwy"]) {
            _imgOperator.image = [UIImage imageNamed:@"jawwyen.png"];
        } else if ([operator isEqualToString:@"Mobily"]) {
            _imgOperator.image = [UIImage imageNamed:@"mobilyen.png"];
        } else if ([operator isEqualToString:@"STC"]) {
            _imgOperator.image = [UIImage imageNamed:@"STC_round.png"];
        } else if ([operator isEqualToString:@"Virgin"]) {
            _imgOperator.image = [UIImage imageNamed:@"virginen.png"];
        } else if ([operator isEqualToString:@"Sawa"]) {
            _imgOperator.image = [UIImage imageNamed:@"sawaen.png"];
        } else if ([operator isEqualToString:@"Quicknet"]) {
            _imgOperator.image = [UIImage imageNamed:@"quickneten.png"];
        } else if ([operator isEqualToString:@"Lebara"]) {
            _imgOperator.image = [UIImage imageNamed:@"lebaraen.png"];
        }else if ([operator isEqualToString:@"iTunes"]) {
            _imgOperator.image = [UIImage imageNamed:@"itunsen.png"];
            
        }else if ([operator isEqualToString:@"Xbox"]) {
            _imgOperator.image = [UIImage imageNamed:@"Xbox Carden.png"];
            
        } else if ([operator isEqualToString:@"Playstation"]) {
            _imgOperator.image = [UIImage imageNamed:@"ps-carden.png"];
            
        } else {
            _imgOperator.image = [UIImage imageNamed:@"appicon_green.png"];
        }
        
    }

    
    NSUserDefaults * plandefaults = [NSUserDefaults standardUserDefaults];
     _PlanTxt.text=[NSString stringWithFormat:@"%@",[[plandefaults stringForKey:@"plandefaults"]stringByReplacingOccurrencesOfString:@" " withString:@""]];
    
    NSUserDefaults * WalletDefaults = [NSUserDefaults standardUserDefaults];
    Walletdetails=    [NSKeyedUnarchiver unarchiveObjectWithData:[WalletDefaults objectForKey:@"WalletDefaults"]];
    
    
    _lblRemainingWallet.text = [NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"remwalletamt"]];
    
    NSString*Amountfromwallet=[NSString stringWithFormat:@"%@",[Walletdetails valueForKey:@"rechargeAmount"]];
    _AmountFromwalletTxt.text = Amountfromwallet;
    
        
}





-(void)viewDidAppear:(BOOL)animated{

    _Namemobilenumber.adjustsFontSizeToFitWidth=YES;
    _NameOperator.adjustsFontSizeToFitWidth=YES;
    _NameRechargeplan.adjustsFontSizeToFitWidth=YES;
    _nameRechargeAmount.adjustsFontSizeToFitWidth=YES;
    _fromWalletName.adjustsFontSizeToFitWidth=YES;
    _NameRemainingWallet.adjustsFontSizeToFitWidth=YES;
    _btnProceedToPayOutlet.titleLabel.adjustsFontSizeToFitWidth=YES;
    _mobilenumberTxt.adjustsFontSizeToFitWidth=YES;
    _OperatorTxt.adjustsFontSizeToFitWidth=YES;
    _PlanTxt.adjustsFontSizeToFitWidth=YES;
    _AmountFromwalletTxt.adjustsFontSizeToFitWidth=YES;
    _lblRcAmount.adjustsFontSizeToFitWidth=YES;
    _lblRemainingWallet.adjustsFontSizeToFitWidth=YES;
    
    
    
    NSUserDefaults * rcOperator = [NSUserDefaults standardUserDefaults];
    NSString*operator=[NSString stringWithFormat:@"%@",[rcOperator stringForKey:@"rcOperator"]];
    
    NSLog(@"operator is: %@",operator);
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        if ([operator isEqualToString:@"Zain"]) {
            _imgOperator.image = [UIImage imageNamed:@"Zainar.png"];
        } else if ([operator isEqualToString:@"Friendi"]) {
            _imgOperator.image = [UIImage imageNamed:@"Friendiar.png"];
        } else if ([operator isEqualToString:@"Jawwy"]) {
            _imgOperator.image = [UIImage imageNamed:@"Jawwyar.png"];
        } else if ([operator isEqualToString:@"Mobily"]) {
            _imgOperator.image = [UIImage imageNamed:@"Mobilyar.png"];
        } else if ([operator isEqualToString:@"STC"]) {
            _imgOperator.image = [UIImage imageNamed:@"STC_round.png"];
        } else if ([operator isEqualToString:@"Virgin"]) {
            _imgOperator.image = [UIImage imageNamed:@"Virginar.png"];
        } else if ([operator isEqualToString:@"Sawa"]) {
            _imgOperator.image = [UIImage imageNamed:@"Sawaar.png"];
        } else if ([operator isEqualToString:@"Quicknet"]) {
            _imgOperator.image = [UIImage imageNamed:@"Quicknetar.png"];
        } else if ([operator isEqualToString:@"Lebara"]) {
            _imgOperator.image = [UIImage imageNamed:@"Leberaar.png"];
        }else if ([operator isEqualToString:@"iTunes"]) {
            _imgOperator.image = [UIImage imageNamed:@"itunsar.png"];
        }else if ([operator isEqualToString:@"Xbox"]) {
            _imgOperator.image = [UIImage imageNamed:@"xbox-cardar.png"];
        } else if ([operator isEqualToString:@"Playstation"]) {
            _imgOperator.image = [UIImage imageNamed:@"ps-cardar.png"];
        } else {
            _imgOperator.image = [UIImage imageNamed:@"appicon_green.png"];
        }
        
    } else {
        
        if ([operator isEqualToString:@"Zain"]) {
            _imgOperator.image = [UIImage imageNamed:@"zainen.png"];
        } else if ([operator isEqualToString:@"Friendi"]) {
            _imgOperator.image = [UIImage imageNamed:@"friendien.png"];
        } else if ([operator isEqualToString:@"Jawwy"]) {
            _imgOperator.image = [UIImage imageNamed:@"jawwyen.png"];
        } else if ([operator isEqualToString:@"Mobily"]) {
            _imgOperator.image = [UIImage imageNamed:@"mobilyen.png"];
        } else if ([operator isEqualToString:@"STC"]) {
            _imgOperator.image = [UIImage imageNamed:@"STC_round.png"];
        } else if ([operator isEqualToString:@"Virgin"]) {
            _imgOperator.image = [UIImage imageNamed:@"virginen.png"];
        } else if ([operator isEqualToString:@"Sawa"]) {
            _imgOperator.image = [UIImage imageNamed:@"sawaen.png"];
        } else if ([operator isEqualToString:@"Quicknet"]) {
            _imgOperator.image = [UIImage imageNamed:@"quickneten.png"];
        } else if ([operator isEqualToString:@"Lebara"]) {
            _imgOperator.image = [UIImage imageNamed:@"lebaraen.png"];
        }else if ([operator isEqualToString:@"iTunes"]) {
            _imgOperator.image = [UIImage imageNamed:@"itunsen.png"];
            
        }else if ([operator isEqualToString:@"Xbox"]) {
            _imgOperator.image = [UIImage imageNamed:@"Xbox Carden.png"];
            
        } else if ([operator isEqualToString:@"Playstation"]) {
            _imgOperator.image = [UIImage imageNamed:@"ps-carden.png"];
            
        } else {
            _imgOperator.image = [UIImage imageNamed:@"appicon_green.png"];
        }
        
    }
    
    
    
    
    if ([operator isEqualToString:@"iTunes"]) {
        _OperatorTxt.text=[MCLocalization stringForKey:@"itunesGC"];
    }else if ([operator isEqualToString:@"Xbox"]){
        _OperatorTxt.text=[MCLocalization stringForKey:@"xboxGC"];
    }else if ([operator isEqualToString:@"Playstation"]){
        _OperatorTxt.text=[MCLocalization stringForKey:@"playstationGC"];
    }else if ([operator isEqualToString:@"Zain"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorTxt.text=@"زين";
            
        }
        else{
            
            _OperatorTxt.text=@"Zain";
        }
    }else if ([operator isEqualToString:@"Sawa"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorTxt.text=@"سوا"  ;
            
        }
        else{
            _OperatorTxt.text=@"Sawa";
            
        }
    }
    else if ([operator isEqualToString:@"Virgin"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorTxt.text=@"فيرجن";}
        else{
            
            _OperatorTxt.text=@"Virgin";
        }
    }
    else if ([operator isEqualToString:@"Friendi"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorTxt.text=@"فريندي";}
        else{
            
            _OperatorTxt.text=@"Friendi";
        }
    }
    else if ([operator isEqualToString:@"Jawwy"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorTxt.text=@"جوي";}
        else{
            
            _OperatorTxt.text=@"Jawwy";
        }
    }
    else if ([operator isEqualToString:@"Quicknet"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorTxt.text=@"كويك نت";}
        else{
            
            _OperatorTxt.text=@"Quicknet";
        }
    }
    else if ([operator isEqualToString:@"Mobily"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorTxt.text=@"موبايلي";}
        else{
            
            _OperatorTxt.text=@"Mobily";
        }
    }
    else if ([operator isEqualToString:@"lebara"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorTxt.text=@"ليبارا";}
        else{
            
            _OperatorTxt.text=@"lebara";
        }
    }
    
    [self loaderWithOperator];
    
    _lblFromName.text = [MCLocalization stringForKey:@"from"];
    _lblWalletName.text = [MCLocalization stringForKey:@"SlidearrWllt"];;
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
 
    // Dispose of any resources that can be recreated.

}




            
            


-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}


// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}



- (void)loaderWithOperator {
    
    
    _loadingView = [[UIView alloc] initWithFrame:CGRectMake(110, 200, 250, 200)];
    _loadingView.center=self.view.center;
    _loadingView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    _loadingView.clipsToBounds = YES;
    _loadingView.layer.cornerRadius = 10.0;
    
    
    
    _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _activityView.frame = CGRectMake(10 , 130, _activityView.bounds.size.width, _activityView.bounds.size.height);
    [_loadingView addSubview:_activityView];
    
    
    
    _loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, 100, self.loadingView.frame.size.width, 100)];
    _loadingLabel.backgroundColor = [UIColor clearColor];
    _loadingLabel.textColor = [UIColor whiteColor];
    _loadingLabel.textAlignment = NSTextAlignmentLeft;
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        _loadingLabel.text = @"عملية الدفع جارية... يرجى الانتظار";
    } else {
        _loadingLabel.text = @"Payment is Processing...\nPlease Wait....";
    }
    
    _loadingLabel.numberOfLines=0;
    [_loadingView addSubview:_loadingLabel];
    
    
    
    
    _loadingimage=[[UIImageView alloc]initWithFrame:CGRectMake(75, 20, 100, 100)];
    _loadingimage.image=_imgOperator.image;
    [_loadingView addSubview:_loadingimage];
    
    
}



-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"quickpayment"]) {
                    
                    [self quickpayment];
                    
                }
                
                
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//



 
-(void)quickpayment{
    
    
    [self.view addSubview:_loadingView];
    [_activityView startAnimating];
    
    double delayInSeconds = 1.0;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        
        NSUserDefaults * rcOperator = [NSUserDefaults standardUserDefaults];
        NSString*operator=[NSString stringWithFormat:@"%@",[rcOperator stringForKey:@"rcOperator"]];
        
        NSLog(@"operator: %@",operator);
        
        NSUserDefaults * trasactionid = [NSUserDefaults standardUserDefaults];
        NSString* orderId =  [trasactionid valueForKey:@"trasactionid"];
        
        NSLog(@"Id is%@",orderId);
        
        NSUserDefaults * serviceIdDefaults = [NSUserDefaults standardUserDefaults];
        NSString*serviceId=[serviceIdDefaults stringForKey:@"serviceIdDefaults"];
        
        NSLog(@"Id is%@",serviceId);
        
    
        NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
        NSDictionary* profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
        
        NSString*mobileNo=[profileDict valueForKey:@"user_mob"];
        
        
        NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        
        NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
        
        NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/insertQuickPayRechargeTransactionsDetails1"];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        
        
        request.HTTPMethod = @"POST";
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        
        
        
        NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
        NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
        
        
        NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
        NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
        

        NSString * userId = [profileDict valueForKey:@"user_mob"];
        
        NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
        
        NSString * userPwd = [pddefaults stringForKey:@"pddd"];
//    NSString*deviceid=[payFort getUDID];

        
        //         NSString * userPwd=@"585858";
        
        NSLog(@"%@",userId);
        NSLog(@"%@",userPwd);
        
        NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
        
        NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
        NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
        
        
        NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
        [request setValue:authValue forHTTPHeaderField:@"Authorization"];

        
        NSDictionary * loginDetailsDict =  @{@"serviceId":serviceId,
                                             @"orderId":orderId,
                                             @"operator":operator,
                                             @"usermob":mobileNo,
                                             @"access_token":accessToken,
                                             @"unique_id":uniqueid,
                                             @"from":@"iphone",
                                             };
        
        
        
        NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
        
        
        NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
        
        
        [request setHTTPBody:postdata];
        
        
        // Create dataTask
        
        NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            
            
            if (!data) {
                
                
                
                NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
                
                NSLog(@"%@",userErrorText);
                
                return;
                
            }
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
                
                
                NSError *deserr;
                
                
                
                NSDictionary*Response = [NSJSONSerialization
                                         
                                         JSONObjectWithData:data
                                         
                                         options:kNilOptions
                                         
                                         error:&deserr];
                
                
                
                
                
                NSString *resSrt =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                
                
                
                
                NSLog(@"Killer Response is%@",Response);
                
                NSLog(@"str Response is%@",resSrt);
                
                
                
                if ([[Response valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"quickpayment";
                    
                    [self checkTokenStatusWebServices];
                    
                    
                }else if ([[Response valueForKey:@"response_message"]isEqualToString:@"userUnauthorized"]){
                    
                    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                    [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                    
                    NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                    [loginName removeObjectForKey:@"userName"];
                    
                    
                    
                    LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                    [self.navigationController pushViewController:login animated:YES];
                    
                }
                
                else{

                    
                    NSUserDefaults *WalletSuccessResponse = [NSUserDefaults standardUserDefaults];
                    NSData * CardTrData = [NSKeyedArchiver archivedDataWithRootObject:Response];
                    [WalletSuccessResponse setObject:CardTrData forKey:@"WalletSuccessResponse"];
                    
                    
                    
                    
                    NSLog(@"got response=%@", Response);
                    
                    if ([Response valueForKey:@"response_message"]) {
                        
                        RehargeSuccessVC * rechargesuccess = [self.storyboard instantiateViewControllerWithIdentifier:@"RehargeSuccessVC"];
                        [self.navigationController pushViewController:rechargesuccess animated:YES];
                        
                        NSUserDefaults * couponcodenum = [NSUserDefaults standardUserDefaults];
                        [couponcodenum removeObjectForKey:@"couponcodenum"];
                        
                    } else {
                        
                        NSLog(@"Something went wrong");
                        
                    }
                    
                    //this will be executed after 2 seconds
        
                }
      
            });
        
        }];
        
        [dataTask resume];
        
        
        });
        
    }


- (IBAction)ProceedtopayQuickpay:(id)sender {
    
    [self quickpayment];
    
}

@end
