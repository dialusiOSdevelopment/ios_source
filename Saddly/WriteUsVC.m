//
//  WriteUsVC.m
//  Saddly
//
//  Created by Sai krishna on 6/9/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "WriteUsVC.h"

#define kOFFSET_FOR_KEYBOARD 80.0


@interface WriteUsVC ()

@end

@implementation WriteUsVC

- (void)viewDidLoad {
    
    
    self.title = [MCLocalization stringForKey:@"writeUs"];
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        _SlecturIssueType.text=@" أختر نوع مشكلتك";
        _EnterUrTransactionId.text=@"أدخل رقم التحويل";
        _AboutURIssue.text=@"أكتب لنا عن مشكلتك";
        
    } else {
       
        _SlecturIssueType.text=@"Select Your Issue Type";
        _EnterUrTransactionId.text=@"Enter Your Transaction ID";
        _AboutURIssue.text=@"Write about your issue";
      
    }
    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    
    // Returning Keyboard For Number Pad.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(NextClicked)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    _txtViewAboutissue.inputAccessoryView = keyboardDoneButtonView;


    _lblAttachmentBackground.layer.borderColor = [[UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:239.0/255.0 alpha:1.0]CGColor];
    _txtViewAboutissue.layer.borderColor = [[UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:239.0/255.0 alpha:1.0] CGColor];

  
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)removingKeyboardByTap {
    
    [self.view endEditing:YES];
    [_txtTxnId resignFirstResponder];
    //[self.txtViewAboutissue resignFirstResponder];
}






-(void)viewDidAppear:(BOOL)animated {
    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];

    
    // Showing Camera & Photos in alertview
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(btnAttachment:)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [_txtFileAttachment setUserInteractionEnabled:YES];
    [_txtFileAttachment addGestureRecognizer:tapKeyboardGesture];


    [self getIssueTypesServicesMethod];
    
    [_txtIssueType setPlaceholder:[MCLocalization stringForKey:@"issuetype"]];
    [_txtTxnId setPlaceholder:[MCLocalization stringForKey:@"txnId"]];
    [_txtTxnDate setPlaceholder:[MCLocalization stringForKey:@"txnDate"]];
    [_txtTxnTime setPlaceholder:[MCLocalization stringForKey:@"txntime"]];
    [_txtFileAttachment setPlaceholder:[MCLocalization stringForKey:@"fileAttach"]];
    
    _lblMinChar75.text = [MCLocalization stringForKey:@"lblMinChar75"];
    
    [_btnSubmitOutlet setTitle:[MCLocalization stringForKey:@"ForgetpasswordSubmitt"] forState:(UIControlStateNormal)];
    
    
    
    // Key board is not appeared for the TextField
    UIView * dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    _txtIssueType.inputView = dummyView;
    _txtTxnDate.inputView = dummyView;
    _txtTxnTime.inputView = dummyView;
    _txtFileAttachment.inputView = dummyView;
    
    
    // Showing Issues on the AlertAction.
    UITapGestureRecognizer * tapGestureIssueType = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(txtIssue)];
    tapGestureIssueType.numberOfTapsRequired = 1;
    [_txtIssueType setUserInteractionEnabled:YES];
    [_txtIssueType addGestureRecognizer:tapGestureIssueType];

    
    // Showing Date on the AlertAction.
    UITapGestureRecognizer * tapGestureDate = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(txtTransactionDate)];
    tapGestureDate.numberOfTapsRequired = 1;
    [_txtTxnDate setUserInteractionEnabled:YES];
    [_txtTxnDate addGestureRecognizer:tapGestureDate];
    

    // Showing Time on the AlertAction.
    UITapGestureRecognizer * tapGestureTime = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(txtTransactionTIme)];
    tapGestureTime.numberOfTapsRequired = 1;
    [_txtTxnTime setUserInteractionEnabled:YES];
    [_txtTxnTime addGestureRecognizer:tapGestureTime];
    
    
}



// Returning KeyBoard
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == self.txtTxnId) {
        [self.txtTxnId resignFirstResponder];
    } else if ([textField isEqual:self.txtTxnDate]) {
        [self.txtTxnDate resignFirstResponder];
    }
    else if ([textField isEqual:self.txtTxnTime]) {
        [self.txtTxnTime resignFirstResponder];
    }
    return YES;
}





-(void)NextClicked{
    [self.txtIssueType becomeFirstResponder];
}

// Characters Length in TextView
//-(void)textViewDidChange:(UITextView *)textView{
    
  //  NSString*ua=@"Characters Left";
    
   //NSString*count = [NSString stringWithFormat:@"%lu",75-_txtViewAboutissue.text.length];
    
  //  _CharactersLeft.text=[ua stringByAppendingString:count];
//}




//Setting the range of the TextField
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text length] == 0) {
        if([_txtViewAboutissue.text length] != 0)
        {
            return YES;
        }
    } else if([[_txtViewAboutissue text] length] > 1000) {
        return NO;
    }
    
    // Dismissing Keyboard for textview
    if([text isEqualToString:@"\n"]) {
        [_txtFileAttachment resignFirstResponder];
        return NO;
    }
    return YES;
}







//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}


// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}



-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"getIssueTypesServicesMethod"]) {
                    [self getIssueTypesServicesMethod];
                }
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//





// getIssueTypes web services method
-(void)getIssueTypesServicesMethod {
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        db= @"AR";
        
    } else {
        
        db = @"SA";
    }
    //
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString*UEmail= [profileDict valueForKey:@"user_email_id"];
    NSString*uMob=   [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    

    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    

    [LoaderClass showLoader:self.view];
    
  
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/profile/getSupportIssuesTypesIOSDetails"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[uMob stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * getIssueTypesDict =  @{@"user_mob":uMob,
                                          @"user_emailId":UEmail,
                                          @"ipAddress":Ipaddress,
                                          @"from":@"iPhone",
                                          @"db":db,
                                          @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                          @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                          };
    
    NSLog(@"Posting getIssueTypesDict is %@",getIssueTypesDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:getIssueTypesDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        

        if (!data) {
            
            NSLog(@"No data returned from server, error ocurred: %@", error);
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            

            
            NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            NSLog(@"resSrt: %@", resSrt);
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            NSMutableDictionary*respdict=[[NSMutableDictionary alloc]init];
            [respdict setObject:Killer forKey:@"List"];
            
            [LoaderClass removeLoader:self.view];
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"getIssueTypesServicesMethod";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                } else {
                                        
                    
                    NSLog(@"Issue Types Details Response %@",Killer);
                    
                    
                    arrIssueTypes = [[Killer valueForKey:@"iosissueTypes"] valueForKey:@"message"];
                    
                    NSLog(@"Issue Types Details Response %@",arrIssueTypes);
                    

                }
            }
        });
        
    }];
    
    [dataTask resume];
    
}




// Posting Registration Details to the Server
-(void)postingQuestionsDetails{
    
    
    [LoaderClass showLoader:self.view];
    
    NSString * regURL = [NSString stringWithFormat:@"https://m.app.saddly.com/support/getQuestions"];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:regURL]];
    NSString * boundary = @"unique-consistent-string";
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString * contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData * body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_mob"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[profileDict valueForKey:@"user_mob"]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"user_email"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[[profileDict valueForKey:@"user_email_id"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"IssueType"] dataUsingEncoding:NSUTF8StringEncoding]];
    NSString * appendedMobNum = _txtIssueType.text;
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",appendedMobNum] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"device"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",@"iphone"] dataUsingEncoding:NSUTF8StringEncoding]];

    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"txn_id"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",_txtTxnId.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"txn_date"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",_txtTxnDate.text] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"txn_time"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[_txtTxnTime.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    if (imgpicked == 10) {
    
        // add image data
        NSData *imageData = UIImageJPEGRepresentation(profilePicImg, 100);
        if (imageData) {
            
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imageName.jpg\r\n", @"file"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            
        } // add image data
    
    } else {
        profilePicImg = nil;
    }
    
   
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"about_issue"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n",[_txtViewAboutissue.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
   
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data.length > 0) {
            
            NSString  * stri  = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            NSLog(@"Send Questions stri %@ hi",stri);
            
            NSString * st = [[stri componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]]componentsJoinedByString:@""];
            
            NSLog(@"Send Questions st %@ hi",st);
            
             [LoaderClass removeLoader:self.view];
            
            if (st.length > 0) {
                
                
                NSUserDefaults * Suceess = [NSUserDefaults standardUserDefaults];
                
                [Suceess setObject:st forKey:@"Suceess"];
                

                
                WriteUsSuccessVC * guestGC = [self.storyboard instantiateViewControllerWithIdentifier:@"WriteUsSuccessVC"];
                [self.navigationController pushViewController:guestGC animated:YES];
                

            }
            
        } else {
            
            NSLog(@"error is %@", error.localizedDescription);
            alertMessage = [[[MCLocalization stringForKey:@"wentWrong"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
            [self alertMethod];
        }
    }];
}



- (void)txtIssue {
    
    alert = [UIAlertController alertControllerWithTitle:nil message:[MCLocalization stringForKey:@"selissuetype"] preferredStyle:UIAlertControllerStyleActionSheet];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake( CGRectGetMidX(self.txtIssueType.frame), CGRectGetMidY(self.txtIssueType.frame), 0, 0);


    
    
    for (int j =0 ; j < arrIssueTypes.count; j++) {
        
        NSString *titleString = arrIssueTypes[j];
        NSString * oneTwo = [[[titleString stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
        
        
        action = [UIAlertAction actionWithTitle:oneTwo style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            self.txtIssueType.text = oneTwo;
        }];
        
        [alert addAction:action];
    }
    
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"Forgetpasswordcancell"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [self presentedViewController];
        
    }];
    
    
    [alert addAction:cancelAction];
    alert.view.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}




- (void)txtTransactionDate {
    
    NSDate* now = [NSDate date];
    // Get current NSDate without seconds & milliseconds, so that I can better compare the chosen date to the minimum & maximum dates.
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    //    NSDateComponents* nowWithoutSecondsComponents = [calendar components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit) fromDate:now] ;
    
    NSDateComponents* nowWithoutSecondsComponents = [calendar components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute) fromDate:now] ;
    
    
    NSDate* nowWithoutSeconds = [calendar dateFromComponents:nowWithoutSecondsComponents] ;
    //  UIDatePicker* picker ;
    // picker.minimumDate = nowWithoutSeconds ;
    
    
    NSLog(@"Date from date picker : %@",nowWithoutSeconds);
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:UIDatePickerModeDate];
    picker.maximumDate = nowWithoutSeconds ;
    [alertController.view addSubview:picker];
    
    
    [alertController addAction:({
        
        UIAlertAction * actions = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            NSLog(@"Date from date picker : %@",picker.date);
            
            
            NSDateFormatter* df = [[NSDateFormatter alloc]init];
            df.dateFormat = @"dd/MM/yyyy";
            NSString* dateString = [df stringFromDate:picker.date];
            
            NSLog(@"only date: %@",dateString);
            
            self.txtTxnDate.text = [NSString stringWithFormat:@"%@",dateString];
            
           
            NSLog(@"nowWithoutSeconds date %@",nowWithoutSeconds);
            
            if (picker.date>nowWithoutSeconds) {
                
                alertMessage=@"Please select a correct date as the selected one is a future date";
                [self alertMethod];
            }
            
            
        }];
        actions;
    })];
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"CANCEL"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [self presentedViewController];
        
    }];
    
    [alertController addAction:cancelAction];
    
    
    UIPopoverPresentationController * popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = self.view;
    popoverController.sourceRect = CGRectMake( CGRectGetMidX(self.txtTxnDate.frame), CGRectGetMidY(self.txtTxnDate.frame), 0, 0);
    
    [self presentViewController:alertController  animated:YES completion:nil];
    
}



- (void)txtTransactionTIme {
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIDatePicker *picker = [[UIDatePicker alloc] init];
    [picker setDatePickerMode:UIDatePickerModeTime];
    [alertController.view addSubview:picker];
    
    
    [alertController addAction:({
        
        UIAlertAction * actions = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            NSLog(@"Time from Time picker: %@",picker.date);
            
            NSDateFormatter* df = [[NSDateFormatter alloc]init];
            df.dateFormat = @"HH:mm";
            NSString* timeString = [df stringFromDate:picker.date];
            
            NSLog(@"Only Time: %@",timeString);
            
            self.txtTxnTime.text = [NSString stringWithFormat:@"%@",timeString];
            
        }];
        actions;
    })];
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"CANCEL"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [self presentedViewController];
        
    }];
    
    [alertController addAction:cancelAction];
    
    
    UIPopoverPresentationController * popoverController = alertController.popoverPresentationController;
    popoverController.sourceView = self.view;
    popoverController.sourceRect = CGRectMake( CGRectGetMidX(self.txtTxnTime.frame), CGRectGetMidY(self.txtTxnTime.frame), 0, 0);
    
    
    [self presentViewController:alertController  animated:YES completion:nil];
    
    
}



- (IBAction)btnIssueType:(id)sender {
    
    [self txtIssue];
    
}


- (IBAction)btnAttachment:(id)sender {
    
    
    // Showing Camera & Photos in alertview
    alert = [UIAlertController alertControllerWithTitle:[MCLocalization stringForKey:@"choosefile"] message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake( CGRectGetMidX(self.txtFileAttachment.frame), CGRectGetMidY(self.txtFileAttachment.frame), 0, 0);
    
    UIAlertAction * camera = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"camera"]
                                                      style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                          
                                                          _imgChooseFileType = @"100";
                                                          picImg = [[UIImagePickerController alloc] init];
                                                          picImg.delegate = self;
                                                          picImg.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                          [picImg setAllowsEditing:YES];
                                                          [self presentViewController:picImg animated:YES completion:nil];
                                                      }];
    
    UIAlertAction * photos = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"photos"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        _imgChooseFileType = @"200";
        
        picImg = [[UIImagePickerController alloc] init];
        picImg.delegate = self;
        picImg.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picImg animated:YES completion:nil];
    }];
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"Forgetpasswordcancell"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [self presentedViewController];
    }];
    
    [alert addAction:camera];
    [alert addAction:photos];
    [alert addAction:cancel];
    [self.view layoutIfNeeded];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}


// Updating profile pic to the Imageview
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    imgpicked = 10;
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:info[UIImagePickerControllerReferenceURL]
             resultBlock:^(ALAsset *fileAsset) {
                 
                 NSLog(@"%@", [[fileAsset defaultRepresentation] filename]);
                 
                 if ([_imgChooseFileType isEqualToString:@"200"]) {
                     _txtFileAttachment.text = [[fileAsset defaultRepresentation] filename];
                     
                 } else {
                     _txtFileAttachment.text = @"PHOTO.JPG";
                 }
                 
                 
             } failureBlock:nil];
    
    
    profilePicImg = [info valueForKey:UIImagePickerControllerOriginalImage];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



- (IBAction)btnSubmit:(id)sender {
    
    // issue type
    if ([_txtIssueType.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].
        length == 0){
        toastMsg = [MCLocalization stringForKey:@"plsselissuetype"];
        [self toastMessagemethod];
    }
//    else if (_txtViewAboutissue.text.length < 75) {
//        
//        toastMsg = [MCLocalization stringForKey:@"plsselissuetype"];
//        [self toastMessagemethod];
//        
//        
//    }
    // txn id & txn date
    else if ([_txtTxnId.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].
             length == 0 && [_txtTxnDate.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        
        // txn id
        if ([_txtTxnId.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        
            toastMsg = [MCLocalization stringForKey:@"plsentTxnId"];
            [self toastMessagemethod];
            
        }
        // txn date
        else  if ([_txtTxnDate.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
            toastMsg = [MCLocalization stringForKey:@"plsseltxndate"];
            [self toastMessagemethod];
        }
        else if ([_txtViewAboutissue.text length]<75){
            
            if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){

                toastMsg = @"يرجى ادخال ما لا يقل عن 75 حرفاً عن مشكلتك";

            } else {
                toastMsg = @"Please Enter Minimum 75 Characters About Your Issue";
            }
            
            [self toastMessagemethod];

        }
        
    }
    
    else if ([_txtViewAboutissue.text length]<75){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            
            alertMessage=@"يرجى ادخال ما لا يقل عن 75 حرفاً عن مشكلتك";
            [self alertMethod];
            
        } else {
            
            alertMessage = @"Please enter minimum 75 characters about your issue";
            [self alertMethod];

            
        }
 
    }
    
    else {
        
        [self postingQuestionsDetails];
    }

    
}




@end
