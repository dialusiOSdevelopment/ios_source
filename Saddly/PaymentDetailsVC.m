//
//  PaymentDetailsVC.m
//  DialuzApp
//
//  Created by Sai krishna on 10/17/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import "PaymentDetailsVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface PaymentDetailsVC ()

@end

@implementation PaymentDetailsVC

- (void)viewDidLoad {
    
    [LoaderClass showLoader:self.view];

    //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Add Money From Card Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //

    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
        
    }
   
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
   
    
      NSUserDefaults *paymentGatewayDetailsDefaults = [NSUserDefaults standardUserDefaults];

    paymentGatewayDetailsDict = [NSKeyedUnarchiver unarchiveObjectWithData:[paymentGatewayDetailsDefaults objectForKey:@"paymentGatewayDetailsDefaults"]];
    
        NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
 
    
    payFort	= [[PayFortController alloc] initWithEnviroment:KPayFortEnviromentProduction];
    deviceId=[payFort getUDID];
    
 
    self.title = [MCLocalization stringForKey:@"confirmPaymentDetails"];

    NSUserDefaults * walletAmountDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * AmntDefaults = [NSUserDefaults standardUserDefaults];
    

    _name.text = [[[[profileDict valueForKey:@"user_name"] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    _mobNum.text = [profileDict valueForKey:@"user_mob"];
    
    _email.text = [[profileDict valueForKey:@"user_email_id"] stringByRemovingPercentEncoding];
    
    if ([[walletAmountDefaults stringForKey:@"amount"] length ]>0) {
        
        _amount.text = [walletAmountDefaults stringForKey:@"amount"];

    } else {
    
        _amount.text = [AmntDefaults stringForKey:@"AmntDefaults"];
    }
    
    //Names
    _nameName.text = [MCLocalization stringForKey:@"name12"];
    _mobNumName.text = [MCLocalization stringForKey:@"onlyMobile"];
    _emailName.text = [MCLocalization stringForKey:@"Email12"];
    _amountName.text = [MCLocalization stringForKey:@"Amount12"];
    _lblEnterSadadName.text = [MCLocalization stringForKey:@"entersadadid"];
    _txtEnterSadadId.placeholder = [MCLocalization stringForKey:@"sadadid"];

    
    
    [_proceedPayName setTitle:[[MCLocalization stringForKey:@"proceedpay"] stringByAppendingString:_amount.text] forState:UIControlStateNormal];
    
    NSUserDefaults * Paymentmethod = [NSUserDefaults standardUserDefaults];
    
    if ([[Paymentmethod stringForKey:@"Paymentmethod"] isEqualToString:[MCLocalization stringForKey:@"sadad"]]) {
        
        _lblEnterSadadName.hidden=NO;
        _txtEnterSadadId.hidden=NO;
        _lblSadadDots.hidden= NO;

    }  else {
        
        _lblEnterSadadName.hidden=YES;
        _txtEnterSadadId.hidden=YES;
        _lblSadadDots.hidden= YES;

       
        [self signatureWebServicesMethod];

    }

    
    _name.adjustsFontSizeToFitWidth=YES;
    _nameName.adjustsFontSizeToFitWidth=YES;
    _mobNum.adjustsFontSizeToFitWidth=YES;
    _email.adjustsFontSizeToFitWidth=YES;
    _amount.adjustsFontSizeToFitWidth=YES;
    _mobNumName.adjustsFontSizeToFitWidth=YES;
    _emailName.adjustsFontSizeToFitWidth=YES;
    _amountName.adjustsFontSizeToFitWidth=YES;
    _lblEnterSadadName.adjustsFontSizeToFitWidth = YES;
    

    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}


// Enabling the Back Button
-(void)viewDidAppear:(BOOL)animated{
    
    _name.adjustsFontSizeToFitWidth=YES;
    _nameName.adjustsFontSizeToFitWidth=YES;
    _mobNum.adjustsFontSizeToFitWidth=YES;
    _email.adjustsFontSizeToFitWidth=YES;
    _amount.adjustsFontSizeToFitWidth=YES;
    _mobNumName.adjustsFontSizeToFitWidth=YES;
    _emailName.adjustsFontSizeToFitWidth=YES;
    _amountName.adjustsFontSizeToFitWidth=YES;
    
    
    
    if (test == 10) {
        
        [self performSegueWithIdentifier:@"HomeFromPaymentDetails" sender:nil];
        
    } else {
    
        [self.navigationItem setHidesBackButton:NO animated:YES];
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
            
            NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
            [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
            
            
            if ([methodname isEqualToString:@"signatureWebServicesMethod"]) {
                [self signatureWebServicesMethod];
            }else if ([methodname isEqualToString:@"returnUrl"]) {
                [self returnUrl];
            }
            
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//



-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];


        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}

// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                          
                                          
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}


// Returning to Next Text fields
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtEnterSadadId) {
        [self.txtEnterSadadId resignFirstResponder];
    }     return YES;
}



//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:_txtEnterSadadId]){
        if([self isAlphaNumericSpecialCharacters:string]) {
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 12;
            
        } else
            return FALSE;
    }
    return YES;
}


// Entering only alphaNumeric & Special Characters not a Arabic Characters
-(BOOL)isAlphaNumericSpecialCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._@-"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}


//For Signature

-(void)signatureWebServicesMethod{
    
    
    
    NSString * db = [MCLocalization stringForKey:@"DBvalue"];
    
   
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary*   profileDict1 = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    NSString*UEmail= [profileDict1 valueForKey:@"user_email_id"];
    
    
    
    NSString*uMob=   [profileDict1 valueForKey:@"user_mob"];
    
    
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    
    
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSString*from=@"iphone";
    
 
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/getTokenSignatureDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict1 valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    
    NSLog(@"%@",userId);
    NSLog(@"%@",userPwd);
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
   
    NSDictionary * OpDict =  @{
                               @"device_id":deviceId,
                               @"db":db,
                               @"from":from,
                               @"ipAddress":Ipaddress,
                               @"user_mob":uMob,
                               @"user_emailId":UEmail,
                               @"access_token":[accessTokenDefaults valueForKey:@"accessTokenDefaults"],
                               @"unique_id":uniqueid
                               
                               };
    
    
    NSLog(@"Posting loginDetailsDict is %@",OpDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:OpDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
            NSString*  resSrt  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Killer Response is%@",Killer);
            
            NSLog(@"str Response is%@",resSrt);
            
            
            
            [LoaderClass removeLoader:self.view];

    

    
    NSLog(@"Signature Response is %@",resSrt);
            
            
    
            if ([[Killer valueForKey:@"Response"]isEqualToString:@"TokenExpired"]) {
                
                methodname=@"signatureWebServicesMethod";
                
                [self checkTokenStatusWebServices];
                
            }else if ([[Killer valueForKey:@"Response"]isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];

            
            
            }else{
    signature = [Killer valueForKey:@"Response"];
            
            
    
    [self sendTestJsonCommand];
      }
        });
        
      
        
    }];
    
    
    [dataTask resume];
    
    
    
    
    
}

 


-(void)sendTestJsonCommand {
    
    
    NSMutableDictionary * Tokendict = [[NSMutableDictionary alloc] init];
    
    [Tokendict setValue:@"SDK_TOKEN" forKey:@"service_command"];
    [Tokendict setValue:[payFort getUDID] forKey:@"device_id"];
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        [Tokendict setValue:@"ar" forKey:@"language"];
    } else {
        [Tokendict setValue:@"en" forKey:@"language"];
    }
    
    [Tokendict setValue:[paymentGatewayDetailsDict valueForKey:@"access_code"] forKey:@"access_code"];
    [Tokendict	setValue:[paymentGatewayDetailsDict valueForKey:@"merchant_identifier"] forKey:@"merchant_identifier"];
    [Tokendict	setValue:signature forKey:@"signature"];
    
    NSError * error;
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:Tokendict options:NSJSONWritingPrettyPrinted error:&error];
    
    if (error) {
        NSLog(@"Error (%@), error: %@", Tokendict, error);
        return;
    }
    
    NSLog(@"now sending this dictionary...\n%@\n\n\n", Tokendict);
    #define appService [NSURL \
    URLWithString:@"https://paymentservices.payfort.com/FortAPI/paymentApi"]
    
    // Create request object
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:appService];
    
    // Set method, body & content-type
    request.HTTPMethod = @"POST";
    request.HTTPBody = jsonData;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setValue:
     [NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    NSString *apiKey  = @"cWZwbWdlcmZkdGV4em5kc2ZnZGdkZzpxZ2h4Y2ZnaHR5bmpta3U=";
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", apiKey];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *r, NSData *data, NSError *error) {
         
         if (!data) {
             
             NSLog(@"No data returned from server, error ocurred: %@", error);
             NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
             NSLog(@"%@",userErrorText);
             return;
         }
         
        
        
        NSError *deserr;
         responseDict = [NSJSONSerialization
                         JSONObjectWithData:data
                         options:kNilOptions
                         error:&deserr];
        
        [LoaderClass removeLoader:self.view];
        
        NSLog(@"so, here's the responseDict: %@", responseDict);
        

        
     }];
}


#pragma mark - Navigation


- (void)sdkResult:(id)response{
    
    test = 20;
    
    if([response isKindOfClass:[NSDictionary class]]){
        NSDictionary * responseDic = response;
        
        SDKresponsedict = responseDic;
        
        NSLog(@"response SDK..%@",response);
        
        [self returnUrl];
        
    } else if (response !=	nil	&&	![response	isEqualToString:@""] &&	![response	isEqualToString:@"nil"])	{
        ///	Invalid	Request	Error	Message
    } else	{
        ///	Unknown	Error	Including	Connectivity	Issues
    }
}




-(void)Sadadservices{
    
    NSString * db = [MCLocalization stringForKey:@"DBvalue"];
    
    
    NSString*username=[[[[profileDict valueForKey:@"user_name"] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    
    NSString*mobile = [profileDict valueForKey:@"user_mob"];
    
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString * ipAddress =  [ipaddressdefault stringForKey:@"ipaddressdefault"];
    
    
    
    
    NSUserDefaults * Couponcode = [NSUserDefaults standardUserDefaults];
    NSString*cpn;
    
    if ([cpn length] <1) {
        
        cpn=@"Nil";
    } else{
        
        cpn=    [Couponcode valueForKey:@"Couponecodenum"];
    }
    
    
 
    
    
    ///
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/addMoney/addMoneyToWalletDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    

    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    //     NSString * userPwd=@"585858";
    
    NSLog(@"%@",userId);
    NSLog(@"%@",userPwd);
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    

    
    
    
    NSDictionary * OpDict =  @{
                               @"db":db,
                               @"username":username,
                               @"userMob":mobile,
                               @"paymentMethod":@"SADAD",
                               @"ipAddress":ipAddress,
                               @"login_status":@"iphone_SA",
                               @"order_description":@"AddMoney",
                               @"user_emailId":[profileDict valueForKey:@"user_email_id"],
                               @"sadad_username":_txtEnterSadadId.text,
                               @"couponCode":cpn,
                               @"access_token":[accessTokenDefaults valueForKey:@"accessTokenDefaults"],
                               @"unique_id":uniqueid
                               
                               };
    
    
    NSLog(@"Posting loginDetailsDict is %@",OpDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:OpDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
            NSString*  str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Killer Response is%@",Killer);
            
            NSLog(@"str Response is%@",str);
            
            
            
            [LoaderClass removeLoader:self.view];
            
            if ([[[Killer valueForKey:@"addMoneytowalletStatus"] valueForKey:@"response_message"]isEqualToString:@"TokenExpired"]) {
                methodname=@"Sadadservices";
                
                
                
                
                [self checkTokenStatusWebServices];
            }else if ([[[Killer valueForKey:@"addMoneytowalletStatus"] valueForKey:@"response_message"]isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];

                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
                
                
            }else{
            
          
                NSString*URl=[Killer valueForKey:@"response_message"];
                
                NSString*urlstring=[Killer valueForKey:@"transaction_url"];
                
                NSLog(@"URL is %@",URl);
                
                
              
                NSUserDefaults * UrlDefaults = [NSUserDefaults standardUserDefaults];
                [UrlDefaults setObject:urlstring forKey:@"url"];
                
                
                SadadViewController * rechargesuccess = [self.storyboard instantiateViewControllerWithIdentifier:@"SadadViewController"];
                
                [self.navigationController pushViewController:rechargesuccess animated:YES];
                
            
            }
        });
            
    }];
    
    [dataTask resume];
    
}




- (IBAction)btnProceedToPay:(id)sender {
    
    
    NSUserDefaults * Paymentmethod = [NSUserDefaults standardUserDefaults];
    
    
    if ([[Paymentmethod stringForKey:@"Paymentmethod"] isEqualToString:[MCLocalization stringForKey:@"sadad"]]) {
        
        if ([_txtEnterSadadId.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
            
            toastMsg = [MCLocalization stringForKey:@"plsentSadadId"];
            [self toastMessagemethod];
            
        } else if (_txtEnterSadadId.text.length < 6 || _txtEnterSadadId.text.length > 12){
            
            toastMsg = [MCLocalization stringForKey:@"sadad6-12char"];
            [self toastMessagemethod];
            
        }  else {
            
            [self Sadadservices];
        }
        
    } else {
        
        
        
        
        payFort.delegate = self;
        //if	you	need	to	switch	on	the	Payfort	Response	page
        payFort.IsShowResponsePage = YES;
        //if	you	need	to	set	custome	Payfort	view
        [payFort		setPayFortCustomViewNib:@"PayFortView2"];
        
        //Generate	the	request	dictionary	as	follow
        NSMutableDictionary	* requestDictionary = [[NSMutableDictionary alloc]init];
        
        
        NSString*amount=[NSString stringWithFormat:@"%@",[paymentGatewayDetailsDict valueForKey:@"amount"]];
        
        
        [requestDictionary setValue:amount forKey:@"amount"];
        
        
        [requestDictionary setValue:[paymentGatewayDetailsDict valueForKey:@"command"] forKey:@"command"];
        [requestDictionary setValue:@"SAR" forKey:@"currency"];
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            [requestDictionary setValue:@"ar" forKey:@"language"];
        } else {
            [requestDictionary setValue:@"en" forKey:@"language"];
        }
        
        [requestDictionary setValue:[responseDict valueForKey:@"sdk_token"] forKey:@"sdk_token"];
        [requestDictionary setValue:[paymentGatewayDetailsDict valueForKey:@"orderId"] forKey:@"merchant_reference"];
        
        NSString * email = [profileDict valueForKey:@"user_email_id"];
        
        [requestDictionary setValue:email forKey:@"customer_email"];
        
        [requestDictionary setValue:[paymentGatewayDetailsDict  valueForKey:@"order_description"]  forKey:@"order_description"];
        
        [requestDictionary	setValue:[paymentGatewayDetailsDict valueForKey:@"token_name"] forKey:@"token_name"];
        
        NSLog(@"Request dictionary is%@",requestDictionary);
        
        test= 10;
        
        
        //	Mandatory
        [payFort setPayFortRequest:requestDictionary];
        //make	the	call
        [payFort callPayFort:self];
        
        [self.navigationItem setHidesBackButton:YES animated:YES];
        
    }
    
}

-(void)returnUrl {
    
    [LoaderClass showLoader:self.view];

    NSString*merchant_reference;
                                 
    
    if ([[SDKresponsedict valueForKey:@"merchant_reference"] length]>0) {
merchant_reference=[SDKresponsedict valueForKey:@"merchant_reference"];
    }
    else{
    
        merchant_reference=@"Nil" ;
    }


    
    NSString*customer_email;
    
    if ([[SDKresponsedict valueForKey:@"customer_email"] length]>0) {
        customer_email    =[SDKresponsedict valueForKey:@"customer_email"];

    }
    else{
        
customer_email=@"Nil";
    }


    
    NSString* userName = [[[[profileDict valueForKey:@"user_name"] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    
    
    NSString*amount;
    
 

    if ([[SDKresponsedict valueForKey:@"amount"] length]>0) {
        
        amount=[SDKresponsedict valueForKey:@"amount"];
    }  else{
        
amount=@"Nil";
    }
  

    
    NSString*authorization_code;
    
    
    if ([[SDKresponsedict valueForKey:@"authorization_code"] length]>0) {
        
        authorization_code=[SDKresponsedict valueForKey:@"authorization_code"];
    }  else{
        
authorization_code=@"Nil";
    }
    
 

    NSString*card_number;
    
    
    if ([[SDKresponsedict valueForKey:@"card_number"] length]>0) {
        
    card_number=[SDKresponsedict valueForKey:@"card_number"];
    } else{
        
      card_number=@"Nil";
    }


    
    NSString*command;
    
    
    if ([[SDKresponsedict valueForKey:@"command"] length]>0) {
        command=[SDKresponsedict valueForKey:@"command"];
        
    } else{
        
command=@"Nil";
    }
    
    

    NSString*currency;
    
    
    
    if ([[SDKresponsedict valueForKey:@"currency"] length]>0) {
        
        currency=[SDKresponsedict valueForKey:@"currency"];
    } else{
        
currency=@"Nil";
    }
    


    NSString*customer_ip;
    
    
    if ([[SDKresponsedict valueForKey:@"customer_ip"] length]>0) {
        
        customer_ip=[SDKresponsedict valueForKey:@"customer_ip"];
    } else{
        
        customer_ip=@"Nil" ;
    }
    
    NSString*customer_name;
    
    if ([[SDKresponsedict valueForKey:@"customer_name"] length]>1) {
        
        customer_name=[[SDKresponsedict valueForKey:@"customer_name"]
                        stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    } else{
    
        customer_name=@"NA";

    }
    
    
    
    
    NSString*eci;
    
    if ([[SDKresponsedict valueForKey:@"eci"] length]>0) {
        eci=[SDKresponsedict valueForKey:@"eci"];
        
    } else{
        
eci=@"Null";
    }
    
   
    NSString*expiry_date;
   
    
    if ([[SDKresponsedict valueForKey:@"expiry_date"] length]>0) {
        expiry_date =[SDKresponsedict valueForKey:@"expiry_date"];
        
    }  else{
        expiry_date=@"Nil"
        ;
    }
    
    
    NSString*fort_id;
    
    if ([[SDKresponsedict valueForKey:@"fort_id"] length]>0) {
        fort_id    =[SDKresponsedict valueForKey:@"fort_id"];

        
    } else{
        
fort_id=@"Nil";
    }
    
    
   

    
    NSString*language;
    
    
    
    if ([[SDKresponsedict valueForKey:@"language"] length]>0) {
        
        language=[[SDKresponsedict valueForKey:@"language"]stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }  else{
        
language=@"Nil";
    }
    
    NSString*order_description;
    
    
    if ([[SDKresponsedict valueForKey:@"order_description"] length]>0) {
        
        
        order_description=[SDKresponsedict valueForKey:@"order_description"];

    }
    else{
        
order_description=@"Nil";
    }
    
    


    NSString*response_code;
    
    
    if ([[SDKresponsedict valueForKey:@"response_code"] length]>0) {
        
        response_code    =[SDKresponsedict valueForKey:@"response_code"];
        
        
    }
    else{
        
        response_code=@"Nil";
    }
    
    

    
    NSString*response_message;
   
                              
    
    if ([[SDKresponsedict valueForKey:@"response_message"] length]>0) {
        
        response_message =[[SDKresponsedict valueForKey:@"response_message"]stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
        
        
        
    }
    else{
        
response_message=@"Nil";
    }
    
    
    NSString*sdk_token;
    
    if ([[SDKresponsedict valueForKey:@"sdk_token"] length]>0) {
        sdk_token    =[SDKresponsedict valueForKey:@"sdk_token"];
  
    }
    else{
        
sdk_token=@"Nil";
    }
    
    NSString*status;
    
    if ([[SDKresponsedict valueForKey:@"status"] length]>0) {
        
        status    =[SDKresponsedict valueForKey:@"status"];

        
    }
    else{
        
status=@"Nil";
    }


    NSString*token_name;
    
    
    if ([[SDKresponsedict valueForKey:@"token_name"] length]>0) {
        
        token_name=[SDKresponsedict valueForKey:@"token_name"];
        
    }
    else{
        
token_name=@"Nil";
    }

    
    
    
    NSUserDefaults * Couponcode = [NSUserDefaults standardUserDefaults];
    
    
    NSString*cpn;
    
    
    if ([[Couponcode valueForKey:@"Couponecodenum"] length] <1) {
        
cpn=@"Nil";
        
    } else{
        cpn    =    [Couponcode valueForKey:@"Couponecodenum"];

    }

    
    ///
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/addMoney/addMoneyToWalletTransactionsDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];

       
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    

    
    NSDictionary * OpDict =  @{
                               @"order_id":merchant_reference,
                               @"customer_email":customer_email,
                               @"authorization_code":authorization_code,
                               @"card_number":card_number,
                               @"username":userName,
                               @"amount":amount,
                               @"command":command,
                               @"currency":currency,
                               @"ipAddress":customer_ip,
                               @"customer_name":customer_name,
                               @"eci":eci,
                               @"expiry_date":expiry_date,
                               @"fort_id":fort_id,
                               @"language":language,
                               @"order_description":order_description,
                               @"payment_method":@"CreditCard",
                               @"response_code":response_code,
                               @"response_message":response_message,
                               @"sdk_token":sdk_token,
                               @"status":status,
                               @"token_name":token_name,
                               @"couponCode":cpn,
                               @"from":@"iphone",
                               @"db":@"SA",
                               @"access_token":[accessTokenDefaults valueForKey:@"accessTokenDefaults"],
                               @"unique_id":uniqueid
                               
                               };
    
    
    NSLog(@"Posting loginDetailsDict is %@",OpDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:OpDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    walletTransactions = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            NSString*  str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            NSLog(@"str Response is%@",str);
            
            
            
            NSLog(@"Killer Response is%@",walletTransactions);
            
          

            
            [LoaderClass removeLoader:self.view];
    
            NSDictionary*Killer=[walletTransactions valueForKey:@"walletTransactions"];
            
            
            if ([[Killer valueForKey:@"response_message"]isEqualToString:@"TokenExpired"]) {
                
                methodname=@"returnUrl";
                
                [self checkTokenStatusWebServices];
            }else if ([[Killer valueForKey:@"response_message"]isEqualToString:@"userUnauthorized"]){
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];

                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
                
                
            }else{
                
            
        
        NSLog(@"return Url Response is :%@",Killer);
        
        returnUrlDetails = Killer;
      
        NSUserDefaults *SuccessResponse = [NSUserDefaults standardUserDefaults];
        NSData * CardTrData = [NSKeyedArchiver archivedDataWithRootObject:Killer];
        [SuccessResponse setObject:CardTrData forKey:@"SuccessResponse"];
       
        WalletSuccessVC * success = [self.storyboard instantiateViewControllerWithIdentifier:@"WalletSuccessVC"];
        [self.navigationController pushViewController:success  animated:YES];
     
            }

        });
        
        
        
    }];
    
    
    [dataTask resume];
    
    
    
    
    
}

@end


