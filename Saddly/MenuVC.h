//
//  MenuVC.h
//  Saddly
//
//  Created by Sai krishna on 1/19/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "HomeVC.h"
#import "MyProfileVC.h"
#import "LoginViewController.h"
#import "FreeSMSVC.h"
#import "OperatorsVC.h"
#import "WalletVC.h"
#import "PayOrSendVC.h"
#import "ChooseLanguage.h"
#import "ContactUSDetailVC.h"
#import "AboutUsVC.h"
#import "DefaultFont.h"
#import "HelpVC.h"
#import "WriteUsVC.h"
#import "ShareViewController.h"

@interface MenuVC : UIViewController <NSURLSessionDelegate> {
    
    UIAlertController * alert;
        
    NSString  * imgSelected, * url, * appendedUrl;
    

    NSDictionary * profileDict;
    
}


@property (strong, nonatomic) IBOutlet UIImageView *imgProfileImage;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblUserEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblMobNum;


@property (strong, nonatomic) IBOutlet UIButton *btnRechargeOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnWalletOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnprofileOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnSendMoneyOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnSendFreeSmsOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnInviteFrndsOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnContactUsOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnAbooutUsOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnSelUrLanguageOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnlogoutOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnShareWithFrndsOutlet;
@property (strong, nonatomic) IBOutlet UIButton *HelpName;




- (IBAction)btnRecharge:(id)sender;
- (IBAction)btnWallet:(id)sender;
- (IBAction)btnprofile:(id)sender;
- (IBAction)btnSendMoney:(id)sender;
- (IBAction)btnSendFreeSms:(id)sender;
- (IBAction)btnInviteFrnds:(id)sender;
- (IBAction)btnContactUs:(id)sender;
- (IBAction)btnAbooutUs:(id)sender;
- (IBAction)btnSelUrLanguage:(id)sender;
- (IBAction)btnlogout:(id)sender;
- (IBAction)btnShareWithFnds:(id)sender;
- (IBAction)Help:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *Support;

- (IBAction)Support:(id)sender;





@end
