//
//  UserContactsVC.h
//  Saddly
//
//  Created by Sai krishna on 1/19/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>

@interface UserContactsVC : UIViewController <UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate, CNContactViewControllerDelegate>{
    
    NSArray * searchedcontacts;
}


@property (nonatomic, strong) NSMutableArray * FinalContact, * Finalnumber;
@property (nonatomic, strong) NSMutableArray * finalcontacts;

@property (strong, nonatomic) NSMutableArray * groupOfContacts;
@property (strong, nonatomic) NSMutableArray * contactNameArray;
@property (strong, nonatomic) NSMutableArray * phoneNumberArray;

@property (strong, nonatomic) IBOutlet UISearchBar *searchBarContacts;
@property (strong, nonatomic) IBOutlet UITableView *tblContacts;


@end
