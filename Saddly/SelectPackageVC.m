//
//  SelectPackageVC.m
//  Saddly
//
//  Created by Sai krishna on 6/27/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "SelectPackageVC.h"

@interface SelectPackageVC ()

@end

@implementation SelectPackageVC

- (void)viewDidLoad {
    
    self.title = [MCLocalization stringForKey:@"selPackage"];
    
    _imgOperator.image =  _operImage;
    btnRcDefaults = [NSUserDefaults standardUserDefaults];
    
    _btnVoiceCallOutlet.hidden = NO;
    _btnInternetDataOutlet.hidden = NO;
    _btnSecondInternetOutlet.hidden = YES;

    
    // Showing Balance
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    _lblBalance.text =  [[NSString stringWithFormat:@"%@",[profileDict valueForKey:@"totalCash"]] stringByAppendingString:@" ﷼"];
    _lblBalance.adjustsFontSizeToFitWidth = YES;
    
    
    [self plansAvailabilityWebServices];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)viewDidAppear:(BOOL)animated {
    
   
    
    [self plansAvailabilityWebServices];
    
}



- (IBAction)btnVoiceCall:(id)sender {
    
    MobileRechargeVC * mobRc =[self.storyboard instantiateViewControllerWithIdentifier:@"MobileRechargeVC"];
   
   
    if ([[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
        type = @"voice";
    } else {
        type = @"US";
    }

    mobRc.operName = _operName;
    mobRc.operImage = _operImage;
    mobRc.operType = type;
    
    
    [self.navigationController pushViewController:mobRc animated:YES];
    
}



- (IBAction)btnInternetData:(id)sender {
    
    MobileRechargeVC * mobRc =[self.storyboard instantiateViewControllerWithIdentifier:@"MobileRechargeVC"];
    
    
    if ([[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
        type = @"data";
    } else {
        type = @"UK";
    }
    
    mobRc.operName = _operName;
    mobRc.operImage = _operImage;
    mobRc.operType = type;
    
    [self.navigationController pushViewController:mobRc animated:YES];

    
}

- (IBAction)btnInfo:(id)sender {
    
    NSString * actualCash = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"actualCash"]];
    
    NSString * bonusCash  = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"bonusCash"]];
    
    
    NSString * actualCashStr, * bonusCashStr;
    
    // Actual & bonus Cash Displaying on Alert
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ ﷼"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ ﷼"], bonusCash];
    } else {
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ SAR"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ SAR"], bonusCash];
    }
    
    alert = [UIAlertController alertControllerWithTitle:@"" message:[[actualCashStr stringByAppendingString:@"\n"] stringByAppendingString:bonusCashStr] preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"] style:UIAlertActionStyleDefault  handler:^(UIAlertAction * action) {
        
        [self presentedViewController];
    }];
    
    
    [alert addAction:okButton];
    alert.view.tintColor = [UIColor colorWithRed:0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    [self presentViewController:alert animated:YES completion:nil];
    
}



-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                
                if ([methodname isEqualToString:@"plansAvailabilityWebServices"]) {
                    
                    [self plansAvailabilityWebServices];
                    
                }
                
                

                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//



-(void)plansAvailabilityWebServices {
    
    
   
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary*   profileDict1 = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    NSString*UEmail= [profileDict1 valueForKey:@"user_email_id"];
    
    NSString*uMob=   [profileDict1 valueForKey:@"user_mob"];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    

    
    //Pass The String to server(YOU SHOULD GIVE YOUR PARAMETERS INSTEAD OF MY PARAMETERS)
        //
    [LoaderClass showLoader:self.view];
    
    
    
    // Create the URLSession on the default configuration
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/plansAvailability"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
 
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    

    NSLog(@"%@",userId);
    NSLog(@"%@",userPwd);
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    

    
    NSDictionary * loginDetailsDict =  @{@"operator":_operName,
                                         
                                         @"user_mob":uMob,
                                         @"user_mob":UEmail,
                                         @"from":@"iphone",
                                         @"ipAddress":Ipaddress,
                                         @"access_token":accessToken,
                                         @"unique_id":uniqueid
                                         
                                         };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    plansavaildict = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
             NSString *resSrt  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Killer Response is%@",plansavaildict);
            
            NSLog(@"str Response is%@",resSrt);
            
            
            NSDictionary*Killer=plansavaildict;
            
            [LoaderClass removeLoader:self.view];
            
            
    
       
    
    NSLog(@"plansAvailabilityWebServices response=%@", Killer);
            
            
            if ([[[Killer valueForKey:@"response_message"] firstObject] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"plansAvailabilityWebServices";
                
                [self checkTokenStatusWebServices];
                
                
            }else if ([[[Killer valueForKey:@"response_message"] firstObject] isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{
    NSString * dataAvaStr, * voiceAvaStr, * ukAvaStr, * usAvaStr;
    
    
    if ([[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
        
        
        NSArray * dataAvail = [[Killer valueForKey:@"data"] firstObject];
        NSArray * voiceAvail = [[Killer valueForKey:@"voice"] firstObject];
        
        
        dataAvaStr = [NSString stringWithFormat:@"%@",dataAvail];
        voiceAvaStr = [NSString stringWithFormat:@"%@",voiceAvail];
        
        NSLog(@"voiceAvaStr %@",voiceAvaStr);
        NSLog(@"dataAvaStr %@",dataAvaStr);
        
        
        if ([voiceAvaStr isEqualToString:@"Available"]) {
            _btnVoiceCallOutlet.hidden = NO;
        } else{
            _btnVoiceCallOutlet.hidden = YES;
        }
        
        
        if ([dataAvaStr isEqualToString:@"Available"]) {
            _btnInternetDataOutlet.hidden = NO;
        } else{
            _btnInternetDataOutlet.hidden = YES;
        }
        
        if ([voiceAvaStr isEqualToString:@"Not Available"] && [dataAvaStr isEqualToString:@"Available"]) {
            
            _btnVoiceCallOutlet.hidden = YES;
            _btnInternetDataOutlet.hidden = YES;
            _btnSecondInternetOutlet.hidden = NO;

        }


        [_btnVoiceCallOutlet setTitle:[MCLocalization stringForKey:@"voicePackage"] forState:UIControlStateNormal ];
        [_btnInternetDataOutlet setTitle:[MCLocalization stringForKey:@"dataPackage"] forState:UIControlStateNormal];
        [_btnSecondInternetOutlet setTitle:[MCLocalization stringForKey:@"dataPackage"] forState:UIControlStateNormal];


    } else {
        
        
        NSArray * ukAvail = [[Killer valueForKey:@"UK"] firstObject];
        NSArray * usAvail = [[Killer valueForKey:@"US"] firstObject];
        
        
        ukAvaStr = [NSString stringWithFormat:@"%@",ukAvail];
        usAvaStr = [NSString stringWithFormat:@"%@",usAvail];
        
        NSLog(@"usAvaStr %@",usAvaStr);
        NSLog(@"ukAvaStr %@",ukAvaStr);
        
        
        if ([usAvaStr isEqualToString:@"Available"]) {
            _btnVoiceCallOutlet.hidden = NO;
        } else{
            _btnVoiceCallOutlet.hidden = YES;
        }
        
        if ([ukAvaStr isEqualToString:@"Available"]) {
            _btnInternetDataOutlet.hidden = NO;
        } else{
            _btnInternetDataOutlet.hidden = YES;
        }
        
        
        
        if ([ukAvaStr isEqualToString:@"Available"] && [usAvaStr isEqualToString:@"Not Available"]) {
            
            _btnVoiceCallOutlet.hidden = YES;
            _btnInternetDataOutlet.hidden = YES;
            _btnSecondInternetOutlet.hidden = NO;
            
        }


        [_btnVoiceCallOutlet setTitle:[MCLocalization stringForKey:@"usPackage"] forState:UIControlStateNormal ];
        [_btnInternetDataOutlet setTitle:[MCLocalization stringForKey:@"ukPackage"] forState:UIControlStateNormal];
        [_btnSecondInternetOutlet setTitle:[MCLocalization stringForKey:@"ukPackage"] forState:UIControlStateNormal];

        
    }
   
    
    _btnVoiceCallOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnVoiceCallOutlet.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btnVoiceCallOutlet.titleLabel.numberOfLines = 2;
    
    _btnInternetDataOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnInternetDataOutlet.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btnInternetDataOutlet.titleLabel.numberOfLines = 2;
    
    _btnSecondInternetOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnSecondInternetOutlet.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btnSecondInternetOutlet.titleLabel.numberOfLines = 2;
    
            }
                       });
        
        
    }];
    
    
    [dataTask resume];
    
    
    
    
    
}





@end
