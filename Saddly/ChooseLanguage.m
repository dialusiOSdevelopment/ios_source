//
//  ChooseLanguage.m
//  Saddly
//
//  Created by Sai krishna on 1/24/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "ChooseLanguage.h"

@interface ChooseLanguage ()

@end

@implementation ChooseLanguage

- (void)viewDidLoad {
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
    }
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    [_btnEngOutlet setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateNormal];
    [_btnEngOutlet setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateSelected];
    
    [_btnAraboutlet setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateNormal];
    [_btnAraboutlet setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateSelected];
    
    [_btnEngOutlet setSelected:NO];
    [_btnAraboutlet setSelected:NO];
    
    
    
    // Setting Vertical line.
    UIView *borderBottom1 = [[UIView alloc] initWithFrame:CGRectMake(_lblBackEnglish.frame.origin.x - 20.0, 46.0, _lblBackEnglish.frame.size.width, 1.0)];
    borderBottom1.backgroundColor = [UIColor whiteColor];
    [_lblBackEnglish addSubview:borderBottom1];
    

    // Setting Vertical line.
    UIView *borderBottom2 = [[UIView alloc] initWithFrame:CGRectMake(_lblBackArabic.frame.origin.x -20.0, 46.0, _lblBackArabic.frame.size.width, 1.0)];
    borderBottom2.backgroundColor = [UIColor whiteColor];
    [_lblBackArabic addSubview:borderBottom2];
    
    
    
    UITapGestureRecognizer * englishGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnEnglish:)];
    englishGesture.numberOfTapsRequired = 1;
    [_lblBackEnglish setUserInteractionEnabled:YES];
    [_lblBackEnglish addGestureRecognizer:englishGesture];
    
    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * arabicGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnArabic:)];
    arabicGesture.numberOfTapsRequired = 1;
    [_lblBackArabic setUserInteractionEnabled:YES];
    [_lblBackArabic addGestureRecognizer:arabicGesture];
    

    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}



 


//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}


- (IBAction)btnEnglish:(id)sender {
    
    if([_btnEngOutlet isSelected]==YES)
    {
        [_btnEngOutlet setSelected:YES];
        [_btnAraboutlet setSelected:NO];
    }
    else{
        [_btnEngOutlet setSelected:YES];
        [_btnAraboutlet setSelected:NO];
    }
    [MCLocalization sharedInstance].language = @"en";
    [_btnEngOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
}


- (IBAction)btnArabic:(id)sender {
    
    if([_btnAraboutlet isSelected]==YES)
    {
        [_btnAraboutlet setSelected:YES];
        [_btnEngOutlet setSelected:NO];
    }
    else{
        [_btnAraboutlet setSelected:YES];
        [_btnEngOutlet setSelected:NO];
    }
    
    [MCLocalization sharedInstance].language = @"ru";
    [_btnAraboutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
   
}

- (IBAction)btnChange:(id)sender {

    if ([_btnEngOutlet isSelected] == NO && [_btnAraboutlet isSelected] == NO) {
        toastMsg = [MCLocalization stringForKey:@"plsselurlan"];
        [self toastMessagemethod];
    }
    else {
        
        [self performSegueWithIdentifier:@"homeFeomLanguage" sender:nil];
    }

}



@end
