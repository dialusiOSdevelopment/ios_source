//
//  BannersCollectionViewCell.m
//  Saddly
//
//  Created by Sai krishna on 7/12/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "BannersCollectionViewCell.h"

NSString *const kCustomCellIdentifier = @"CustomCell";


@implementation BannersCollectionViewCell




- (void)setPhotoModel:(PhotoModel *)photoModel {
    _photoModel = photoModel;
    
    
    
    _imgBanner.image = photoModel.image;
    _lblMsg1.text = photoModel.imageDescription;
    _lblMsg2.text=photoModel.imageTitle;
    _lblUseCodeOutlet.text=photoModel.imageButton;
}


@end
