//
//  RehargeSuccessVC.m
//  DialuzApp
//
//  Created by Sai krishna on 11/11/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import "RehargeSuccessVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface RehargeSuccessVC ()

@end

@implementation RehargeSuccessVC


- (void)viewDidLoad {
    
        
    //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Recharge Success Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //

    
    _TnqTxtname.adjustsFontSizeToFitWidth=YES;
    _MobileSuccesName.adjustsFontSizeToFitWidth=YES;
    _OperatorSuccessName.adjustsFontSizeToFitWidth=YES;
    _RechargeAmountSuccessName.adjustsFontSizeToFitWidth=YES;
    _txnStatusName.adjustsFontSizeToFitWidth=YES;
    _walletAmountName.adjustsFontSizeToFitWidth=YES;
    _MobilenumberSuccessTxt.adjustsFontSizeToFitWidth=YES;
    _AmountSucceessTxt.adjustsFontSizeToFitWidth=YES;
    _txnStatusSuccessTxt.adjustsFontSizeToFitWidth=YES;
    _lblWalletAmount.adjustsFontSizeToFitWidth=YES;
    _lblVoucherCode.adjustsFontSizeToFitWidth = YES;
    _lblVoucherCodeName.adjustsFontSizeToFitWidth = YES;
    _lblIqmaNumberName.adjustsFontSizeToFitWidth = YES;
    _lblUSSDCode.adjustsFontSizeToFitWidth = YES;
    

    
    _btnVoucherCodeCopyOutlet.hidden = NO;
    _lblUSSDCode.hidden = YES;
    _btnCopyOutlet.hidden = YES;

   

    
    _txtViewContact.hidden = NO;
    _txtviewFailureRespMsgs.hidden = NO;
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
    }
    
    
    NSUserDefaults *SuccessResponsecard = [NSUserDefaults standardUserDefaults];
    SuccessResponseDict = [NSKeyedUnarchiver unarchiveObjectWithData:[SuccessResponsecard objectForKey:@"SuccessResponsecard"]];
    
    
    NSUserDefaults *WalletSuccessResponse = [NSUserDefaults standardUserDefaults];
    WalletSuccessDict=[NSKeyedUnarchiver unarchiveObjectWithData:[WalletSuccessResponse objectForKey:@"WalletSuccessResponse"]];
    
    
    NSLog(@"WalletSuccessDict: %@",WalletSuccessDict);
    NSLog(@"SuccessResponseDict: %@",SuccessResponseDict);
    
    
    
    NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
        
        self.title = [MCLocalization stringForKey:@"RcStatus"];
        _OperatorSuccessName.text=[MCLocalization stringForKey:@"Operator"] ;

        
    } else {
        
        self.title = [MCLocalization stringForKey:@"purchaseStatus"];
        _OperatorSuccessName.text=[MCLocalization stringForKey:@"store"] ;
        
    }
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    
    NSUserDefaults * couponcodenum = [NSUserDefaults standardUserDefaults];
    [couponcodenum removeObjectForKey:@"couponcodenum"];
    
    
    _TnqTxtname.text=[MCLocalization stringForKey:@"Tnxtxt"] ;
    _MobileSuccesName.text=[MCLocalization stringForKey:@"MobileNumberCard"] ;
    _RechargeAmountSuccessName.text=[MCLocalization stringForKey:@"RechargeAmountSuccess"] ;
    _txnStatusName.text = [MCLocalization stringForKey:@"txnStatus"];
    _walletAmountName.text = [MCLocalization stringForKey:@"WalletAmount"];
    _lblVoucherCodeName.text = [MCLocalization stringForKey:@"vouchCode"];
    _lblIqmaNumberName.text = [MCLocalization stringForKey:@"iqmaNum"];
    
    
    [_DoneSuccessName setTitle:[MCLocalization stringForKey:@"done"] forState:UIControlStateNormal];
    [_btnClickToRcOutlet setTitle:[MCLocalization stringForKey:@"clicktoRc"] forState:UIControlStateNormal];

    
    NSUserDefaults * rcOperator = [NSUserDefaults standardUserDefaults];
    NSString*operator=[NSString stringWithFormat:@"%@",[rcOperator stringForKey:@"rcOperator"]];
    _OperatorSuccessTxt = operator;
    
    NSUInteger num = [SuccessResponseDict count];
    
    NSLog(@"SuccessResponseDict count is %lu",num);
    
    
    
    if (num > 0) {
        
        // From wallet User Defaults
        _MobilenumberSuccessTxt.text = [NSString stringWithFormat:@"%@",[SuccessResponseDict valueForKey:@"rechargeMob"]];

        _AmountSucceessTxt.text = [[NSString stringWithFormat:@"%@", [SuccessResponseDict valueForKey:@"rechargeAmount"]] stringByAppendingString:@" ﷼"];
        
        if ([[SuccessResponseDict valueForKey:@"response_message"] isEqual:[NSNull null]]) {

            _txnStatusSuccessTxt.text = [MCLocalization stringForKey:@"failure"];
            
            _imgSuccessFailOutlet.image = [UIImage imageNamed:@"cancel.png"];
            
            _lblVoucherCode.text = @"XXXX XX XXXX";
            

            
        } else {
            
            _imgSuccessFailOutlet.image = [UIImage imageNamed:@"success.png"];

            _txnStatusSuccessTxt.text = [NSString stringWithFormat:@"%@",[SuccessResponseDict  valueForKey:@"response_message"]];

        }
        
        
        _lblWalletAmount.text = [NSString stringWithFormat:@"%@", [SuccessResponseDict  valueForKey:@"walletAmount"]];
        
        _lblVoucherCode.text =  [NSString stringWithFormat:@"%@", [SuccessResponseDict  valueForKey:@"rechargevoucherCode"]];
        
        _txtEnterIqamaNumber = [NSString stringWithFormat:@"%@", [SuccessResponseDict valueForKey:@"iqmaNumber"]];
        
         _txtViewRefNum = [NSString stringWithFormat:[[[MCLocalization stringForKey:@"inCaseRefNo"] stringByAppendingString:@" %@ "] stringByAppendingString:[MCLocalization stringForKey:@"whileContactingDialus"]], [SuccessResponseDict  valueForKey:@"orderId"]];
        
        _txtViewContact.text = [[[MCLocalization stringForKey:@"dialusContactInfo"] stringByAppendingString:@"\n"] stringByAppendingString:_txtViewRefNum];
        
        _txtviewFailureRespMsgs.text = [[[MCLocalization stringForKey:@"dialusContactInfo"] stringByAppendingString:@"\n"] stringByAppendingString:_txtViewRefNum];

        NSLog(@"_txtviewFailureRespMsgs.text %@",_txtviewFailureRespMsgs.text);
        NSLog(@"_txtViewContact %@",_txtViewContact.text);
        
        NSString*msg=[SuccessResponseDict valueForKey:@"response_message"];
        
        if ([msg isEqual:[NSNull null]]) {
            msg=[MCLocalization stringForKey:@"failure"];
        }
        
        if (![msg isEqualToString:@"Success"]||[msg isEqualToString:@"success"]) {
            
            _txnStatusSuccessTxt.text = [MCLocalization stringForKey:@"failure"];
            _txnStatusSuccessTxt.textColor = [UIColor redColor];
            
            _btnClickToRcOutlet.hidden = YES;
            
            _txtViewContact.hidden = YES;
            _txtviewFailureRespMsgs.hidden = NO;
            
            _btnVoucherCodeCopyOutlet.hidden = YES;
            

            
            _imgSuccessFailOutlet.image = [UIImage imageNamed:@"cancel.png"];

            _lblVoucherCode.text = @"XXXX XX XXXX";
            

        } else {
            
            _txnStatusSuccessTxt.text = [MCLocalization stringForKey:@"success"];
            _txnStatusSuccessTxt.textColor = [UIColor colorWithRed:0.0/255.0 green:128.0/255.0 blue:64.0/255.0 alpha:1.0];
            
 
            
            _imgSuccessFailOutlet.image = [UIImage imageNamed:@"success.png"];

            
            
            if ( [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"200"]) {
                
                _btnClickToRcOutlet.hidden = YES;
                _lblUSSDCode.hidden = YES;
                _btnCopyOutlet.hidden = YES;
                
                _txtviewFailureRespMsgs.hidden = NO;
                
                _txtViewContact.hidden = YES;
                
            } else {
                
            _btnClickToRcOutlet.hidden = NO;
          
            
            
            _txtViewContact.hidden = NO;
            _txtviewFailureRespMsgs.hidden = YES;
            
            _btnVoucherCodeCopyOutlet.hidden = NO;

            }

        }
        
      
        
    } else {
        
        // From Card User Defaults
        _MobilenumberSuccessTxt.text = [NSString stringWithFormat:@"%@",[WalletSuccessDict  valueForKey:@"rechargeMob"]];

        _txtEnterIqamaNumber =  [NSString stringWithFormat:@"%@", [WalletSuccessDict valueForKey:@"iqmaNumber"]];

        _AmountSucceessTxt.text = [[NSString stringWithFormat:@"%@", [WalletSuccessDict valueForKey:@"rechargeAmount"]] stringByAppendingString:@" ﷼"];
        
        
        if ([[WalletSuccessDict valueForKey:@"response_message"] isEqual:[NSNull null]]) {
            
            _txnStatusSuccessTxt.text = [MCLocalization stringForKey:@"failure"];
            _imgSuccessFailOutlet.image = [UIImage imageNamed:@"cancel.png"];
            
            _lblVoucherCode.text = @"XXXX XX XXXX";
            

        } else {
            
            _txnStatusSuccessTxt.text = [NSString stringWithFormat:@"%@",[WalletSuccessDict valueForKey:@"response_message"]];
            _imgSuccessFailOutlet.image = [UIImage imageNamed:@"success.png"];
            
        }
        
        _lblWalletAmount.text = [NSString stringWithFormat:@"%@", [WalletSuccessDict  valueForKey:@"walletAmount"]];
        
        _lblVoucherCode.text = [NSString stringWithFormat:@"%@", [WalletSuccessDict  valueForKey:@"rechargevoucherCode"]];
        
        _txtViewRefNum= [NSString stringWithFormat:[[[MCLocalization stringForKey:@"inCaseRefNo"] stringByAppendingString:@" %@ "] stringByAppendingString:[MCLocalization stringForKey:@"whileContactingDialus"]], [WalletSuccessDict valueForKey:@"orderId"]];
        
        _txtViewContact.text = [[[MCLocalization stringForKey:@"dialusContactInfo"] stringByAppendingString:@"\n"] stringByAppendingString:_txtViewRefNum];
        
        _txtviewFailureRespMsgs.text = [[[MCLocalization stringForKey:@"dialusContactInfo"] stringByAppendingString:@"\n"] stringByAppendingString:_txtViewRefNum];

        NSLog(@"_txtviewFailureRespMsgs.text %@",_txtviewFailureRespMsgs.text);
        NSLog(@"_txtViewContact %@",_txtViewContact.text);

        
        NSString*msg=[WalletSuccessDict valueForKey:@"response_message"];
        
        if ([msg isEqual:[NSNull null]]) {
            msg=[MCLocalization stringForKey:@"failure"];
        }
        
        if (![msg isEqualToString:@"Success"]||[msg isEqualToString:@"success"]) {
            
            _txnStatusSuccessTxt.text = [MCLocalization stringForKey:@"failure"];
            _txnStatusSuccessTxt.textColor = [UIColor redColor];
            
            _btnClickToRcOutlet.hidden = YES;
          


            
            _txtViewContact.hidden = YES;
            _txtviewFailureRespMsgs.hidden = NO;
            
            _btnVoucherCodeCopyOutlet.hidden = YES;

            _imgSuccessFailOutlet.image = [UIImage imageNamed:@"cancel.png"];
            
            _lblVoucherCode.text = @"XXXX XX XXXX";
            

         
        } else {
            
            _txnStatusSuccessTxt.text = [MCLocalization stringForKey:@"success"];
            _txnStatusSuccessTxt.textColor = [UIColor colorWithRed:0.0/255.0 green:128.0/255.0 blue:64.0/255.0 alpha:1.0];
            
 
            
            _imgSuccessFailOutlet.image = [UIImage imageNamed:@"success.png"];

            
            if ( [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"200"]) {
                
                _btnClickToRcOutlet.hidden = YES;
                _lblUSSDCode.hidden = YES;
                _btnCopyOutlet.hidden = YES;
                
                _txtviewFailureRespMsgs.hidden = NO;
                
                _txtViewContact.hidden = YES;
                
            } else {
            
           
                _btnClickToRcOutlet.hidden = NO;
                
                
                _txtViewContact.hidden = NO;
                _txtviewFailureRespMsgs.hidden = YES;
                
                _btnVoucherCodeCopyOutlet.hidden = NO;

            }


        }
        
    }// total if-else
    
    _txnStatusSuccessTxt.lineBreakMode = NSLineBreakByWordWrapping;
    _txnStatusSuccessTxt.numberOfLines = 2;
    
    if ([_OperatorSuccessTxt  isEqualToString:@"Jawwy"] || [_OperatorSuccessTxt  isEqualToString:@"Lebara"]) {
        
        _btnClickToRcOutlet.hidden = YES;
        _lblUSSDCode.hidden = YES;
        _btnCopyOutlet.hidden = YES;
        
        _txtviewFailureRespMsgs.hidden = NO;
        _lblIqmaNumberName.hidden=YES;
        
        _txtViewContact.hidden=YES;
        
    }
    else if ([_OperatorSuccessTxt  isEqualToString:@"Mobily"]) {
        
        if ([[SuccessResponseDict valueForKey:@"planType"]isEqualToString:@"voice"]||[[WalletSuccessDict valueForKey:@"planType"]isEqualToString:@"voice"]) {
            
            voucherCode = [[[[@"*1400*" stringByAppendingString:_lblVoucherCode.text]
                             stringByAppendingString:@"*"] stringByAppendingString: _txtEnterIqamaNumber] stringByAppendingString:@"#"];
            
            
            _btnClickToRcOutlet.hidden = NO;
            _lblUSSDCode.hidden = YES;
            _btnCopyOutlet.hidden = YES;
            
            _txtviewFailureRespMsgs.hidden = YES;
            
            _lblIqmaNumberName.hidden=NO;
            
            _txtViewContact.hidden=NO;
            
            
        } else{
            
            _btnClickToRcOutlet.hidden = YES;
            _lblUSSDCode.hidden = YES;
            _btnCopyOutlet.hidden = YES;
            
            _txtviewFailureRespMsgs.hidden = NO;
            
            _lblIqmaNumberName.hidden=YES;
            
            _txtViewContact.hidden=YES;
            
        }
        
    }

    NSString * op = _OperatorSuccessTxt;
    
    if ([op isEqualToString:@"Zain"]) {
        _imgOperator.image = [UIImage imageNamed:@"zain_300.png"];
//        _viewTxns.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"zainBG.png"]];
        
    } else if ([op isEqualToString:@"Friendi"]) {
        _imgOperator.image = [UIImage imageNamed:@"friendi_300.png"];
        _viewTxns.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"skyblue-bg.png"]];
        
    } else if ([op isEqualToString:@"Jawwy"]) {
        _imgOperator.image = [UIImage imageNamed:@"jawwy_300.png"];
//        _viewTxns.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"jawwy-BG.png"]];
        
    } else if ([op isEqualToString:@"Mobily"]) {
        _imgOperator.image = [UIImage imageNamed:@"mobily_300.png"];
//        _viewTxns.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"mobily-BG.png"]];
        
    } else if ([op isEqualToString:@"STC"]) {
        _imgOperator.image = [UIImage imageNamed:@"STC_round.png"];
//        _viewTxns.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sawa-BG.png"]];
        
    } else if ([op isEqualToString:@"Virgin"]) {
        _imgOperator.image = [UIImage imageNamed:@"virgin_300.png"];
//        _viewTxns.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"virigin-BG.png"]];
        
    } else if ([op isEqualToString:@"Sawa"]) {
        _imgOperator.image = [UIImage imageNamed:@"sawa_300.png"];
//        _viewTxns.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sawa-BG.png"]];
        
    } else if ([op isEqualToString:@"Quicknet"]) {
        _imgOperator.image = [UIImage imageNamed:@"quicknetr_300.png"];
//        _viewTxns.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"sawa-BG.png"]];
        
    } else if ([op isEqualToString:@"Lebara"]) {
        _imgOperator.image = [UIImage imageNamed:@"lebra_300.png"];
//        _viewTxns.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"lebara-BG.jpg"]];
        
    } else    if ([op isEqualToString:@"iTunes"]) {
        _imgOperator.image = [UIImage imageNamed:@"iTunes_70.png"];
        _viewTxns.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"pink-bg.png"]];
        
    } else if ([op isEqualToString:@"Google+Play"]) {
        _imgOperator.image = [UIImage imageNamed:@"Google_Play_70.png"];
        _viewTxns.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ash-bg.png"]];
        
    } else if ([op isEqualToString:@"Xbox"]) {
        _imgOperator.image = [UIImage imageNamed:@"Xbox_70.png"];
        _viewTxns.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ash-bg.png"]];
        
    } else if ([op isEqualToString:@"Playstation"]) {
        _imgOperator.image = [UIImage imageNamed:@"Playstation_70.png"];
        _viewTxns.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"green-bg.png"]];
        
    } else {
        
        _imgOperator.image = [UIImage imageNamed:@"70.png"];
        _viewTxns.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"skyblue-bg.png"]];
        
    }
    
    _viewTxns.layer.cornerRadius = 10;
    _viewTxns.contentMode = UIViewContentModeScaleToFill;

   
    
    _lblIqmaNumberName.hidden = YES;
    _lblUSSDCode.hidden = YES;
    _btnCopyOutlet.hidden = YES;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    
    _TnqTxtname.adjustsFontSizeToFitWidth=YES;
    _MobileSuccesName.adjustsFontSizeToFitWidth=YES;
    _OperatorSuccessName.adjustsFontSizeToFitWidth=YES;
    _RechargeAmountSuccessName.adjustsFontSizeToFitWidth=YES;
    _txnStatusName.adjustsFontSizeToFitWidth=YES;
    _walletAmountName.adjustsFontSizeToFitWidth=YES;
    _MobilenumberSuccessTxt.adjustsFontSizeToFitWidth=YES;
    _AmountSucceessTxt.adjustsFontSizeToFitWidth=YES;
    _txnStatusSuccessTxt.adjustsFontSizeToFitWidth=YES;
    _lblWalletAmount.adjustsFontSizeToFitWidth=YES;

    
    _txnStatusSuccessTxt.lineBreakMode = NSLineBreakByWordWrapping;
    _txnStatusSuccessTxt.numberOfLines = 2;
    
}

// Removing keyboard
-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}
//
//// Returning keyboard
//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    if (textField == self.txtEnterIqamaNumber) {
//        [self.txtEnterIqamaNumber resignFirstResponder];
//    }
//    return YES;
//}


-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
        [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}




// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}





// Entering only alphaNumeric not a Arabic Characters
-(BOOL)isAlphaNumericSpecialCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}



/*
 * Called when a purchase is processed and verified.
 */
- (void)onPurchaseCompleted {
    
    // Assumes a tracker has already been initialized with a property ID, otherwise
    // this call returns null.
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    
    NSUInteger num = [SuccessResponseDict count];
    
    NSLog(@"number ids %lu",num);
    
    
    NSString * tracTxnId, * tracTxnResMsg;
    NSNumber * tracAmount;
    
    if (num > 0) {
    
        tracTxnId = [NSString stringWithFormat:@"%@",[SuccessResponseDict  valueForKey:@"orderId"]];
        
        tracTxnResMsg = [NSString stringWithFormat:@"%@",[SuccessResponseDict  valueForKey:@"response_message"]];
        tracAmount = [SuccessResponseDict valueForKey:@"rechargeAmount"];

        
    } else {
        
        
        tracTxnId = [NSString stringWithFormat:@"%@", [WalletSuccessDict valueForKey:@"orderId"]];
        
        tracTxnResMsg = [NSString stringWithFormat:@"%@",[WalletSuccessDict valueForKey:@"response_message"]];

        tracAmount = [WalletSuccessDict valueForKey:@"rechargeAmount"];


    }
    
    
    [tracker send:[[GAIDictionaryBuilder createTransactionWithId:@"0_123456"             // (NSString) Transaction ID
                                                     affiliation:@"In-app Store"         // (NSString) Affiliation
                                                         revenue:@2.16F                  // (NSNumber) Order revenue (including tax and shipping)
                                                             tax:@0.17F                  // (NSNumber) Tax
                                                        shipping:@0                      // (NSNumber) Shipping
                                                    currencyCode:@"USD"] build]];        // (NSString) Currency code
    
    
    [tracker send:[[GAIDictionaryBuilder createItemWithTransactionId:tracTxnId         // (NSString) Transaction ID
                                                                name:@"Saddly Recharge"  // (NSString) Product Name
                                                                 sku:tracTxnResMsg            // (NSString) Product SKU
                                                            category:_OperatorSuccessTxt  // (NSString) Product category
                                                               price:tracAmount               // (NSNumber) Product price
                                                            quantity:@1                  // (NSInteger) Product quantity
                                                        currencyCode:@"SAR"] build]];    // (NSString) Currency code
    
    
}




- (BOOL)isValidMobileNumber:(NSString*)number
{
    number=[number stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([number length] != 10 ) {
        
        return FALSE;
    }
    NSString *Regex = @"^([0-9]*)$";
    NSPredicate *mobileTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex];
    
    return [mobileTest evaluateWithObject:number];
}




- (IBAction)btnClickToRc:(id)sender {
    
    QuickRechargeVC * quickRc = [self.storyboard instantiateViewControllerWithIdentifier:@"QuickRechargeVC"];
    
    quickRc.voucherCodeFrmSuccess = _lblVoucherCode.text;
    quickRc.iqmaNumberFrmSuccess =  _txtEnterIqamaNumber;
    quickRc.operatorFrmSuccess = _OperatorSuccessTxt;
    
    [self.navigationController pushViewController:quickRc animated:YES];
    
}

- (IBAction)btnCopy:(id)sender {
    
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _lblUSSDCode.text;
    
    toastMsg  = [MCLocalization stringForKey:@"ussdCopied"];
    [self toastMessagemethod];
    
}

- (IBAction)btnVoucherCodeCopy:(id)sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _lblVoucherCode.text;
    
    toastMsg  = [MCLocalization stringForKey:@"voucherCodeCopy"];
    [self toastMessagemethod];
    
}




- (IBAction)DoneButtonAction:(id)sender {

    
    [self onPurchaseCompleted];
    
    NSUserDefaults *SuccessResponse = [NSUserDefaults standardUserDefaults];
    [SuccessResponse removeObjectForKey:@"SuccessResponse"];
    
    NSUserDefaults *SuccessResponsecard = [NSUserDefaults standardUserDefaults];
    [SuccessResponsecard removeObjectForKey:@"SuccessResponsecard"];
    
    NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
    [selectedOperator removeObjectForKey:@"selectedOperator"];
    
    NSUserDefaults * recentRcOperatorsDefaults = [NSUserDefaults standardUserDefaults];
    [recentRcOperatorsDefaults removeObjectForKey:@"recentRcOperatorsDefaults"];
    
    
    
    [self performSegueWithIdentifier:@"HomeFromRcSuccess" sender:nil];
    
    
}




@end
