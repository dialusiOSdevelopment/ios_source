//
//  HomeVC.m
//  Saddly
//
//  Created by Sai krishna on 1/19/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "HomeVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


#define TRANSFORM_CELL_VALUE CGAffineTransformMakeScale(0.8, 0.8)
#define ANIMATION_SPEED 0.2


@interface HomeVC ()

@end



@implementation HomeVC{
    
    
}



- (void)viewDidLoad {
    
//    [self abc];
    
    
    self.title = [MCLocalization stringForKey:@"Slidearrhme"];
    _scrollView.delegate = self;
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:YES];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Home Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    
    [self localIPAddresses];
    [self profileWebServicesMethod];
    
    
    NSUserDefaults * Notification = [NSUserDefaults standardUserDefaults];
    NSDictionary * notiDict=    [Notification  valueForKey:@"NotificationDefault"];
    
    NSLog(@"Notification Dict is: %@",notiDict);
    
    NSString * abc = [[[notiDict valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"body"];
    
    NSLog(@"Notification Body: %@",abc);
    
    
    //    UIImage* needHelp = [UIImage imageNamed:@"need_help.png"];
    //    CGRect frameimg = CGRectMake(0, 0, needHelp.size.width, needHelp.size.height);
    //    UIButton * helpButton = [[UIButton alloc] initWithFrame:frameimg];
    //    [helpButton setBackgroundImage:needHelp forState:UIControlStateNormal];
    //    [helpButton addTarget:self action:@selector(HelpView)
    //            forControlEvents:UIControlEventTouchUpInside];
    //    [helpButton setShowsTouchWhenHighlighted:NO];
    
    //    UIBarButtonItem *secondButton = [[UIBarButtonItem alloc] initWithCustomView:helpButton];
    //    self.navigationItem.rightBarButtonItem = secondButton;
    
    
    
    [super viewDidLoad];
    
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:YES];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    _lblServicesName.text = [MCLocalization stringForKey:@"services"];
    _lblWelcomeBackName.text = [MCLocalization stringForKey:@"welcomeback"];
    _lblYourBalName.text = [MCLocalization stringForKey:@"urBalance"];
    _lblWelcomeBackName.adjustsFontSizeToFitWidth = YES;
    _lblYourBalName.adjustsFontSizeToFitWidth = YES;
    
    
    [_btnRcOutlet setTitle:[MCLocalization stringForKey:@"recharge"] forState:UIControlStateNormal];
    [_btnAddMoneyOutlet setTitle:[MCLocalization stringForKey:@"addmoney"] forState:UIControlStateNormal];
    [_btnGiftCardsOutlet setTitle:[MCLocalization stringForKey:@"GiftCards"] forState:UIControlStateNormal];
    [_btnTransferValueOutlet setTitle:[MCLocalization stringForKey:@"transferName"] forState:UIControlStateNormal];
    
    
    _btnRcOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnTransferValueOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnAddMoneyOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnGiftCardsOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    
    
    
    // Assigning the Names to the tab bar controller
    [[self.tabBarController.tabBar.items objectAtIndex:0] setTitle:[MCLocalization stringForKey:@"Slidearrhme"]];
    [[self.tabBarController.tabBar.items objectAtIndex:1] setTitle:[MCLocalization stringForKey:@"account"]];
    [[self.tabBarController.tabBar.items objectAtIndex:2] setTitle:[MCLocalization stringForKey:@"offers"]];
    [[self.tabBarController.tabBar.items objectAtIndex:3] setTitle:[MCLocalization stringForKey:@"shareTitle"]];
    [[self.tabBarController.tabBar.items objectAtIndex:4] setTitle:[MCLocalization stringForKey:@"notifications"]];
    
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self.collecBanners reloadData];
        
    });
    
}


- (void)viewDidAppear:(BOOL)animated {
    
    
    
    [self localIPAddresses];
    [self profileWebServicesMethod];
    
    
    
    NSUserDefaults * Notification = [NSUserDefaults standardUserDefaults];
    NSDictionary * notiDict=    [Notification  valueForKey:@"NotificationDefault"];
    
    NSLog(@"Notification Dict is: %@",notiDict);
    NSString * abc = [[[notiDict valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"body"];
    
    if (abc.length > 1) {
        [[self.tabBarController.tabBar.items objectAtIndex:4] setBadgeValue:@"1"];
    } else {
        [[self.tabBarController.tabBar.items objectAtIndex:4] setBadgeValue:nil];
    }
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self.collecBanners reloadData];
        
    });
    
    
    
}






-(void)HelpView{
    
    HelpVC * help = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpVC"];
    help.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:help animated:YES];
    
}


//local IP Addresses
- (void)localIPAddresses {
    
    NSString *publicIP = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"https://icanhazip.com/"] encoding:NSUTF8StringEncoding error:nil];
    
    Ipaddress = [publicIP stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]]; // IP comes with a newline for some reason
    
    if (Ipaddress .length <1) {
        NSString *publicIP = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"https://api.ipify.org?format=json"] encoding:NSUTF8StringEncoding error:nil];
        
        Ipaddress = [publicIP stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        
    }
    
    if (Ipaddress .length <1) {
        
        NSMutableArray *ipAddresses = [NSMutableArray array] ;
        struct ifaddrs *allInterfaces;
        
        // Get list of all interfaces on the local machine:
        if (getifaddrs(&allInterfaces) == 0) {
            struct ifaddrs *interface;
            
            // For each interface ...
            for (interface = allInterfaces; interface != NULL; interface = interface->ifa_next) {
                unsigned int flags = interface->ifa_flags;
                struct sockaddr *addr = interface->ifa_addr;
                
                // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
                if ((flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING)) {
                    if (addr->sa_family == AF_INET || addr->sa_family == AF_INET6) {
                        
                        // Convert interface address to a human readable string:
                        char host[NI_MAXHOST];
                        getnameinfo(addr, addr->sa_len, host, sizeof(host), NULL, 0, NI_NUMERICHOST);
                        
                        [ipAddresses addObject:[[NSString alloc] initWithUTF8String:host]];
                    }
                }
            }
            
            freeifaddrs(allInterfaces);
        }
        
        
        if (ipAddresses.count < 1) {
            Ipaddress = 0;
        } else {
            Ipaddress =  [ipAddresses objectAtIndex:1];
        }
        
    }
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    [ipaddressdefault setObject:Ipaddress forKey:@"ipaddressdefault"];
    
    NSLog(@"IP address %@",Ipaddress);
    
    
}





-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDetails valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
   
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"getBannerDetailsWebServices"]) {
                    
                    [self getBannerDetailsWebServices];
                    
                }
                else if  ([methodname isEqualToString:@"profileWebServicesMethod"]) {
                    
                    [self profileWebServicesMethod];
                    
                }

            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//


//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}


// Alert Method
-(void)alertMethod{
    
    alert =  [UIAlertController
              alertControllerWithTitle:alertTitle
              message:alertMessage
              preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}


- (IBAction)btnBonusCashInfo:(id)sender {
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        alertMessage = @"فقط يمكنك الحصول عليها عند الشحن بميلغ  100 ريال او اكثر";
        
    } else {
        
        alertMessage = @"Can be Redeemed only with 100SAR and above Recharges";
        
    }
    
    [self alertMethod];
    
}



- (IBAction)btnInfo:(id)sender {
    
    NSString * actualCash = [NSString stringWithFormat:@"%@",[profileDetails valueForKey:@"actualCash"]];
    
    NSString * bonusCash  = [NSString stringWithFormat:@"%@",[profileDetails valueForKey:@"bonusCash"]];
    
    
    NSString * actualCashStr, * bonusCashStr;
    
    // Actual & bonus Cash Displaying on Alert
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ ﷼"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ ﷼"], bonusCash];
    } else {
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ SAR"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ SAR"], bonusCash];
    }
    
    alert = [UIAlertController alertControllerWithTitle:@"" message:[[actualCashStr stringByAppendingString:@"\n"] stringByAppendingString:bonusCashStr] preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"] style:UIAlertActionStyleDefault  handler:^(UIAlertAction * action) {
        
        [self presentedViewController];
    }];
    
    
    [alert addAction:okButton];
    alert.view.tintColor = [UIColor colorWithRed:0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    [self presentViewController:alert animated:YES completion:nil];
    
}



// Profile Webservices Method
-(void)profileWebServicesMethod {
    
    NSString * userName;
    
    NSUserDefaults * logOTPMobNumDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
    
    if ([loginName stringForKey:@"userName"].length > 1) {
        userName = [loginName stringForKey:@"userName"];
        
    } else if ([logOTPMobNumDefaults stringForKey:@"logOTPMobNumDefaults"].length > 1) {
        userName = [logOTPMobNumDefaults stringForKey:@"logOTPMobNumDefaults"];
    }
    
    
    
    [LoaderClass showLoader:self.view];
    
    
    
    // Create the URLSession on the default configuration
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/profile/getuserprofileDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
   
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=  [uuidDefaults stringForKey:@"uuidDefaults"];
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    NSUserDefaults * userid = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * userpwd = [NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [userid valueForKey:@"userId"];
    NSString * userPwd = [userpwd valueForKey:@"userPwd"];
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    
    NSDictionary * ProfDict =  @{@"username":userName,
                                 
                                 @"from":@"iPhone",
                                 
                                 @"ipAddress":Ipaddress,
                                 @"accessToken":accessToken,
                                 @"uniqueid":uniqueid
                                 
                                 };
    
    
    NSLog(@"Posting loginDetailsDict is %@",ProfDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:ProfDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    profdict = [NSJSONSerialization
                                         
                                         JSONObjectWithData:data
                                         
                                         options:kNilOptions
                                         
                                         error:&deserr];
            
            
            
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"str Response is%@",str);
            
            
            NSLog(@"Killer Response is%@",profdict);
            

            
            
            [LoaderClass removeLoader:self.view];
            
            
            profileDetails = [profdict valueForKey:@"profileStatus"];
            NSLog(@"profileDetails Response %@",profileDetails);

            
            
            if ([[[profdict valueForKey:@"profileStatus"]valueForKey:@"response_message" ] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"profileWebServicesMethod";
                
                
                [self profileWebServicesMethod];
                
                
            }else if ([[[profdict valueForKey:@"profileStatus"]valueForKey:@"response_message" ]isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{
                
                
                // Saving profile Details Dictionary to NSUserDefaults.
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                NSData * profileDetailsData = [NSKeyedArchiver archivedDataWithRootObject:profileDetails];
                [profileDetailsDefaults setObject:profileDetailsData forKey:@"profileDetailsDefaults"];
                
                NSString * uName = [[[[profileDetails valueForKey:@"user_name"] stringByRemovingPercentEncoding]componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
                
                finalBannesrsArray = [[NSMutableArray alloc] init];
                [finalBannesrsArray removeAllObjects];
                
                [self getBannerDetailsWebServices];
                
                
                NSString * totCash = [[NSString stringWithFormat:@"%@",[profileDetails valueForKey:@"totalCash"]] stringByAppendingString:@" SAR"];
                
                _lblUserName.text = [[MCLocalization stringForKey:@"welcomeback"] stringByAppendingString:uName] ;
                _lblWalletAmount.text = totCash;
                _lblWalletAmount.adjustsFontSizeToFitWidth = YES;
                
                
                _lblUserName.lineBreakMode = NSLineBreakByWordWrapping;
                _lblUserName.numberOfLines = 2;
                _lblUserName.adjustsFontSizeToFitWidth = YES;
                
                
                
                
                NSString * actualCash = [NSString stringWithFormat:@"%@",[profileDetails valueForKey:@"actualCash"]];
                
                NSString * bonusCash  = [NSString stringWithFormat:@"%@",[profileDetails valueForKey:@"bonusCash"]];
                
                
                NSString * actualCashStr, * bonusCashStr;
                
                // Actual & bonus Cash
                if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
                    
                    actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ ﷼"],actualCash];
                    bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ ﷼"], bonusCash];
                } else {
                    actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ SAR"],actualCash];
                    bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ SAR"], bonusCash];
                }
                
                _lblActualCash.text = actualCashStr;
                _lblBonusCash.text = bonusCashStr;
                
                _lblActualCash.adjustsFontSizeToFitWidth = YES;
                _lblBonusCash.adjustsFontSizeToFitWidth = YES;
            
                
            }
        
        });
        
        
        
    }];
    
    
    [dataTask resume];
    
}


-(void)abc {
    
    
    // Setup the request with URL
    NSString * url123 = [NSString stringWithFormat:@"https://m.app.saddly.com/authentication/checkTokenStatus"];

    
    
    NSDictionary * ProfDict =  @{@"ipaddress":@"183.82.100.179",
                                       @"mac_address":@"02:00:00:00:00:00",
                                       @"access_token":@"6c2e46358f7425f6a12663def06d4d993924a3551c9c62c02a40ea8c8652538f",
                                       @"user_mob":@"966585858585",
                                       @"from":@"iPhone",
                                       @"unique_id":@"139EE8D1-7CE0-42FE-BBE5-E411AA3B34E4",
                                       @"device_model":@"iPhone 5s",
                                       @"genKey":@"4d45584b3b944d7ea2f438b5b8c2e9941f91de49ca401fcb9108415098757958",
                                       @"token_type":@"fresh_token",
                                       
                                       };

    NSLog(@"Posting to global %@",ProfDict);
    
    WebServicesMethods * profileServices = [[WebServicesMethods alloc] init];
    profileServices.target = self;
    profileServices.action = @selector(profileResponse:);
//    [profileServices  sendURLGetRequestWithURLString:recentRcUrl];
    
    

    [profileServices checkTokenStatusWebServices:url123 userMobileNumber:@"966585858585" userPassword:@"585858" dictFromMethod:ProfDict];
    
}

-(void)profileResponse:(id)response{
    
    NSLog( @"Global method response %@",response);
    
}


- (IBAction)btnShare:(id)sender {
    
    
    ShareViewController * shareVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareViewController"];
    shareVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:shareVC animated:YES];
    
}

- (IBAction)btnHelp:(id)sender {
    
    HelpVC * help = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpVC"];
    help.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:help animated:YES];
    
    
}

- (IBAction)Pagination:(id)sender {
}

- (IBAction)btnAddMoney:(id)sender {
    
    WalletVC * wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"WalletVC"];
    wallet.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:wallet animated:YES];
    
}



- (IBAction)btnTransferValue:(id)sender {
    
    PayOrSendVC * payOrSend = [self.storyboard instantiateViewControllerWithIdentifier:@"PayOrSendVC"];
    payOrSend.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:payOrSend animated:YES];
    
}

- (IBAction)btnRecharge:(id)sender {
    
    OperatorsVC * operator = [self.storyboard instantiateViewControllerWithIdentifier:@"OperatorsVC"];
    operator.hidesBottomBarWhenPushed = YES;
    
    NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
    [btnRcDefaults setObject:@"100" forKey:@"btnRcDefaults"];
    
    [self.navigationController pushViewController:operator animated:YES];
    
}



- (IBAction)btnGiftCards:(id)sender {
    
    OperatorsVC * operator = [self.storyboard instantiateViewControllerWithIdentifier:@"OperatorsVC"];
    operator.hidesBottomBarWhenPushed = YES;
    
    NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
    [btnRcDefaults setObject:@"200" forKey:@"btnRcDefaults"];
    
    [self.navigationController pushViewController:operator animated:YES];
    
}






-(void)getBannerDetailsWebServices {
    
    [LoaderClass showLoader:self.view];
    
    NSString * db;
    
    NSString *buildVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    NSLog(@"buildVersion %@",buildVersion);

    NSLog(@"app version %@",appVersion);
    
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary*   profileDict1 = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    NSString*UEmail= [profileDict1 valueForKey:@"user_email_id"];
    NSString*uMob=   [profileDict1 valueForKey:@"user_mob"];
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        db = @"AR";
        
    } else {
        db = @"SA";
        
    }
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/profile/getBannerDetails"];
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=  [uuidDefaults stringForKey:@"uuidDefaults"];
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    
    NSString * userId = [profileDict1 valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSLog(@"%@",userId);
    NSLog(@"%@",userPwd);
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    //  db=%@&from=%@&appversion=%@&user_mob=%@&user_emailId=%@&ipAddress=%@"
    
    NSDictionary * bannerDetailsDict =  @{@"db":db,
                                         
                                         @"appversion":appVersion,
                                         
                                         @"user_mob":uMob,
                                         @"user_emailId":UEmail,
                                         @"ipAddress":Ipaddress,
                                         @"access_token":accessToken,
                                         @"unique_id":uniqueid
                                         
                                         };
    
    
    
    NSLog(@"Posting bannerDetailsDict is %@",bannerDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:bannerDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    banddict = [NSJSONSerialization
                                         
                                         JSONObjectWithData:data
                                         
                                         options:kNilOptions
                                         
                                         error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Killer Response is%@",banddict);
            
            NSLog(@"str Response is%@",str);
            
            [LoaderClass removeLoader:self.view];
         
            
            NSLog(@"Banner Details Response=%@",banddict);
            
            NSDictionary*Killer=[banddict valueForKey:@"bannersStatus"];
            
            if ([[[banddict valueForKey:@"bannersStatus"]valueForKey:@"reponse_message" ] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"profileWebServicesMethod";
                
                
                [self profileWebServicesMethod];
                
                
            }else if ([[[banddict valueForKey:@"bannersStatus"]valueForKey:@"reponse_message" ]isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{
                

            
            if ([[profileDetails valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {

            
                
                
                
                demoVideoURLLink = [NSString stringWithFormat:@"%@",[[[Killer valueForKey:@"How to Use App"] valueForKey:@"subimagePath"] firstObject]];
                
                NSLog(@"Demo video url link is %@",demoVideoURLLink);
                
                
                for (int i = 0; i < [Killer allKeys].count; i++) {
                    
                    NSString *titleString = [Killer allKeys] [i];
                    NSDictionary * bannerDetails = (NSDictionary *) [[Killer valueForKey:titleString] firstObject];
                    
                    [finalBannesrsArray addObject:bannerDetails];
                    
                }
                
                [self.collecBanners reloadData];
                
                
                //    NSLog(@"finalBannesrsArray %@",finalBannesrsArray);
                
                bannerMsg1Array  = [finalBannesrsArray valueForKey:@"Header"];
                bannerMsg2Array = [finalBannesrsArray valueForKey:@"content1"];
                bannerMainImgPathArray = [finalBannesrsArray valueForKey:@"mainimagePath"];
                bannerSubImgPathArray = [finalBannesrsArray valueForKey:@"subimagePath"];
                bannerButtonTextArray = [Killer allKeys];
                bannerNavigateToArray = [finalBannesrsArray valueForKey:@"NavigateTo"];
                bannerTitleArray = [finalBannesrsArray valueForKey:@"title"];
                bannerTextColorArray = [finalBannesrsArray valueForKey:@"textcolor"];
                bannerCellBGColorArray = [finalBannesrsArray valueForKey:@"cellBackgroundcolor"];
                bannerValidHeaderArray = [finalBannesrsArray valueForKey:@"ValidateHeader"];
                
                
                NSLog(@"bannerMsg1Array %@",bannerMsg1Array);
                NSLog(@"bannerMsg2Array %@",bannerMsg2Array);
                NSLog(@"bannerMainImgPathArray %@",bannerMainImgPathArray);
                NSLog(@"bannerSubImgPathArray %@",bannerSubImgPathArray);
                NSLog(@"bannerButtonTextArray %@",bannerButtonTextArray);
                NSLog(@"bannerNavigateToArray %@",bannerNavigateToArray);
                NSLog(@"bannerTitleArray %@",bannerTitleArray);
                NSLog(@"bannerTextColorArray %@",bannerTextColorArray);
                NSLog(@"bannerCellBGColorArray %@",bannerCellBGColorArray);
                NSLog(@"bannerValidHeaderArray %@",bannerValidHeaderArray);

            }
            }
        });
        
        
    }];
    
    
    [dataTask resume];
}

#pragma mark --
#pragma mark - UICollectionView Delegate Methods


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return bannerMsg1Array.count;
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    cell = (BannersCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    if (cell == nil) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"BannersCollectionViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    //    cell.photoModel = _photoModelsDatasource[indexPath.row];
    
    
    
    //     For Main-Image
    NSString * imgUrl1 = [bannerMainImgPathArray objectAtIndex:indexPath.row];
    
    cell.imgBanner.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[imgUrl1 stringByReplacingOccurrencesOfString:@"+" withString:@"%20"]]]];
    
    cell.lblMsg1.text = [[[[bannerMsg1Array objectAtIndex:indexPath.row] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    
    cell.lblMsg2.text = [[[[bannerMsg2Array objectAtIndex:indexPath.row] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    
    cell.lblUseCodeOutlet.text = [[[[bannerButtonTextArray objectAtIndex:indexPath.row] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    
    
    cell.lblMsg1.adjustsFontSizeToFitWidth = YES;
    cell.lblMsg2.adjustsFontSizeToFitWidth = YES;
    cell.lblUseCodeOutlet.adjustsFontSizeToFitWidth = YES;
    
    cell.lblMsg2.lineBreakMode = NSLineBreakByWordWrapping;
    cell.lblMsg2.numberOfLines = 2;
    
    cell.lblMsg1.textColor = [self colorWithHexString:[bannerTextColorArray objectAtIndex:indexPath.row]];
    cell.lblUseCodeOutlet.backgroundColor = [self colorWithHexString:[bannerTextColorArray objectAtIndex:indexPath.row]];
    
    cell.backgroundColor = [self colorWithHexString:[bannerCellBGColorArray objectAtIndex:indexPath.row]];
    
    // Setting the Page Control pages
    int pages = floor(_collecBanners.contentSize.width/_collecBanners.frame.size.width);
    [_paginationOutlet setNumberOfPages:pages];
    
    
    return cell;
    
    
}




// Images in the Collection View are fit to the size of the screen.
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return collectionView.bounds.size;
    
}




- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([[bannerNavigateToArray objectAtIndex:indexPath.row] isEqualToString:@"QuickRecharge"]) {
        
        QuickRechargeVC * quickRC = [self.storyboard instantiateViewControllerWithIdentifier:@"QuickRechargeVC"];
        quickRC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:quickRC animated:YES];
        
    }
    
    else if ([[bannerNavigateToArray objectAtIndex:indexPath.row] isEqualToString:@"store"]) {
        
        NSString *iTunesLink = @"https://itunes.apple.com/us/app/%D8%B3%D8%AF%D8%AF%D9%84%D9%8A/id1201116755?ls=1&mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        
    }
    
    else if ([[bannerNavigateToArray objectAtIndex:indexPath.row] isEqualToString:@"youtube"]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:demoVideoURLLink]];
        
    }
    
    else {
        
        BannerTAndCVC * bannerTC = [self.storyboard instantiateViewControllerWithIdentifier:@"BannerTAndCVC"];
        bannerTC.hidesBottomBarWhenPushed = YES;
        
        NSString * title = [[[[bannerTitleArray objectAtIndex:indexPath.row] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
        
        bannerTC.bannerTitle = title;
        bannerTC.bannerType = [bannerButtonTextArray objectAtIndex:indexPath.row];
        bannerTC.bannerNavigateTo = [bannerNavigateToArray objectAtIndex:indexPath.row];
        bannerTC.bannerButtonColor = [bannerTextColorArray objectAtIndex:indexPath.row];
        bannerTC.bannerValidHeader = [bannerValidHeaderArray objectAtIndex:indexPath.row];
        
        
        // For Sub-Image
        NSString * imgUrl2 = [bannerSubImgPathArray objectAtIndex:indexPath.row];
        
        bannerTC.bannerSubImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[imgUrl2 stringByReplacingOccurrencesOfString:@"+" withString:@"%20"]]]];
        
        [self.navigationController pushViewController:bannerTC animated:YES];
        
    }
    
}




-(UIColor*)colorWithHexString:(NSString*)hex {
    
    
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
    
}


#pragma mark - UIScrollVewDelegate for UIPageControl

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = _collecBanners.frame.size.width;
    float currentPage = _collecBanners.contentOffset.x / pageWidth;
    
    if (0.0f != fmodf(currentPage, 1.0f))
    {
        _paginationOutlet.currentPage = currentPage + 1;
    }
    else
    {
        _paginationOutlet.currentPage = currentPage;
    }
    NSLog(@"finishPage: %ld", (long)_paginationOutlet.currentPage);
}





@end

