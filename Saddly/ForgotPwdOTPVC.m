//
//  ForgotPwdOTPVC.m
//  Saddly
//
//  Created by Sai krishna on 8/6/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "ForgotPwdOTPVC.h"

@interface ForgotPwdOTPVC ()

@end

@implementation ForgotPwdOTPVC

- (void)viewDidLoad {
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    

    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    
    // Returning Keyboard For Number Pad.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(removingKeyboardByTap)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    _txtEnterOTP.inputAccessoryView = keyboardDoneButtonView;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated {
        
    _lblEnterOTPName.text = [MCLocalization stringForKey:@"enterOTP"];
    _txtViewMsg.text = [MCLocalization stringForKey:@"willSendotpMobNum"];
    _txtEnterOTP.placeholder = [MCLocalization stringForKey:@"enterOTP"];
    
    [_btnSubmitOTPOutlet setTitle:[MCLocalization stringForKey:@"ForgetpasswordSubmitt"] forState:UIControlStateNormal];
    
    _lblEnterOTPName.adjustsFontSizeToFitWidth = YES;
    _btnSubmitOTPOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        _lblGreetings.text=@" رائع، شكرا لك";
      
    } else {
       
        
        _lblGreetings.text=@"Wonderful,Thankyou";
       
    }
    

    
}


-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}



//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:_txtEnterOTP]) {
        
        if([self isNumeric:string]){
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 6;
        } else  {
            return FALSE;
        }
        
    }
    return YES;
}


// Entering only numbers not a special Characters
-(BOOL)isNumeric:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}


// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = self.view.bounds;
    
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}




-(void) checkTokenStatusWebServices {
    
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":_forgotMobnum,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                if ([methodname isEqualToString:@"ForgetPasswordchangePwd"]) {
                    [self ForgetPasswordchangePwd];
                    
                }
                
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//





-(void)ForgetPasswordchangePwd {
    
    NSString*userMobNum= _forgotMobnum;
    NSString*db= [MCLocalization stringForKey:@"DBvalue"];;
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    
    
    [LoaderClass showLoader:self.view];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/forgotPassword/changepassword"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSDictionary * forPwdOTPDetailsDict =  @{@"usermob":userMobNum,
                                             @"db":db,
                                             @"from":@"iPhone",
                                             @"password":_forOTPPwd,
                                             @"OTP":_txtEnterOTP.text,
                                             @"ipAddress":Ipaddress,
                                             @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                             @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                             };
    
    NSLog(@"Posting forPwdOTPDetailsDict is %@",forPwdOTPDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:forPwdOTPDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            NSError *deserr;
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"str  Response is%@",str);
            NSLog(@"Killer  Response is :%@",[Killer valueForKey:@"response_message"]);
            
            
            [LoaderClass removeLoader:self.view];
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"ForgetPasswordchangePwd";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                } else if ([[Killer valueForKey:@"response_message"] isEqualToString:@"password updated successfully"]) {
                                        
                    
                    alert = [UIAlertController
                             alertControllerWithTitle:[MCLocalization stringForKey:@"urpwdupdated"]
                             message:nil
                             preferredStyle:UIAlertControllerStyleAlert];
                    
                    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"] style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          
                                                          
                                                          LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                                          [self.navigationController pushViewController:login animated:YES];

                                                          
                                                      }];
                    [alert addAction:okButton];
                    [self presentViewController:alert animated:YES completion:nil];
                    

                    
                    
                } else if ([[Killer valueForKey:@"response_message"] isEqualToString:@"otp expired"] ) {
                    
                    
                    alert = [UIAlertController
                             alertControllerWithTitle:[MCLocalization stringForKey:@"plsTryAgain"]
                             message:nil
                             preferredStyle:UIAlertControllerStyleAlert];
                    
                    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          
                                                          [self.navigationController popViewControllerAnimated:YES];
                                                          
                                                      }];
                    [alert addAction:okButton];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                } else if ([[Killer valueForKey:@"response_message"] isEqualToString:@"failure"] || [[Killer valueForKey:@"response"] isEqualToString:@"some thing went wrong"]) {
                   
                    
                    alert = [UIAlertController
                             alertControllerWithTitle:[MCLocalization stringForKey:@"wentWrong"]
                             message:[MCLocalization stringForKey:@"plsTryAgain"]
                             preferredStyle:UIAlertControllerStyleAlert];
                    
                    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          
                                                          [self.navigationController popViewControllerAnimated:YES];
                                                          
                                                      }];
                    [alert addAction:okButton];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    
                } else {
                    
                    alert = [UIAlertController
                             alertControllerWithTitle:[MCLocalization stringForKey:@"wentWrong"]
                             message:[MCLocalization stringForKey:@"plsTryAgain"]
                             preferredStyle:UIAlertControllerStyleAlert];
                    
                    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          
                                                          [self.navigationController popViewControllerAnimated:YES];
                                                          
                                                      }];
                    [alert addAction:okButton];
                    [self presentViewController:alert animated:YES completion:nil];
                    

                }
                
            }
            
        });
        
    }];
    
   [dataTask resume];
    
}





- (IBAction)btnShowPwd:(UIButton *)sender {
    
    
    
        sender.selected  =! sender.selected;
        if (sender.selected)
        {
            _txtEnterOTP.secureTextEntry = NO;
            [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
            [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        } else {
            _txtEnterOTP.secureTextEntry = YES;
            [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
            [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        }
    
}


- (IBAction)btnSubmitOTP:(id)sender {
    

    if (_txtEnterOTP.text.length < 1) {
        toastMsg = [MCLocalization stringForKey:@"EnterOTP"];
        [self toastMessagemethod];
    } else {
        [self ForgetPasswordchangePwd];
    }
    
    
}


@end
