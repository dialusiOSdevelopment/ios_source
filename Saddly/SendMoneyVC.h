//
//  SendMoneyVC.h
//  Saddly
//
//  Created by Sai krishna on 1/20/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "Validate.h"
#import "WebServicesMethods.h"
#import "UIView+Toast.h"
#import "LoaderClass.h"
#import "ConnectivityManager.h"
#import "UserContactsVC.h"
#import "HomeVC.h"
#import "WalletVC.h"
#import "SendMoneySuccessVC.h"
#import "InsufficientWalBalVC.h"
#import "LoginViewController.h"



#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
#import "WebServicesMethods.h"



@interface SendMoneyVC : UIViewController <UITextFieldDelegate, NSURLSessionDelegate> {
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg, * userEnteredNum;
    
    NSArray * rc;
    
    NSDictionary * sendMoneyDetails;
    NSDictionary * profileDict;
    
    NSString * methodname;
    NSString * a123;
    
}


@property (strong, nonatomic) IBOutlet UILabel *lblActualCash;
@property (strong, nonatomic) IBOutlet UILabel *lblBonusCash;

- (IBAction)btnBonusCashInfo:(id)sender;





@property (strong, nonatomic) IBOutlet UITextField *txtReceiptentNum;
@property (strong, nonatomic) IBOutlet UILabel *lblCurrentBal;
@property (strong, nonatomic) IBOutlet UITextField *txtEnterAmount;
@property (strong, nonatomic) IBOutlet UIButton *btnContactOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnSendNowOutlet;
@property (strong, nonatomic) IBOutlet UITextField *txtComments;

@property (strong, nonatomic) IBOutlet UITextField *Nameofreciever;
@property (strong, nonatomic) IBOutlet UILabel *lblCharLeft;


//Name
@property (strong, nonatomic) IBOutlet UILabel *lblWalletBalName;
@property (strong, nonatomic) IBOutlet UILabel *lblToName;
@property (strong, nonatomic) IBOutlet UILabel *lblAmountName;
@property (strong, nonatomic) IBOutlet UILabel *lblComments;
@property (strong, nonatomic) IBOutlet UILabel *lblInsuffWalBal;
@property (strong, nonatomic) IBOutlet UILabel *lblCharLeftName;



- (IBAction)btnContacts:(id)sender;
- (IBAction)btnInfo:(id)sender;
- (IBAction)btnAddAmount:(id)sender;
- (IBAction)btnSendNow:(id)sender;


@end
