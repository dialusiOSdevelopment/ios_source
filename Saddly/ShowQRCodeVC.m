//
//  ShowQRCodeVC.m
//  DialuzApp
//
//  Created by Sai krishna on 1/2/17.
//  Copyright © 2017 Dialuz. All rights reserved.
//

#import "ShowQRCodeVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface ShowQRCodeVC ()

@end

@implementation ShowQRCodeVC

@synthesize docController;


- (void)viewDidLoad {
    
    _lblUserName.adjustsFontSizeToFitWidth=YES;
    _lbluserMobNum.adjustsFontSizeToFitWidth=YES;
    _lblNameScanDialusCode.adjustsFontSizeToFitWidth=YES;
    _lblNameEntMobNum.adjustsFontSizeToFitWidth=YES;
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
        
    }
    
    
    
//    self.title = @"QR Code";
    
   
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];

    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];


    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        _imgDialusLogo.image = [UIImage imageNamed:@"Accepted-here-ar-black.png"];
    } else if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        _imgDialusLogo.image = [UIImage imageNamed:@"Accepted-here-en-black.png"];
    }
    
    
    _imgQRCode.image = [UIImage mdQRCodeForString:[[profileDict valueForKey:@"user_mob"] substringFromIndex:3] size:_imgQRCode.bounds.size.width fillColor:[UIColor blackColor]];
    
    // User Name
    NSTextAttachment * sendMoneyImg = [[NSTextAttachment alloc] init];
    sendMoneyImg.image = [UIImage imageNamed:@"send-money-32x32.png"];
    CGFloat offsetY1 = -5.0;
    sendMoneyImg.bounds = CGRectMake(0, offsetY1, sendMoneyImg.image.size.width, sendMoneyImg.image.size.height);
    
    NSAttributedString *attachmentString1 = [NSAttributedString attributedStringWithAttachment:sendMoneyImg];
    NSMutableAttributedString * struserName = [[NSMutableAttributedString alloc] initWithString:@""];
    [struserName appendAttributedString:attachmentString1];
    
    NSString * userName = [[[[profileDict valueForKey:@"user_name"] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    
    NSMutableAttributedString *myString2= [[NSMutableAttributedString alloc] initWithString:   [@" " stringByAppendingString:[[[MCLocalization stringForKey:@"toPay"] stringByAppendingString:@" "] stringByAppendingString:userName]]];
    [struserName appendAttributedString:myString2];
    
    self.lblUserName.textAlignment=NSTextAlignmentCenter;
    self.lblUserName.attributedText= struserName;
    self.lblUserName.adjustsFontSizeToFitWidth = YES;
    self.lblUserName.minimumScaleFactor = 0.5;
    
    
    // User Mobile Number
    NSTextAttachment * smartPhoneImg = [[NSTextAttachment alloc] init];
    smartPhoneImg.image = [UIImage imageNamed:@"phone-icon-32x32.png"];
    CGFloat offsetY = -5.0;
    smartPhoneImg.bounds = CGRectMake(0, offsetY, smartPhoneImg.image.size.width, smartPhoneImg.image.size.height);
    
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:smartPhoneImg];
    NSMutableAttributedString * strUserMobNum = [[NSMutableAttributedString alloc] initWithString:@""];
    [strUserMobNum appendAttributedString:attachmentString];
    
    NSString * userMobNum = [profileDict valueForKey:@"user_mob"];
    
    NSMutableAttributedString * myString1= [[NSMutableAttributedString alloc] initWithString:  [@"  " stringByAppendingString:userMobNum]];
    [strUserMobNum appendAttributedString:myString1];
    self.lbluserMobNum.textAlignment=NSTextAlignmentCenter;
    self.lbluserMobNum.attributedText=strUserMobNum;
    self.lbluserMobNum.adjustsFontSizeToFitWidth = YES;
    self.lbluserMobNum.minimumScaleFactor = 0.5;
    
    
    _lblNameEntMobNum.text = [[[MCLocalization stringForKey:@"or"] stringByAppendingString:@" "]stringByAppendingString:[MCLocalization stringForKey:@"txt_password"]];
    _lblNameScanDialusCode.text = [MCLocalization stringForKey:@"scanDialCode"];
    
    [_SharebttnName setTitle:[MCLocalization stringForKey:@"share"] forState:UIControlStateNormal];
    [_DnldBttnName setTitle:[MCLocalization stringForKey:@"download"] forState:UIControlStateNormal];
    [_PrintBttnName setTitle:[MCLocalization stringForKey:@"print"] forState:UIControlStateNormal];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{

    _lblUserName.adjustsFontSizeToFitWidth=YES;
    _lbluserMobNum.adjustsFontSizeToFitWidth=YES;
    _lblNameScanDialusCode.adjustsFontSizeToFitWidth=YES;
    _lblNameEntMobNum.adjustsFontSizeToFitWidth=YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];


        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}




 

- (IBAction)DownloadQrcode:(id)sender{
    
    _PrintBttnName.hidden=YES;
    _DnldBttnName.hidden=YES;
    _SharebttnName.hidden=YES;

    [self captureScreen];

 
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        
        UIGraphicsBeginImageContextWithOptions(self.aView.bounds.size, NO, [UIScreen mainScreen].scale);
    } else {
        UIGraphicsBeginImageContext(self.aView.bounds.size);
    }
    [self.aView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *data = UIImagePNGRepresentation(image);
    [data writeToFile:@"snapshot.png" options:NSDataWritingWithoutOverwriting error:Nil];
    [data writeToFile:@"snapshot.png" atomically:YES];
    
    UIImageWriteToSavedPhotosAlbum([UIImage imageWithData:data], nil, nil, nil);
    
    alertMessage=@"Image saved to your Gallery";
    [self alertMethod];
  
    _PrintBttnName.hidden=NO;
    _DnldBttnName.hidden=NO;
    _SharebttnName.hidden=NO;
    
    
    
}


-(NSMutableData *)createPDFDatafromUIView:(UIView*)aView{

    // Creates a mutable data object for updating with binary data, like a byte array
  pdfData = [NSMutableData data];
    
    // Points the pdf converter to the mutable data object and to the UIView to be converted
    UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil);
    UIGraphicsBeginPDFPage();
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    
    
    // draws rect to the view and thus this is captured by UIGraphicsBeginPDFContextToData
    
    [aView.layer renderInContext:pdfContext];
    
    // remove PDF rendering context
    UIGraphicsEndPDFContext();
    
    return pdfData;
}


-(NSString*)createPDFfromUIView:(UIView*)aView saveToDocumentsWithFileName:(NSString*)aFilename
{
    // Creates a mutable data object for updating with binary data, like a byte array
  pdfData = [self createPDFDatafromUIView:aView];
    
    // Retrieves the document directories from the iOS device
    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    
    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
    NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:aFilename];
    
    // instructs the mutable data object to write its context to a file on disk
    [pdfData writeToFile:documentDirectoryFilename atomically:YES];
    
    killer=documentDirectoryFilename;
    
    NSLog(@"documentDirectoryFileName: %@",documentDirectoryFilename);
    return documentDirectoryFilename;
    
    
    
}



- (IBAction)ShareQrcode:(id)sender{
    
    
    _PrintBttnName.hidden=YES;
    _DnldBttnName.hidden=YES;
    _SharebttnName.hidden=YES;
    
    [self createPDFDatafromUIView:_aView];
    
    [self createPDFfromUIView:_aView saveToDocumentsWithFileName:killer];

    
    UIActivityViewController * activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[pdfData] applicationActivities:nil];
    
    activityVC.popoverPresentationController.sourceView = self.view;
    activityVC.popoverPresentationController.sourceRect = CGRectMake( CGRectGetMidX(self.SharebttnName.frame), CGRectGetMidY(self.SharebttnName.frame), 0, 0);

    
    NSArray * excludeActivities = @[UIActivityTypePrint];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];

    
    _PrintBttnName.hidden=NO;
    _DnldBttnName.hidden=NO;
    _SharebttnName.hidden=NO;
    
}

- (UIImage *) captureScreen {
    
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    CGRect rect = [keyWindow bounds];
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [keyWindow.layer renderInContext:context];
    img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
    
    
}

-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


- (IBAction)PrintQRcode:(id)sender{
    
    _PrintBttnName.hidden=YES;
    _DnldBttnName.hidden=YES;
    _SharebttnName.hidden=YES;

    [self createPDFDatafromUIView:_aView];
    
    [self createPDFfromUIView:_aView saveToDocumentsWithFileName:killer];
    
    
    UIActivityViewController * activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[pdfData] applicationActivities:nil];
    
    activityVC.popoverPresentationController.sourceView = self.view;
    activityVC.popoverPresentationController.sourceRect = CGRectMake( CGRectGetMidX(self.PrintBttnName.frame), CGRectGetMidY(self.PrintBttnName.frame), 0, 0);
    
    NSArray * excludeActivities = @[UIActivityTypeMail,UIActivityTypeAirDrop,UIActivityTypeMessage,UIActivityTypePostToVimeo,UIActivityTypePostToFlickr,UIActivityTypePostToTwitter,UIActivityTypePostToFacebook,UIActivityTypeSaveToCameraRoll,UIActivityTypeAddToReadingList];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
    _PrintBttnName.hidden=NO;
    _DnldBttnName.hidden=NO;
    _SharebttnName.hidden=NO;

}
    


@end
