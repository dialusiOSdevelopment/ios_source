//
//  HomeVC.h
//  Saddly
//
//  Created by Sai krishna on 1/19/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BannersCollectionViewCell.h"
#import "OperatorsVC.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <netdb.h>
#import "DefaultFont.h"
#import "HelpVC.h"

#import "WebServicesMethods.h"
#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
#import "BannerTAndCVC.h"
#import "QuickRechargeVC.h"

#import "PayOrSendVC.h"
#import "WalletVC.h"
#import "ShareViewController.h"
#import "LoginViewController.h"






@interface HomeVC : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, NSURLSessionDelegate> {
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    
    NSString * Ipaddress;
    UIImageView *infoImageView;
    
    NSDictionary * profileDetails, * logNoticeDetails;
    
    NSMutableArray * finalBannesrsArray;
    
    BannersCollectionViewCell *cell;
    NSArray * bannerMsg1Array, * bannerMsg2Array, * bannerMainImgPathArray, * bannerSubImgPathArray, * bannerButtonTextArray, * bannerNavigateToArray, * bannerTitleArray, * bannerTextColorArray, * bannerCellBGColorArray, * bannerValidHeaderArray;
    
    NSString * demoVideoURLLink;
    
    NSString * methodname, * a123;


}



@property (strong, nonatomic) IBOutlet UILabel *lblActualCash;
@property (strong, nonatomic) IBOutlet UILabel *lblBonusCash;

- (IBAction)btnBonusCashInfo:(id)sender;



@property (nonatomic, retain) UIScrollView *scrollView;


@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblWalletAmount;

@property (strong, nonatomic) IBOutlet UIButton *btnAddMoneyOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnRcOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnGiftCardsOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnTransferValueOutlet;

@property (strong, nonatomic) IBOutlet UICollectionView *collecBanners;

- (IBAction)btnInfo:(id)sender;


// Names
@property (strong, nonatomic) IBOutlet UILabel *lblServicesName;
@property (strong, nonatomic) IBOutlet UILabel *lblWelcomeBackName;
@property (strong, nonatomic) IBOutlet UILabel *lblYourBalName;


@property (strong, nonatomic) IBOutlet UIPageControl *paginationOutlet;


- (IBAction)btnShare:(id)sender;
- (IBAction)btnHelp:(id)sender;
- (IBAction)Pagination:(id)sender;


- (IBAction)btnAddMoney:(id)sender;
- (IBAction)btnTransferValue:(id)sender;
- (IBAction)btnRecharge:(id)sender;
- (IBAction)btnGiftCards:(id)sender;





@end
