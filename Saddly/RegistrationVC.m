//
//  RegistrationVC.m
//  Saddly
//
//  Created by Sai krishna on 1/19/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.


#import "RegistrationVC.h"

@import FirebaseInstanceID;
@import FirebaseMessaging;

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface RegistrationVC ()

@end

@implementation RegistrationVC

- (void)viewDidLoad {
    
    
    //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Registration Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //

    
    NSString *token = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", token);

    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
        
    }

    
    [self localIPAddresses];
    [self localize];
    self.title = [MCLocalization stringForKey:@"signUp"];

    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    _txtMobNum.text = @"";
//    [_txtName becomeFirstResponder];
    
    
    [_btnFemaleOutlet setImage:[UIImage imageNamed:@"no radiobutton-black.png"] forState:UIControlStateNormal];
    [_btnFemaleOutlet setImage:[UIImage imageNamed:@"yes Radio button-black.png"] forState:UIControlStateSelected];
    
    [_btnMaleOutlet setImage:[UIImage imageNamed:@"no radiobutton-black.png"] forState:UIControlStateNormal];
    [_btnMaleOutlet setImage:[UIImage imageNamed:@"yes Radio button-black.png"] forState:UIControlStateSelected];
    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];


    // Returning Keyboard For Number Pad.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(NextClicked)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    _txtMobNum.inputAccessoryView = keyboardDoneButtonView;

    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}




-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];

    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    
    [self.navigationController setNavigationBarHidden:NO];
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
      
        
        // Btn Male Transformation
        _btnMaleOutlet.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        _btnMaleOutlet.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        _btnMaleOutlet.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        
        
        // Btn Female Transformation
        _btnFemaleOutlet.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        _btnFemaleOutlet.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        _btnFemaleOutlet.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
       
    } else {
        
        // Btn Male Transformation
        _btnMaleOutlet.transform = CGAffineTransformMakeScale(1.0, -1.0);
        _btnMaleOutlet.titleLabel.transform = CGAffineTransformMakeScale(1.0, -1.0);
        _btnMaleOutlet.imageView.transform = CGAffineTransformMakeScale(1.0, -1.0);
        
        
        // Btn Female Transformation
        _btnFemaleOutlet.transform = CGAffineTransformMakeScale(1.0, -1.0);
        _btnFemaleOutlet.titleLabel.transform = CGAffineTransformMakeScale(1.0, -1.0);
        _btnFemaleOutlet.imageView.transform = CGAffineTransformMakeScale(1.0, -1.0);
        
    }
}



- (void)localize{
    
    [_txtName setPlaceholder:[MCLocalization stringForKey:@"RegName"]];
    [_txtCity setPlaceholder:[MCLocalization stringForKey:@"RegCity"]];
    [_txtEmail setPlaceholder:[MCLocalization stringForKey:@"RegMAilid"]];
    [_txtMobNum setPlaceholder:[MCLocalization stringForKey:@"RegMobile"]];
    [_txtPassword setPlaceholder:[MCLocalization stringForKey:@"Regpassword"]];
    [_txtConfirmPassword setPlaceholder:[MCLocalization stringForKey:@"RegConfirmpassword"]];
     [_txtReferralCode setPlaceholder:[MCLocalization stringForKey:@"referralCode"]];
    
    
    _lblSelUrGender.text = [MCLocalization stringForKey:@"selUrGenderOptional"];
    _lblSelUrGender.adjustsFontSizeToFitWidth = YES;
    
//    _lblmobNumBackground.layer.borderColor = [[UIColor colorWithRed:239.0/255.0 green:239.0/255.0 blue:239.0/255.0 alpha:1.0]CGColor];
    
    [_btnMaleOutlet setTitle:[MCLocalization stringForKey:@"malebuttnname"] forState:(UIControlStateNormal)];
    [_btnFemaleOutlet setTitle:[MCLocalization stringForKey:@"femalebuttonname"] forState:(UIControlStateNormal)];
    [_btnCheckboxTCOutlet setTitle:[MCLocalization stringForKey:@"t&c"] forState:(UIControlStateNormal)];
    [_btnRegOutlet setTitle:[MCLocalization stringForKey:@"registerbuttnname"] forState:(UIControlStateNormal)];

    
    _btnMaleOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnFemaleOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;

    
    _btnCheckboxTCOutlet.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btnCheckboxTCOutlet.titleLabel.numberOfLines = 2;
    
    
    
    [super viewDidLoad];
}

 


-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}

-(void)NextClicked{
    [self.txtEmail becomeFirstResponder];
}



// Returning to Next Text fields
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtName) {
        [self.txtCity becomeFirstResponder];
    } else if (textField == self.txtCity) {
        [self.txtMobNum becomeFirstResponder];
    } else if (textField == self.txtMobNum) {
        [self.txtEmail becomeFirstResponder];
    } else if (textField == self.txtEmail) {
        [self.txtPassword becomeFirstResponder];
    } else if (textField == self.txtPassword) {
        [self.txtConfirmPassword becomeFirstResponder];
    } else if (textField == self.txtConfirmPassword) {
        [self.txtReferralCode becomeFirstResponder];
    } else if (textField == self.txtReferralCode) {
        [self.txtReferralCode resignFirstResponder];
    }

    return YES;
}



//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if ([textField isEqual:_txtMobNum]) {
        
        if([self isNumeric:string]){
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 9;
        } else  {
            return FALSE;
        }
        
    } else if ([textField isEqual:_txtEmail]){
        if([self isAlphaNumericSpecialCharacters:string])
            return TRUE;
        else
            return FALSE;
    } else if ([textField isEqual:_txtName]){
        
        if([self isAlphaNumericWithSpace:string]) {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 40;
        } else
            return FALSE;
        
    }  else if ([textField isEqual:_txtCity]){
        if([self isAlphaNumericWithoutNumbers:string]) {
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 20;
            
        } else
            return FALSE;
        
    } else if ([textField isEqual:_txtPassword] || [textField isEqual:_txtConfirmPassword]){
        if([string isEqualToString:@" "]){
            // Returning no here to restrict whitespace
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return TRUE && newLength <= 20;
    }
    return YES;
}

// Entering only numbers not a special Characters
-(BOOL)isNumeric:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}





// Entering only alphaNumeric & Special Characters not a Arabic Characters
-(BOOL)isAlphaNumericSpecialCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._@-"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}

// Entering all Except special Characters
-(BOOL)isAlphaNumericWithSpace:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzأ ب ت ث ح خ  د ه ع غ ف ق ث ص ض ذ ط ك م ن ت ل ي س ش ظ ز و ة ى لا ر ؤ ء ئ ج"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}


// Entering all Except special Characters
-(BOOL)isAlphaNumericWithoutNumbers:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzأ ب ت ث ح خ  د ه ع غ ف ق ث ص ض ذ ط ك م ن ت ل ي س ش ظ ز و ة ى لا ر ؤ ء ئ ج"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}

// Entering all Except special Characters
-(BOOL)isSpace:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@" "] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}

// Entering all Except special Characters
-(BOOL)isCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzأ ب ت ث ح خ  د ه ع غ ف ق ث ص ض ذ ط ك م ن ت ل ي س ش ظ ز و ة ى لا ر ؤ ء ئ "] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}


//local IP Addresses
- (void)localIPAddresses {
    
    NSString *publicIP = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"https://icanhazip.com/"] encoding:NSUTF8StringEncoding error:nil];
    
    Ipaddress = [publicIP stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]]; // IP comes with a newline for some reason
    
    if (Ipaddress .length <1) {
        NSString *publicIP = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"https://api.ipify.org?format=json"] encoding:NSUTF8StringEncoding error:nil];
        
        Ipaddress = [publicIP stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        
    }
    
    if (Ipaddress .length <1) {
        
        NSMutableArray *ipAddresses = [NSMutableArray array] ;
        struct ifaddrs *allInterfaces;
        
        // Get list of all interfaces on the local machine:
        if (getifaddrs(&allInterfaces) == 0) {
            struct ifaddrs *interface;
            
            // For each interface ...
            for (interface = allInterfaces; interface != NULL; interface = interface->ifa_next) {
                unsigned int flags = interface->ifa_flags;
                struct sockaddr *addr = interface->ifa_addr;
                
                // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
                if ((flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING)) {
                    if (addr->sa_family == AF_INET || addr->sa_family == AF_INET6) {
                        
                        // Convert interface address to a human readable string:
                        char host[NI_MAXHOST];
                        getnameinfo(addr, addr->sa_len, host, sizeof(host), NULL, 0, NI_NUMERICHOST);
                        
                        [ipAddresses addObject:[[NSString alloc] initWithUTF8String:host]];
                    }
                }
            }
            
            freeifaddrs(allInterfaces);
        }
        
        
        if (ipAddresses.count < 1) {
            Ipaddress = 0;
        } else {
            Ipaddress =  [ipAddresses objectAtIndex:1];
        }
        
    }
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    [ipaddressdefault setObject:Ipaddress forKey:@"ipaddressdefault"];
    
    NSLog(@"IP address %@",Ipaddress);
    
    
}






// alert method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}




- (IBAction)btnCheckboxTC:(id)sender {
    
    alertTitle = [MCLocalization stringForKey:@"applyingt&c"];
    alertMessage = [MCLocalization stringForKey:@"checkinwebt&c"];
    [self alertMethod];
    
}



- (IBAction)btnMale:(id)sender {
    
    if([_btnMaleOutlet isSelected] == YES)
    {
        [_btnMaleOutlet setSelected:YES];
        [_btnFemaleOutlet setSelected:NO];
        gender = @"male";
    } else{
        [_btnMaleOutlet setSelected:YES];
        [_btnFemaleOutlet setSelected:NO];
        gender = @"male";
    }
    _profilePicImg.image = [UIImage imageNamed:@"man.png"];
}



- (IBAction)btnFemale:(id)sender {
    
    if([_btnFemaleOutlet isSelected] == YES)
    {
        [_btnFemaleOutlet setSelected:YES];
        [_btnMaleOutlet setSelected:NO];
        gender = @"female";
    } else{
        [_btnFemaleOutlet setSelected:YES];
        [_btnMaleOutlet setSelected:NO];
        gender = @"female";
    }
    _profilePicImg.image = [UIImage imageNamed:@"female.png"];

}


- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString * phoneRegex = @"^((5))[0-9]{8}$";
    NSPredicate * phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phoneNumber];
}




-(void) checkTokenStatusWebServices {
    
   
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":@"",
                                       @"user_mob":@"Guest",
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"fresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"mobEmailWebServicesMethod"]) {
                    [self mobEmailWebServicesMethod];
                }
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//




-(void)mobEmailWebServicesMethod{
    
    db = [MCLocalization stringForKey:@"DBvalue"];
    
    NSString * appendedMobNum = [@"966" stringByAppendingString:_txtMobNum.text];
    NSString * email = _txtEmail.text;
    NSString * from = @"iPhone";
    
    NSString*city=[_txtCity.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSString * userName = [_txtName.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    [LoaderClass showLoader:self.view];
    
    ///
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/registration/registrationVerification"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSDictionary * userVeriDict =  @{
                                         @"username":userName,
                                         @"usermob":appendedMobNum,
                                         @"db":db,
                                         @"useremail":email,
                                         @"from":from,
                                         @"city":city,
                                         @"ipAddress":Ipaddress,
                                         @"password":_txtPassword.text,
//                                         @"reg_macAddreess":Macaddress,
                                        
//                                         @"devicemodel":deviceName,
//                                         @"deviceversion":deviceVersion,
                                         };
    
    
    NSLog(@"posting userVeriDict is %@",userVeriDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:userVeriDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *deserr;
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str1  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            NSLog(@"Killer Response is %@",Killer);
            NSLog(@"str Response is %@",str1);
            
            [LoaderClass removeLoader:self.view];
            
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"mobEmailWebServicesMethod";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                } else {
            
                    
                    NSString * str = [Killer valueForKey:@"status"];
                    
                    if ([str isEqualToString:@"Please Enter the Correct Email Address"]) {
                        toastMsg = str;
                        [self toastMessagemethod];
                    } else if ([str isEqualToString:@"Please Enter the Mobile Number"]) {
                        toastMsg = str;
                        [self toastMessagemethod];
                    } else  if ([str isEqualToString:@"Please Enter the UserName"]) {
                        toastMsg = str;
                        [self toastMessagemethod];
                    } else  if ([str isEqualToString:@"Please Enter the City"]) {
                        toastMsg = str;
                        [self toastMessagemethod];
                    } else  if ([str isEqualToString:@"Please Enter the Mobile Number with 9 digits starting with 5"]) {
                        toastMsg = str;
                        [self toastMessagemethod];
                    } else  if ([str isEqualToString:@"Special characters are not allowed in Username"]) {
                        toastMsg = str;
                        [self toastMessagemethod];
                    } else  if ([str isEqualToString:@"Special characters and numbers not allowed in City"]) {
                        toastMsg = str;
                        [self toastMessagemethod];
                    } else  if ([str isEqualToString:@"something went wrong"]) {
                        toastMsg = str;
                        [self toastMessagemethod];
                    } else if ([str isEqualToString:@"Mobile number already exist"]) {
                        
                        toastMsg = [[[MCLocalization stringForKey:@"mobilealreadytaken"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"Chooseanotherone"]];
                        [self toastMessagemethod];
                        
                    } else if ([str isEqualToString:@"Email Id already exist"]) {
                        toastMsg = [[[MCLocalization stringForKey:@"emailalreadytaken"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"Chooseanotherone"]];
                        [self toastMessagemethod];
                        
                    } else if ([str isEqualToString:@"Success"]) {
                        
                        RegisterWithOTP * registerOTP = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterWithOTP"];
                        registerOTP.OTPfromServerResponse = str;
                        [self.navigationController pushViewController:registerOTP animated:YES];
                        
                        
                        registerOTP.OTPMobNum = _txtMobNum.text;
                        registerOTP.OTPPwd = _txtConfirmPassword.text;
                        registerOTP.OTPdb = db;
                        registerOTP.OTPUSerName = _txtName.text;
                        registerOTP.OTPUserEmailId = _txtEmail.text;
                        registerOTP.OTPGender = gender;
                        registerOTP.OTPUserCity = _txtCity.text;
                        registerOTP.OTPReferralCode = _txtReferralCode.text;
                        
                    } else {
                        
                        [self presentedViewController];
                    }

            
                }
            }
        });
        
    }];
    
    
    [dataTask resume];

}



- (IBAction)btnEnterShowPwd:(UIButton *)sender {
    
    sender.selected  =! sender.selected;
    if (sender.selected)
    {
        _txtPassword.secureTextEntry = NO;
        [_btnEnterShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        [_btnEnterShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
    } else {
        _txtPassword.secureTextEntry = YES;
        [_btnEnterShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        [_btnEnterShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
    }
    
}


- (IBAction)btnConfirmShowPwd:(UIButton *)sender {
    
    sender.selected  =! sender.selected;
    if (sender.selected)
    {
        _txtConfirmPassword.secureTextEntry = NO;
        [_btnConfirmShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        [_btnConfirmShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
    } else {
        _txtConfirmPassword.secureTextEntry = YES;
        [_btnConfirmShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        [_btnConfirmShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
    }
    
}








// Submitting all fields to the server
- (IBAction)btnRegister:(id)sender {
    
    [self checkTokenStatusWebServices];
    
    // Validating the Password for the repeated characters
    NSCountedSet* characterCounts = [[NSCountedSet alloc] init];
    // This ensures that we deal with all unicode code points correctly
    [_txtConfirmPassword.text enumerateSubstringsInRange:NSMakeRange(0, [_txtConfirmPassword.text length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
        [characterCounts addObject:substring];
    }];
    NSString* highestCountCharacterSequence = nil;
    NSUInteger highestCharacterCount = 0;
    for (NSString* characterSequence in characterCounts) {
        NSUInteger currentCount = [characterCounts countForObject:characterSequence];
        if (currentCount > highestCharacterCount) {
            highestCountCharacterSequence = characterSequence;
            highestCharacterCount = currentCount;
        }
    }
    NSLog(@"Highest Character Count in _txtPassword is %@ with count of %lu", highestCountCharacterSequence, (unsigned long)highestCharacterCount);
    
    
    
    // Name
    if ([_txtName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].
        length == 0){
        toastMsg = [MCLocalization stringForKey:@"Namealert"];
        [self toastMessagemethod];
    }
    // City
    else if ([_txtCity.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].
             length == 0){
        toastMsg = [MCLocalization stringForKey:@"Cityvalid"];
        [self toastMessagemethod];
    }
    // mobile Number
    else if ([_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg =  [MCLocalization stringForKey:@"Mobilealert"];
        [self toastMessagemethod];
    }
    else if (![Validate isValidMobileNumber:[_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg =   [MCLocalization stringForKey:@"mobilevalidate"];
        [self toastMessagemethod];
    }
    else if (![self validatePhone:[_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg = [MCLocalization stringForKey:@"startWith5"];
        [self toastMessagemethod];
    }
    // Email Id
    else if ([_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg = [MCLocalization stringForKey:@"Mailidvalidate"];
        [self toastMessagemethod];
    }
    else if (![Validate isValidEmailId:[_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg = [MCLocalization stringForKey:@"Mailidnotvalid"];
        [self toastMessagemethod];
    }
    // Password
    else if ([_txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg = [MCLocalization stringForKey:@"Passwordvalidate"];
        [self toastMessagemethod];
    } else if (_txtPassword.text.length < 8){
        toastMsg = [MCLocalization stringForKey:@"pwd8char"];
        [self toastMessagemethod];
    }
    // Confirm password
    else if ([_txtConfirmPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg =   [MCLocalization stringForKey:@"confirmpass"];
        [self toastMessagemethod];
    } else if (_txtConfirmPassword.text.length < 8){
        toastMsg = [MCLocalization stringForKey:@"cnfrmpwd8char"];
        [self toastMessagemethod];
    }
    // Comparising passwords
    else if (![_txtConfirmPassword.text isEqualToString:_txtPassword.text]) {
        toastMsg = [MCLocalization stringForKey:@"Passmatch"];
        [self toastMessagemethod];
    }
    // Not entering the Repeated characters
    else if (highestCharacterCount > 1) {
        toastMsg = [MCLocalization stringForKey:@"repeatNotAllow"];
        [self toastMessagemethod];
    }
    // Sequential Validations are not allowed
    
    else {
        [self mobEmailWebServicesMethod];
    }

}



@end
