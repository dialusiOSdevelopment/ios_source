//
//  WriteUsSuccessVC.m
//  Saddly
//
//  Created by Sai krishna on 6/20/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "WriteUsSuccessVC.h"

@interface WriteUsSuccessVC ()

@end

@implementation WriteUsSuccessVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
       _TransactText.numberOfLines=2;
    _TransactText.adjustsFontSizeToFitWidth=YES;
    
    NSUserDefaults * Suceess = [NSUserDefaults standardUserDefaults];
    
 NSString*Transactid=   [Suceess valueForKey:@"Suceess"];

    _TransactID.text=Transactid;
    
    [self.navigationItem setHidesBackButton:YES animated:YES];

    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        _contacttext.text=  [NSString stringWithFormat:@"%@ \r %@ \r %@ \r %@",@" أنسخ معك رقم التزكرة خلال تواصلك مع info@saddly.com",@"سوف نرد علي هذه المشكلة في خلال 48 ساعة",@"أو",@"سوف تصل إالينا على الرقم 920000092 "];
        
        _TransactText.text=@"تم ارسال رقم التذكرة إلينا بنجاح";
        
       
        [_DoneBttn setTitle:@"تم" forState:UIControlStateNormal];

        

    } else {
        _contacttext.text=  [NSString stringWithFormat:@"%@ \r %@ \r %@ \r %@",@"quote this ticket id while contacting info@saddlycom",@"we will reply on this issue within 48 hours",@"OR",@"you can reach us on 920000092"];

        _TransactText.text=@"Your Ticket ID is submitted successfully with us";
        
      
        [_DoneBttn setTitle:@"Done" forState:UIControlStateNormal];

    }

   
    
    _contacttext.numberOfLines=0;
    _contacttext.adjustsFontSizeToFitWidth=YES;
    
    
    
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{

    [self.navigationItem setHidesBackButton:YES animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 

- (IBAction)DoneBttn:(id)sender {
    
        [self performSegueWithIdentifier:@"DonetoHome" sender:nil];
    
}



@end
