//
//  MobileRechargeVC.h
//  Saddly
//
//  Created by Sai krishna on 1/21/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RechargeConfirmationVC.h"
#import "WebServicesMethods.h"
#import "UIView+Toast.h"
#import "ConnectivityManager.h"
#import "MCLocalization.h"
#import "UserContactsVC.h"
#import "BrowsePlansVC.h"
#import "Validate.h"
#import "LoaderClass.h"
#import "RecentsTableViewCell.h"
//#import "OffersTableViewCell.h"
#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"


@interface MobileRechargeVC : UIViewController <UITextFieldDelegate, NSURLSessionDelegate>{
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle,* alertMessage, * toastMsg, *operatorName;
    NSArray * operatorNameKey, * operatorNameValue, * operatorImages;
    NSDictionary * operatorDetails, * recentRechargeDetails;
    NSMutableArray * FinalOperatorArray;
    NSString * userEnteredNum, * utf, * cpncode;
    NSDictionary * Cpncoderesponse;
    UITextField * txtCouponCode;
    
    NSInteger count;
    
    NSArray * rc, *Number,*date, *operators, *planType, * rechargePlan,* rechargeamt,*rechargemob,*service_id;
    
    NSUInteger height;
    CALayer *bottomBorder;
    
    NSString * btnOperatorOutlet;
    
    NSDictionary * profileDict;
    
    NSString * methodname, * a123;
    
    
    
    
     
}


@property (strong,nonatomic) NSString  * operName;
@property (strong,nonatomic) UIImage  * operImage;
@property (strong,nonatomic) NSString  * operType;



@property (strong, nonatomic) IBOutlet UILabel *lblActualCash;
@property (strong, nonatomic) IBOutlet UILabel *lblBonusCash;

- (IBAction)btnBonusCashInfo:(id)sender;



@property (strong, nonatomic) IBOutlet UIImageView *imgOperator;

@property (strong, nonatomic) IBOutlet UILabel *lblWalletBalance;
@property (strong, nonatomic) IBOutlet UILabel *lblCurrentBal;

@property (strong, nonatomic) IBOutlet UITextField *txtMobNum;
@property (strong, nonatomic) IBOutlet UITextField *Amount;

@property (strong, nonatomic) IBOutlet UILabel *lbl;

@property (strong, nonatomic) IBOutlet UILabel *lblOperatorsDiplaying;
@property (strong, nonatomic) IBOutlet UILabel *lblLoadOperator;

@property (strong, nonatomic) IBOutlet UIButton *btnloadBrowsePlansOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnAfterLoadOperatoOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnHaveCouponOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnRcOutlet;
@property (strong, nonatomic) IBOutlet UILabel *lblCouponCodeSuccessFail;


@property (strong, nonatomic) IBOutlet UILabel *lblRecentRcsName;

@property (strong, nonatomic) IBOutlet UITableView *tblRecents;

// Actions
- (IBAction)btnContacts:(id)sender;
- (IBAction)btnInfo:(id)sender;
- (IBAction)btnLoadBrowsePlans:(id)sender;
- (IBAction)btnBrowsePlans:(id)sender;
- (IBAction)btnHaveACouponCode:(id)sender;
- (IBAction)btnProceedToRc:(id)sender;


@end
