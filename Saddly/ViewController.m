//
//  ViewController.m
//  Saddly
//
//  Created by Sai krishna on 2/7/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    
    
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 


-(void)viewDidAppear:(BOOL)animated {
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"])
    {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        _pageTitles = @[@"home 5th.png", @"begining.png", @"launch@saddly", @"menu"];
        _pageImages = @[@"home 5th.png", @"begining.png", @"launch_saddly.png", @"menu.png"];
        
        
        // Create page view controller
        self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
        self.pageViewController.dataSource = self;
        
        PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
        NSArray *viewControllers = @[startingViewController];
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        
        // Change the size of page view controller
        self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
        
        [self addChildViewController:_pageViewController];
        [self.view addSubview:_pageViewController.view];
        [self.pageViewController didMoveToParentViewController:self];
        
    } else {
        
   
        // Fetching the data from the profileDetailsDefaults.
        NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
        profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
        
        NSString * Uid = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"user_mob"]];
        
        NSLog(@"Uid %@",Uid);
        
        NSUInteger jj= Uid.length;
        
        NSLog(@"length is %lu",(unsigned long)jj);
        
        if ([[profileDict valueForKey:@"user_mob"] length]>0)  {
            
            [self performSegueWithIdentifier:@"togoHomeFromPaging" sender:nil];
            
        } else {
            
            [self performSegueWithIdentifier:@"loginFrompaging" sender:nil];

        }
    }

}



#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}



- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}



- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}


- (IBAction)btnSkip:(id)sender {
    
    [self performSegueWithIdentifier:@"loginFrompaging" sender:nil];
    
//    if ([[profileDict valueForKey:@"user_mob"] length]>0)  {
//        
//        [self performSegueWithIdentifier:@"togoHomeFromPaging" sender:nil];
//        
//    } else {
//        
//        [self performSegueWithIdentifier:@"loginFrompaging" sender:nil];
//        
//    }
    
}



@end
