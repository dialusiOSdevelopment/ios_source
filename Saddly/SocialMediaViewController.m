//
//  SocialMediaViewController.m
//  Saddly
//
//  Created by Kishan Gandhi on 21/03/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "SocialMediaViewController.h"
//#import <GoogleOpenSource/GoogleOpenSource.h>

@interface SocialMediaViewController ()
@end

@implementation SocialMediaViewController


- (void)viewDidLoad {
    
    self.title = [MCLocalization stringForKey:@"folowUsOn"];


    NSLog(@"social Media Dict %@",_socialMediaDict);

    
    [FBSDKSettings autoLogAppEventsEnabled]; //enableBetaFeature:FBBetaFeaturesLikeButton];
    [FBSDKSettings setAutoLogAppEventsEnabled:[NSNumber numberWithInt:1]];
    //FBSDKLikeControl *likeControl = [[FBSDKLikeControl alloc] init];
    _likeButton.objectID = @"649411828594217";//"106672399863953";
    self.likeButton.likeControlAuxiliaryPosition = FBSDKLikeControlAuxiliaryPositionBottom;
    
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL
                          URLWithString:[_socialMediaDict valueForKey:@"facebook"]];
    
    _shareButton.shareContent = content;
    
    
    
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */




- (IBAction)btnFbLike:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_socialMediaDict valueForKey:@"facebook"]]];
    
}




    - (IBAction)btnInstagramFollow:(id)sender {
    
           [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_socialMediaDict valueForKey:@"instagram"]]];
    }

    - (IBAction) didTapgoogleplusShare: (id)sender {
        }


- (IBAction)Twitter:(id)sender {
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_socialMediaDict valueForKey:@"twitter"]]];

    
//    NSArray *urls = [NSArray arrayWithObjects:
//                     @"twitter://user?screen_name={handle}", // Twitter
//                     @"tweetbot:///user_profile/{handle}", // TweetBot
//                     @"echofon:///user_timeline?{handle}", // Echofon
//                     @"twit:///user?screen_name={handle}", // Twittelator Pro
//                     @"x-seesmic://twitter_profile?twitter_screen_name={handle}", // Seesmic
//                     @"x-birdfeed://user?screen_name={handle}", // Birdfeed
//                     @"tweetings:///user?screen_name={handle}", // Tweetings
//                     @"simplytweet:?link=http://twitter.com/{handle}", // SimplyTweet
//                     @"icebird://user?screen_name={handle}", // IceBird
//                     @"fluttr://user/{handle}", // Fluttr
//                     @"http://twitter.com/{handle}",
//                     nil];
//    
//    UIApplication *application = [UIApplication sharedApplication];
//    
//    for (NSString *candidate in urls) {
//        NSURL *url = [NSURL URLWithString:[candidate stringByReplacingOccurrencesOfString:@"{handle}" withString:@"Saddlyksa?lang=ar"]];
//        if ([application canOpenURL:url]) {
//            [application openURL:url];
//            // Stop trying after the first URL that succeeds
//            return;
//        }
//    }
    
    
}


- (IBAction)Googleplus:(id)sender {
    
    id<GPPShareBuilder> shareBuilder = [[GPPShare sharedInstance] shareDialog];
    
    // This line will fill out the title, description, and thumbnail from
    // the URL that you are sharing and includes a link to that URL.
    [shareBuilder setURLToShare:[NSURL URLWithString:[_socialMediaDict valueForKey:@"googleplus"]]];
    
    [shareBuilder open];

}


@end

