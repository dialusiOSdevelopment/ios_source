//
//  FAQuestionsVC.m
//  Saddly
//
//  Created by Sai krishna on 3/9/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "FAQuestionsVC.h"

@interface FAQuestionsVC ()

@end

@implementation FAQuestionsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    arrayforBool=[[NSMutableArray alloc]init];
    arrayfaq = [[NSMutableArray alloc] init];
    
    [self FrequesntsWebServicesMethod];
   
//    self.tblFaqs.backgroundColor = [UIColor colorWithRed:2/255.0 green:181/255.0 blue:108/255.0 alpha:1.0];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tblFaqs reloadData];
}


// alert method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}





-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary*   profileDict1 = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    

    
    NSString * userId = [profileDict1 valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
            
            NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
            [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
            
            
            if ([methodname isEqualToString:@"FrequesntsWebServicesMethod"]) {
                [self FrequesntsWebServicesMethod];
                
            }
            
            
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//






-(void)FrequesntsWebServicesMethod {
    
    NSString * db;
    
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary*   profileDict1 = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    ;

    
    NSString*UEmail= [profileDict1 valueForKey:@"user_email_id"];
    NSString* userPwd= [pddefaults stringForKey:@"pddd"];
    NSString*uMob=   [profileDict1 valueForKey:@"user_mob"];
                      
  
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];

    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        db = @"AR";
        
        
        // Create the URLSession on the default configuration
        NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
        
        
        // Setup the request with URL
        NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/getQuestionDetails/getQuestionsnansersDetails"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        
        request.HTTPMethod = @"POST";
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        
        NSString * encodeMobPwd = [[uMob stringByAppendingString:@":"] stringByAppendingString:userPwd];
        
        NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
        NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
        
        
        NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
        [request setValue:authValue forHTTPHeaderField:@"Authorization"];
        
        
        NSDictionary * arFARsDict =  @{@"user_mob":uMob,
                                         @"user_emailId":UEmail,
                                         @"ipAddress":Ipaddress,
                                         @"from":@"iPhone",
                                         @"db":db,
                                         @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                         @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                         };
        
        NSLog(@"Posting arFARsDict is %@",arFARsDict);
        
        
        NSData *postdata = [NSJSONSerialization dataWithJSONObject:arFARsDict options:NSJSONWritingPrettyPrinted error:nil];
        
        
        [request setHTTPBody:postdata];
        
        
        // Create dataTask
        NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if (!data) {
                
                NSLog(@"No data returned from server, error ocurred: %@", error);
                NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
                NSLog(@"%@",userErrorText);
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
     
                
                NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                
                NSLog(@"resSrt: %@", resSrt);
                NSError *deserr;
                
                
                
                NSDictionary*Response=[NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
                
                
                
                if ([[Response valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                    
                    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                    [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                    
                    NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                    [loginName removeObjectForKey:@"userName"];
                    
                    
                    LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                    [self.navigationController pushViewController:login animated:YES];
                    
                } else  {
                    
                    
                    if ([[Response valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                        
                        methodname=@"sendMoneyWebServicesMethod";
                        
                        NSLog( @"Method Name %@",methodname);
                        [self checkTokenStatusWebServices];
                        
                    } else {
                        
                        
                        
                        NSDictionary *Freq =[Response valueForKey:@"qnadetails"];
                        
                        
                        NSLog(@"freq response: %@", Freq);
                        
                        NSLog(@"Response keys is%@",[Freq allKeys]);
                        
                        
                        arSectionTitleArray = [[NSArray alloc]initWithArray:[Freq allKeys]];
                        
                        NSLog(@"arSectionTitleArray are%@",arSectionTitleArray);
                        
                        arFaqAns = [Freq allValues];
                        
                        NSLog(@"arFaqAns  are%@",arFaqAns);
                        
                        
                        
                        for(NSString *title in arSectionTitleArray)
                        {
                            NSMutableDictionary *faq = [[NSMutableDictionary alloc] init];
                            [faq setValue:title forKey:@"title"];
                            [faq setValue:[Freq valueForKey:title] forKey:@"description"];
                            [faq setValue:@"FALSE" forKey:@"show"];
                            [arrayfaq addObject:faq];
                        }
                        [self.tblFaqs reloadData];
                        

                    }

                }
               
            });
            
        }];
        
        [dataTask resume];
        
        
    } else {
        
        db = @"SA";
        
        // Create the URLSession on the default configuration
        NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
        
        
        // Setup the request with URL
        NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/getQuestionDetails/getQuestionsnansersDetails"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        
        request.HTTPMethod = @"POST";
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        
        NSString * encodeMobPwd = [[uMob stringByAppendingString:@":"] stringByAppendingString:userPwd];
        
        NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
        NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
        
        
        NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
        [request setValue:authValue forHTTPHeaderField:@"Authorization"];
        
        
        NSDictionary * enFARsDict =  @{@"user_mob":uMob,
                                       @"user_emailId":UEmail,
                                       @"ipAddress":Ipaddress,
                                       @"from":@"iPhone",
                                       @"db":db,
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                       };
        
        NSLog(@"Posting enFARsDict is %@",enFARsDict);
        
        
        NSData *postdata = [NSJSONSerialization dataWithJSONObject:enFARsDict options:NSJSONWritingPrettyPrinted error:nil];
        
        
        [request setHTTPBody:postdata];
        
        
        // Create dataTask
        NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if (!data) {
                
                NSLog(@"No data returned from server, error ocurred: %@", error);
                NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
                NSLog(@"%@",userErrorText);
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
      
                NSLog(@"resSrt: %@", resSrt);
                NSError *deserr;
                
                
                
                NSDictionary*Response=[NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
                
                
//                
//
                
                NSDictionary *Freq =[Response valueForKey:@"qnadetails"];
                
                
                NSLog(@"freq response: %@", Freq);
                
                
                
                enSectionTitleArray=[[NSArray alloc]initWithArray:[Freq allKeys]];
                
                NSLog(@"enSectionTitleArray are%@",enSectionTitleArray);
                
                enFaqAns=[Freq allValues];
                
                NSLog(@"enFaqAns  are%@",enFaqAns);
                
                
                
                //    for (int i=0; i<[enSectionTitleArray count]; i++) {
                //        [arrayforBool addObject:[NSNumber numberWithBool:NO]];
                //    }
                for(NSString *title in enSectionTitleArray)
                {
                    NSMutableDictionary *faq = [[NSMutableDictionary alloc] init];
                    [faq setValue:title forKey:@"title"];
                    [faq setValue:[Freq valueForKey:title] forKey:@"description"];
                    [faq setValue:@"FALSE" forKey:@"show"];
                    [arrayfaq addObject:faq];
                }
                
                
                [self.tblFaqs reloadData];

            });

        }];

        [dataTask resume];
    }

}









#pragma mark --
#pragma mark - UITableView Delegate Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
//    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"])
//        return [arSectionTitleArray count];
//    else
//        return [enSectionTitleArray count];
    return arrayfaq.count;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableDictionary *dict = arrayfaq[section];
    if([[dict valueForKey:@"show"] boolValue]) {
        return 1;
    } else {
        return 0;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@""];
    
    if (cell == nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
    }
    
    NSMutableDictionary *dict = arrayfaq[indexPath.section];
    cell.textLabel.text = [dict valueForKey:@"description"];
    
    
    
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
     // Automatic height adjustment of the textlabel
    self.tblFaqs.estimatedRowHeight = 80;
    self.tblFaqs.rowHeight = UITableViewAutomaticDimension;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.backgroundColor =  [UIColor colorWithRed:2/255.0 green:181/255.0 blue:108/255.0 alpha:1.0];
    self.tblFaqs.tableFooterView = [[UIView alloc] init];
    return cell;
}




- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    self.tblFaqs.separatorColor = [UIColor clearColor];
    
    // Creating View
    UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0,0,_tblFaqs.frame.size.width, 40)];
    sectionView.tag=section;
    
    // Generating labels
    UILabel *viewLabel= [[UILabel alloc]initWithFrame:CGRectMake(5, 0, _tblFaqs.frame.size.width-10, 40)];
//    viewLabel.backgroundColor= [UIColor colorWithRed:2/255.0 green:181/255.0 blue:108/255.0 alpha:1.0];

    viewLabel.textColor=[UIColor blackColor];
    viewLabel.font=[UIFont systemFontOfSize:16.0f];
    viewLabel.numberOfLines = 2;
    
    NSMutableDictionary *dict = arrayfaq[section];
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        viewLabel.text=[dict valueForKey:@"title"];

    } else {
        
        //viewLabel.text=[enSectionTitleArray objectAtIndex:section];
        viewLabel.text=[dict valueForKey:@"title"];
    
    }
    
    // Left-Arrow image
    headerImage = [[UIImageView alloc] initWithFrame:sectionView.frame];
    headerImage = [[UIImageView alloc] initWithFrame:CGRectMake(_tblFaqs.frame.size.width-25,13,10,13)];
    headerImage.contentMode = UIViewContentModeScaleAspectFill;
    [headerImage setClipsToBounds:YES];
    
    
    for (int i = 0; i < arrayfaq.count; i++) {
        headerImage.image= [UIImage imageNamed:[NSString stringWithFormat:@"double_down.png"]];
        
    }
    
    
//    UIImageView *imgVew = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"double_down.png"]];

    // Adding SubViews to view.
    [sectionView addSubview:viewLabel];
    [sectionView addSubview:headerImage];

    
    
    UITapGestureRecognizer  * headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [sectionView addGestureRecognizer:headerTapped];
    
    
    return sectionView;
}


- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    
   // NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    int section = (int) gestureRecognizer.view.tag;
    
    NSMutableDictionary *dictselected = arrayfaq[section];
    

        for (NSMutableDictionary *dict in arrayfaq) {
            
            if(dict != dictselected) {
                
                if ([[dict valueForKey:@"show"] boolValue]) {
                
                    [dict setValue:@"FALSE" forKey:@"show"];
                }
            }
        }
//
    if ([[dictselected valueForKey:@"show"] boolValue]) {
        
        [dictselected setValue:@"FALSE" forKey:@"show"];
    }
    else{
        [dictselected setValue:@"TRUE" forKey:@"show"];
        
    }
    
            
       // [_tblFaqs reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationFade];
            [_tblFaqs reloadData];
    
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //[arrayforBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:NO]];
    NSMutableDictionary *dictselected = arrayfaq[indexPath.section];
    
    
    for (NSMutableDictionary *dict in arrayfaq) {
        if(dict != dictselected)
        {
        if ([[dict valueForKey:@"show"] boolValue]) {
            
            [dict setValue:@"FALSE" forKey:@"show"];
        }
        }
    }
    //
    if ([[dictselected valueForKey:@"show"] boolValue]) {
        
        [dictselected setValue:@"FALSE" forKey:@"show"];
    }
    else{
        [dictselected setValue:@"TRUE" forKey:@"show"];
        
    }

    
    
    // [_tblFaqs reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationFade];
    [_tblFaqs reloadData];
    
   // [_tblFaqs reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];

}



@end
