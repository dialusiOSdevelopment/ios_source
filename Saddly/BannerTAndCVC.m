//
//  BannerTAndCVC.m
//  Saddly
//
//  Created by Sai krishna on 7/14/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "BannerTAndCVC.h"

@interface BannerTAndCVC ()

@end

@implementation BannerTAndCVC

- (void)viewDidLoad {
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [self.navigationController setHidesBottomBarWhenPushed:YES];
    
    
    self.title = _bannerTitle;
    _imgBanner.image = _bannerSubImage;
    navigation = _bannerNavigateTo;
    
    NSString * bb = [[[_bannerType stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    [_btnBookNowOutlet setTitle:bb forState:UIControlStateNormal];
    _btnBookNowOutlet.backgroundColor = [self colorWithHexString:_bannerButtonColor];
    
    
    self.tblTAndC.separatorColor = [UIColor clearColor];
    [self getTermsnConditionsWebServices];
    [self.tblTAndC reloadData];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"getTermsnConditionsWebServices"]) {
                    
                    [self getTermsnConditionsWebServices];
                    
                }
                
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//




-(void)viewDidAppear:(BOOL)animated {
    
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    

    [self.navigationController setHidesBottomBarWhenPushed:YES];
    
    self.title = _bannerTitle;
    _imgBanner.image = _bannerSubImage;
    navigation = _bannerNavigateTo;
    
    NSString * bb = [[[_bannerType stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    [_btnBookNowOutlet setTitle:bb forState:UIControlStateNormal];
    _btnBookNowOutlet.backgroundColor = [self colorWithHexString:_bannerButtonColor];
    
    
    self.tblTAndC.separatorColor = [UIColor clearColor];
    [self getTermsnConditionsWebServices];
    [self.tblTAndC reloadData];
    
}




-(void)getTermsnConditionsWebServices {
    

    
    
    [LoaderClass showLoader:self.view];
    
    NSString * db;
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        db = @"AR";
        
    } else {
        
        db = @"SA";
        
    }
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary*   profileDict1 = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    NSString*UEmail= [profileDict1 valueForKey:@"user_email_id"];

    NSString*uMob=   [profileDict1 valueForKey:@"user_mob"];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    
    
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSString*from=@"iphone";
    
    
    
        NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
        NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
        // Setup the request with URL
    
        NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/profile/getTermsnConditions"];
    
    
    
    
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
        request.HTTPMethod = @"POST";
    
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
    
        NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
        NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    NSString * userId = [profileDict1 valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
     NSDictionary * loginDetailsDict =  @{@"db":db,
    
                                             @"bannerType":_bannerType,
    
                                             @"user_mob":uMob,
                                             @"user_emailId":UEmail,
                                             @"ipAddress":Ipaddress,
                                          @"from":from,
                                             @"access_token":accessToken,
                                             @"unique_id":uniqueid
    
                                             };
        

    
        NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
        NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
        [request setHTTPBody:postdata];
    
    
        // Create dataTask
    
        NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    
    
    
            if (!data) {
    
    
    
                NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
    
                NSLog(@"%@",userErrorText);
    
                return;
    
            }
    
    
            dispatch_async(dispatch_get_main_queue(), ^{
    
    
    
    
    
                NSError *deserr;
    
    
    
                NSDictionary*    Killer = [NSJSONSerialization
    
                                           JSONObjectWithData:data
    
                                           options:kNilOptions
    
                                           error:&deserr];
    
    
    
    
    
                NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                
                [LoaderClass removeLoader:self.view];
                
                
                NSLog(@"Killer Response is%@",Killer);
                
                NSLog(@"str Response is%@",str);
                
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"getTermsnConditionsWebServices";
                    
                    
                    [self checkTokenStatusWebServices];
                    
                    
                }else if ([[Killer valueForKey:@"response_message"]isEqualToString:@"userUnauthorized"]){
                    
                    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                    [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                    
                    NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                    [loginName removeObjectForKey:@"userName"];
                    
                    
                    
                    LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                    [self.navigationController pushViewController:login animated:YES];
                    
                }
                
                else {
                    
                    tAndCArray = [[Killer valueForKey: @"termsnconditions" ] valueForKey: _bannerType];
                
               
                    if (tAndCArray.count <10) {
                        
                        _ScrllView.scrollEnabled=NO;
                    }else{
                        
                        _ScrllView.scrollEnabled=YES;
                        
                    }
                    
                }
            });
            
            
        }];
    
    
    [dataTask resume];
}


-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
    
}


#pragma mark --
#pragma mark - UITableView Delegate Methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tAndCArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@""];
    
    if (cell == nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
    }
    
    
    cell.textLabel.text = [[[[tAndCArray objectAtIndex:indexPath.row] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.numberOfLines = 0;
    
    // Automatic height adjustment of the textlabel
    self.tblTAndC.estimatedRowHeight = 80;
    self.tblTAndC.rowHeight = UITableViewAutomaticDimension;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor]; // [UIColor colorWithRed:235.0/255.0 green:236.0/255.0 blue:236.0/255.0 alpha:1.0];
    
//
    
    NSString *text = [@"\u2736  " stringByAppendingString:[tAndCArray objectAtIndex:indexPath.row]];
    NSDictionary *attributes = @{ NSFontAttributeName : [UIFont systemFontOfSize:16.0f] };
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:NSMakeRange(0, 1)]; // color the bullet
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(1, attrStr.length - 1)]; // color the rest
    
    cell.textLabel.attributedText = attrStr;
    
    self.tblTAndC.separatorColor = [UIColor clearColor];
    self.tblTAndC.tableFooterView = [[UIView alloc] init];
    return cell;
}







- (IBAction)btnBookNow:(id)sender {
    
    
    if ([navigation isEqualToString:@"Addmoney"]) {
        
        WalletVC * wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"WalletVC"];
        wallet.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:wallet animated:YES];
        
        
    } else if ([navigation isEqualToString:@"SendMoney"]){
        
        PayOrSendVC * payOrSend = [self.storyboard instantiateViewControllerWithIdentifier:@"PayOrSendVC"];
        payOrSend.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:payOrSend animated:YES];
        
        
        
    }  else if ([navigation isEqualToString:@"Recharge"]){
        
        OperatorsVC * operator = [self.storyboard instantiateViewControllerWithIdentifier:@"OperatorsVC"];
        operator.hidesBottomBarWhenPushed = YES;
        
        NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
        [btnRcDefaults setObject:@"100" forKey:@"btnRcDefaults"];
        
        [self.navigationController pushViewController:operator animated:YES];
        
    }  else if ([navigation isEqualToString:@"QuickRecharge"]){
        
        QuickRechargeVC   * quickrecarge = [self.storyboard instantiateViewControllerWithIdentifier:@"QuickRecharge"];
        
        quickrecarge.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:quickrecarge animated:YES];
        
        
        
    }  else if ([navigation isEqualToString:@"Giftcards"]){
        
        OperatorsVC * operator = [self.storyboard instantiateViewControllerWithIdentifier:@"OperatorsVC"];
        operator.hidesBottomBarWhenPushed = YES;
        
        NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
        [btnRcDefaults setObject:@"200" forKey:@"btnRcDefaults"];
        
        [self.navigationController pushViewController:operator animated:YES];
        
        
    }  else if ([navigation isEqualToString:@"SendFreeSMS"]){
        
        FreeSMSVC * sms = [self.storyboard instantiateViewControllerWithIdentifier:@"FreeSMSVC"];
        sms.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:sms animated:YES];
        
        
        
    }  else if ([navigation isEqualToString:@"Share"]){
        
        
        ShareViewController * shareVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareViewController"];
        shareVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:shareVC animated:YES];
        
        
    } else if ([navigation isEqualToString:@"Notifications"]){
        
        //        PayOrSendVC * payOrSend = [self.storyboard instantiateViewControllerWithIdentifier:@"PayOrSendVC"];
        //        payOrSend.hidesBottomBarWhenPushed = YES;
        //
        //        [self.navigationController pushViewController:payOrSend animated:YES];
                
        
    } else if ([navigation isEqualToString:@"Updateprofile"]){
        
        
        MyProfileVC * profile = [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileVC"];
        profile.hidesBottomBarWhenPushed = YES;
        
        [self.navigationController pushViewController:profile animated:YES];
        
        
    }else if ([navigation isEqualToString:@"ContactUs"]){
        
        ContactUSDetailVC * contactUs = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUSDetailVC"];
        contactUs.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:contactUs animated:YES];
        
        
    }else if ([navigation isEqualToString:@"AboutUs"]){
        
        AboutUsVC * aboutUs = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsVC"];
        aboutUs.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:aboutUs animated:YES];
        
    } else{
        
        [self presentedViewController];
    }
    
    
}




@end
