//
//  HelpVC.m
//  Saddly
//
//  Created by Sai krishna on 3/3/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "HelpVC.h"
#import <GoogleOpenSource/GoogleOpenSource.h>

@interface HelpVC ()

@end

@implementation HelpVC

- (void)viewDidLoad {

    
    self.title = [MCLocalization stringForKey:@"help"];
   
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    


    [self socialMediaURLS];

    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 

-(void)viewDidAppear:(BOOL)animated {
    
    [self socialMediaURLS];
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    [_btnFAQsOutlet setTitle:[MCLocalization stringForKey:@"faqs"] forState:UIControlStateNormal];
    [_btnDemoVideoOutlet setTitle:[MCLocalization stringForKey:@"demovideo"] forState:UIControlStateNormal];
    [_btnfollowUsOnoutlet setTitle:[MCLocalization stringForKey:@"folowUsOn"] forState:UIControlStateNormal];
    [_btnWriteUsOutlet setTitle:[MCLocalization stringForKey:@"writeUs"] forState:UIControlStateNormal];


    _btnFAQsOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnDemoVideoOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnfollowUsOnoutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnWriteUsOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    
}

// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = self.view.bounds;
    
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}




-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
            
            NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
            [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
            
            
            if ([methodname isEqualToString:@"socialMediaURLS"]) {
                [self socialMediaURLS];
                
            }
            
            
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//





-(void)socialMediaURLS {
    
//    [LoaderClass showLoader:self.view];
    
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary*   profileDict1 = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    ;

    
    NSString*UEmail= [profileDict1 valueForKey:@"user_email_id"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    NSString*uMob=   [profileDict1 valueForKey:@"user_mob"];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    

    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/profile/getSocialMediaURLSDetails"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[uMob stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];

    
    NSDictionary * socilaMediaDetailsDict =  @{@"usermob":uMob,
                                         @"user_emailId":UEmail,
                                         @"ipAddress":Ipaddress,
                                         @"from":@"iPhone",
                                         @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                         @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                        };
    
    NSLog(@"Posting socilaMediaDetailsDict is %@",socilaMediaDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:socilaMediaDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        

        NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        
        NSLog(@"resSrt: %@", resSrt);
        NSError *deserr;
        
        NSDictionary*    Killer = [NSJSONSerialization
                                   JSONObjectWithData:data
                                   options:kNilOptions
                                   error:&deserr];
        
        
        NSLog(@"Response is%@",[Killer valueForKey:@"socailmediaURLS"]);
        
        NSLog(@"Response is%@",resSrt);
        
        
        if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
            
            NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
            [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
            
            NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
            [loginName removeObjectForKey:@"userName"];
            
            
            LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            [self.navigationController pushViewController:login animated:YES];
            
        } else  {
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"sendMoneyWebServicesMethod";
                
                NSLog( @"Method Name %@",methodname);
                [self checkTokenStatusWebServices];
                
            } else {
                
                
                
                //        [LoaderClass removeLoader:self.view];
                
                ///
                NSLog(@"social Media URLs Response: %@",[Killer valueForKey:@"socailmediaURLS"]);
                socialMediaDetails = [Killer valueForKey:@"socailmediaURLS"];
                
                NSLog(@"social Media Details all keys: %@",[socialMediaDetails allKeys]);
                NSLog(@"social Media Details all Values: %@",[socialMediaDetails allValues]);
                

            }
            
        }
        
    }];
    
    
    [dataTask resume];

    
}



- (IBAction)btnFAQs:(id)sender {
    
    FAQuestionsVC * faqs = [self.storyboard instantiateViewControllerWithIdentifier:@"FAQuestionsVC"];
    [self.navigationController pushViewController:faqs animated:YES];

}

- (IBAction)btnDemoVideo:(id)sender {
    
//    
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://m.youtube.com/watch?v=_BD_vCqgX5s"]];
//    
//    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[socialMediaDetails valueForKey:@"youtube"]]];

}

- (IBAction)btnFollowUsOn:(id)sender {
    
    SocialMediaViewController * social = [self.storyboard instantiateViewControllerWithIdentifier:@"SocialMediaViewController"];
    
    social.socialMediaDict = socialMediaDetails;
    
    [self.navigationController pushViewController:social animated:YES];
    
    
}

- (IBAction)btnWriteUs:(id)sender {
    
    WriteUsVC * writeUs = [self.storyboard instantiateViewControllerWithIdentifier:@"WriteUsVC"];
    [self.navigationController pushViewController:writeUs animated:YES];
    
}






@end
