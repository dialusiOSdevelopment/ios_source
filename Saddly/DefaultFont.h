////
////  DefaultFont.h
////  Saddly
////
////  Created by gandhi on 15/02/17.
////  Copyright © 2017 mullangi gandhi. All rights reserved.
////
//
#import <UIKit/UIKit.h>

@interface UIView (DefaultFontAndColor)

-(void)setDefaultFontFamily:(NSString*)fontFamily andSubViews:(BOOL)isSubViews andColor:(UIColor*) color;

@end

//#import <UIKit/UIKit.h>
//
//@interface UILabel (Helper)
//
//+(UILabel*)labelWithText:(NSString*)text;
//- (void)reSize;
//- (void)reSizeFixCenter;
//@end
