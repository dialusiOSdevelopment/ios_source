//
//  UpdateDetails.h
//  Saddly
//
//  Created by gandhi on 02/05/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoaderClass.h"
#import "WebServicesMethods.h"
#import "ConnectivityManager.h"
#import "MCLocalization.h"
#import "UIView+Toast.h"
#import "Validate.h"
#import "LoginViewController.h"

@interface UpdateDetails : UIViewController <UITextFieldDelegate, CNContactViewControllerDelegate, NSURLSessionDelegate>{

    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg,*user_mob;
    NSString * Ipaddress,*name;
    NSString * latitude, * longitude;


    NSUInteger namecount , numbercount,differcount1,differcount2;
    NSString* Names, * Numbers;
    NSString * deviceName, * deviceVersion;

    NSDictionary * profileDict;


     NSString * methodname, * a123;

    
}





@property (nonatomic, strong) NSMutableArray * FinalContact, * Finalnumber;
@property (nonatomic, strong) NSMutableArray * finalcontacts;

@property (strong, nonatomic) NSMutableArray * groupOfContacts;
@property (strong, nonatomic) NSMutableArray * contactNameArray;
@property (strong, nonatomic) NSMutableArray * phoneNumberArray;




@property (strong, nonatomic) IBOutlet UITextField *TxtUserName;
@property (strong, nonatomic) IBOutlet UITextField *TxtEmailid;
@property (strong, nonatomic) IBOutlet UITextField *TxtPassword;
@property (strong, nonatomic) IBOutlet UITextField *TxtconfirmPassword;
@property (strong, nonatomic) IBOutlet UIButton * btnDoneOutlet;


@property (strong, nonatomic) IBOutlet UIButton *btnPwdOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnConfPwdOutlet;



- (IBAction)Done:(id)sender;
- (IBAction)ShowTxtpass:(id)sender;
- (IBAction)ShowTxtcnfpass:(id)sender;

@end
