//
//  ForgotPwdMobNumVC.h
//  Saddly
//
//  Created by Sai krishna on 8/6/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "LoaderClass.h"
#import "UIView+Toast.h"
#import "ConnectivityManager.h"
#import "Validate.h"
#import "LoginViewController.h"
#import "ForgotPwdOTPVC.h"



@interface ForgotPwdMobNumVC : UIViewController <UITextFieldDelegate, NSURLSessionDelegate> {
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    
    NSString * methodname, * a123;

    
}


@property (strong, nonatomic) NSString * forgotMobnum;

@property (strong, nonatomic) IBOutlet UITextField *txtPwd;
@property (strong, nonatomic) IBOutlet UITextField *txtConfirmPwd;
@property (strong, nonatomic) IBOutlet UIButton *btnSendOutlet;


@property (strong, nonatomic) IBOutlet UIButton *btnShowPwdOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnShowConfirmPwdOutlet;




- (IBAction)btnShowPwd:(id)sender;
- (IBAction)btnShowConfirmPwd:(id)sender;



- (IBAction)btnSend:(id)sender;


@end
