//
//  SelectPackageVC.h
//  Saddly
//
//  Created by Sai krishna on 6/27/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MobileRechargeVC.h"
#import "MCLocalization.h"

@interface SelectPackageVC : UIViewController <NSURLSessionDelegate>  {
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle,* alertMessage;
    
    NSDictionary * profileDict;
    NSString * type;
    
    
    NSUserDefaults * btnRcDefaults;
    
    NSString * methodname, * a123;
    
   
}







@property (strong,nonatomic) NSString  * operName;
@property (strong,nonatomic) UIImage  * operImage;



@property (strong, nonatomic) IBOutlet UILabel *lblBalance;

@property (strong, nonatomic) IBOutlet UIImageView *imgOperator;
@property (strong, nonatomic) IBOutlet UIButton *btnVoiceCallOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnInternetDataOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnSecondInternetOutlet;



- (IBAction)btnVoiceCall:(id)sender;
- (IBAction)btnInternetData:(id)sender;

- (IBAction)btnInfo:(id)sender;


@property (strong, nonatomic) IBOutlet UILabel *lblYourBalanceName;


@end
