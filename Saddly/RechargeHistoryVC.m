//
//  RechargeHistoryVC.m
//  DialuzApp
//
//  Created by Sai krishna on 10/22/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import "RechargeHistoryVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface RechargeHistoryVC ()

@end

@implementation RechargeHistoryVC



- (void)viewDidLoad {
    
    self.title = [MCLocalization stringForKey:@"myRcs"];
    
    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]} forState:UIControlStateNormal];
    
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0]} forState:UIControlStateSelected];
    
    [_segment setTintColor:[UIColor whiteColor]];
    
    reponsedata = [[NSMutableDictionary alloc] init];
    rcMonthListTitle = [[NSMutableArray alloc] init];
    amMonthListTitle = [[NSMutableArray alloc] init];
    
    [_segment setTitle:[MCLocalization stringForKey:@"recharge"]  forSegmentAtIndex:0];
    [_segment setTitle:[MCLocalization stringForKey:@"SlidearrWllt"]  forSegmentAtIndex:1];
    
    self.viewRC.hidden = NO;
    self.viewAddMoney.hidden = YES;
    [self txnWebServicesMethod];
    [self.tblRCHist reloadData];
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}



-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"txnWebServicesMethod"]) {
                    [self txnWebServicesMethod];
                }
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//




// Segmented Control for Recharge & Add Money
- (IBAction)segmentChanged:(UISegmentedControl * )sender {
    
    switch (sender.selectedSegmentIndex) {
            //RECHARGE
        case 0:
            self.viewRC.hidden = NO;
            self.viewAddMoney.hidden = YES;
            [self txnWebServicesMethod];
            [self.tblRCHist reloadData];
            break;
            //ADDMONEY
        case 1:
            self.viewRC.hidden = YES;
            self.viewAddMoney.hidden = NO;
            [self txnWebServicesMethod];
            [self.tblAddMoneyHist reloadData];
            break;
            //DEFAULT
        default:
            break;
    }
}



-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
        [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
        
        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}




// For Recahrge & Add Money History
-(void)txnWebServicesMethod{
    
    self.tblRCHist.tableFooterView = [[UIView alloc] init];
    self.tblAddMoneyHist.tableFooterView = [[UIView alloc] init];
    
    NSString * userMobNum = [profileDict valueForKey:@"user_mob"];

    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
NSString * uemail = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"user_email_id"]];
    
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSString*from=@"iphone";
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    
    
    
    if (_segment.selectedSegmentIndex == 0) {
        
       
        
        [LoaderClass showLoader:self.view];
        
        
        // Create the URLSession on the default configuration
        NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
        
        // Setup the request with URL
        NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/getTotalTransactionsDetails"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        
        request.HTTPMethod = @"POST";
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        
        NSString * encodeMobPwd = [[userMobNum stringByAppendingString:@":"] stringByAppendingString:userPwd];
        
        NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
        NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
        
        
        NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
        [request setValue:authValue forHTTPHeaderField:@"Authorization"];
        
        
        NSDictionary * rcHistDetailsDict =  @{@"user_mob":userMobNum,
                                                @"transaction_type":@"Recharge",
                                                @"user_emailId":uemail,
                                                @"from":from,
                                                @"ipAddress":Ipaddress,
                                                @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                                @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                                };
        
        NSLog(@"Posting rcHistDetailsDict  is %@",rcHistDetailsDict);
        
        
        NSData *postdata = [NSJSONSerialization dataWithJSONObject:rcHistDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
        
        [request setHTTPBody:postdata];
        
        
        // Create dataTask
        NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if (!data) {
                
                NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
                NSLog(@"%@",userErrorText);
                return;
            }
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
                NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                
                
                NSLog(@"resSrt: %@", resSrt);
                NSError *deserr;
                
                [self.tblRCHist reloadData];
                
                [LoaderClass removeLoader:self.view];
                
                NSDictionary*Response=[NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
                
                reponsedata=[NSJSONSerialization
                             JSONObjectWithData:data
                             options:kNilOptions
                             error:&deserr];
                
                
                
                NSLog(@"got response=%@", Response);
                
                NSString*resp=[[[[[Response valueForKey:@"response_message"]valueForKey:@"response_message"]valueForKey:@"resposne_message"]valueForKey:@"response_message"]firstObject];
                NSLog(@"got response=%@", resp);
                
                
                if ([resp isEqualToString:@"userUnauthorized"]) {
                    
                    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                    [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                    
                    NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                    [loginName removeObjectForKey:@"userName"];
                    
                    
                    LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                    [self.navigationController pushViewController:login animated:YES];
                    
                } else  {
                    
                    
                    if ([resp isEqualToString:@"TokenExpired"]) {
                        
                        methodname=@"txnWebServicesMethod";
                        
                        
                        
                        NSLog( @"Method Name %@",methodname);
                        [self checkTokenStatusWebServices];
                        
                    }
                    else{

                
                rcYearList = [[Response objectForKey:@"myTransactions"] allKeys];
                
                NSLog(@"Yearlist %@",rcYearList);
                
                rcCount = 0, rcFinalCount = 0;
                
                
                rcMonthDictData = [[NSMutableArray alloc]init];
                rcFinalData = [[NSMutableArray alloc]init];
                
                // Year
                for (id year in rcYearList) {
                    
                    rcMonthList = [[[Response objectForKey:@"myTransactions"] objectForKey:year] allKeys];
                    NSLog(@"rcMonthList %@",rcMonthList);
                    
                    // Month
                    for (id month in rcMonthList) {
                        
                        [rcMonthListTitle addObject:[NSString stringWithFormat:@"%@ - %@",month,year]];
                        rcCount=[[[[[Response objectForKey:@"myTransactions"] objectForKey:year] objectForKey:month] valueForKey:@"order_id"] count];
                        
                        rcFinalCount = rcFinalCount + rcCount;
                        
                        NSLog(@"rcFinalCount  is : %lu",(unsigned long)rcFinalCount);
                        
                        rcDetails =  [[[Response objectForKey:@"myTransactions"] objectForKey:year] objectForKey:month];
                        
                        [rcMonthDictData addObjectsFromArray:rcDetails];
                        
                        NSLog(@"rcMonthDictData %@",rcMonthDictData);
                        
                    } // month for
                    
                    [rcFinalData addObjectsFromArray:rcMonthDictData];
                    
                } // year for
                
                NSLog(@"rcFinalData  %@",rcFinalData);
                NSLog(@"rcFinalData count %lu",(unsigned long)[rcFinalData count]);
                
                if (rcFinalCount >=1) {
                    
                    _rcAmountArray = [rcFinalData valueForKey:@"amount"];
                    _rcDateTimeArray = [rcFinalData valueForKey:@"date_time"];
                    _rcOrderIdArray = [rcFinalData valueForKey:@"order_id"];
                    _rcResMsgArray = [rcFinalData valueForKey:@"response_message"];
                    _rcStatusArray = [rcFinalData valueForKey:@"status"];
                    _rcMonthArray = [rcFinalData valueForKey:@"month"];
                    _rcYearArray = [rcFinalData valueForKey:@"year"];
                    _rcOperatorArray = [rcFinalData valueForKey:@"operator"];
                    _rcMobNumArray = [rcFinalData valueForKey:@"mobileno"];
                    _rcAmountFromCardArray = [rcFinalData valueForKey:@"amountFromCard"];
                    _rcAmountFromWalletArray = [rcFinalData valueForKey:@"amountFromWallet"];
                    
                    
                    height= 60.0f;
                    [self.tblRCHist reloadData];
                  
                    
                } else {
                    
                    UILabel * noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tblRCHist.bounds.size.width, self.tblRCHist.bounds.size.height)];
                    noDataLabel.text             = @""; // [MCLocalization stringForKey:@"nohistFound"];
                    noDataLabel.textColor        = [UIColor blackColor];
                    noDataLabel.textAlignment    = NSTextAlignmentCenter;
                    self.tblRCHist.backgroundView = noDataLabel;
                    
                    
                    UIImageView *labelBackground = [[UIImageView alloc] initWithFrame:CGRectMake(self.tblRCHist.bounds.size.width / 2, self.tblRCHist.bounds.size.height / 2, self.tblRCHist.bounds.size.width / 3.5, self.tblRCHist.bounds.size.height / 3.5)];
                    
                    labelBackground.center = self.tblRCHist.center;
                    
                    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
                        
                        labelBackground.image = [UIImage imageNamed:@"no-history-found-ar.png"];
                    } else {
                        
                        labelBackground.image = [UIImage imageNamed:@"no-history-found-en.png"];
                    }
                    
                    
                    [noDataLabel addSubview:labelBackground];
                    noDataLabel.backgroundColor = [UIColor clearColor];
                    
                    
                    self.tblRCHist.tableFooterView = [[UIView alloc] init];
                    height = 0;
                }
                    }
                    
                    
                }
            });
        }];
        
        [dataTask resume];

        
    } else {
        
        [LoaderClass showLoader:self.view];
        
        
        // Create the URLSession on the default configuration
        NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
        
        // Setup the request with URL
        NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/getTotalTransactionsDetails"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        
        request.HTTPMethod = @"POST";
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        
        NSString * encodeMobPwd = [[userMobNum stringByAppendingString:@":"] stringByAppendingString:userPwd];
        
        NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
        NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
        
        
        NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
        [request setValue:authValue forHTTPHeaderField:@"Authorization"];
        
        
        NSDictionary * rcHistDetailsDict =  @{@"user_mob":userMobNum,
                                              @"transaction_type":@"Recharge",
                                              @"user_emailId":uemail,
                                              @"from":from,
                                              @"ipAddress":Ipaddress,
                                              @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                              @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                              };
        
        NSLog(@"Posting rcHistDetailsDict  is %@",rcHistDetailsDict);
        
        
        NSData *postdata = [NSJSONSerialization dataWithJSONObject:rcHistDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
        
        [request setHTTPBody:postdata];
        
        
        // Create dataTask
        NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            if (!data) {
                
                NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
                NSLog(@"%@",userErrorText);
                return;
            }
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
       
                
                NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                
                NSLog(@"resSrt: %@", resSrt);
                NSError *deserr;
                
                [self.tblRCHist reloadData];
                
                [LoaderClass removeLoader:self.view];
                
                NSDictionary*Response=[NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
                
                reponsedata=[NSJSONSerialization
                             JSONObjectWithData:data
                             options:kNilOptions
                             error:&deserr];
                
                
                NSLog(@"got response=%@", Response);
                
                
                [self.tblAddMoneyHist reloadData];
                
                NSLog(@"add Money Response %@",response);
                
                
                NSString*resp=[[[[[Response valueForKey:@"response_message"]valueForKey:@"response_message"]valueForKey:@"resposne_message"]valueForKey:@"response_message"]firstObject];
                NSLog(@"got response=%@", resp);
                
                
                if ([resp isEqualToString:@"userUnauthorized"]) {
                    
                    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                    [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                    
                    NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                    [loginName removeObjectForKey:@"userName"];
                    
                    
                    LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                    [self.navigationController pushViewController:login animated:YES];
                    
                } else  {
                    
                    
                    if ([resp isEqualToString:@"TokenExpired"]) {
                        
                        methodname=@"txnWebServicesMethod";
                        
                        
                        
                        NSLog( @"Method Name %@",methodname);
                        [self checkTokenStatusWebServices];
                        
                    }
                    else{
                        

                
                amYearList = [[Response objectForKey:@"myTransactions"] allKeys];
                
                amCount = 0, amFinalCount = 0;
                
                
                
                amMonthDictData = [[NSMutableArray alloc]init];
                amFinalData = [[NSMutableArray alloc]init];
                
                
                // Year
                for (id year in amYearList) {
                    
                    amMonthList = [[[Response objectForKey:@"myTransactions"] objectForKey:year] allKeys];
                    
                    // Month
                    for (id month in amMonthList) {
                        
                        
                        [amMonthListTitle addObject:[NSString stringWithFormat:@"%@ - %@",month,year]];
                        amCount=[[[[[Response objectForKey:@"myTransactions"] objectForKey:year] objectForKey:month] valueForKey:@"order_id"] count];
                        
                        amFinalCount = amFinalCount + amCount;
                        
                        NSLog(@"amFinalCount  is : %lu",(unsigned long)amFinalCount);
                        
                        addMoneyDetails =  [[[Response objectForKey:@"myTransactions"] objectForKey:year] objectForKey:month];
                        
                        [amMonthDictData addObjectsFromArray:addMoneyDetails];
                        
                        NSLog(@"amMonthDictData %@",amMonthDictData);
                        
                    } // month for
                    
                    [amFinalData addObjectsFromArray:amMonthDictData];
                    
                } // year for
                
                NSLog(@"amFinalData  %@",amFinalData);
                NSLog(@"amFinalData count %lu",(unsigned long)[amFinalData count]);
                
                if (amFinalCount >=1) {
                    
                    _amAmountArray = [amFinalData valueForKey:@"amount"];
                    _amDateTimeArray = [amFinalData valueForKey:@"date_time"];
                    _amOrderIdArray = [amFinalData valueForKey:@"order_id"];
                    _amResMsgArray = [amFinalData valueForKey:@"response_message"];
                    _amStatusArray = [amFinalData valueForKey:@"status"];
                    _amMonthArray = [amFinalData valueForKey:@"month"];
                    _amYearArray = [amFinalData valueForKey:@"year"];
                    _amMobNumArray = [amFinalData valueForKey:@"mobileno"];
                    _amAmountFromCardArray = [amFinalData valueForKey:@"amountFromCard"];
                    _amAmountFromWalletArray = [amFinalData valueForKey:@"amountFromWallet"];
                    
                    
                    
                    height= 60.0f;
                    [self.tblAddMoneyHist reloadData];
                    
                } else {
                    
                    UILabel * noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tblAddMoneyHist.bounds.size.width, self.tblAddMoneyHist.bounds.size.height)];
                    noDataLabel.text             = @""; // [MCLocalization stringForKey:@"nohistFound"];
                    noDataLabel.textColor        = [UIColor blackColor];
                    noDataLabel.textAlignment    = NSTextAlignmentCenter;
                    self.tblAddMoneyHist.backgroundView = noDataLabel;
                    
                    
                    UIImageView *labelBackground = [[UIImageView alloc] initWithFrame:CGRectMake(self.tblAddMoneyHist.bounds.size.width / 2, self.tblAddMoneyHist.bounds.size.height / 2, self.tblAddMoneyHist.bounds.size.width / 3.5, self.tblAddMoneyHist.bounds.size.height / 3.5)];
                    
                    labelBackground.center = self.tblAddMoneyHist.center;
                    
                    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
                        
                        labelBackground.image = [UIImage imageNamed:@"no-history-found-ar.png"];
                    } else {
                        
                        labelBackground.image = [UIImage imageNamed:@"no-history-found-en.png"];
                    }
                    
                    
                    [noDataLabel addSubview:labelBackground];
                    noDataLabel.backgroundColor = [UIColor clearColor];
                    
                    
                    self.tblAddMoneyHist.tableFooterView = [[UIView alloc] init];
                    height = 0;
                }

                    }}
            });
            
        }];

        [dataTask resume];

    }// if-else
    
}







#pragma mark --
#pragma mark - UITableView Delegate Methods



//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//
//    if (_segment.selectedSegmentIndex == 0) {
//        return [rcMonthList count];
//    } else {
//        return [amMonthList count];
//    }
////    return 1;
//}


//- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//
//    return @"Jan";
//
//}


//ControllersSupportiWebServices
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (_segment.selectedSegmentIndex == 0) {
        NSArray *towscount =  [[[reponsedata objectForKey:@"myTransactions"] objectForKey:@"2017"] objectForKey:rcMonthList[section]];
        NSInteger count = [towscount count];
        return count;
    } else {
        NSArray *towscount =  [[[reponsedata objectForKey:@"myTransactions"] objectForKey:@"2017"] objectForKey:amMonthList[section]];
        NSInteger count = [towscount count];
        return count;
    }
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    
    if (_segment.selectedSegmentIndex == 0) {
        NSInteger count = [rcMonthList count];
        return count;
    } else {
        NSInteger count = [amMonthList count];
        return count;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    
    // Creating View
    UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0 , self.view.frame.size.width, 25)];
    sectionView.tag=section;
    
    // Generating labels
    UILabel *viewLabel= [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 25)];
    viewLabel.backgroundColor= [UIColor colorWithRed:2/255.0 green:181/255.0 blue:108/255.0 alpha:1.0];
    
    viewLabel.textColor=[UIColor blackColor];
    viewLabel.font=[UIFont systemFontOfSize:16.0f];
    
    
    if (_segment.selectedSegmentIndex == 0) {
        
        viewLabel.text = rcMonthListTitle[section]; //[NSString stringWithFormat:@"%@ - 2017",rcMonthList[section]];
        
    }  else{
        
        viewLabel.text = amMonthListTitle[section];//[NSString stringWithFormat:@"%@ - 2017",amMonthList[section]];
    }
    
    
   
    [sectionView addSubview:viewLabel];
    return sectionView;
    
}




-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_segment.selectedSegmentIndex == 0) {
        
        
        NSString * cellIdentifier=@"CellRecharge";
        
        
//        RechargeHistoryCell * cellRC = (RechargeHistoryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        RechargeHistoryCell*cellRC =   [_tblRCHist dequeueReusableCellWithIdentifier:cellIdentifier];

        
        if (cellRC == nil) {
            NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"RechargeHistoryCell" owner:self options:nil];
            cellRC = [nib objectAtIndex:0];
        }
        
        
        NSArray *towscount =  [[[reponsedata objectForKey:@"myTransactions"] objectForKey:@"2017"] objectForKey:rcMonthList[indexPath.section]];
        
        dictRC = towscount[indexPath.row];
        
        
        cellRC.lblRCAmount.adjustsFontSizeToFitWidth=YES;
        cellRC.lblRCStatus.adjustsFontSizeToFitWidth=YES;
        cellRC.lblRCRespMsg.adjustsFontSizeToFitWidth=YES;
        cellRC.lblRCMobNum.adjustsFontSizeToFitWidth=YES;
        
        
        cellRC.RCOrderId = [NSString stringWithFormat:@"%@", [dictRC valueForKey:@"order_id"]];
        cellRC.RCDateTime = [NSString stringWithFormat:@"%@", [dictRC valueForKey:@"date_time"]];
        
        cellRC.RCAmountFromCard = [NSString stringWithFormat:@"%@", [dictRC valueForKey:@"amountFromCard"]];
        cellRC.RCAmountFromWallet = [NSString stringWithFormat:@"%@", [dictRC valueForKey:@"amountFromWallet"]];
        
        cellRC.RCRefundedAmount = [NSString stringWithFormat:@"%@", [dictRC valueForKey:@"refunded_amount"]];
        
        cellRC.lblRCAmount.text = [NSString stringWithFormat:@"%@ SAR",[dictRC valueForKey:@"amount"]];
        cellRC.lblRCMobNum.text = [NSString stringWithFormat:@"%@",[dictRC valueForKey:@"mobileno"]];
        
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        NSMutableAttributedString *myString1;
        
        
        if ([[dictRC valueForKey:@"response_message"] isEqualToString:@"Success"]) {
            
            cellRC.lblRCRespMsg.text = [MCLocalization stringForKey:@"success"];
            cellRC.RCResMsg = [MCLocalization stringForKey:@"success"];
            cellRC.lblRCRespMsg.textColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
            attachment.image = [UIImage imageNamed:@"right.png"];
            
        } else if ([[dictRC valueForKey:@"response_message"] isEqualToString:@"Additional Cash"]) {
            cellRC.lblRCRespMsg.text = [MCLocalization stringForKey:@"additionalCash"];
            cellRC.RCResMsg = [MCLocalization stringForKey:@"additionalCash"];
            cellRC.lblRCRespMsg.textColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
            attachment.image = [UIImage imageNamed:@"right.png"];
            
        } else if ([[dictRC valueForKey:@"response_message"] isEqualToString:@"Refund"]) {
            
            cellRC.lblRCRespMsg.text = [MCLocalization stringForKey:@"refund"];
            cellRC.RCResMsg = [MCLocalization stringForKey:@"refund"];
            cellRC.lblRCRespMsg.textColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
            attachment.image = [UIImage imageNamed:@"right.png"];
            
        } else {
            
            cellRC.lblRCRespMsg.text = [MCLocalization stringForKey:@"failure"];
            cellRC.RCResMsg = [MCLocalization stringForKey:@"failure"];
            cellRC.lblRCRespMsg.textColor = [UIColor redColor];
            attachment.image = [UIImage imageNamed:@"wrong.png"];
            
        }
        
        CGFloat offsetY = -3.0;
        attachment.bounds = CGRectMake(0, offsetY, attachment.image.size.width, attachment.image.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:@""];
        [myString appendAttributedString:attachmentString];
        myString1 = [[NSMutableAttributedString alloc] initWithString:cellRC.lblRCRespMsg.text];
        [myString appendAttributedString:myString1];
        cellRC.lblRCRespMsg.textAlignment=NSTextAlignmentRight;
        cellRC.lblRCRespMsg.attributedText=myString;
        
        
        op = [NSString stringWithFormat:@"%@", [dictRC valueForKey:@"operator"]];
        cellRC.RCOperator = op;
        
        NSString * statusName = [NSString stringWithFormat:@"%@", [dictRC valueForKey:@"status"]];
        
        if ([statusName containsString:@"Gift Card"]) {
            
            cellRC.lblRCStatus.text = [MCLocalization stringForKey:@"giftcard"];
            
            if ([op isEqualToString:@"iTunes"]) {
                cellRC.imgRC.image = [UIImage imageNamed:@"iTunes_70.png"];
            } else if ([op isEqualToString:@"Google+Play"]) {
                cellRC.imgRC.image = [UIImage imageNamed:@"Google_Play_70.png"];
            } else if ([op isEqualToString:@"Xbox"]) {
                cellRC.imgRC.image = [UIImage imageNamed:@"Xbox_70.png"];
            } else if ([op isEqualToString:@"Playstation"]) {
                cellRC.imgRC.image = [UIImage imageNamed:@"Playstation_70.png"];
            }
            
        } else if ([statusName containsString:@"Refund"]) {
            
            cellRC.lblRCStatus.text = [MCLocalization stringForKey:@"refund"];
            cellRC.imgRC.image = [UIImage imageNamed:@"refund.png"];
            
        } else if ([statusName containsString:@"Recharge"]) {
            
            cellRC.lblRCStatus.text = [MCLocalization stringForKey:@"recharge"];
            
            if ([op isEqualToString:@"Zain"]) {
                cellRC.imgRC.image = [UIImage imageNamed:@"zain_300.png"];
            } else if ([op isEqualToString:@"Friendi"]) {
                cellRC.imgRC.image = [UIImage imageNamed:@"friendi_300.png"];
            } else if ([op isEqualToString:@"Jawwy"]) {
                cellRC.imgRC.image = [UIImage imageNamed:@"jawwy_300.png"];
            } else if ([op isEqualToString:@"Mobily"]) {
                cellRC.imgRC.image = [UIImage imageNamed:@"mobily_300.png"];
            } else if ([op isEqualToString:@"STC"]) {
                cellRC.imgRC.image = [UIImage imageNamed:@"STC_round.png"];
            } else if ([op isEqualToString:@"Virgin"]) {
                cellRC.imgRC.image = [UIImage imageNamed:@"virgin_300.png"];
            } else if ([op isEqualToString:@"Sawa"]) {
                cellRC.imgRC.image = [UIImage imageNamed:@"sawa_300.png"];
            } else if ([op isEqualToString:@"Quicknet"]) {
                cellRC.imgRC.image = [UIImage imageNamed:@"quicknetr_300.png"];
            } else if ([op isEqualToString:@"Lebara"]) {
                cellRC.imgRC.image = [UIImage imageNamed:@"lebra_300.png"];
            }
            
        } else {
            
            cellRC.lblRCStatus.text = [MCLocalization stringForKey:@"recharge"];
            cellRC.imgRC.image = [UIImage imageNamed:@"70.png"];
        }
        
        cellRC.lblRCStatus.textColor = [UIColor blueColor];
        
        
        // image
        cellRC.imgRC.layer.cornerRadius = 20.0;
        cellRC.imgRC.layer.borderWidth = 1;
        cellRC.imgRC.layer.masksToBounds = YES;
        cellRC.imgRC.contentMode = UIViewContentModeScaleAspectFill;
        
     
        
        
        _tblRCHist.alwaysBounceVertical = NO;
        // Disabling the seperator line when the data is not present
        
        cellRC.backgroundColor =  [UIColor colorWithRed:235.0/255.0 green:236.0/255.0 blue:236.0/255.0 alpha:1.0];
        cellRC.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        self.tblRCHist.tableFooterView = [[UIView alloc] init];
        
        return cellRC;
        
    } else {
        
        NSString * cellIdentifier=@"CellAddMoney";
        
        
        AddMoneyHistoryCell * cellAM = (AddMoneyHistoryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
//        AddMoneyHistoryCell* cellAM =   [_tblAddMoneyHist dequeueReusableCellWithIdentifier:cellIdentifier];

        
        if (cellAM == nil) {
            NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"AddMoneyHistoryCell" owner:self options:nil];
            cellAM = [nib objectAtIndex:0];
        }
        
        
        NSArray *towscount =  [[[reponsedata objectForKey:@"myTransactions"] objectForKey:@"2017"] objectForKey:amMonthList[indexPath.section]];
        
        
        dictAM = towscount[indexPath.row];
        
        // image
        if ([[dictAM valueForKey:@"status"] isEqualToString:@"AddMoney"]){
            cellAM.ImgAM.image = [UIImage imageNamed:@"addmoney_new.png"];
        } else if ([[dictAM valueForKey:@"status"] isEqualToString:@"SendMoney"]) {
            cellAM.ImgAM.image = [UIImage imageNamed:@"send.png"];
        } else if ([[dictAM valueForKey:@"status"] isEqualToString:@"ReceivedMoney"]) {
            cellAM.ImgAM.image = [UIImage imageNamed:@"re.png"];
        } else if ([[dictAM valueForKey:@"status"] isEqualToString:@"BonusCash"]) {
            cellAM.ImgAM.image = [UIImage imageNamed:@"addmoney_new.png"];
        } else if ([[dictAM valueForKey:@"status"] isEqualToString:@"AdditionalCash"]) {
            cellAM.ImgAM.image = [UIImage imageNamed:@"addmoney_new.png"];
        } else if ([[dictAM valueForKey:@"status"] isEqualToString:@"GiftCard"] || [[dictAM valueForKey:@"status"] isEqualToString:@"Gift Card"]) {
            cellAM.ImgAM.image = [UIImage imageNamed:@"70.png"];
        } else if ([[dictAM valueForKey:@"status"] isEqualToString:@"Refund"]) {
            cellAM.ImgAM.image = [UIImage imageNamed:@"refund.png"];
        } else  {
            cellAM.ImgAM.image = [UIImage imageNamed:@"70.png"];
        }
        
        cellAM.ImgAM.layer.cornerRadius = 20;
        cellAM.ImgAM.layer.borderWidth = 1;
        cellAM.ImgAM.layer.masksToBounds = YES;
        cellAM.ImgAM.contentMode = UIViewContentModeScaleToFill;
        
        cellAM.lblAMAmount.adjustsFontSizeToFitWidth=YES;
        cellAM.lblAMRepMsg.adjustsFontSizeToFitWidth=YES;
        cellAM.lblAMStatus.adjustsFontSizeToFitWidth=YES;
        cellAM.lblAMMobNum.adjustsFontSizeToFitWidth = YES;
        
        
        cellAM.AMOrderId = [NSString stringWithFormat:@"%@", [dictAM valueForKey:@"order_id"]];
        cellAM.AMDateTime = [NSString stringWithFormat:@"%@", [dictAM valueForKey:@"date_time"]];
        
        
        cellAM.AMAmountFromCard = [NSString stringWithFormat:@"%@", [dictAM valueForKey:@"amountFromCard"]];
        cellAM.AMAmountFromWallet = [NSString stringWithFormat:@"%@", [dictAM valueForKey:@"amountFromWallet"]];
        
        
        
        cellAM.lblAMAmount.text = [NSString stringWithFormat:@"%@ SAR", [dictAM valueForKey:@"amount"]];
        cellAM.lblAMMobNum.text = [NSString stringWithFormat:@"%@", [dictAM valueForKey:@"mobileno"]];
        
        
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        NSMutableAttributedString *myString1;
        
        
        if ([[dictAM valueForKey:@"response_message"] isEqualToString:@"Success"]) {
            cellAM.lblAMRepMsg.text = [MCLocalization stringForKey:@"success"];
            cellAM.AMResMsg = [MCLocalization stringForKey:@"success"];
            cellAM.lblAMRepMsg.textColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
            attachment.image = [UIImage imageNamed:@"right.png"];
            
        } else if ([[dictAM valueForKey:@"response_message"] isEqualToString:@"Additional Cash"]) {
            cellAM.lblAMRepMsg.text = [MCLocalization stringForKey:@"additionalCash"];
            cellAM.AMResMsg = [MCLocalization stringForKey:@"additionalCash"];
            cellAM.lblAMRepMsg.textColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
            attachment.image = [UIImage imageNamed:@"right.png"];
            
        } else {
            
            cellAM.lblAMRepMsg.text = [MCLocalization stringForKey:@"failure"];
            cellAM.AMResMsg = [MCLocalization stringForKey:@"failure"];
            cellAM.lblAMRepMsg.textColor = [UIColor redColor];
            attachment.image = [UIImage imageNamed:@"wrong.png"];
            
        }
        
        CGFloat offsetY = -3.0;
        attachment.bounds = CGRectMake(0, offsetY, attachment.image.size.width, attachment.image.size.height);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:@""];
        [myString appendAttributedString:attachmentString];
        myString1 = [[NSMutableAttributedString alloc] initWithString:cellAM.lblAMRepMsg.text];
        
        [myString appendAttributedString:myString1];
        cellAM.lblAMRepMsg.textAlignment=NSTextAlignmentRight;
        cellAM.lblAMRepMsg.attributedText=myString;
        
        
        
        
        if ([[dictAM valueForKey:@"status"] isEqual:@"Recharge"]) {
            cellAM.lblAMStatus.text = [MCLocalization stringForKey:@"recharge"];
            cellAM.lblAMStatus.textColor = [UIColor blueColor];
        } else if([[dictAM valueForKey:@"status"] isEqual:@"BonusCash"]) {
            cellAM.lblAMStatus.text = [MCLocalization stringForKey:@"bonucash"];
            cellAM.lblAMStatus.textColor = [UIColor brownColor];
        } else if([[dictAM valueForKey:@"status"] isEqual:@"AdditionalCash"]) {
            cellAM.lblAMStatus.text = [MCLocalization stringForKey:@"addiCash"];
            cellAM.lblAMStatus.textColor = [UIColor blackColor];
        } else if([[dictAM valueForKey:@"status"] isEqual:@"AddMoney"]) {
            cellAM.lblAMStatus.text = [MCLocalization stringForKey:@"moneyAddedWallet"];
            cellAM.lblAMStatus.textColor = [UIColor purpleColor];
        } else if([[dictAM valueForKey:@"status"] isEqual:@"SendMoney"]) {
            cellAM.lblAMStatus.text = [MCLocalization stringForKey:@"senMon"];
            cellAM.lblAMStatus.textColor = [UIColor redColor];
        } else if([[dictAM valueForKey:@"status"] isEqual:@"ReceivedMoney"]) {
            cellAM.lblAMStatus.text = [MCLocalization stringForKey:@"receivedMoney"];
            cellAM.lblAMStatus.textColor = [UIColor grayColor];
        }  else if([[dictAM valueForKey:@"status"] isEqual:@"GiftCard"]) {
            cellAM.lblAMStatus.text = [MCLocalization stringForKey:@"giftcard"];
            cellAM.lblAMStatus.textColor = [UIColor grayColor];
        } else {
            cellAM.lblAMStatus.text = [MCLocalization stringForKey:@"SlidearrWllt"];
            cellAM.lblAMStatus.textColor = [UIColor purpleColor];
        }
        
        
        
        _tblAddMoneyHist.alwaysBounceVertical = NO;
        
        // Disabling the seperator line when the data is not present
        self.tblAddMoneyHist.tableFooterView = [[UIView alloc] init];
        cellAM.backgroundColor =  [UIColor colorWithRed:235.0/255.0 green:236.0/255.0 blue:236.0/255.0 alpha:1.0];
        cellAM.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        self.tblAddMoneyHist.tableFooterView = [[UIView alloc] init];
        
        
        return cellAM;
    }
}






-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return height;
}


//// Animation for the table view appearing
//-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    CGRect frame = cell.frame;
//    [cell setFrame:CGRectMake(0, self.tblRCHist.frame.size.height, self.tblRCHist.frame.size.width, self.tblRCHist.frame.size.height)];
//    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve  animations:^{
//        [cell setFrame:frame];
//    } completion:^(BOOL finished) {
//    }];
//}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TransactionDetailsVC * transactions = [self.storyboard instantiateViewControllerWithIdentifier:@"TransactionDetailsVC"];
    
    if (_segment.selectedSegmentIndex == 0) {
        
        RechargeHistoryCell * selectedCellRC = (RechargeHistoryCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        transactions.tagStr = @"1000";
        
        transactions.txnImg = selectedCellRC.imgRC.image;
        transactions.txnStatus = selectedCellRC.lblRCStatus.text;
        transactions.txnAmount = selectedCellRC.lblRCAmount.text;
        transactions.txnMobNum = selectedCellRC.lblRCMobNum.text;
        
        transactions.txnResMsg = selectedCellRC.RCResMsg;
        transactions.txnOrderId = selectedCellRC.RCOrderId;
        transactions.txnDate = selectedCellRC.RCDateTime;
        transactions.txnOperator = selectedCellRC.RCOperator;
        
        transactions.txnAmtFrmCard = selectedCellRC.RCAmountFromCard;
        transactions.txnAmtFrmWallet = selectedCellRC.RCAmountFromWallet;
        transactions.txnRefundedAmount = selectedCellRC.RCRefundedAmount;
        
        
        
    } else {
        
        AddMoneyHistoryCell *selectedCellAM = (AddMoneyHistoryCell *) [tableView cellForRowAtIndexPath:indexPath];
        
        transactions.txnImg = selectedCellAM.ImgAM.image;
        transactions.txnStatus = selectedCellAM.lblAMStatus.text;
        transactions.txnAmount = selectedCellAM.lblAMAmount.text;
        transactions.txnMobNum = selectedCellAM.lblAMMobNum.text;
        
        transactions.txnResMsg = selectedCellAM.AMResMsg;
        transactions.txnOrderId = selectedCellAM.AMOrderId;
        transactions.txnDate = selectedCellAM.AMDateTime;
        transactions.txnOperator = selectedCellAM.AMOperator;
        
        transactions.txnAmtFrmCard = selectedCellAM.AMAmountFromCard;
        transactions.txnAmtFrmWallet = selectedCellAM.AMAmountFromWallet;
        
        
    }
    
    transactions.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:transactions animated:YES];
    
    
}




@end

