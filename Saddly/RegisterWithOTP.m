//
//  RegisterWithOTP.m
//  Saddly
//
//  Created by Sai krishna on 1/20/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "RegisterWithOTP.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@import FirebaseInstanceID;
@import FirebaseMessaging;

@interface RegisterWithOTP ()

@end

@implementation RegisterWithOTP

- (void)viewDidLoad {
    
    //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Registration With OTP Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        _lblGreetings.text=@" رائع، شكرا لك";
        _lblActivationCodeName.text=@" كود التفعيل";
        _txtEnterCode.placeholder=@"كود التفعيل";
        _txtViewMsg.text=@"لقد تم أرسل  الرمز إلى رقم الجوال المسجل أو البريد الإلكتروني";
        
        [_btnRegistrationOutlet setTitle:@"تسجيل" forState:UIControlStateNormal];
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
        _lblGreetings.text=@"Wonderful,Thankyou";
        _lblActivationCodeName.text=@"Activation Code";
        _txtEnterCode.placeholder=@"Activation Code";
        _txtViewMsg.text = @"We have Send that Code to your Registered Mobile Number and Email Id";
        
        [_btnRegistrationOutlet setTitle:@"Done" forState:UIControlStateNormal];
        
    }
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [self getMacAddress];
    [self localIPAddresses];
    [self contactsToStore];
    
    [self deviceName];
    NSLog(@"device Name is: %@", deviceName);
    
    deviceVersion = [[UIDevice currentDevice] systemVersion];
    NSLog(@"device Version is: %@",deviceVersion);
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    
    // Returning Keyboard For Number Pad.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(removingKeyboardByTap)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    _txtEnterCode.inputAccessoryView = keyboardDoneButtonView;
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{

    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        _lblGreetings.text=@" رائع، شكرا لك";
        _lblActivationCodeName.text=@" كود التفعيل";
        _txtEnterCode.placeholder=@"كود التفعيل";
        _txtViewMsg.text=@"لقد تم أرسل  الرمز إلى رقم الجوال المسجل أو البريد الإلكتروني";
        
        [_btnRegistrationOutlet setTitle:@"تسجيل" forState:UIControlStateNormal];
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
        
        _lblGreetings.text=@"Wonderful,Thankyou";
        _lblActivationCodeName.text=@"Activation Code";
        _txtEnterCode.placeholder=@"Activation Code";
        _txtViewMsg.text=@"We have send that code to your registered  mobile number and email id";
        
        [_btnRegistrationOutlet setTitle:@"Done" forState:UIControlStateNormal];
        
    }
    
}


-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
        [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
        
        
        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}



-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}



- (NSString*) deviceName
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    static NSDictionary* deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{@"i386"      :@"Simulator",
                              @"x86_64"    :@"Simulator",
                              @"iPod1,1"   :@"iPod Touch",        // (Original)
                              @"iPod2,1"   :@"iPod Touch",        // (Second Generation)
                              @"iPod3,1"   :@"iPod Touch",        // (Third Generation)
                              @"iPod4,1"   :@"iPod Touch",        // (Fourth Generation)
                              @"iPod7,1"   :@"iPod Touch",        // (6th Generation)
                              @"iPhone1,1" :@"iPhone",            // (Original)
                              @"iPhone1,2" :@"iPhone",            // (3G)
                              @"iPhone2,1" :@"iPhone",            // (3GS)
                              @"iPad1,1"   :@"iPad",              // (Original)
                              @"iPad2,1"   :@"iPad 2",            //
                              @"iPad3,1"   :@"iPad",              // (3rd Generation)
                              @"iPhone3,1" :@"iPhone 4",          // (GSM)
                              @"iPhone3,3" :@"iPhone 4",          // (CDMA/Verizon/Sprint)
                              @"iPhone4,1" :@"iPhone 4S",         //
                              @"iPhone5,1" :@"iPhone 5",          // (model A1428, AT&T/Canada)
                              @"iPhone5,2" :@"iPhone 5",          // (model A1429, everything else)
                              @"iPad3,4"   :@"iPad",              // (4th Generation)
                              @"iPad2,5"   :@"iPad Mini",         // (Original)
                              @"iPhone5,3" :@"iPhone 5c",         // (model A1456, A1532 | GSM)
                              @"iPhone5,4" :@"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" :@"iPhone 5s",         // (model A1433, A1533 | GSM)
                              @"iPhone6,2" :@"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPhone7,1" :@"iPhone 6 Plus",     //
                              @"iPhone7,2" :@"iPhone 6",          //
                              @"iPhone8,1" :@"iPhone 6S",         //
                              @"iPhone8,2" :@"iPhone 6S Plus",    //
                              @"iPhone8,4" :@"iPhone SE",         //
                              @"iPhone9,1" :@"iPhone 7",          //
                              @"iPhone9,3" :@"iPhone 7",          //
                              @"iPhone9,2" :@"iPhone 7 Plus",     //
                              @"iPhone9,4" :@"iPhone 7 Plus",     //
                              
                              @"iPad4,1"   :@"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   :@"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   :@"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   :@"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                              @"iPad4,7"   :@"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                              @"iPad6,7"   :@"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                              @"iPad6,8"   :@"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                              @"iPad6,3"   :@"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                              @"iPad6,4"   :@"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                              };
    }
    
    deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        }
        else {
            deviceName = @"Unknown";
        }
    }
    
    return deviceName;
}



//local IP Addresses
- (void)localIPAddresses {
    
    NSString *publicIP = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"https://icanhazip.com/"] encoding:NSUTF8StringEncoding error:nil];
    
    Ipaddress = [publicIP stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]]; // IP comes with a newline for some reason
    
    if (Ipaddress .length <1) {
        NSString *publicIP = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"https://api.ipify.org?format=json"] encoding:NSUTF8StringEncoding error:nil];
        
        Ipaddress = [publicIP stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        
    }
    
    if (Ipaddress .length <1) {
        
        NSMutableArray *ipAddresses = [NSMutableArray array] ;
        struct ifaddrs *allInterfaces;
        
        // Get list of all interfaces on the local machine:
        if (getifaddrs(&allInterfaces) == 0) {
            struct ifaddrs *interface;
            
            // For each interface ...
            for (interface = allInterfaces; interface != NULL; interface = interface->ifa_next) {
                unsigned int flags = interface->ifa_flags;
                struct sockaddr *addr = interface->ifa_addr;
                
                // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
                if ((flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING)) {
                    if (addr->sa_family == AF_INET || addr->sa_family == AF_INET6) {
                        
                        // Convert interface address to a human readable string:
                        char host[NI_MAXHOST];
                        getnameinfo(addr, addr->sa_len, host, sizeof(host), NULL, 0, NI_NUMERICHOST);
                        
                        [ipAddresses addObject:[[NSString alloc] initWithUTF8String:host]];
                    }
                }
            }
            
            freeifaddrs(allInterfaces);
        }
        
        
        if (ipAddresses.count < 1) {
            Ipaddress = 0;
        } else {
            Ipaddress =  [ipAddresses objectAtIndex:1];
        }
        
    }
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    [ipaddressdefault setObject:Ipaddress forKey:@"ipaddressdefault"];
    
    NSLog(@"IP address %@",Ipaddress);
    
    
}


// Getting mac address
- (NSString *)getMacAddress
{
    int                 mgmtInfoBase[6];
    char                *msgBuffer = NULL;
    size_t              length;
    unsigned char       macAddress[6];
    struct if_msghdr    *interfaceMsgStruct;
    struct sockaddr_dl  *socketStruct;
    NSString            *errorFlag = NULL;
    
    // Setup the management Information Base (mib)
    mgmtInfoBase[0] = CTL_NET;        // Request network subsystem
    mgmtInfoBase[1] = AF_ROUTE;       // Routing table info
    mgmtInfoBase[2] = 0;
    mgmtInfoBase[3] = AF_LINK;        // Request link layer information
    mgmtInfoBase[4] = NET_RT_IFLIST;  // Request all configured interfaces
    
    // With all configured interfaces requested, get handle index
    if ((mgmtInfoBase[5] = if_nametoindex("en0")) == 0)
        errorFlag = @"if_nametoindex failure";
    else
    {
        // Get the size of the data available (store in len)
        if (sysctl(mgmtInfoBase, 6, NULL, &length, NULL, 0) < 0)
            errorFlag = @"sysctl mgmtInfoBase failure";
        else
        {
            // Alloc memory based on above call
            if ((msgBuffer = malloc(length)) == NULL)
                errorFlag = @"buffer allocation failure";
            else
            {
                // Get system information, store in buffer
                if (sysctl(mgmtInfoBase, 6, msgBuffer, &length, NULL, 0) < 0)
                    errorFlag = @"sysctl msgBuffer failure";
            }
        }
    }
    
    // Befor going any further...
    if (errorFlag != NULL)
    {
        NSLog(@"Error: %@", errorFlag);
        return errorFlag;
    }
    
    // Map msgbuffer to interface message structure
    interfaceMsgStruct = (struct if_msghdr *) msgBuffer;
    
    // Map to link-level socket structure
    socketStruct = (struct sockaddr_dl *) (interfaceMsgStruct + 1);
    
    // Copy link layer address data in socket structure to an array
    memcpy(&macAddress, socketStruct->sdl_data + socketStruct->sdl_nlen, 6);
    
    // Read from char array into a string object, into traditional Mac address format
    NSString *macAddressString = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                                  macAddress[0], macAddress[1], macAddress[2],
                                  macAddress[3], macAddress[4], macAddress[5]];
    NSLog(@"Mac Address: %@", macAddressString);
    
    
    Macaddress=macAddressString;
    
    // Release the buffer memory
    free(msgBuffer);
    
    
    return macAddressString;
}






////Setting the range of the TextField
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    
//    
//    if ([textField isEqual:_txtEnterCode]) {
//        
//        if([self isNumeric:string]){
//            NSUInteger newLength = [textField.text length] + [string length] - range.length;
//            return TRUE && newLength <= 4;
//        } else  {
//            return FALSE;
//        }
//        
//    }
//    return YES;
//}


// Entering only numbers not a special Characters
-(BOOL)isNumeric:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}


-(void)contactsToStore{
    
    self.groupOfContacts = [@[] mutableCopy];
    self.phoneNumberArray = [@[] mutableCopy];
    self.contactNameArray = [@[] mutableCopy];
    
    
    [self getallcontacts];
    
    for (CNContact * contact in self.groupOfContacts) {
        
        NSMutableArray * myArr = [[NSMutableArray alloc] init];
        [myArr addObject:contact.givenName];
        
        NSArray * phoneNum = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
        [self.contactNameArray addObjectsFromArray:myArr];
        [self.phoneNumberArray addObjectsFromArray:phoneNum];
    } // for
    
    _FinalContact = _contactNameArray;
    _Finalnumber = _phoneNumberArray;
    
    namecount=[_FinalContact count];
    numbercount=[_Finalnumber count];
    
    
    if (namecount < numbercount) {
        differcount1=numbercount-namecount;
        
        NSLog(@"differcountis %lu",differcount1);
        
    }
    else if (namecount>numbercount){
        
        differcount2=namecount-numbercount;}
    NSLog(@"differcountis %lu",differcount2);
    
    
    
    for (NSUInteger i=0; i<differcount1; i++) {
        
        [_FinalContact addObject:@"NoName"];
        
        
    }
    
    for (NSUInteger i=0; i<differcount2; i++) {
        
        [_Finalnumber addObject:@"NoNumber"];
        
    }
    
    
}


-(void)getallcontacts{
    
    if ([CNContactStore class]) {
        CNContactStore * addressBook = [[CNContactStore alloc]init];
        
        NSArray * keysToFetch = @[CNContactGivenNameKey ,
                                  CNContactEmailAddressesKey,
                                  CNContactFamilyNameKey,
                                  CNContactPhoneNumbersKey,
                                  CNContactPostalAddressesKey];
        
        CNContactFetchRequest * fetchRequest = [[CNContactFetchRequest alloc] initWithKeysToFetch:keysToFetch];
        
        [addressBook enumerateContactsWithFetchRequest:fetchRequest error:nil usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
            [self.groupOfContacts addObject:contact];
        }]; // Block
    } // if
} // getallcontacts

// for Posting Contacts
-(void)contactsWebServicesMethod{
    
    
    
    NSString*FinalName=[NSString new];
    NSString*FinalNumber=[NSString new];
    
    NSLog(@"count is%lu",(unsigned long)_FinalContact.count);
    
    for (NSInteger i=0; i<_FinalContact.count; i++) {
        
        if (i == 200) {
            break;
        }
        
        Names=_FinalContact[i];
        Numbers=_Finalnumber[i];
        
        NSLog(@"names are%@",Names);
        
        NSLog(@"numbers are%@",Numbers);
        
        if (Names.length==0) {
            Names=@"Namenotavailable";
        }
        
        Names=[Names stringByReplacingOccurrencesOfString:@"@" withString:@""];
        
        FinalName= [[[[[[[FinalName stringByAppendingString:Names] stringByAppendingString:@"SADDLY!@"]stringByReplacingOccurrencesOfString:@"+" withString:@""] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        if (Numbers.length==0) {
            Numbers=@"numberNotavailable";
        }
        
        FinalNumber=[[[[[[FinalNumber stringByAppendingString:Numbers]stringByAppendingString:@"SADDLY!@"]stringByReplacingOccurrencesOfString:@"+" withString:@""]stringByReplacingOccurrencesOfString:@"*" withString:@"" ]stringByReplacingOccurrencesOfString:@"#" withString:@""]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        
    }
    
    NSLog(@"numbers are%@",FinalNumber);
    NSLog(@"names are%@",FinalName);
    
    
    NSMutableDictionary*Contactstostore=[[NSMutableDictionary alloc]init];
    
    [Contactstostore setObject:FinalName forKey:@"contact_name"];
    
    [Contactstostore setObject:FinalNumber forKey:@"contact_mob"];
    
    
    NSString*user_mob=[@"966" stringByAppendingString:_OTPMobNum];
    
    [Contactstostore setObject:user_mob forKey:@"user_mob"];
    
    [Contactstostore setObject:@"iphone" forKey:@"login_status"];
    
    
    
    NSError * error;
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:Contactstostore options:NSJSONWritingPrettyPrinted error:&error];
    
    NSLog(@"Dictionary is%@",Contactstostore);
    
    if (error) {
        NSLog(@"Error (%@), error: %@", Contactstostore, error);
        return;
    }
    
#define appService [NSURL \
URLWithString:@"https://m.app.saddly.com/userlogin/inserMobcontacts1"]
    
    // Create request object
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:appService];
    
    // Set method, body & content-type
    request.HTTPMethod = @"POST";
    request.HTTPBody = jsonData;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setValue:
     [NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    NSString *apiKey  = @"cWZwbWdlcmZkdGV4em5kc2ZnZGdkZzpxZ2h4Y2ZnaHR5bmpta3U=";
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", apiKey];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    

    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *r, NSData *data, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        NSError *deserr;
        
        
        NSDictionary*    Killer = [NSJSONSerialization
                                   JSONObjectWithData:data
                                   options:kNilOptions
                                   error:&deserr];
        
        
        NSLog(@"data is %@",data);
        
        NSString *  str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        
        
        NSLog(@"Response is%@",Killer);
        
        NSLog(@"Response is%@",str);
        
        
    }];
    
}



// alert method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
    
}




#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
    
    
    if (longitude.length <1) {
        
        longitude=@"0";
        
    }  if (latitude.length <1) {
        
        latitude=@"0";
        
    }
    
    NSLog(@"latitude : %@, longitude :%@", latitude, longitude);
    
}




-(void) checkTokenStatusWebServices {
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":@"Guest",
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"postingRegistrationDetails"]) {
                    [self postingRegistrationDetails];
                }
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//





// posting Registration Details to the Server
-(void)postingRegistrationDetails{
    
    db = [MCLocalization stringForKey:@"DBvalue"];
    
    [LoaderClass showLoader:self.view];
    
    if (longitude.length <1) {
        
        longitude=@"0";
        
    }  if (latitude.length <1) {
        
        latitude=@"0";
        
    }
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    
    
    
    
    NSLog(@"latitude: %@, longitude :%@", latitude, longitude);
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/registration/doRegistrationDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSString * gender;
    
    if ([_OTPGender isEqualToString:@"male"] || [_OTPGender isEqualToString:@"female"]) {
        
        gender = _OTPGender;
        
    } else {
        gender = @"null";
        
    }
  
    
    NSDictionary * registrationDict =  @{@"reg_username":_OTPUSerName,
                                       @"reg_usermob":[@"966" stringByAppendingString:_OTPMobNum],
                                       @"reg_useremailId":_OTPUserEmailId,
                                       @"reg_userPwd":_OTPPwd,
                                       @"reg_from":@"iPhone",
                                       @"reg_db":_OTPdb,
                                       @"reg_ipAddress":Ipaddress,
                                       @"reg_gender":gender,
                                       @"reg_city":_OTPUserCity,
                                       @"reg_longitude":longitude,
                                       @"reg_latitude":latitude,
                                       @"referalCode":_OTPReferralCode,
                                       @"reg_macAddreess":Macaddress,
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"otp":_txtEnterCode.text,
                                       @"devicemodel":deviceName,
                                       @"deviceversion":deviceVersion,
                                      };
    
    
    NSLog(@"posting registrationDict is %@",registrationDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:registrationDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{

            
            NSError *deserr;
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            NSLog(@"killer  %@ ",Killer);
            
            NSString * stri  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSString * st1 = [[stri componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]]componentsJoinedByString:@""];
            
            NSLog(@"registration response st is %@ ",st1);
            
            
            [LoaderClass removeLoader:self.view];
            
            NSString * st = [NSString stringWithFormat:@"%@",[Killer valueForKey:@"regStatus"]];
            
            if ([st isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([st isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"postingRegistrationDetails";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                } else if ([st isEqualToString:@"Please enter correct OTP"]) {
                        
                        toastMsg = [[[MCLocalization stringForKey:@"incorrOTP"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"ValidOTP"]];
                        [self toastMessagemethod];
                        
                } else if ([st isEqualToString:@"success"]) {
                    
                    
                    mobileno=[@"966" stringByAppendingString:_OTPMobNum];
                    NSLog(@"Mobile Number is: %@",_OTPMobNum);
                    
                    
                    
                    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
                    [pddefaults setObject:_OTPPwd forKey:@"pddd"];

                    
                    NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                    [loginName setObject:mobileno forKey:@"userName"];
                    
                    [self contactsWebServicesMethod];
                    
                    [self performSegueWithIdentifier:@"homeFromRegistration" sender:nil];
                    
                    
                    } else if ([st isEqualToString:@"OTP Expired"]) {
                        
                        
                        alert = [UIAlertController
                                 alertControllerWithTitle:@"OTP Expired"
                                 message:[MCLocalization stringForKey:@"plsTryAgain"]
                                 preferredStyle:UIAlertControllerStyleAlert];
                        
                        okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              
                                                              [self.navigationController popViewControllerAnimated:YES];
                                                              
                                                          }];
                        [alert addAction:okButton];
                        [self presentViewController:alert animated:YES completion:nil];
                       
                    } else {
                        
                        
                        alert = [UIAlertController
                                 alertControllerWithTitle:[MCLocalization stringForKey:@"wentWrong"]
                                 message:[MCLocalization stringForKey:@"plsTryAgain"]
                                 preferredStyle:UIAlertControllerStyleAlert];
                        
                        okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              
                                                              [self.navigationController popViewControllerAnimated:YES];
                                                              
                                                          }];
                        [alert addAction:okButton];
                        [self presentViewController:alert animated:YES completion:nil];
                        
                    }

                
            }

        });
     
     }];
            
            
    [dataTask resume];
}








- (IBAction)btnShowPwd:(UIButton *)sender {
    
    sender.selected  =! sender.selected;
    
    if (sender.selected)
    {
        _txtEnterCode.secureTextEntry = NO;
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
    } else {
        _txtEnterCode.secureTextEntry = YES;
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
    }

    
}



- (IBAction)btnResendCode:(id)sender {
    
}

- (IBAction)btnRegistration:(id)sender {
    
    
    [locationManager stopUpdatingLocation];
    

    NSLog(@"Entered OTP is %@",_txtEnterCode.text);
    NSLog(@"Recieved OTP from Server is %@",_OTPfromServerResponse);
    
    if ([_txtEnterCode.text length] < 1) {
        
        toastMsg = [MCLocalization stringForKey:@"EnterOTP"];
        [self toastMessagemethod];
        
       
    } else {
        
        [self postingRegistrationDetails];
        
    }
    
//    else if ([_OTPfromServerResponse isEqualToString:_txtEnterCode.text]) {
//        
//        [self Devicedetails];
//        [self postingRegistrationDetails];
//        
//        
//    }
//    else {
//        
//        toastMsg = [[[MCLocalization stringForKey:@"incorrOTP"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"ValidOTP"]];
//        [self toastMessagemethod];
//        
//        
//    }
    
    
}



@end
