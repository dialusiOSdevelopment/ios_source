//
//  RegisterWithOTP.h
//  Saddly
//
//  Created by Sai krishna on 1/20/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import <CoreLocation/CoreLocation.h>
#import "HomeVC.h"
#import "RegistrationVC.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <netdb.h>

// for Getting Mac address
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>

#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
#import <sys/utsname.h>




@interface RegisterWithOTP : UIViewController <CNContactViewControllerDelegate, CLLocationManagerDelegate, NSURLSessionDelegate, UITextFieldDelegate>{

    NSDictionary * loginDetails;
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    NSString * db;
    
    CLLocationManager *locationManager;
    NSString* latitude, *longitude, * Names, * Numbers;

//    NSString * IpAddress;
    
    NSUInteger namecount , numbercount,differcount1,differcount2;
   
    NSString * mobileno;
    NSString * deviceName, * deviceVersion;
    
    NSString * Ipaddress,*name;
    
    NSString * Macaddress, * a123, * methodname;


   

}

@property (strong, nonatomic) NSString * OTPfromServerResponse;

@property (nonatomic, strong) NSMutableArray * FinalContact, * Finalnumber;
@property (nonatomic, strong) NSMutableArray * finalcontacts;

@property (strong, nonatomic) NSMutableArray * groupOfContacts;
@property (strong, nonatomic) NSMutableArray * contactNameArray;
@property (strong, nonatomic) NSMutableArray * phoneNumberArray;



@property (strong, nonatomic) NSString * OTPMobNum, * OTPPwd,  * OTPdb, * OTPUSerName, * OTPUserEmailId, * OTPGender, * OTPUserCity, * OTPFrom, * OTPLongitude, * OTPLatitude, * OTPReferralCode;

@property (strong, nonatomic) IBOutlet UILabel *lblGreetings;
@property (strong, nonatomic) IBOutlet UILabel *lblActivationCodeName;
@property (strong, nonatomic) IBOutlet UITextField *txtEnterCode;
@property (strong, nonatomic) IBOutlet UITextView *txtViewMsg;
@property (strong, nonatomic) IBOutlet UIButton *btnResendCodeOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnRegistrationOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnShowPwdOutlet;



- (IBAction)btnShowPwd:(id)sender;

- (IBAction)btnResendCode:(id)sender;
- (IBAction)btnRegistration:(id)sender;

@end
