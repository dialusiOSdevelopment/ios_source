//
//  UpdateDetails.m
//  Saddly
//
//  Created by gandhi on 02/05/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "UpdateDetails.h"

@interface UpdateDetails ()

@end

@implementation UpdateDetails

- (void)viewDidLoad {
    
    NSUserDefaults * userid = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * userpwd = [NSUserDefaults standardUserDefaults];
    
    
 user_mob = [userid valueForKey:@"userId"];
    NSString * userPwd = [userpwd valueForKey:@"userPwd"];

    
    [super viewDidLoad];
    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    

    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];

    
    // Returning Keyboard For Number Pad.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(NextClicked)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
  //  _TxtPassword.inputAccessoryView = keyboardDoneButtonView;
    _TxtconfirmPassword.inputAccessoryView = keyboardDoneButtonView;


    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{

    [_TxtUserName setPlaceholder:[MCLocalization stringForKey:@"RegName"]];
    [_TxtEmailid setPlaceholder:[MCLocalization stringForKey:@"RegMAilid"]];
    [_TxtPassword setPlaceholder:[MCLocalization stringForKey:@"Regpassword"]];
    [_TxtconfirmPassword setPlaceholder:[MCLocalization stringForKey:@"confirmpass"]];
    [_btnDoneOutlet setTitle:[MCLocalization stringForKey:@"done"] forState:UIControlStateNormal];


    
}
-(void)NextClicked{
    [self.view endEditing:YES];
}

-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}

-(void)checkTokenStatusWebServices {
    
    
    [LoaderClass showLoader:self.view];
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[user_mob stringByAppendingString:@":"] stringByAppendingString:_TxtPassword.text];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
   NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
NSString* Macaddress=   [macaddressDefaults valueForKey:@"macaddressDefaults"];
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
 NSString* UUID=  [uuidDefaults valueForKey:@"uuidDefaults"];

    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
 NSString*accessToken =[accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    NSString * tokenType = @"fresh_token";
    
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
NSString* genKey=   [genKeyDefaults valueForKey:@"genKeyDefaults"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":Macaddress,
                                       @"access_token":accessToken,
                                       @"user_mob":user_mob,
                                       @"from":@"iPhone",
                                       @"unique_id":UUID,
                                       @"device_model":deviceName,
                                       @"genKey":genKey,
                                       @"token_type":tokenType,
                                       };
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            [LoaderClass removeLoader:self.view];
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
            
            NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
            [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
            
            
            
            if ([methodname isEqualToString:@"UpdateDetails"]) {
                [self UpdateDetails];
            }

            
            
            
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 


// Returning to Next Text fields
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.TxtUserName) {
        [self.TxtEmailid becomeFirstResponder];
    } else if (textField == self.TxtEmailid) {
        [self.TxtPassword becomeFirstResponder];
    } else if (textField == self.TxtPassword) {
        [self.TxtconfirmPassword becomeFirstResponder];
    } else if (textField == self.TxtconfirmPassword) {
        [self.TxtconfirmPassword resignFirstResponder];
    } 
    return YES;
}


//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if ([textField isEqual:_TxtEmailid]){
        
        if([self isAlphaNumericSpecialCharacters:string])
            return TRUE;
        else
            return FALSE;
        
    } else if ([textField isEqual:_TxtUserName]){
        
        if([self isAlphaNumericWithSpace:string]) {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 40;
        } else
            return FALSE;
        
    }  else if ([textField isEqual:_TxtPassword] || [textField isEqual:_TxtconfirmPassword]){
        if([string isEqualToString:@" "]){
            // Returning no here to restrict whitespace
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return TRUE && newLength <= 20;
    }
    return YES;
}





// Entering only alphaNumeric & Special Characters not a Arabic Characters
-(BOOL)isAlphaNumericSpecialCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._@-"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}

// Entering all Except special Characters
-(BOOL)isAlphaNumericWithSpace:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzأ ب ت ث ح خ  د ه ع غ ف ق ث ص ض ذ ط ك م ن ت ل ي س ش ظ ز و ة ى لا ر ؤ ء ئ "] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}

// Entering all Except special Characters
-(BOOL)isSpace:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@" "] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}

// Entering all Except special Characters
-(BOOL)isCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzأ ب ت ث ح خ  د ه ع غ ف ق ث ص ض ذ ط ك م ن ت ل ي س ش ظ ز و ة ى لا ر ؤ ء ئ "] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}





// Moving the view Up & Down for the textfield
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    //move the main view, so that the keyboard does not hide it.
    if  (self.view.frame.origin.y < 0)
    {
        [self animateTextField: textField up: NO];
    }
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = textField.frame.origin.y / 2; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
} // Moving the view Up & Down for the textfield




// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = self.view.bounds;
    
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}





-(void)DeviceToken{
    
    {
        
        
        NSString *token = [[FIRInstanceID instanceID] token];
        NSLog(@"InstanceID token: %@", token);
        
        
        
        NSString*Number =user_mob;
        NSString * devicetype = @"iphone_SA";
        
        
        
        NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
        
        
        NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
        
        
        // Setup the request with URL
        
        NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/notificationDetails/insertDeviceTokenDetails"];
        
        
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        
        
        request.HTTPMethod = @"POST";
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*UUID=    [uuidDefaults valueForKey:@"uuidDefaults"];

        
        
        NSString * userId = [@"966" stringByAppendingString:user_mob];
        NSString * userPwd = _TxtPassword.text;
        
        NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
        
        NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
        NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
        
        
        NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
        [request setValue:authValue forHTTPHeaderField:@"Authorization"];
        
        
        //Request:user_mob,devicetoken,devicetype,unique_id,access_Token,from;
        
        
        
        NSDictionary * loginDetailsDict =  @{@"user_mob":user_mob,
                                             @"devicetype":devicetype,
                                             @"devicetoken":token,
                                             @"ipAddress":Ipaddress,
                                             @"from":@"iphone",
                                             @"access_token":a123,
                                             @"unique_id":UUID
                                             
                                             };
        
        
        NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
        
        
        NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
        
        
        [request setHTTPBody:postdata];
        
        
        // Create dataTask
        
        NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            
            
            if (!data) {
                
                
                
                NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
                
                NSLog(@"%@",userErrorText);
                
                return;
                
            }
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                
                
                
                NSError *deserr;
                
                
                
                NSDictionary*    Killer = [NSJSONSerialization
                                           
                                           JSONObjectWithData:data
                                           
                                           options:kNilOptions
                                           
                                           error:&deserr];
                
                
                
                
                
                NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                
                [LoaderClass removeLoader:self.view];
                
                
                NSLog(@"Killer Response is%@",Killer);
                
                NSLog(@"str Response is%@",str);
                
            });
            
            
        }];
        
        
        [dataTask resume];
    
}

}



// Contacts
-(void)contactsToStore{
    
    self.groupOfContacts = [@[] mutableCopy];
    self.phoneNumberArray = [@[] mutableCopy];
    self.contactNameArray = [@[] mutableCopy];
    
    
    [self getallcontacts];
    
    for (CNContact * contact in self.groupOfContacts) {
        
        NSMutableArray * myArr = [[NSMutableArray alloc] init];
        [myArr addObject:contact.givenName];
        
        NSArray * phoneNum = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
        [self.contactNameArray addObjectsFromArray:myArr];
        [self.phoneNumberArray addObjectsFromArray:phoneNum];
    } // for
    
    _FinalContact = _contactNameArray;
    _Finalnumber = _phoneNumberArray;
    
    namecount=[_FinalContact count];
    numbercount=[_Finalnumber count];
    
    
    if (namecount < numbercount) {
        differcount1=numbercount-namecount;
        
        NSLog(@"differcountis %lu",differcount1);
        
    }
    else if (namecount>numbercount){
        
        differcount2=namecount-numbercount;}
    NSLog(@"differcountis %lu",differcount2);
    
    
    
    for (NSUInteger i=0; i<differcount1; i++) {
        
        [_FinalContact addObject:@"NoName"];
        
    }
    
    for (NSUInteger i=0; i<differcount2; i++) {
        
        [_Finalnumber addObject:@"NoNumber"];
        
    }
    
}


-(void)getallcontacts{
    
    if ([CNContactStore class]) {
        CNContactStore * addressBook = [[CNContactStore alloc]init];
        
        NSArray * keysToFetch = @[CNContactGivenNameKey ,
                                  CNContactEmailAddressesKey,
                                  CNContactFamilyNameKey,
                                  CNContactPhoneNumbersKey,
                                  CNContactPostalAddressesKey];
        
        CNContactFetchRequest * fetchRequest = [[CNContactFetchRequest alloc] initWithKeysToFetch:keysToFetch];
        
        [addressBook enumerateContactsWithFetchRequest:fetchRequest error:nil usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
            [self.groupOfContacts addObject:contact];
        }]; // Block
    } // if
} // getallcontacts


// for Posting Contacts
-(void)contactsWebServicesMethod{
    
    
    
    NSString*FinalName=[NSString new];
    NSString*FinalNumber=[NSString new];
    
    NSLog(@"count is%lu",(unsigned long)_FinalContact.count);
    
    for (NSInteger i=0; i<_FinalContact.count; i++) {
        
        if (i == 200) {
            break;
        }
        
        Names=_FinalContact[i];
        Numbers=_Finalnumber[i];
        
        NSLog(@"names are%@",Names);
        
        NSLog(@"numbers are%@",Numbers);
        
        if (Names.length==0) {
            Names=@"Namenotavailable";
        }
        
        Names=[Names stringByReplacingOccurrencesOfString:@"@" withString:@""];
        
        FinalName= [[[[[[[FinalName stringByAppendingString:Names] stringByAppendingString:@"SADDLY!@"]stringByReplacingOccurrencesOfString:@"+" withString:@""] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        if (Numbers.length==0) {
            Numbers=@"numberNotavailable";
        }
        
        FinalNumber=[[[[[[FinalNumber stringByAppendingString:Numbers]stringByAppendingString:@"SADDLY!@"]stringByReplacingOccurrencesOfString:@"+" withString:@""]stringByReplacingOccurrencesOfString:@"*" withString:@"" ]stringByReplacingOccurrencesOfString:@"#" withString:@""]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        
    }
    
    NSLog(@"numbers are%@",FinalNumber);
    NSLog(@"names are%@",FinalName);
    
    
    
    
    
    //
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/login/inserMobcontacts1"];
    
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*UUID=    [uuidDefaults valueForKey:@"uuidDefaults"];
    
    
    NSString * userId = user_mob;
    NSString * userPwd = _TxtPassword.text;
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    //     Request:contact_name,contact_mob,user_mob,login_status,unique_id,access_Token,from;
    
    NSDictionary * loginDetailsDict =  @{@"contact_name":FinalName,
                                         
                                         @"contact_mob":FinalNumber,
                                         @"from":@"iphone",
                                         @"user_mob":user_mob,
                                         @"login_status":@"iphone",
                                         @"access_Token":a123,
                                         @"unique_id":UUID
                                         
                                         };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            [LoaderClass removeLoader:self.view];
            
            
            NSLog(@"Killer Response is%@",Killer);
            
            
            
            NSLog(@"str Response is%@",str);
            
        });
        
        
    }];
    
    
    [dataTask resume];
}





-(void)Devicedetails {
    
    NSString *token = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", token);
    
    NSString*Number =user_mob;
    
    
    
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];

    
    NSString*devicemodel=deviceName;
    NSString*deviceversion=deviceVersion;
    NSString * ipAddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/login/insertdevicedetails1"];
    
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    
    NSString * userId = [@"966" stringByAppendingString:_TxtUserName.text];
    NSString * userPwd = _TxtPassword.text;
    
    NSUserDefaults * userid = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * userpwd = [NSUserDefaults standardUserDefaults];
    
    [userid setObject:userId forKey:@"userId"];
    [userpwd setObject:userPwd forKey:@"userPwd"];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*UUID=    [uuidDefaults valueForKey:@"uuidDefaults"];
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    //Request:user_mob,devicemodel,deviceversion,from,user_emailId,ipAddress,access_Tokenunique_id;
    
    
    
    NSDictionary * loginDetailsDict =  @{@"user_mob":Number,
                                         
                                         @"devicemodel":devicemodel,
                                         @"deviceversion":deviceversion,
                                         @"ipAddress":ipAddress,
                                         @"from":@"iPhone",
                                         @"access_Token":a123,
                                         @"unique_id":UUID
                                         
                                         };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            [LoaderClass removeLoader:self.view];
            
            
            //            NSLog(@"Killer Response is%@",Killer);
            
            
            
            NSLog(@"str Response is%@",str);
            
        });
        
        
    }];
    
    
    [dataTask resume];
}




// get OTP Webservices Method
-(void)UpdateDetails{
    
    
    NSUserDefaults * Guestnum = [NSUserDefaults standardUserDefaults];
    
    
    NSString*mobilenumber=[[@"966" stringByAppendingString:[Guestnum valueForKey:@"Guestnum"]] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSString*login_status=@"iphone_SA";
    
    NSString*username=[_TxtUserName.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSString*emailid=[_TxtEmailid.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSString*password=[_TxtconfirmPassword.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*ipaddress=  [[ipaddressdefault valueForKey:@"ipaddressdefault"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

    
    NSUserDefaults * latitudedef = [NSUserDefaults standardUserDefaults];
    latitude = [[latitudedef valueForKey:@"latitude"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    
    NSUserDefaults * longitudedef = [NSUserDefaults standardUserDefaults];
    longitude = [[longitudedef valueForKey:@"longitude"]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    
    NSUserDefaults*guestotp=[NSUserDefaults standardUserDefaults];
  NSString*otp=  [guestotp valueForKey:@"guestotp"];

    NSLog(@"%@",otp);
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
NSString* accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
NSString*uuid=  [uuidDefaults stringForKey:@"uuidDefaults"];
    

    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/profile/updateProfile"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[user_mob stringByAppendingString:@":"] stringByAppendingString:password];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    if (longitude.length <1) {
        longitude=@"0";
        
    }  if (latitude.length <1) {
        latitude=@"0";
        
    }
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    NSDictionary * loginDetailsDict =  @{@"user_name":username,
                                         @"user_email_id":emailid,
                                         @"user_pwd":password,
                                         @"ipAddress":ipaddress,
                                         @"user_mob":mobilenumber,
                                         @"otp":otp,
                                         @"access_token":accessToken,
                                         @"unique_id":uuid,
                                         @"from":@"iphone",
                                         };
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     otpFromServer2  =[Killer valueForKey:@"profileStatus"];
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",otpFromServer2);
            
            [LoaderClass removeLoader:self.view];
    
   
        
        NSLog(@"Update Profile Response is: %@",otpFromServer2);
        
        [LoaderClass removeLoader:self.view];
        
        NSMutableDictionary*RespDct=[[NSMutableDictionary alloc]init];
        
        [RespDct setObject:otpFromServer2 forKey:@"Message"];
    
        
        NSLog(@"Get Guest OTP response is :%@",RespDct);
        
            
            
            if ([[RespDct objectForKey:@"Message"] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"UpdateDetails";
                
                [self UpdateDetails];
                
                
            }else if ([[RespDct objectForKey:@"Message"] isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{

                
                if ([[RespDct objectForKey:@"Message"] isEqualToString:@"something went wrong"]) {
                    
                    alertMessage = [[[MCLocalization stringForKey:@"wentWrong"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
                    [self alertMethod];
                    
                } else if ([[RespDct objectForKey:@"Message"] isEqualToString:@"failure"]) {
                    
                    alertTitle =  [MCLocalization stringForKey:@"failure"];
                    alertMessage = [[[MCLocalization stringForKey:@"wentWrong"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
                    [self alertMethod];
                    
                } else if ([[RespDct objectForKey:@"Message"] isEqualToString:@"Email Id already exist"]) {
                    
                    toastMsg = [[[MCLocalization stringForKey:@"emailalreadytaken"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"Chooseanotherone"]];
                    [self toastMessagemethod];
                    
                } else if ([[RespDct objectForKey:@"Message"] isEqualToString:@"invalid otp"]) {
                    
                    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"SA"]) {
                        toastMsg = @"Pleasen enter correct otp";
                        [self toastMessagemethod];
                        
                    }else{
                        toastMsg=@" يرجى إدخال كلمة مرور صحيحة";
                        
                        [self toastMessagemethod];
                    }
                    
                }else if ([[RespDct objectForKey:@"Message"] isEqualToString:@"otp expired"]) {
                    
                    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"SA"]) {
                        toastMsg = @"Otp is expired";
                        [self toastMessagemethod];
                        
                    }else{
                        toastMsg=@"كلمة المرور المؤقته غير متاحه";
                        [self toastMessagemethod];
                    }
                }
                else if ([[RespDct objectForKey:@"Message"] isEqualToString:@"success"]) {
                    
                    
                    [self DeviceToken];
                    [self contactsToStore];
                    [self contactsWebServicesMethod];
                    [self Devicedetails];
                    
                    [self performSegueWithIdentifier:@"GuestHome" sender:nil];
                    
                }
                
                
        
            }
            
        });
        
    }];
    
    [dataTask resume];
    ////////////
    
}



- (IBAction)ShowTxtpass:(UIButton *)sender {
    
    sender.selected  =! sender.selected;
    
    if (sender.selected) {
        
        _TxtPassword.secureTextEntry = NO;
        [_btnPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        [_btnPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        
    } else {
        
        _TxtPassword.secureTextEntry = YES;
        [_btnPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        [_btnPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        
    }
}



- (IBAction)ShowTxtcnfpass:(UIButton *)sender {
    
    sender.selected  =! sender.selected;
    
    if (sender.selected) {
        
        _TxtconfirmPassword.secureTextEntry = NO;
        [_btnConfPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        [_btnConfPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        
    } else {
        
        _TxtconfirmPassword.secureTextEntry = YES;
        [_btnConfPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        [_btnConfPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        
    }
    
}








- (IBAction)Done:(id)sender {
    
    if ([_TxtUserName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].
        length == 0){
        toastMsg = [MCLocalization stringForKey:@"Namealert"];
        [self toastMessagemethod];
    } else if ([_TxtEmailid.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg = [MCLocalization stringForKey:@"Mailidvalidate"];
        [self toastMessagemethod];
    }
    else if (![Validate isValidEmailId:[_TxtEmailid.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg = [MCLocalization stringForKey:@"Mailidnotvalid"];
        [self toastMessagemethod];
    }
    // Password
    else if ([_TxtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg = [MCLocalization stringForKey:@"Passwordvalidate"];
        [self toastMessagemethod];
    } else if (_TxtPassword.text.length < 8){
        toastMsg = [MCLocalization stringForKey:@"pwd6char"];
        [self toastMessagemethod];
    }
    // Confirm password
    else if ([_TxtconfirmPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg =   [MCLocalization stringForKey:@"RegConfirmpassword"];
        [self toastMessagemethod];
    } else if (_TxtconfirmPassword.text.length < 8){
        toastMsg = [MCLocalization stringForKey:@"cnfrmpwd6char"];
        [self toastMessagemethod];
    }
    // Comparising passwords
    else if (![_TxtconfirmPassword.text isEqualToString:_TxtPassword.text]) {
        toastMsg = [MCLocalization stringForKey:@"Passmatch"];
        [self toastMessagemethod];
    }
    else{
    
        [self UpdateDetails];
    }
    
}


    
@end
