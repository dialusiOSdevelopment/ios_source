//
//  MyProfileVC.h
//  Saddly
//
//  Created by Sai krishna on 1/19/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuVC.h"
#import "RechargeHistoryVC.h"
#import "UpdataProfileVC.h"
#import "UpdatePwdVC.h"

@interface MyProfileVC : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    
    NSArray * profileOptions, * profileOptionsImages;
    
    NSString  * imgSelected, * url, * appendedUrl;
    UIImagePickerController * picImg;

    NSDictionary * profileDict;

    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage;

}


@property (strong, nonatomic) IBOutlet UIImageView *imgProfileImage;

@property (strong, nonatomic) IBOutlet UITableView *tblOptions;

- (IBAction)btnChangeProfilePic:(id)sender;


@end
