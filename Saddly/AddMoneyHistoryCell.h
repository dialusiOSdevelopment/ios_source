//
//  AddMoneyHistoryCell.h
//  DialuzApp
//
//  Created by Sai krishna on 1/9/17.
//  Copyright © 2017 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddMoneyHistoryCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblAMStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblAMRepMsg;
@property (strong, nonatomic) IBOutlet UILabel *lblAMAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblAMMobNum;

@property (strong, nonatomic)  NSString * AMResMsg;
@property (strong, nonatomic)  NSString * AMOperator;
@property (strong, nonatomic)  NSString * AMOrderId;
@property (strong, nonatomic)  NSString * AMDateTime;



@property (strong, nonatomic)  NSString * AMAmountFromCard;
@property (strong, nonatomic)  NSString * AMAmountFromWallet;



@property (strong, nonatomic) IBOutlet UIImageView *ImgAM;


@end
