//
//  AboutUsVC.m
//  DialuzApp
//
//  Created by Sai krishna on 8/25/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import "AboutUsVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface AboutUsVC ()

@end

@implementation AboutUsVC


- (void)viewDidLoad {
    
    self.title = [MCLocalization stringForKey:@"SlidearrAbtus"];
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    NSString * staticUrl;
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        staticUrl = @"http://www.dialus.com/english/images/www/saddly/aboutus/aboutus_ar.png";
        
    } else {
        
        staticUrl = @"http://www.dialus.com/english/images/www/saddly/aboutus/aboutus_sa.png";
        
    }
    
    NSURL * url = [NSURL URLWithString:staticUrl];
    NSData * imageData = [[NSData alloc] initWithContentsOfURL:url];

    _ImgAboutus.image = [UIImage imageWithData:imageData];


    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 






@end
