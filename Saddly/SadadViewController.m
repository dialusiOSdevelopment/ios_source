//
//  SadadViewController.m
//  DialuzApp
//
//  Created by gandhi on 29/12/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import "SadadViewController.h"

@interface SadadViewController ()

@end

@implementation SadadViewController

- (void)viewDidLoad {
    
    
    //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Add Money SADAD Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //

    
    self.title = [MCLocalization stringForKey:@"sadad"];
    self.navigationItem.hidesBackButton = YES;

    
    [_btnDoneOutlet setTitle:[MCLocalization stringForKey:@"done"] forState:UIControlStateNormal];
    
    NSUserDefaults * UrlDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString*URl=   [UrlDefaults valueForKey:@"url"];
    
    url = [NSURL URLWithString:URl];
    
    
    NSString*  postString = [NSString stringWithFormat:@"%@", URl];
    
    NSString*   postString1 = [postString stringByAddingPercentEscapesUsingEncoding:(NSStringEncoding)NSUTF8StringEncoding];
    
    [_WebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:postString1]]];
    
}


-(void)viewDidAppear:(BOOL)animated {
        
    self.navigationItem.hidesBackButton = YES;
    
}





@end


