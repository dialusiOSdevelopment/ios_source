//
//  QuickRechargeVC.h
//  Saddly
//
//  Created by Sai krishna on 7/17/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WebServicesMethods.h"
#import "MCLocalization.h"
#import "ConnectivityManager.h"
#import "LoaderClass.h"
#import "UIView+Toast.h"
#import "Validate.h"
#import "QuickRcCollectionCell.h"
#import "LoginViewController.h"

@interface QuickRechargeVC : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate, NSURLSessionDelegate> {
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle,* alertMessage, * toastMsg;
    
    NSArray * operatorNames, * operatorImages;
    NSString * selOperatorName, * voucherCode;
    
    NSDictionary * profileDict, * iqamaDetails;
    
    NSString * methodname, * a123;
    
    
    
}

@property (strong, nonatomic) NSString * voucherCodeFrmSuccess;
@property (strong, nonatomic) NSString * iqmaNumberFrmSuccess;
@property (strong, nonatomic) NSString * operatorFrmSuccess;


@property (strong, nonatomic) IBOutlet UICollectionView *collecOperators;

@property (strong, nonatomic) IBOutlet UITextField *txtVoucherCode;
@property (strong, nonatomic) IBOutlet UITextField *txtIqmaNumber;
@property (strong, nonatomic) IBOutlet UIButton *btnRcoutlet;
@property (strong, nonatomic) IBOutlet UILabel *lblUSSDCode;

@property (strong, nonatomic) IBOutlet UILabel *lblSelOperatorName;



- (IBAction)btnRecharge:(id)sender;
- (IBAction)btnCopy:(id)sender;


@property (strong, nonatomic) IBOutlet UILabel *lblVoucherCodeName;
@property (strong, nonatomic) IBOutlet UILabel *lblIqmaNumName;

@property (strong, nonatomic) IBOutlet UIButton *btnCopyOutlet;



@end
