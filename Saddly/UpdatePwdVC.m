//
//  UpdatePwdVC.m
//  Saddly
//
//  Created by Sai krishna on 8/7/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "UpdatePwdVC.h"

@interface UpdatePwdVC ()

@end

@implementation UpdatePwdVC

- (void)viewDidLoad {
    
    
    self.title = [MCLocalization stringForKey:@"changePwd"];
    
    // Names
    _lblOldPwd.text = [MCLocalization stringForKey:@"oldpwd"];
    _lblNewPwd.text = [MCLocalization stringForKey:@"newpwd"];
    _lblConfirmPwd.text = [MCLocalization stringForKey:@"confPwd"];
    _txtCurrentPwd.placeholder = [MCLocalization stringForKey:@"enteroldpwd"];
    _txtNewPwd.placeholder = [MCLocalization stringForKey:@"enternewpwd"];
    _txtConfirmPwd.placeholder = [MCLocalization stringForKey:@"reenternewPwd"];
    
    [_btnUpdatePwdOutlet setTitle:[MCLocalization stringForKey:@"update"] forState:UIControlStateNormal];
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    

    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



-(void)removingKeyboardByTap {
    
    [self.view endEditing:YES];
}


//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:_txtCurrentPwd] || [textField isEqual:_txtNewPwd] || [textField isEqual:_txtConfirmPwd]){
        if([string isEqualToString:@" "]){
            // Returning no here to restrict whitespace
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return TRUE && newLength <= 20;
    }
    return YES;
}



// Returning to Next Text fields
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtCurrentPwd) {
        [self.txtNewPwd becomeFirstResponder];
    } else if (textField == self.txtNewPwd) {
        [self.txtConfirmPwd becomeFirstResponder];
    }else if (textField == self.txtConfirmPwd) {
        [self.txtConfirmPwd resignFirstResponder];
    }
    return YES;
}


// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}




- (IBAction)btnOldShowPwd:(UIButton *)sender {
    
    sender.selected  = ! sender.selected;
    if (sender.selected)
    {
        _txtCurrentPwd.secureTextEntry = NO;
        [_btnOldShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        [_btnOldShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
    } else {
        _txtCurrentPwd.secureTextEntry = YES;
        [_btnOldShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        [_btnOldShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
    }
    
    
    
}

- (IBAction)btnNewShowPwd:(UIButton *)sender {
    
    sender.selected  = ! sender.selected;
    if (sender.selected)
    {
        _txtNewPwd.secureTextEntry = NO;
        [_btnNewShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        [_btnNewShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
    } else {
        _txtNewPwd.secureTextEntry = YES;
        [_btnNewShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        [_btnNewShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
    }
    
    
    
}

- (IBAction)btnReEnterNewShowPwd:(UIButton *)sender{
    
    sender.selected  = ! sender.selected;
    if (sender.selected)
    {
        _txtConfirmPwd.secureTextEntry = NO;
        [_btnReEnterNewShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        [_btnReEnterNewShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
    } else {
        _txtConfirmPwd.secureTextEntry = YES;
        [_btnReEnterNewShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        [_btnReEnterNewShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
    }
    
    
    
}





-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
  
       
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"ForgetPasswordOTPVeri"]) {
                    
                    [self ForgetPasswordOTPVeri];
                    
                }
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//



-(void)ForgetPasswordOTPVeri {
    
    NSString*userMobNum= [profileDict valueForKey:@"user_mob"];
    NSString*db= [MCLocalization stringForKey:@"DBvalue"];;
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    
    
    [LoaderClass showLoader:self.view];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/forgotPassword/forgotpasswordOTPVerification"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    

    
    NSDictionary * forPwdOTPDetailsDict =  @{@"usermob":userMobNum,
                                             @"db":db,
                                             @"from":@"iPhone",
                                             @"ipAddress":Ipaddress,
                                             @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                             @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                             };
    
    NSLog(@"Posting forPwdOTPDetailsDict is %@",forPwdOTPDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:forPwdOTPDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            NSError *deserr;
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"str  Response is%@",str);
            NSLog(@"Killer  Response is :%@",Killer);
            
            
            [LoaderClass removeLoader:self.view];
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"ForgetPasswordOTPVeri";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                } else if ([[Killer valueForKey:@"response"] isEqualToString:@"Success"]) {
                    
                    
                    UpdatePwdOTPVC * updatePwdOtp = [self.storyboard instantiateViewControllerWithIdentifier:@"UpdatePwdOTPVC"];
                    updatePwdOtp.updateOTPPwd = _txtConfirmPwd.text;
                    [self.navigationController pushViewController:updatePwdOtp animated:YES];
                    
                    
                } else if ([[Killer valueForKey:@"response"] isEqualToString:@"failure"]) {
                    
                    
                    toastMsg = [[[MCLocalization stringForKey:@"wentWrong"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
                    [self toastMessagemethod];
                    
                } else if ([[Killer valueForKey:@"response"] isEqualToString:@"some thing went wrong"]) {
                    
                    toastMsg = [[[MCLocalization stringForKey:@"wentWrong"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
                    [self toastMessagemethod];
                    
                }
                
            }
            
        });
        
    }];
    
    [dataTask resume];
    
}


    



- (IBAction)btnUpdatePwd:(id)sender {
    
    // Old Password
    if ([_txtCurrentPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg = [MCLocalization stringForKey:@"plsEntoldPwd"];
        [self toastMessagemethod];
    } else if (_txtCurrentPwd.text.length < 8){
        toastMsg = [MCLocalization stringForKey:@"oldPwd6Char"];
        [self toastMessagemethod];
    }
    // New Password
    else if ([_txtNewPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg = [MCLocalization stringForKey:@"plsEntNewPwd"];
        [self toastMessagemethod];
    } else if (_txtNewPwd.text.length < 8){
        toastMsg = [MCLocalization stringForKey:@"newPwd6Char"];
        [self toastMessagemethod];
    }
    // Confirm password
    else if ([_txtConfirmPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg =   [MCLocalization stringForKey:@"confirmpass"];
        [self toastMessagemethod];
    } else if (_txtConfirmPwd.text.length < 8){
        toastMsg = [MCLocalization stringForKey:@"cnfrmpwd6char"];
        [self toastMessagemethod];
    }
    // Comparising new & Confirm passwords
    else if (![_txtConfirmPwd.text isEqualToString:_txtNewPwd.text]) {
        toastMsg = [MCLocalization stringForKey:@"newconpwdmatch"];
        [self toastMessagemethod];
    }
    // Comparising Old & Confirm passwords
    else if ([_txtConfirmPwd.text isEqualToString:_txtCurrentPwd.text]) {
        toastMsg = [MCLocalization stringForKey:@"oldconfpwdsame"];
        [self toastMessagemethod];
    } else {
        [self ForgetPasswordOTPVeri];
    }
}





@end
