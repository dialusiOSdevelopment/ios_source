//
//  ForgotPwdVC.m
//  Saddly
//
//  Created by Sai krishna on 1/20/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "ForgotPwdVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface ForgotPwdVC ()

@end

@implementation ForgotPwdVC

- (void)viewDidLoad {
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
        
    }

    
    self.title = [MCLocalization stringForKey:@"Forgetpasswordtitle"];
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];

    
    _lblGreetings. text = [MCLocalization stringForKey:@"Forgetpasswordtitle"];
    _lblMobNumName.text = [MCLocalization stringForKey:@"txt_password"];
    _txtViewMsg.text = [MCLocalization stringForKey:@"wesentpwdtoMobNum"];
    _txtMobNum.placeholder = [MCLocalization stringForKey:@"mobNum"];

    _lblMobNumName.adjustsFontSizeToFitWidth = YES;
    [_btnSendOutlet setTitle:[MCLocalization stringForKey:@"sendName"] forState:UIControlStateNormal];


    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    // Returning Keyboard For Number Pad.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(removingKeyboardByTap)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    _txtMobNum.inputAccessoryView = keyboardDoneButtonView;
    

    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}

// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = self.view.bounds;
    
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];


        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}




-(void) checkTokenStatusWebServices {
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":@"",
                                       @"user_mob":[@"966" stringByAppendingString:_txtMobNum.text],
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"fresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                [self ForgetPasswordServices];
                
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//



-(void)ForgetPasswordServices{
    
    NSString*userMobNum= [@"966" stringByAppendingString: _txtMobNum.text ];
    NSString*db= [MCLocalization stringForKey:@"DBvalue"];;
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    [LoaderClass showLoader:self.view];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/forgotPassword/getOTPForForgotPassword"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSDictionary * sendMoneyDetailsDict =  @{@"usermob":userMobNum,
                                             @"db":db,
                                             @"from":@"iPhone",
                                             @"ipAddress":Ipaddress,
                                             @"access_token":a123,
                                             @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                             };
    
    NSLog(@"Posting sendMoneyDetailsDict is %@",sendMoneyDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:sendMoneyDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            

            
            NSError *deserr;
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];

            
            
            NSLog(@"str  Response is%@",str);
            NSLog(@"Killer  Response is :%@",Killer);
            
            
            [LoaderClass removeLoader:self.view];
            

            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"ForgetPasswordServices";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                } else if ([[Killer valueForKey:@"response"] isEqualToString:@"Success"]) {
//                        
//                        toastMsg = [MCLocalization stringForKey:@"pwdRegMobNumEmail"];
//                        [self toastMessagemethod];
//                        
//                        [NSTimer scheduledTimerWithTimeInterval: 3.0
//                                                         target: self
//                                                       selector:@selector(onTick:)
//                                                       userInfo: nil repeats:NO];
                    
                    
                    ForgotPwdMobNumVC * forMobNum = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPwdMobNumVC"];
                    forMobNum.forgotMobnum = [@"966" stringByAppendingString:_txtMobNum.text];
                    [self.navigationController pushViewController:forMobNum animated:YES];
                    
                        
                } else if ([[Killer valueForKey:@"response"] isEqualToString:@"failure"]) {
//                        
//                        toastMsg = [MCLocalization stringForKey:@"PleaseEnterValidEmail/Mobile"];
//                        [self toastMessagemethod];
                    
                    toastMsg = [[[MCLocalization stringForKey:@"wentWrong"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
                    [self toastMessagemethod];

                } else if ([[Killer valueForKey:@"response"] isEqualToString:@"some thing went wrong"]) {
                        
                        toastMsg = [[[MCLocalization stringForKey:@"wentWrong"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
                        [self toastMessagemethod];
                        
                }

            }
             
        });
        
    }];
    
    [dataTask resume];
    
}




-(void)onTick:(NSTimer *)timer {
    
    LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:login animated:YES];
    
}

 



//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:_txtMobNum]) {
        
        if([self isNumeric:string]){
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 9;
        } else  {
            return FALSE;
        }
        
    }
    return YES;
}


// Entering only numbers not a special Characters
-(BOOL)isNumeric:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}



- (BOOL)validatePhone:(NSString *)phoneNumber {
    NSString * phoneRegex = @"^((\\+)|(5))[0-9]{8}$";
    NSPredicate * phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phoneNumber];
}




- (IBAction)btnSend:(id)sender {
    
    
    if ([_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg =  [MCLocalization stringForKey:@"Mobilealert"];
        [self toastMessagemethod];
    }
    else if (![Validate isValidMobileNumber:[_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg =   [MCLocalization stringForKey:@"mobilevalidate"];
        [self toastMessagemethod];
    }
    else if (![self validatePhone:[_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg =   [MCLocalization stringForKey:@"startWith5"];
        [self toastMessagemethod];
    } else {

        [self checkTokenStatusWebServices];
        
        
    }
}
                       
                       
                       
@end
