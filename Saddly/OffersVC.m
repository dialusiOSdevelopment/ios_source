//
//  OffersVC.m
//  Saddly
//
//  Created by Sai krishna on 1/21/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "OffersVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface OffersVC ()

@end

@implementation OffersVC


- (void)viewDidLoad {
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];

    
    
    self.title = [MCLocalization stringForKey:@"offers"];
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated {
    
    [self offerNamesServices];
    
}



-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
            
            NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
            [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
            
            
            if ([methodname isEqualToString:@"offerNamesServices"]) {
                [self offerNamesServices];
            }
          
            
            
            
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//




// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = self.view.bounds;
    
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}





-(void)offerNamesServices{

    

    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary*   profileDict1 = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString*UEmail= [profileDict1 valueForKey:@"user_email_id"];
    NSString*uMob=   [profileDict1 valueForKey:@"user_mob"];
    
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSString*from=@"iphone";
    
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    
    
    
    [LoaderClass showLoader:self.view];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/getOffersDetails"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[uMob stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * offersDetailsDict =  @{@"user_mob":uMob,
                                            @"user_emailId":UEmail,
                                            @"ipAddress":Ipaddress,
                                            @"from":from,
                                            @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                            @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                            };
    
    NSLog(@"Posting offersDetailsDict  is %@",offersDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:offersDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError * deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Response is%@",Killer);
            
//            NSLog(@"Response is%@",str);
            
            
            
            
            [LoaderClass removeLoader:self.view];
            
            NSLog(@"offers Details Response: %@",[Killer valueForKey:@"rechargeoffers"]);
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"offerNamesServices";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                }
                
                else{
            
            offerDetails = [Killer valueForKey:@"rechargeoffers"];
                    
                    NSLog(@"%@",offerDetails);
            
            if (offerDetails.count > 0) {
                
                                
                offerNames = [offerDetails valueForKey:@"offer_name"];
                
                height = 171.0f;
                [_collecOffers reloadData];
                
                NSLog(@"%@",offerNames);
                
            }
            
//            
//                    if (offerDetails.count > 0) {
//                        int i;
//                        
//                        for (i=0; i<offerDetails.count; i++) {
//                            NSArray*    offerName = [offerDetails valueForKey:@"offer_name"];
//                            
//                            offerNames=[offerName]
//                            
//                        }

                        else {
                
                UILabel * noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.collecOffers.bounds.size.width, self.collecOffers.bounds.size.height)];
                noDataLabel.textColor        = [UIColor whiteColor];
                noDataLabel.textAlignment    = NSTextAlignmentCenter;
                self.collecOffers.backgroundView = noDataLabel;
                self.collecOffers.backgroundColor =  [UIColor colorWithRed:2/255.0 green:181/255.0 blue:108/255.0 alpha:1.0];
                self.view.backgroundColor =  [UIColor colorWithRed:2/255.0 green:181/255.0 blue:108/255.0 alpha:1.0];
                
                noDataLabel.numberOfLines = 2;
                
                if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
                    
                    noDataLabel.text = @"لاتتوفر عروض حاليا";
                    
                } else {
                    
                    noDataLabel.text  = @"No Offers Available Right Now";
                }
                
                height = 0;
                
            }

                }
            }
        });
    }];

    [dataTask resume];
}






#pragma mark --
#pragma mark - UICollectionView Delegate Methods


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return offerNames.count;
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    NSString * staticUrl;
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        staticUrl = @"http://www.dialus.com/english/images/www/saddly/offer_images_AR/";
    } else {
        staticUrl = @"http://www.dialus.com/english/images/www/saddly/offer_images/";
        
    }
    
    NSString * imgJpgUrl = [staticUrl stringByAppendingString:[[offerNames objectAtIndex:indexPath.row] stringByAppendingString:@".png"]];
    
    NSURL * url = [NSURL URLWithString:imgJpgUrl];
    NSData * imageData = [[NSData alloc] initWithContentsOfURL:url];
    
    cell.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageWithData:imageData]];
    
    return cell;
    
}






- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if ([offerNames containsObject:@"Referalcode"]) {
        
        NSString * one;
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            
            one = @" تُستخدم العلاوة النقدية عند الشحن بمبلغ 100 ريال او اكثر مع الشحن القادم\n  بعد كل عملية تسجيل ناجحة برقم المشاركة ، سوف تحصل علي واحد ريال \n لا يوجد سقف على ارباحك \nليس هنالك سقف على أرقام المشاركة ";
            
        } else {
            
            one = @"No limit on number of referrals\nNo limit on your earnings\n\nAfter every successful registration with  your referal code you will get 1 SAR\nCashback can be used for 100SAR (OR) more in next recharge";
        }
        
        
        alert = [UIAlertController
                 alertControllerWithTitle:[MCLocalization stringForKey:@"howtoRedeem"]
                 message:one
                 preferredStyle:UIAlertControllerStyleAlert];
        
        alert.view.frame = [[UIScreen mainScreen] bounds];
        
        okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"shareTitle"]
                                            style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action) {
                                              
                                              ShareViewController * shareVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareViewController"];
                                              shareVC.hidesBottomBarWhenPushed = YES;
                                              [self.navigationController pushViewController:shareVC animated:YES];
                                          }];
        
        UIAlertAction * cancel = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"Forgetpasswordcancell"]
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action) {
                                                            
                                                            [self presentedViewController];
                                                            
                                                        }];
        
        [alert addAction:cancel];
        [alert addAction:okButton];
        [self presentViewController:alert animated:YES completion:nil];
        
    
    } else {
        
        [self presentedViewController];
        
    }
    
}





@end
