//
//  OperatorsVC.h
//  Saddly
//
//  Created by Sai krishna on 1/21/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectPackageVC.h"
#import "OperatorsCollectionCellVC.h"


@interface OperatorsVC : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, NSURLSessionDelegate> {
    
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    
    NSArray * operatorNames, * operatorImages, * operatorNamesGiftCards , * showingOperators,*operatorNamesAR;
    NSDictionary * operatorDetails;
    
    NSString * methodname, * a123;
    NSDictionary * profileDict;
    
}


@property (strong, nonatomic) IBOutlet UIImageView *imgOperatorbanner;

@property (strong, nonatomic) IBOutlet UILabel *lblSelOperatorName;
@property (strong, nonatomic) IBOutlet UICollectionView *collecOperators;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewOperaotrs;


@end
