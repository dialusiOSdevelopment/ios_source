//
//  WriteUsVC.h
//  Saddly
//
//  Created by Sai krishna on 6/9/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServicesMethods.h"
#import "LoaderClass.h"
#import "ConnectivityManager.h"
#import "MCLocalization.h"
#import "Validate.h"
#import "UIView+Toast.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "WriteUsSuccessVC.h"
#import "LoginViewController.h"

//@import AssetsLibrary;


@interface WriteUsVC : UIViewController <UITextFieldDelegate,UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, NSURLSessionDelegate > {
    
//    , UIDocumentPickerDelegate, UIDocumentMenuDelegate
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg, * db;
    UIAlertAction * action;
    NSDictionary * profileDict;
    
    
    UIImagePickerController * picImg;
    NSUInteger imgpicked;

    UIImage *profilePicImg;
    
    NSDictionary * issueTypesDetails;
    NSArray * arrIssueTypes;
    
    NSString * a123, * methodname;
}


@property (strong, nonatomic) UIView *loadingView;
@property (strong, nonatomic) UILabel *loadingLabel;
@property (strong, nonatomic) UIImageView * loadingimage;
@property (strong, nonatomic) NSString * imgChooseFileType;


@property (strong, nonatomic) UIDatePicker *myDatePicker;



@property (strong, nonatomic) IBOutlet UILabel *SlecturIssueType;
@property (strong, nonatomic) IBOutlet UILabel *AboutURIssue;
@property (strong, nonatomic) IBOutlet UILabel *EnterUrTransactionId;


@property (strong, nonatomic) IBOutlet UITextField *txtIssueType;
@property (strong, nonatomic) IBOutlet UITextField *txtTxnId;
@property (strong, nonatomic) IBOutlet UITextField *txtTxnDate;
@property (strong, nonatomic) IBOutlet UITextField *txtTxnTime;
@property (strong, nonatomic) IBOutlet UITextView *txtViewAboutissue;
@property (strong, nonatomic) IBOutlet UITextField *txtFileAttachment;

@property (strong, nonatomic) IBOutlet UIButton *btnSubmitOutlet;

@property (strong, nonatomic) IBOutlet UILabel *lblAttachmentBackground;


@property (strong, nonatomic) IBOutlet UILabel *lblMinChar75;


- (IBAction)btnIssueType:(id)sender;

- (IBAction)btnAttachment:(id)sender;
- (IBAction)btnSubmit:(id)sender;










@end
