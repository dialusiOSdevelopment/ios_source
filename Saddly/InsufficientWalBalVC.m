//
//  InsufficientWalBalVC.m
//  DialuzApp
//
//  Created by Sai krishna on 12/30/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import "InsufficientWalBalVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface InsufficientWalBalVC ()

@end

@implementation InsufficientWalBalVC

- (void)viewDidLoad {
    
    _lblTotAmoSent.adjustsFontSizeToFitWidth=YES;
    _lblActualCash.adjustsFontSizeToFitWidth=YES;
    _lblPayBalAmount.adjustsFontSizeToFitWidth=YES;
    _lblActCashName.adjustsFontSizeToFitWidth=YES;
    _lblSelPaymGateway.adjustsFontSizeToFitWidth=YES;
    _lblPayBalAmoName.adjustsFontSizeToFitWidth=YES;
    
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
    }
    
    
    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    self.title = [MCLocalization stringForKey:@"confirmdetails"];
    _txtPayment.hidden = YES;
    _txtEnterSadadId.hidden = YES;
    
 
    
    _lblActCashName.text = [MCLocalization stringForKey:@"actVal"];
    _lblPayBalAmoName.text = [MCLocalization stringForKey:@"payBalVal"];
    _lblSelPaymGateway.text = [MCLocalization stringForKey:@"selPaymentGateway"];
    _txtEnterSadadId.placeholder = [MCLocalization stringForKey:@"entersadadid"];
    
    [_btnCreditDebOutlet setTitle:[MCLocalization stringForKey:@"CREDIT"]   forState:UIControlStateNormal];
    [_DEBITATMNAME setTitle:[MCLocalization stringForKey:@"DEBIT/ATMCARD"] forState:UIControlStateNormal];
    [_btnSadadOutlet setTitle:[MCLocalization stringForKey:@"sadad"] forState:UIControlStateNormal];
    
    
    
    NSString * actualCashStr = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"actualCash"]];
    
    _lblActualCash.text = actualCashStr;
    
    
    NSUserDefaults * totalAmountSentDefaults = [NSUserDefaults standardUserDefaults];
    
    _lblTotAmoSent.text = [[MCLocalization stringForKey:@"totAmoSent"] stringByAppendingString:[totalAmountSentDefaults stringForKey:@"totalAmountSentDefaults"]];
    
    NSInteger i =  [[totalAmountSentDefaults stringForKey:@"totalAmountSentDefaults"] intValue] - [actualCashStr intValue];
    _lblPayBalAmount.text = [NSString stringWithFormat:@"%ld", (long)i];
    
    [_btnPayNowOutlet setTitle:[[MCLocalization stringForKey:@"proceedpay"] stringByAppendingString:_lblPayBalAmount.text] forState:UIControlStateNormal];
    
    
    [_btnCreditDebOutlet setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateSelected];
    [_btnCreditDebOutlet setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateNormal];
    
    [_DEBITATMNAME setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateSelected];
    
    [_DEBITATMNAME setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateNormal];
    
    [_btnSadadOutlet setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateSelected];
    [_btnSadadOutlet setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateNormal];
    
    [_btnCreditDebOutlet setSelected:NO];
    [_btnSadadOutlet setSelected:NO];
    [_DEBITATMNAME setSelected:NO];


    payFort	= [[PayFortController alloc] initWithEnviroment:KPayFortEnviromentProduction];
    deviceid=[payFort getUDID];
    
    [self signatureWebServicesMethod];

    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
   
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    
    // Returning Keyboard For Number Pad.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(NextClicked)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    _txtEnterSadadId.inputAccessoryView = keyboardDoneButtonView;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}

-(void)NextClicked{
    [self.txtEnterSadadId resignFirstResponder];
}



-(void)viewDidAppear:(BOOL)animated {
    
    
    _lblTotAmoSent.adjustsFontSizeToFitWidth=YES;
    _lblActualCash.adjustsFontSizeToFitWidth=YES;
    _lblPayBalAmount.adjustsFontSizeToFitWidth=YES;
    _lblActCashName.adjustsFontSizeToFitWidth=YES;
    _lblSelPaymGateway.adjustsFontSizeToFitWidth=YES;
    _lblPayBalAmoName.adjustsFontSizeToFitWidth=YES;
    
    
    
    if (test == 10) {
        [self performSegueWithIdentifier:@"HomeFrmInSuffBal" sender:nil];
        
    } else{

        self.navigationItem.hidesBackButton = NO;
    }

}

-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
        
            
                    a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                    
                    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                    [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                    
                    
                    if ([methodname isEqualToString:@"Sadadservices"]) {
                        [self Sadadservices];
                    }
                    else if ([methodname isEqualToString:@"paymentGatewaywebServicesMethod"]) {
                        [self checkTokenStatusWebServices];
                    }
                    else if ([methodname isEqualToString:@"signatureWebServicesMethod"]) {
                        [self signatureWebServicesMethod];
                    } else if ([methodname isEqualToString:@"returnUrl"]) {
                        [self returnUrl];
                    }

                    
                    
            
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//


// Moving the view Up & Down for the textfield
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    //move the main view, so that the keyboard does not hide it.
    if  (self.view.frame.origin.y < 0)
    {
        [self animateTextField: textField up: NO];
    }
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = textField.frame.origin.y / 2; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
} // Moving the view Up & Down for the textfield




// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}


-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];


        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}


 





//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:_txtEnterSadadId]){
        if([self isAlphaNumericSpecialCharacters:string])
        {
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 12;
            
        }
        else
            return FALSE;
    }
    return YES;
}


// Entering only alphaNumeric & Special Characters not a Arabic Characters
-(BOOL)isAlphaNumericSpecialCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._@-"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}


- (IBAction)btnCheckBox:(id)sender{
    
    
    switch ([sender tag]) {
        case 0:
            if([_btnCreditDebOutlet isSelected]==YES) {
                
                [_btnCreditDebOutlet setSelected:YES];
                [_btnSadadOutlet setSelected:NO];
                [_DEBITATMNAME setSelected:NO];
                
            } else{
                [_btnCreditDebOutlet setSelected:YES];
                [_btnSadadOutlet setSelected:NO];
                [_DEBITATMNAME setSelected:NO];

            }

            _txtPayment.text = [MCLocalization stringForKey:@"CREDIT"];
            [self paymentGatewaywebServicesMethod];
            
            _btnPayNowOutlet.hidden=YES;
            _txtPayment.hidden = NO;
            _txtEnterSadadId.hidden = YES;
            
            break;
        case 1:
            if([_DEBITATMNAME isSelected]==YES) {
                
                [_DEBITATMNAME setSelected:YES];
                [_btnSadadOutlet setSelected:NO];
                [_btnCreditDebOutlet setSelected:NO];
                
            } else{
                [_DEBITATMNAME setSelected:YES];
                [_btnSadadOutlet setSelected:NO];
                [_btnCreditDebOutlet setSelected:NO];

            }
            
            _txtPayment.text = [MCLocalization stringForKey:@"DEBIT/ATMCARD"];
            [self paymentGatewaywebServicesMethod];
            
            _btnPayNowOutlet.hidden=YES;
            
            
            _txtPayment.hidden = NO;
            _txtEnterSadadId.hidden = YES;
            break;
            
        case 2:
            if([_btnSadadOutlet isSelected]==YES) {
                [_DEBITATMNAME setSelected:NO];

               
                [_btnCreditDebOutlet setSelected:NO];
                [_btnSadadOutlet setSelected:YES];
                
                
            } else{
                [_DEBITATMNAME setSelected:NO];

                [_btnCreditDebOutlet setSelected:NO];
                [_btnSadadOutlet setSelected:YES];
                
            }
            
        _txtPayment.text = [MCLocalization stringForKey:@"sadad"];

         _txtPayment.hidden = NO;
            _txtEnterSadadId.hidden = NO;

            break;
        default:
            break;
    }
    
    if ([_txtPayment.text isEqualToString:[MCLocalization stringForKey:@"sadad"]]) {
        NSLog(@"");
    }
    else{
    [LoaderClass showLoader:self.view];
    }
}


-(void)Sadadservices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * db = [MCLocalization stringForKey:@"DBvalue"];
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    
    
    [LoaderClass showLoader:self.view];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/AddMoney/addMoneyToWalletDetails"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    
    
    NSString*username=[[[[profileDict valueForKey:@"user_name"] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    NSString*mobile = [profileDict valueForKey:@"user_mob"];
    NSString * amount = _lblPayBalAmount.text;
   
    
    NSDictionary * ssadadServicesDetailsDict =  @{@"db":db,
                                                  @"username":username,
                                                  @"userMob":mobile,
                                                  @"amount":amount,
                                                  @"paymentMethod":@"SADAD",
                                                  @"ipAddress":Ipaddress,
                                                  @"login_status":@"iphone_SA",
                                                  @"order_description":@"AddMoney",
                                                  @"user_emailId":[profileDict valueForKey:@"user_email_id"],
                                                  @"sadad_username":_txtEnterSadadId.text,
                                                  @"couponCode":@"Nill",
                                                  @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                                  @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                                  
                                                 };
    
    NSLog(@"Posting ssadadServicesDetailsDict is %@",ssadadServicesDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:ssadadServicesDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *deserr;
            
            [LoaderClass removeLoader:self.view];
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*  str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Response is%@",Killer);
            
            NSLog(@"Response is%@",str);
            
            [LoaderClass removeLoader:self.view];
            
            
            NSString*URl=[Killer valueForKey:@"response_message"];
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"Sadadservices";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                }
                
                else{
            
            NSString*urlstring=[Killer valueForKey:@"transaction_url"];
            
            NSLog(@"URL is %@",URl);
            
            
            if (![URl isEqualToString:@"Success"]) {
                
                alertMessage=[MCLocalization stringForKey:@"OLPID"];
                [self alertMethod];
                
            } else {
                
                NSUserDefaults * UrlDefaults = [NSUserDefaults standardUserDefaults];
                [UrlDefaults setObject:urlstring forKey:@"url"];
                
                
                SendMoneySadadViewController * rechargesuccess = [self.storyboard instantiateViewControllerWithIdentifier:@"SendMoneySadadViewController"];
                
                [self.navigationController pushViewController:rechargesuccess animated:YES];
                
            
                }
            
            }
            }
        });
     
     }];
    
    [dataTask resume];
    
}



// Returning to Next Text fields
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
     if (textField == self.txtEnterSadadId) {
        [self.txtEnterSadadId resignFirstResponder];
    }     return YES;
}



// Webservices for Paymeny Gateway
-(void)paymentGatewaywebServicesMethod {
    
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    

    NSString * db = [MCLocalization stringForKey:@"DBvalue"];   
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * amountSentDefaults = [NSUserDefaults standardUserDefaults];
    
    
    
    [LoaderClass showLoader:self.view];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/addMoney/addMoneyToWalletDetails"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
  
    NSString * userName = [[[[profileDict valueForKey:@"user_name"] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    NSString*mobile = [profileDict valueForKey:@"user_mob"];
    
   
    
    // Saving Send Money Details Dictionary to NSUserDefaults.
    NSUserDefaults *sendMoneyDetailsDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * sendMoneyDict = [NSKeyedUnarchiver unarchiveObjectWithData:[sendMoneyDetailsDefaults objectForKey:@"sendMoneyDetailsDefaults"]];
    
    
    NSDictionary * addMoneyWalletDict =  @{@"db":db,
                                           @"username":userName,
                                           @"userMob":mobile,
                                           @"user_emailId":[profileDict valueForKey:@"user_email_id"],
                                           @"amount":[amountSentDefaults stringForKey:@"amountSentDefaults"],
                                           @"paymentMethod":_txtPayment.text,
                                           @"ipAddress":Ipaddress,
                                           @"login_status":@"iphone_SA",
                                           @"order_description":@"SendMoney",
                                           @"order_id":[sendMoneyDict valueForKey:@"orderId"],
                                           @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                           @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                          };
    
    NSLog(@"Posting addMoneyWalletDict is %@",addMoneyWalletDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:addMoneyWalletDict options:NSJSONWritingPrettyPrinted error:nil];
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            _btnPayNowOutlet.hidden=NO;
            
            
            NSLog(@"payment Gateway response is :%@",[Killer valueForKey:@"addMoneytowalletStatus"]);
            
            paymentGatewayDetails = [Killer valueForKey:@"addMoneytowalletStatus"];
            
            
            if ([[paymentGatewayDetails valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[paymentGatewayDetails valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"paymentGatewaywebServicesMethod";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                }
                
                else{

            
            [LoaderClass removeLoader:self.view];
            
            
            [self sendTestJsonCommand];
                    
                    
                }
                
            }
            
        });
        
    }];
    
    [dataTask resume];

    
}




 //For Signature
 
 -(void)signatureWebServicesMethod{
     
     
     
     NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
     
     
     NSString * db = [MCLocalization stringForKey:@"DBvalue"];
     NSString * userPwd = [pddefaults stringForKey:@"pddd"];
     NSString * uMob=   [profileDict valueForKey:@"user_mob"];
     
     
     
     NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
     NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
     
     
     NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
     NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
     
     
     [LoaderClass showLoader:self.view];
     
     
     // Create the URLSession on the default configuration
     NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
     
     NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
     
     
     
     // Setup the request with URL
     NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/getTokenSignatureDetails"];
     NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
     
     
     request.HTTPMethod = @"POST";
     [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
     [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
     
     
     NSString * encodeMobPwd = [[uMob stringByAppendingString:@":"] stringByAppendingString:userPwd];
     
     NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
     NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
     NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
     
     
     NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
     [request setValue:authValue forHTTPHeaderField:@"Authorization"];
     

     
     NSDictionary * tokenSignDetailsDict =  @{@"device_id":deviceid,
                                            @"db":db,
                                            @"ipAddress":Ipaddress,
                                            @"from":@"iphone",
                                            @"user_mob":uMob,
                                            @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                            @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                            };
     
     NSLog(@"Posting tokenSignDetailsDict is %@",tokenSignDetailsDict);
     
     
     NSData *postdata = [NSJSONSerialization dataWithJSONObject:tokenSignDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
     
     [request setHTTPBody:postdata];
     
     
     // Create dataTask
     NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
         
         if (!data) {
             
             NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
             NSLog(@"%@",userErrorText);
             return;
         }
         
         
         dispatch_async(dispatch_get_main_queue(), ^{
             
             
             
             NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
             
             NSLog(@"resSrt: %@", resSrt);
             NSError *deserr;
             
             
             NSDictionary*Response=[NSJSONSerialization
                                    JSONObjectWithData:data
                                    options:kNilOptions
                                    error:&deserr];
             
             
             NSLog(@"Response response is=%@", Response);
             
             
             NSLog(@"resSrt Response is %@",resSrt);
             
             
             
             if ([[Response valueForKey:@"Response"] isEqualToString:@"userUnauthorized"]) {
                 
                 NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                 [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                 
                 NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                 [loginName removeObjectForKey:@"userName"];
                 
                 
                 LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                 [self.navigationController pushViewController:login animated:YES];
                 
             } else  {
                 
                 
                 if ([[Response valueForKey:@"Response"]isEqualToString:@"TokenExpired"]) {
                     
                     methodname=@"signatureWebServicesMethod";
                     
                     NSLog( @"Method Name %@",methodname);
                     [self checkTokenStatusWebServices];
                     
                 }
                 
                 else{

             
             signature = [Response valueForKey:@"Response"];
             
             
             
             
             [self sendTestJsonCommand];
                 }}

         });
         
     }];
     
     [dataTask resume];

 
}




-(void)sendTestJsonCommand{
    
    NSMutableDictionary * Tokendict = [[NSMutableDictionary alloc]init];
    
    [Tokendict setValue:@"SDK_TOKEN" forKey:@"service_command"];
    [Tokendict setValue:[payFort getUDID] forKey:@"device_id"];
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        [Tokendict setValue:@"ar" forKey:@"language"];
    } else {
        [Tokendict setValue:@"en" forKey:@"language"];
    }
    
    [Tokendict setValue:[paymentGatewayDetails valueForKey:@"access_code"] forKey:@"access_code"];
    
    NSLog(@"Tokendictis%@",Tokendict);
    
    [Tokendict	setValue:[paymentGatewayDetails valueForKey:@"merchant_identifier"] forKey:@"merchant_identifier"];
    [Tokendict	setValue:signature forKey:@"signature"];
    
    NSError * error;
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:Tokendict options:NSJSONWritingPrettyPrinted error:&error];
    
    if (error) {
        NSLog(@"Error (%@), error: %@", Tokendict, error);
        return;
    }
    
    NSLog(@"now sending this dictionary...\n%@\n\n\n", Tokendict);
#define appService [NSURL \
URLWithString:@"https://paymentservices.payfort.com/FortAPI/paymentApi"]
    
    // Create request object
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:appService];
    
    // Set method, body & content-type
    request.HTTPMethod = @"POST";
    request.HTTPBody = jsonData;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setValue:
     [NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
//    NSString *apiKey  = @"cWZwbWdlcmZkdGV4em5kc2ZnZGdkZzpxZ2h4Y2ZnaHR5bmpta3U=";
//    
//    NSString * authValue = [NSString stringWithFormat:@"Basic %@", apiKey];
//    [request setValue:authValue forHTTPHeaderField:@"Authorization"];

    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *r, NSData *data, NSError *error) {
        
        if (!data) {
            
            NSLog(@"No data returned from server, error ocurred: %@", error);
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        NSError *deserr;
        responseDict = [NSJSONSerialization
                        JSONObjectWithData:data
                        options:kNilOptions
                        error:&deserr];
        
        NSLog(@"so, here's the responseDict: %@", responseDict);
        [LoaderClass removeLoader:self.view];
    }];
}
 



#pragma mark - Navigation

- (void)sdkResult:(id)response{
    
    test = 20;
    
    if([response isKindOfClass:[NSDictionary class]]){
        
        NSDictionary * responseDic = response;
        
        SDKresponsedict = responseDic;
        
        NSLog(@"response SDK..%@",response);
        
        [self returnUrl];

                
    } else if (response !=	nil	&&	![response	isEqualToString:@""] &&	![response	isEqualToString:@"nil"])	{
        ///	Invalid	Request	Error	Message
    } else	{
        ///	Unknown	Error	Including	Connectivity	Issues
    }
}


-(void)returnUrl {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    
    
    [LoaderClass showLoader:self.view];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/addMoney/addMoneyToWalletTransactionsDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSString*customer_name;
    
    if ([[SDKresponsedict valueForKey:@"customer_name"] length]>1) {
        
        customer_name=[[[SDKresponsedict valueForKey:@"customer_name"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    } else{
        
        customer_name=@"NA";
        
    }
    
    
    NSString*merchant_reference=[SDKresponsedict valueForKey:@"merchant_reference"];
    NSString*customer_email=[SDKresponsedict valueForKey:@"customer_email"];
    NSString * userName = [[[[profileDict valueForKey:@"user_name"] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    NSString*amount=[SDKresponsedict valueForKey:@"amount"];
    NSString*authorization_code=[SDKresponsedict valueForKey:@"authorization_code"];
    NSString*card_number=[SDKresponsedict valueForKey:@"card_number"];
    NSString*command=[[SDKresponsedict valueForKey:@"command"]stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSString*currency=[SDKresponsedict valueForKey:@"currency"];
    NSString*customer_ip=[SDKresponsedict valueForKey:@"customer_ip"];
    NSString*eci=[SDKresponsedict valueForKey:@"eci"];
    NSString*expiry_date=[SDKresponsedict valueForKey:@"expiry_date"];
    
    NSString*fort_id=[SDKresponsedict valueForKey:@"fort_id"];
    NSString*language=[SDKresponsedict valueForKey:@"language"];
    NSString*order_description=[SDKresponsedict valueForKey:@"order_description"];
    
    NSString*payment_option=[SDKresponsedict valueForKey:@"payment_option"];
    
    NSString*response_code=[SDKresponsedict valueForKey:@"response_code"];
    NSString*response_message=[[SDKresponsedict valueForKey:@"response_message"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSString*sdk_token=[SDKresponsedict valueForKey:@"sdk_token"];
    NSString*status=[SDKresponsedict valueForKey:@"status"];
    NSString*token_name=[paymentGatewayDetails valueForKey:@"token_name"];
    
    
    
    NSDictionary * addMonTxnDetailsDict =  @{@"order_id":merchant_reference,
                                             @"customer_email":customer_email,
                                             @"username":userName,
                                             @"amount":amount,
                                             @"authorization_code":authorization_code,
                                             @"card_number":card_number,
                                             @"command":command,
                                             @"currency":currency,
                                             @"ipAddress":customer_ip,
                                             @"customer_name":customer_name,
                                             @"eci":eci,
                                             @"expiry_date":expiry_date,
                                             @"fort_id":fort_id,
                                             @"language":language,
                                             @"order_description":order_description,
                                             @"payment_method":@"CreditCard",
                                             @"response_code":response_code,
                                             @"response_message":response_message,
                                             @"sdk_token":sdk_token,
                                             @"status":status,
                                             @"couponCode":@"Nill",
                                             @"db":@"SA",
                                             @"token_name":token_name,
                                             @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                             @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                             @"from":@"iPhone",
                                           };
    
    NSLog(@"Posting addMonTxnDetailsDict is %@",addMonTxnDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:addMonTxnDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            
            NSError *deserr;
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"str return Url Response is%@",str);
            NSLog(@"Killer return Url Response is :%@",Killer);
            
            
            NSLog(@"Killer return Url Response is :%@",[Killer valueForKey:@"walletTransactions"]);
            
            returnUrlDetails = [Killer valueForKey:@"walletTransactions"];
            
            
            if ([[returnUrlDetails valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[returnUrlDetails valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"returnUrl";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                }
                
                else{
                
                
                if ([[returnUrlDetails valueForKey:@"response_message"] isEqualToString:@"SendMoneySuccess"]) {
                    
                    alert = [UIAlertController
                             alertControllerWithTitle:[MCLocalization stringForKey:@"congrats"]
                             message:[MCLocalization stringForKey:@"amountSuccToFrnd"]
                             preferredStyle:UIAlertControllerStyleAlert];
                    
                    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          
                                                          [self performSegueWithIdentifier:@"HomeFrmInSuffBal" sender:nil];
                                                          
                                                      }];
                    [alert addAction:okButton];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                } else {
                    
                    alert = [UIAlertController
                             alertControllerWithTitle:[MCLocalization stringForKey:@"failure"]
                             message:[MCLocalization stringForKey:@"plsTryAgain"]
                             preferredStyle:UIAlertControllerStyleAlert];
                    
                    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          
                                                          
                                                          
                                                          [self performSegueWithIdentifier:@"HomeFrmInSuffBal" sender:nil];
                                                      }];
                    [alert addAction:okButton];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }}
            
        });
         
    }];
    
    [dataTask resume];

}


- (IBAction)btnPayNow:(id)sender {
    
    if ([_txtPayment.text isEqualToString:[MCLocalization stringForKey:@"sadad"]]) {
        
        if ([_txtEnterSadadId.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
            
            toastMsg = [MCLocalization stringForKey:@"plsentSadadId"];
            [self toastMessagemethod];
            
        } else if (_txtEnterSadadId.text.length < 6 || _txtEnterSadadId.text.length > 12){
            
            toastMsg = [MCLocalization stringForKey:@"sadad6-12char"];
            [self toastMessagemethod];
            
        }  else {
            
            [self Sadadservices];
        }
        
    } else{
    
    
    NSLog(@"%lu",[[responseDict valueForKey:@"sdk_token"] length]);
    
    if ([[responseDict valueForKey:@"sdk_token"] length]<1) {
        NSLog(@"123");
    } else {
        
    
    NSLog(@"%lu",[[paymentGatewayDetails valueForKey:@"access_code"] length]);
    
    if ([[paymentGatewayDetails valueForKey:@"access_code"] length]>0) {
        if ([_btnSadadOutlet isSelected] == NO && [_btnCreditDebOutlet isSelected] == NO &&[_DEBITATMNAME isSelected] == NO ) {
            
            toastMsg = [MCLocalization stringForKey:@"plsselPaygateway"];
            [self toastMessagemethod];
        } else if( [_txtPayment.text isEqualToString:[MCLocalization stringForKey:@"sadad"]]) {
            [_btnCreditDebOutlet setSelected:YES];
            [_btnSadadOutlet setSelected:NO];
            
            alertMessage = @"cmg soon";
            [self alertMethod];
        }
        else if([_lblPayBalAmount.text intValue] < 10) {
            
            toastMsg = [MCLocalization stringForKey:@"balamount10"];
            [self toastMessagemethod];
            
        }
        
        else {
            

            payFort.delegate = self;
            //if	you	need	to	switch	on	the	Payfort	Response	page
            payFort.IsShowResponsePage = YES;
            //if	you	need	to	set	custome	Payfort	view
            [payFort		setPayFortCustomViewNib:@"PayFortView2"];
            
            //Generate	the	request	dictionary	as	follow
            NSMutableDictionary	* requestDictionary = [[NSMutableDictionary alloc]init];
            
            NSString * email = [profileDict valueForKey:@"user_email_id"];
            
            [requestDictionary setValue:[paymentGatewayDetails objectForKey:@"amount"] forKey:@"amount"];
            [requestDictionary setValue:[paymentGatewayDetails objectForKey:@"command"] forKey:@"command"];
            [requestDictionary setValue:@"SAR" forKey:@"currency"];
            
            if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
                [requestDictionary setValue:@"ar" forKey:@"language"];
            } else {
                [requestDictionary setValue:@"en" forKey:@"language"];
            }
            
            [requestDictionary setValue:[responseDict valueForKey:@"sdk_token"] forKey:@"sdk_token"];
            [requestDictionary setValue:[paymentGatewayDetails objectForKey:@"orderId"] forKey:@"merchant_reference"];
            
            
            [requestDictionary setValue:email forKey:@"customer_email"];
            [requestDictionary setValue:[paymentGatewayDetails objectForKey:@"order_description"]  forKey:@"order_description"];
            
            [requestDictionary	setValue:[paymentGatewayDetails objectForKey:@"token_name"] forKey:@"token_name"];
            
            test= 10;
            
            
            //	Mandatory
            [payFort setPayFortRequest:requestDictionary];
            //make	the	call
            [payFort callPayFort:self];
            
            [self.navigationItem setHidesBackButton:YES animated:YES];
            
        }
    }
    else{
        NSLog(@"DOnt see the output");
    }
    
}

    }}

- (IBAction)DEBITATMCARD:(id)sender {
}
@end
