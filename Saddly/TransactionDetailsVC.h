//
//  TransactionDetailsVC.h
//  Saddly
//
//  Created by Sai krishna on 5/6/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MCLocalization.h"
#import "MobileRechargeVC.h"
#import "WalletVC.h"
#import "InvoiceDetailsVC.h"
#import "SendMoneyVC.h"

@interface TransactionDetailsVC : UIViewController <NSURLSessionDelegate> {
    
     NSString * methodname, * a123;
    
}

@property (strong, nonatomic) UIImage * txnImg;
@property (strong, nonatomic) NSString * txnStatus;
@property (strong, nonatomic) NSString * txnAmount;
@property (strong, nonatomic) NSString * txnMobNum;
@property (strong, nonatomic) NSString * txnResMsg;
@property (strong, nonatomic) NSString * txnOperator;
@property (strong, nonatomic) NSString * txnOrderId;
@property (strong, nonatomic) NSString * txnDate;
@property (strong, nonatomic) NSString * tagStr;
@property (strong, nonatomic) NSString * txnAmtFrmCard;
@property (strong, nonatomic) NSString * txnAmtFrmWallet;
@property (strong, nonatomic) NSString * txnRefundedAmount;






@property (strong, nonatomic) IBOutlet UIImageView *imgOperator;


@property (strong, nonatomic) IBOutlet UILabel *lblStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblMobNum;
@property (strong, nonatomic) IBOutlet UILabel *lblResMsg;
@property (strong, nonatomic) IBOutlet UILabel *lblOperator;
@property (strong, nonatomic) IBOutlet UILabel *lblWallet;
@property (strong, nonatomic) IBOutlet UILabel *lblCard;



@property (strong, nonatomic) IBOutlet UILabel *lblOrderId;
@property (strong, nonatomic) IBOutlet UILabel *lblTransactedOn;


// Names
@property (strong, nonatomic) IBOutlet UILabel *lblSaudiName;
@property (strong, nonatomic) IBOutlet UILabel *lblPaidUsingName;
@property (strong, nonatomic) IBOutlet UILabel *lblOrderIdName;
@property (strong, nonatomic) IBOutlet UILabel *lblTransactedOnName;
@property (strong, nonatomic) IBOutlet UILabel *lblSeperator;





@property (strong, nonatomic) IBOutlet UIView *viewWallet;
@property (strong, nonatomic) IBOutlet UILabel *walLblOrderIdName;
@property (strong, nonatomic) IBOutlet UILabel *walLblTransactedOnName;

@property (strong, nonatomic) IBOutlet UILabel *walLblOrderId;
@property (strong, nonatomic) IBOutlet UILabel *walLblTransactedOn;

@property (strong, nonatomic) IBOutlet UIButton *walBtnRepeatOutlet;

- (IBAction)walBtnRepeat:(id)sender;


@property (strong, nonatomic) IBOutlet UIView *viewRefund;

@property (strong, nonatomic) IBOutlet UILabel *lblRefundReceivedName;
@property (strong, nonatomic) IBOutlet UILabel *lblMobRc;
@property (strong, nonatomic) IBOutlet UILabel *lblMobRcName;





@property (strong, nonatomic) IBOutlet UIButton *btnRepeatOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnInvoiceOutlet;

- (IBAction)btnRepeat:(id)sender;
- (IBAction)btnInvoice:(id)sender;


@end
