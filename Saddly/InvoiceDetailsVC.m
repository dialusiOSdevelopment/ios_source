//
//  InvoiceDetailsVC.m
//  Saddly
//
//  Created by Sai krishna on 5/12/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "InvoiceDetailsVC.h"

@interface InvoiceDetailsVC ()

@end

@implementation InvoiceDetailsVC

- (void)viewDidLoad {
    
    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    NSString * userMobNum = [profileDict valueForKey:@"user_mob"];
    
    
    //    btnSaddlyImage
    
    _lblOrderNum.text = _inOrderId;
    _lblDate.text = _inDate;
    _lblUserMobNum.text = userMobNum;
    _lblRcAmount.text = _inRcAmount;
    _lblValueFrmWallet.text = [_inValueFromWallet stringByAppendingString:@" SAR"];
    _lblValueFromCard.text = [_inValueFromCard stringByAppendingString:@" SAR"];
    _lblTotal.text = _inTotal;
    
    _btnTrustName.titleLabel.text = @"في سددلي نحن القيمة التي تثق بها .المال هو مالك حتي تحصل علي شئ يمكن الدفع من اجله";
    
    
    _lblRcMobNum.text = [[[@"Recharge of " stringByAppendingString:_inOperator] stringByAppendingString:@" Mob Num "] stringByAppendingString:_inRcMobNum];
    
    
    _lblTxnReceiptName.adjustsFontSizeToFitWidth = YES;
    _lblOrderNum.adjustsFontSizeToFitWidth = YES;
    _lblDate.adjustsFontSizeToFitWidth = YES;
    _lblUserMobNum.adjustsFontSizeToFitWidth = YES;
    _lblAddress.adjustsFontSizeToFitWidth = YES;
    
    _lblRcMobNum.adjustsFontSizeToFitWidth = YES;
    _lblRcAmount.adjustsFontSizeToFitWidth = YES;
    
    _lblValueFrmWalletName.adjustsFontSizeToFitWidth = YES;
    _lblValueFrmWallet.adjustsFontSizeToFitWidth = YES;
    
    _lblValueFrmCardName.adjustsFontSizeToFitWidth = YES;
    _lblValueFromCard.adjustsFontSizeToFitWidth = YES;
    
    _lblTotalName.adjustsFontSizeToFitWidth = YES;
    _lblTotal.adjustsFontSizeToFitWidth = YES;
    _btnTrustName.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    
    _lblAddress.lineBreakMode = NSLineBreakByWordWrapping;
    _lblAddress.numberOfLines = 2;
    
    _lblRcMobNum.lineBreakMode = NSLineBreakByWordWrapping;
    _lblRcMobNum.numberOfLines = 2;
    
    _btnTrustName.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btnTrustName.titleLabel.numberOfLines = 0;
    
    


    UIView *view=self.view;
    NSString*name=@"Saddly Invoice";
    
    [self createPDFfromUIView:view saveToDocumentsWithFileName:name];
    [self createPDFfromUIView:self.view saveToDocumentsWithFileName:@"Saddly Invoice.pdf"];
    

    [LoaderClass showLoader:self.view];
    self.navigationController.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.view.hidden = YES;


   
    noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    noDataLabel.text             = @"Please Wait...\nWe are Preparing the InVoice";
    noDataLabel.numberOfLines = 2;
    noDataLabel.textColor        = [UIColor blackColor];
    noDataLabel.textAlignment    = NSTextAlignmentCenter;
    [self.navigationController.view addSubview:noDataLabel];
    
    noDataLabel.hidden = NO;

    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






 




-(NSMutableData *)createPDFDatafromUIView:(UIView*)aView
{
    // Creates a mutable data object for updating with binary data, like a byte array
    NSMutableData *pdfData = [NSMutableData data];
    
    // Points the pdf converter to the mutable data object and to the UIView to be converted
    UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil);
    UIGraphicsBeginPDFPage();
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    
    
    // draws rect to the view and thus this is captured by UIGraphicsBeginPDFContextToData
    
    [aView.layer renderInContext:pdfContext];
    
    // remove PDF rendering context
    UIGraphicsEndPDFContext();
    
    return pdfData;
}





#pragma mark - UIDocumentInteractionControllerDelegate

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    return self;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller
{
    return self.view;
}

- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller
{
    return self.view.frame;
}





-(void)createPDFfromUIView:(UIView*)aView saveToDocumentsWithFileName:(NSString*)aFilename

{
    NSMutableData *pdfData = [NSMutableData data];
    
    UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil);
    UIGraphicsBeginPDFPage();
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    
    
    [aView.layer renderInContext:pdfContext];
    UIGraphicsEndPDFContext();
    
    NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
    
    NSString* documentDirectory = [documentDirectories objectAtIndex:0];
    documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:aFilename];
    NSString *file = [documentDirectory stringByAppendingPathComponent:@"Saddly Invoice.pdf"];
    NSURL *urlPdf = [NSURL fileURLWithPath: file];
    
    [pdfData writeToFile:documentDirectoryFilename atomically:YES];
    NSLog(@"documentDirectoryFileName: %@",documentDirectoryFilename);
    
    [LoaderClass removeLoader:self.view];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pdfPath = [documentsDirectory stringByAppendingPathComponent:@"Saddly Invoice.pdf"];
    NSData *myData = [NSData dataWithContentsOfFile: pdfPath];
    
    
    
    NSString *sPathPDF = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"Saddly Invoice.pdf"]]; //path example for a local stored pdf
    NSURL *urlPDF = [NSURL fileURLWithPath:sPathPDF];
    UIDocumentInteractionController *dicPDF = [UIDocumentInteractionController interactionControllerWithURL: urlPDF];
    [dicPDF setDelegate:self];
    [dicPDF presentPreviewAnimated: YES];

    
    
    [self.navigationController popViewControllerAnimated:YES];

    
}

-(void)viewDidDisappear:(BOOL)animated {
    noDataLabel.hidden = YES;
}




@end
