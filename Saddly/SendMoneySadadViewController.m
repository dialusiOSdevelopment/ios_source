//
//  SendMoneySadadViewController.m
//  Saddly
//
//  Created by gandhi on 22/03/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "SendMoneySadadViewController.h"

@interface SendMoneySadadViewController ()

@end

@implementation SendMoneySadadViewController


- (void)viewDidLoad {
   
    
    
    //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Send Money SADAD Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //

    
    self.title = [MCLocalization stringForKey:@"sadad"];
    self.navigationItem.hidesBackButton = YES;
    
    
    [_btnDoneOutlet setTitle:[MCLocalization stringForKey:@"done"] forState:UIControlStateNormal];
    
    NSUserDefaults * UrlDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString*URl=   [UrlDefaults valueForKey:@"url"];
    
    url = [NSURL URLWithString:URl];
    
    
    NSString*  postString = [NSString stringWithFormat:@"%@", URl];
    
    NSString*   postString1 = [postString stringByAddingPercentEscapesUsingEncoding:(NSStringEncoding)NSUTF8StringEncoding];
    
    [_Webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:postString1]]];
    
}

-(void)viewDidAppear:(BOOL)animated {
    
    self.navigationItem.hidesBackButton = YES;
    
}



@end


