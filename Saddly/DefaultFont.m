//
//  DefaultFont.m
//  Saddly
//
//  Created by gandhi on 15/02/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
////
//
#import "DefaultFont.h"

@implementation UIView (DefaultFontAndColor)

//sets the default font for view classes by default
-(void)setDefaultFontFamily:(NSString*)fontFamily andSubViews:(BOOL)isSubViews andColor:     (UIColor*) color
{
    if ([self isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)self;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        if( color )
            lbl.textColor = color;
    }
    else if ([self isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)self;
        [btn.titleLabel setFont:[UIFont fontWithName:fontFamily size:[[btn.titleLabel font] pointSize]]];
        
        if( color )
        {
            btn.tintColor = color;
        }
    }
    
    if (isSubViews)
    {
        for (UIView *sview in self.subviews)
        {
            [sview setDefaultFontFamily:fontFamily andSubViews:YES andColor:color];
        }
    }
}
@end

///
//#import "DefaultFont.h"
//
//@implementation UILabel (Helper)
//+(UILabel *)labelWithText:(NSString *)text{
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
//    label.text = text;
//    label.backgroundColor = [UIColor colorWithWhite:1 alpha:0];
//    CGSize textSize = [[label text] sizeWithFont:[label font]];
//    
//    CGSize winSize = [UIScreen mainScreen].bounds.size;
//    if (winSize.width<textSize.width) {
//        textSize.width = winSize.width;
//    }
//    CGRect frame = label.frame;
//    frame.size = textSize;
//    label.frame = frame;
//    
//    label.adjustsFontSizeToFitWidth = YES;
//    label.minimumFontSize = 10;
//    
//    return label;
//}
//
//- (void)reSize{
//    CGSize winSize = [UIScreen mainScreen].bounds.size;
//    CGSize textSize = [[self text] sizeWithFont:[self font]];
//    CGRect frame = self.frame;
//    textSize.width = textSize.width>winSize.width ? winSize.width : textSize.width;
//    frame.size = textSize;
//    self.frame = frame;
//}
//
//- (void)reSizeFixCenter{
//    CGPoint center = self.center;
//    CGSize winSize = [UIScreen mainScreen].bounds.size;
//    CGSize textSize = [[self text] sizeWithFont:[self font]];
//    CGRect frame = self.frame;
//    textSize.width = textSize.width>winSize.width ? winSize.width : textSize.width;
//    frame.size = textSize;
//    self.frame = frame;
//    
//    self.center = center;
//}
//@end
