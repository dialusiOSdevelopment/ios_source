//
//  BrowsePlansVC.h
//  DialuzApp
//
//  Created by Sai krishna on 11/10/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServicesMethods.h"
#import "LoaderClass.h"
#import "ConnectivityManager.h"
#import "DataTableViewCell.h"
#import "VoiceTableViewCell.h"
#import "MCLocalization.h"
#import "LoaderClass.h"
#import "LoginViewController.h"


@interface BrowsePlansVC : UIViewController<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, NSURLSessionDelegate>{
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * selectedAmount,*Planname,* serviceidname;
    NSDictionary * voiceDetails, * dataDetails;
    NSArray * voicePlans, * voiceAmount, * voiceArrServiceId, * voiceResMsg, * rc, * dataPlans, * dataAmount, * dataArrServiceId;
    NSMutableArray * FinalRcDetails;
    NSUInteger height;
    CALayer *bottomBorder;

  
}


@property (strong, nonatomic) UIImage * operatorImage;
@property (strong,nonatomic) NSString  * operType;


@property (strong, nonatomic) IBOutlet UICollectionView *collecOperator;



//@property (strong, nonatomic) IBOutlet UIView *viewVoice;
//@property (strong, nonatomic) IBOutlet UIView *viewData;
//@property (strong, nonatomic) IBOutlet UISegmentedControl *segment;
//@property (strong, nonatomic) IBOutlet UITableView *tblVoice;
//@property (strong, nonatomic) IBOutlet UITableView *tblData;

//- (IBAction)segmentValueChanged:(id)sender;

@end
