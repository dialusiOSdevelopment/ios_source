//
//  Validate.h
//  ColdHubApp
//
//  Created by Venkat on 02/08/15.
//  Copyright (c) 2015 Drughub. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Validate : NSObject
{
    
}

+ (BOOL)isNull:(NSString*)str;
+ (BOOL)isValidEmailId:(NSString*)email;
+ (BOOL)isValidMobileNumber:(NSString*)number;
+ (BOOL) isValidUserName:(NSString*)userName;
+ (BOOL) isValidPassword:(NSString*)password;
+ (BOOL) isValidAge:(NSString*)age;
+(BOOL) validateStringContainsAlphabetsOnly:(NSString*)string;
+(BOOL) validateStringContainsAlphanumericOnly:(NSString*)string;
+(BOOL)validateZipCode:(NSString *)zipCode;
+(BOOL)validateMobileNumber:(NSString *)mobileNumber;

@end
