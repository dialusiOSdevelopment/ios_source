//
//  WalletSuccessVC.h
//  DialuzApp
//
//  Created by Sai krishna on 1/6/17.
//  Copyright © 2017 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"


#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"



@interface WalletSuccessVC : UIViewController{

    NSDictionary*SuccessResponseDict;
    NSString * Ref;
    
}


@property (strong, nonatomic) IBOutlet UILabel *lblAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIImageView *imgStatus;

@property (strong, nonatomic) IBOutlet UILabel *lblNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblRefNum;

@property (strong, nonatomic) IBOutlet UIButton *btnBackToHomeOutlet;


// Names
@property (strong, nonatomic) IBOutlet UILabel *lblCongrats;
@property (strong, nonatomic) IBOutlet UILabel *lblUSuccAddName;
@property (strong, nonatomic) IBOutlet UILabel *lblToName;
@property (strong, nonatomic) IBOutlet UITextView *ContactUS;

@property (strong, nonatomic) IBOutlet UITextView *LBLNUMMAil;



- (IBAction)btnBackToHome:(id)sender;

@end
