//
//  GuestOTP.h
//  Saddly
//
//  Created by gandhi on 02/05/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoaderClass.h"
#import "ConnectivityManager.h"
#import "UIView+Toast.h"
#import "Validate.h"
#import "LoginViewController.h"
#import "UpdateDetails.h"
#import "MCLocalization.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <netdb.h>

@interface GuestOTP : UIViewController{

    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    NSString * Ipaddress,*name;
    
    NSDictionary  * getOTPDetails, * loginWithOTPDetails, * profileDict;
    
    CLLocationManager *locationManager;
    
    NSString * latitude, * longitude;
    
    NSString * deviceName, * deviceVersion,*response1;

     NSString * methodname, * a123;

}


@property (strong, nonatomic) IBOutlet UIButton *btnShowPwdOutlet;

- (IBAction)btnShowPwd:(id)sender;





@property (strong, nonatomic) IBOutlet UITextField *OTP;
@property (strong, nonatomic) IBOutlet UIButton *BtnRegister;
@property (strong, nonatomic) IBOutlet UITextView *TxtActivationCode;
@property (strong, nonatomic) IBOutlet UILabel *Greetingtxt;
@property (strong, nonatomic) IBOutlet UILabel *LblActivationcode;
- (IBAction)Submit:(id)sender;

@end
