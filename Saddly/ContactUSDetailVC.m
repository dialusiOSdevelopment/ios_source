//
//  ContactUSDetailVC.m
//  DialuzApp
//
//  Created by gandhi on 23/06/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import "ContactUSDetailVC.h"

@interface ContactUSDetailVC ()

@end

@implementation ContactUSDetailVC


- (void)viewDidLoad {
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
        
    }

    
    self.title = [MCLocalization stringForKey:@"Slifdearrcont"];
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        _txtView.text = @"الرياض,\n مخرج5 \n , الرياض.\n تلفون : 4565073-11-966\n الرقم الموحد:920000092  \n info@saddly.com:البريد الالكتروني";
        [_CntryName setTitle:@" المملكة العربية السعودية " forState:UIControlStateNormal];
        
    } else {
        _txtView.text = @"Riyadh,\nExit 5,\nRiyadh.\nPhone : 966-11-4565073,\nPhone : 920000092,\nEmail :info@saddly.com  ";
      
        [_CntryName setTitle:@" SAUDI ARABIA" forState:UIControlStateNormal];

    }
   
    _CntryName.titleLabel.adjustsFontSizeToFitWidth = YES;


    [super viewDidLoad];
    
    {
        NSString *location = @"5th,Riyadh";
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:location
                     completionHandler:^(NSArray* placemarks, NSError* error){
                         if (error) {
                             alertMessage=[MCLocalization stringForKey:@"unableToFetch"];
                             [self alertMethod];
                             
                         } else if (placemarks && placemarks.count > 0) {
                             CLPlacemark *topResult = [placemarks firstObject];
                             MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                             
                             
                             MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(placemark.coordinate, 5000, 5000);
                             
                             
                             [self.mapview setRegion:region animated:YES];
                             [self.mapview addAnnotation:placemark];
                         }
                     }
         ];
    }
}



-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];


        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}




-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

@end

