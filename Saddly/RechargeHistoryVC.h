//
//  RechargeHistoryVC.h
//  DialuzApp
//
//  Created by Sai krishna on 10/22/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServicesMethods.h"
#import "MCLocalization.h"
#import "ConnectivityManager.h"
#import "RechargeHistoryCell.h"
#import "AddMoneyHistoryCell.h"
#import "LoaderClass.h"
#import "MyProfileVC.h"
#import "TransactionDetailsVC.h"


@interface RechargeHistoryVC : UIViewController<UITableViewDataSource,UITableViewDelegate, NSURLSessionDelegate>{
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle,* alertMessage;
    NSDictionary  * addMoneyDetails;
    NSUInteger height;
    
    NSUInteger amCount , amFinalCount , rcCount , rcFinalCount ;

    NSMutableArray  * amMonthDictData,  * amFinalData;
    NSMutableArray  * rcMonthDictData,  * rcFinalData;
    
    NSArray * rcYearList,* rcDetails,* amYearList;
    NSArray * rcMonthList, * amMonthList;
    NSMutableArray * rcMonthListTitle, * amMonthListTitle;
    
    NSDictionary * profileDict;
    NSMutableDictionary *reponsedata;
    
    
    NSString * op;
    
    NSDictionary * dictRC, * dictAM;
    
    NSString * lblRCOrderId, * lblRCDateTime, * lblAMOrderId, * lblAMDateTime;
    
     NSString * methodname, * a123;
    
}


@property (strong, nonatomic) IBOutlet UITableView *tblRCHist;
@property (strong, nonatomic) IBOutlet UITableView *tblAddMoneyHist;

@property (strong, nonatomic) IBOutlet UISegmentedControl *segment;


- (IBAction)segmentChanged:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *viewRC;
@property (strong, nonatomic) IBOutlet UIView *viewAddMoney;

@property (strong, nonatomic) NSArray * rcAmountArray, * rcDateTimeArray, * rcOrderIdArray, * rcResMsgArray, * rcStatusArray, * rcMonthArray, * rcYearArray, * rcOperatorArray, * rcMobNumArray, * amAmountArray, * amDateTimeArray, * amOrderIdArray, * amResMsgArray, * amStatusArray, * amMonthArray, * amYearArray, * amOperatorArray, * amMobNumArray, * rcAmountFromCardArray, * rcAmountFromWalletArray, * amAmountFromCardArray, * amAmountFromWalletArray;

@end
