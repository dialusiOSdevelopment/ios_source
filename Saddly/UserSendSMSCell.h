//
//  UserSendSMSCell.h
//  DialuzApp
//
//  Created by Sai krishna on 12/22/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserSendSMSCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblMobNum;
@property (strong, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet UITextView *txtViewSentMsg;

@end
