//
//  GuestSelectPackageVC.m
//  Saddly
//
//  Created by Sai krishna on 6/27/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "GuestSelectPackageVC.h"

@interface GuestSelectPackageVC ()

@end

@implementation GuestSelectPackageVC

- (void)viewDidLoad {
    
    self.title = [MCLocalization stringForKey:@"selPackage"];

    
    _imgOperator.image =  _operImage;
    btnRcDefaults = [NSUserDefaults standardUserDefaults];
    
    _btnVoiceCallOutlet.hidden = NO;
    _btnInternetDataOutlet.hidden = NO;
    _btnSecondInternetOutlet.hidden = YES;

   

    [self plansAvailabilityWebServices];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)viewWillAppear:(BOOL)animated {
    
    [self plansAvailabilityWebServices];
    
}


- (IBAction)btnVoiceCall:(id)sender {
    
    GuestRechargeVC * guestRc =[self.storyboard instantiateViewControllerWithIdentifier:@"GuestRechargeVC"];
    
    btnRcDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
        type = @"voice";
    } else {
        type = @"US";
    }
    
    guestRc.operName = _operName;
    guestRc.operImage = _operImage;
    guestRc.operType = type;
    
    
    [self.navigationController pushViewController:guestRc animated:YES];
    
}



- (IBAction)btnInternetData:(id)sender {
    
    GuestRechargeVC * guestRc =[self.storyboard instantiateViewControllerWithIdentifier:@"GuestRechargeVC"];
    
    btnRcDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
        type = @"data";
    } else {
        type = @"UK";
    }
    
    guestRc.operName = _operName;
    guestRc.operImage = _operImage;
    guestRc.operType = type;
    
    [self.navigationController pushViewController:guestRc animated:YES];
    
    
}




-(void) checkTokenStatusWebServices {
    
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":@"Guest",
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"plansAvailabilityWebServices"]) {
                    [self plansAvailabilityWebServices];
                }
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//






-(void)plansAvailabilityWebServices {
    
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];

    
    [LoaderClass showLoader:self.view];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/plansAvailability1"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
//    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
//    
//    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
//    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
//    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
//    
//    
//    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
//    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    NSDictionary * plansAvialDict =  @{@"operator":_operName,
                                     @"ipAddress":Ipaddress,
                                     @"from":@"iPhone",
                                    
                                     };
    
    NSLog(@"Posting plansAvialDict is %@",plansAvialDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:plansAvialDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSLog(@"No data returned from server, error ocurred: %@", error);
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
    
            
            NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            NSLog(@"resSrt: %@", resSrt);
            NSError *deserr;
            
            
            [LoaderClass removeLoader:self.view];
            
            NSDictionary*Response=[NSJSONSerialization
                                   JSONObjectWithData:data
                                   options:kNilOptions
                                   error:&deserr];
            
            
            NSLog(@"plansAvailabilityWebServices response=%@", Response);
            NSString * dataAvaStr, * voiceAvaStr, * ukAvaStr, * usAvaStr;
            
            
            
            if ([[Response valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Response valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"plansAvailabilityWebServices";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                }
                
                
                else if ([[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
                    
                    
                    NSArray * dataAvail = [[Response valueForKey:@"data"] firstObject];
                    NSArray * voiceAvail = [[Response valueForKey:@"voice"] firstObject];
                    
                    
                    dataAvaStr = [NSString stringWithFormat:@"%@",dataAvail];
                    voiceAvaStr = [NSString stringWithFormat:@"%@",voiceAvail];
                    
                    NSLog(@"dataAvaStr %@",dataAvaStr);
                    NSLog(@"voiceAvaStr %@",voiceAvaStr);
                    
                    
                    if ([voiceAvaStr isEqualToString:@"Available"]) {
                        _btnVoiceCallOutlet.hidden = NO;
                    } else{
                        _btnVoiceCallOutlet.hidden = YES;
                    }
                    
                    
                    if ([dataAvaStr isEqualToString:@"Available"]) {
                        _btnInternetDataOutlet.hidden = NO;
                    } else{
                        _btnInternetDataOutlet.hidden = YES;
                    }
                    
                    if ([voiceAvaStr isEqualToString:@"Not Available"] && [dataAvaStr isEqualToString:@"Available"]) {
                        
                        _btnVoiceCallOutlet.hidden = YES;
                        _btnInternetDataOutlet.hidden = YES;
                        _btnSecondInternetOutlet.hidden = NO;
                        
                    }
                    
                    
                    [_btnVoiceCallOutlet setTitle:[MCLocalization stringForKey:@"voicePackage"] forState:UIControlStateNormal ];
                    [_btnInternetDataOutlet setTitle:[MCLocalization stringForKey:@"dataPackage"] forState:UIControlStateNormal];
                    [_btnSecondInternetOutlet setTitle:[MCLocalization stringForKey:@"dataPackage"] forState:UIControlStateNormal];
                    
                    
                } else {
                    
                    NSArray * ukAvail = [[Response valueForKey:@"UK"] firstObject];
                    NSArray * usAvail = [[Response valueForKey:@"US"] firstObject];
                    
                    
                    ukAvaStr = [NSString stringWithFormat:@"%@",ukAvail];
                    usAvaStr = [NSString stringWithFormat:@"%@",usAvail];
                    
                    NSLog(@"ukAvaStr %@",ukAvaStr);
                    NSLog(@"usAvaStr %@",usAvaStr);
                    
                    
                    if ([usAvaStr isEqualToString:@"Available"]) {
                        _btnVoiceCallOutlet.hidden = NO;
                    } else{
                        _btnVoiceCallOutlet.hidden = YES;
                    }
                    
                    if ([ukAvaStr isEqualToString:@"Available"]) {
                        _btnInternetDataOutlet.hidden = NO;
                    } else{
                        _btnInternetDataOutlet.hidden = YES;
                    }
                    
                    
                    
                    if ([ukAvaStr isEqualToString:@"Available"] && [usAvaStr isEqualToString:@"Not Available"]) {
                        
                        _btnVoiceCallOutlet.hidden = YES;
                        _btnInternetDataOutlet.hidden = YES;
                        _btnSecondInternetOutlet.hidden = NO;
                        
                    }
                    
                    
                    [_btnVoiceCallOutlet setTitle:[MCLocalization stringForKey:@"usPackage"] forState:UIControlStateNormal ];
                    [_btnInternetDataOutlet setTitle:[MCLocalization stringForKey:@"ukPackage"] forState:UIControlStateNormal];
                    [_btnSecondInternetOutlet setTitle:[MCLocalization stringForKey:@"ukPackage"] forState:UIControlStateNormal];
                    
                }
                
                
                _btnVoiceCallOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
                _btnVoiceCallOutlet.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
                _btnVoiceCallOutlet.titleLabel.numberOfLines = 2;
                
                
                _btnInternetDataOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
                _btnInternetDataOutlet.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
                _btnInternetDataOutlet.titleLabel.numberOfLines = 2;
                
                _btnSecondInternetOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
                _btnSecondInternetOutlet.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
                _btnSecondInternetOutlet.titleLabel.numberOfLines = 2;


            }
    
        });

    }];

    [dataTask resume];


}







@end
