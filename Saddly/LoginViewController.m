///
//  LoginViewController.m
//  Saddly
//
//  Created by Sai krishna on 1/18/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "LoginViewController.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@import FirebaseInstanceID;
@import FirebaseMessaging;

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    
    KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"AC_APP_UUID" accessGroup:nil];
    NSString *keychainUUID = [keychain objectForKey:(__bridge id)(kSecAttrAccount)];
    NSString *appVersion = [NSString stringWithFormat:@"%@",@"1.0"];
    [keychain setObject:appVersion forKey:(__bridge id)(kSecAttrDescription)];
    if (keychainUUID==nil||[keychainUUID isKindOfClass:[NSNull class]]||keychainUUID.length==0) {
        UUID = [[NSUUID UUID] UUIDString];
        [keychain setObject:UUID forKey:(__bridge id)(kSecAttrAccount)];
    } else {
        UUID = [keychain objectForKey:(__bridge id)(kSecAttrAccount)];
    }
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    [uuidDefaults setObject:UUID forKey:@"uuidDefaults"];
    
    NSLog(@"UUID from key chain %@",UUID);
    
      //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Login Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //
    
    [self.navigationController setNavigationBarHidden:YES];
    [LoaderClass showLoader:self.view];
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    
    NSUserDefaults * guestlogin = [NSUserDefaults standardUserDefaults];
    NSString*kin= [guestlogin valueForKey:@"Guestlogin"];
    
    if ([kin isEqualToString:@"10"]) {
        
        NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
        [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];

        
    }
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    NSString * Uid = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"user_mob"]];
    
    
    NSUInteger jj= Uid.length;
    
    NSLog(@"length is %lu",(unsigned long)jj);
    
    if ([[profileDict valueForKey:@"user_mob"] length]>0)  {
        
        [self performSegueWithIdentifier:@"togoHome" sender:nil];
        
        self.navigationController.view.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:128.0/255.0 blue:64.0/255.0 alpha:1.0];
        
        self.view.hidden = YES;

        
        noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {

            noDataLabel.text = @"جاري التحميل ... \n يرجى الانتظار";
            
        } else {
            
            noDataLabel.text = @"Loading...\nPlease Wait";
        }
        
        
        noDataLabel.numberOfLines = 2;
        noDataLabel.textColor        = [UIColor whiteColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        [self.navigationController.view addSubview:noDataLabel];
        
        noDataLabel.hidden = NO;
        
    } else {
        
        locationManager = [[CLLocationManager alloc] init];
        _imgEmail.hidden = YES;
       
        
        [_btnEmailOutlet setImage:[UIImage imageNamed:@"no radiobutton-black.png"] forState:UIControlStateNormal];
        [_btnEmailOutlet setImage:[UIImage imageNamed:@"yes Radio button-black.png"] forState:UIControlStateSelected];
        
        [_btnMoboutlet setImage:[UIImage imageNamed:@"no radiobutton-black.png"] forState:UIControlStateNormal];
        [_btnMoboutlet setImage:[UIImage imageNamed:@"yes Radio button-black.png"] forState:UIControlStateSelected];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localize) name:MCLocalizationLanguageDidChangeNotification object:nil];
    
        
        [self localize];
        [self deviceName];
        [self getMacAddress];
        [self localIPAddresses];
                
        
        NSLog(@"device Name is: %@", deviceName);
        deviceVersion = [[UIDevice currentDevice] systemVersion];
        
        [self hashed_string:[@"uniqueid=" stringByAppendingString:UUID]];
       
    }
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 

-(void)viewDidAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES];
    [LoaderClass removeLoader:self.view];
    
    
    [_txtMobNum resignFirstResponder];
    [_txtPwd resignFirstResponder];
    
    
    
    
    if(![[ConnectivityManager sharedManager] CheckConnectivity])
    {
        alertTitle = [MCLocalization stringForKey:@"connlost"];
        alertMessage = [MCLocalization stringForKey:@"checknetconn"];
        [self alertMethod];
        return;
    }
    
    
    alert = [UIAlertController alertControllerWithTitle:nil message:@"Select Your Language" preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake( CGRectGetMidX(self.btnChangeOutlet.frame), CGRectGetMidY(self.btnChangeOutlet.frame), 0, 0);
    
    
    
    UIAlertAction * firstAction = [UIAlertAction actionWithTitle:@"English" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        
        [_btnChangeOutlet setTitle:@"عربي" forState:UIControlStateNormal];
        _btnChangeOutlet.hidden = NO;
        [MCLocalization sharedInstance].language = @"en";
        
        
        // Btn Mobile Transformation
        _btnMoboutlet.transform = CGAffineTransformMakeScale(1.0, -1.0);
        _btnMoboutlet.titleLabel.transform = CGAffineTransformMakeScale(1.0, -1.0);
        _btnMoboutlet.imageView.transform = CGAffineTransformMakeScale(1.0, -1.0);
        
        // Btn Email Transformation
        _btnEmailOutlet.transform = CGAffineTransformMakeScale(1.0, -1.0);
        _btnEmailOutlet.titleLabel.transform = CGAffineTransformMakeScale(1.0, -1.0);
        _btnEmailOutlet.imageView.transform = CGAffineTransformMakeScale(1.0, -1.0);
        
    }];
    
    UIAlertAction * secondAction = [UIAlertAction actionWithTitle:@"Arabic/عربي" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        
        [_btnChangeOutlet setTitle:@"English" forState:UIControlStateNormal];
        _btnChangeOutlet.hidden = NO;
        [MCLocalization sharedInstance].language = @"ru";
        
        
        // Btn Mobile Transformation
        _btnMoboutlet.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        _btnMoboutlet.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        _btnMoboutlet.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        
        // Btn Email Transformation
        _btnEmailOutlet.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        _btnEmailOutlet.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        _btnEmailOutlet.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        
    }];
    
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    
    alert.view.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    [self presentViewController:alert animated:YES completion:nil];
    
    
    _txtMobNum.text = @"";
    _txtPwd.text = @"";
    
    _txtMobNum.hidden = NO;
    _lbl966.hidden = NO;
    _imgEmail.hidden = YES;
    [_btnMoboutlet setSelected:YES];
    
    // For mobile & email images Changing animations
    transition = [CATransition animation];
    transition.duration = 2.0f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    
    // Resign First Responder from the mobile text field.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي"  style:UIBarButtonItemStyleDone target:self   action:@selector(NextClicked)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    _txtMobNum.inputAccessoryView = keyboardDoneButtonView;
    
    
   
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    
    [self localize];
    [self deviceName];
    [self getMacAddress];
    [self localIPAddresses];
    [self GuestLogin];

    NSLog(@"device Name is: %@", deviceName);
    
    deviceVersion = [[UIDevice currentDevice] systemVersion];
    NSLog(@"device Version is: %@",deviceVersion);
    
    
    // For First login
    NSUserDefaults *theDefaults;
    int  launchCount;
    //Set up the properties for the integer and default.
    theDefaults = [NSUserDefaults standardUserDefaults];
    launchCount = (int)[theDefaults integerForKey:@"hasRun"] + 1;
    [theDefaults setInteger:launchCount forKey:@"hasRun"];
    [theDefaults synchronize];
    

    
    //Log the amount of times the application has been run
    NSLog(@"This application has been run %d amount of times", launchCount);
    
    //Test if application is the first time running
    if(launchCount == 1) {
        //Run your first launch code (Bring user to info/setup screen, etc.)
        NSLog(@"This is the first time this application has been run");
        
        [self Downloadeddevicedetails];
        
    }
    
    //Test if it has been run before
    if(launchCount >= 2) {
        //Run new code if they have opened the app before (Bring user to home screen etc.
        NSLog(@"This application has been run before");
    }
    
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES];
    
}




- (NSString*) deviceName
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    static NSDictionary* deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{@"i386"      :@"Simulator",
                              @"x86_64"    :@"Simulator",
                              @"iPod1,1"   :@"iPod Touch",        // (Original)
                              @"iPod2,1"   :@"iPod Touch",        // (Second Generation)
                              @"iPod3,1"   :@"iPod Touch",        // (Third Generation)
                              @"iPod4,1"   :@"iPod Touch",        // (Fourth Generation)
                              @"iPod7,1"   :@"iPod Touch",        // (6th Generation)
                              @"iPhone1,1" :@"iPhone",            // (Original)
                              @"iPhone1,2" :@"iPhone",            // (3G)
                              @"iPhone2,1" :@"iPhone",            // (3GS)
                              @"iPad1,1"   :@"iPad",              // (Original)
                              @"iPad2,1"   :@"iPad 2",            //
                              @"iPad3,1"   :@"iPad",              // (3rd Generation)
                              @"iPhone3,1" :@"iPhone 4",          // (GSM)
                              @"iPhone3,3" :@"iPhone 4",          // (CDMA/Verizon/Sprint)
                              @"iPhone4,1" :@"iPhone 4S",         //
                              @"iPhone5,1" :@"iPhone 5",          // (model A1428, AT&T/Canada)
                              @"iPhone5,2" :@"iPhone 5",          // (model A1429, everything else)
                              @"iPad3,4"   :@"iPad",              // (4th Generation)
                              @"iPad2,5"   :@"iPad Mini",         // (Original)
                              @"iPhone5,3" :@"iPhone 5c",         // (model A1456, A1532 | GSM)
                              @"iPhone5,4" :@"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" :@"iPhone 5s",         // (model A1433, A1533 | GSM)
                              @"iPhone6,2" :@"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPhone7,1" :@"iPhone 6 Plus",     //
                              @"iPhone7,2" :@"iPhone 6",          //
                              @"iPhone8,1" :@"iPhone 6S",         //
                              @"iPhone8,2" :@"iPhone 6S Plus",    //
                              @"iPhone8,4" :@"iPhone SE",         //
                              @"iPhone9,1" :@"iPhone 7",          //
                              @"iPhone9,3" :@"iPhone 7",          //
                              @"iPhone9,2" :@"iPhone 7 Plus",     //
                              @"iPhone9,4" :@"iPhone 7 Plus",     //
                              
                              @"iPad4,1"   :@"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   :@"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   :@"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   :@"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                              @"iPad4,7"   :@"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                              @"iPad6,7"   :@"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                              @"iPad6,8"   :@"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                              @"iPad6,3"   :@"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                              @"iPad6,4"   :@"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                              };
    }
    
    deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        }
        else {
            deviceName = @"Unknown";
        }
    }
    
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    [deviceNameDefaults setObject:deviceName forKey:@"deviceNameDefaults"];
    
    return deviceName;
}




// Localisation for English & Arbaic Language
- (void)localize{
    
    _lblWelcomeText.text = [MCLocalization stringForKey:@"LginPagewelcometext"];
    _lblDontHaveAnAcc.text=[MCLocalization stringForKey:@"SignupTxt"];
    _txtPwd.placeholder = [MCLocalization stringForKey:@"inputpassword"];
    _txtMobNum.placeholder = [MCLocalization stringForKey:@"txt_password"];

    
    [_btnEmailOutlet setTitle:[MCLocalization stringForKey:@"ButtonEmail"] forState:UIControlStateNormal];
    [_btnMoboutlet setTitle:[MCLocalization stringForKey:@"ButtonMobile"] forState:UIControlStateNormal];
    [_btnLoginOutlet setTitle:[MCLocalization stringForKey:@"signIn"] forState:UIControlStateNormal];
    [_btnSignupOutlet setTitle:[MCLocalization stringForKey:@"Buttonregistername"] forState:UIControlStateNormal];
    [_btnForgotPwdOutlet setTitle:[MCLocalization stringForKey:@"FrgetPsswrdBttnName"] forState:UIControlStateNormal];
    [_btnLoginOTPOutlet setTitle:[MCLocalization stringForKey:@"loginOtp"] forState:UIControlStateNormal];
    [_btnGuestOutlet setTitle:[MCLocalization stringForKey:@"guest"] forState:UIControlStateNormal];
    
    
    _btnLoginOTPOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnMoboutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnEmailOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnLoginOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnGuestOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;

    _lblWelcomeText.adjustsFontSizeToFitWidth = YES;
    _lblDontHaveAnAcc.adjustsFontSizeToFitWidth = YES;
    _btnChangeOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    [super viewDidLoad];
}




#pragma mark - CLLocationManagerDelegate



- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
    
   
    if (longitude.length <1) {
        longitude=@"0";
        
    }  if (latitude.length <1) {
        latitude=@"0";
        
    }
    
    NSLog(@"latitude : %@, longitude :%@", latitude, longitude);
    
    
    NSUserDefaults * latitudedef = [NSUserDefaults standardUserDefaults];
    [latitudedef setObject:latitude forKey:@"latitude"];
    
    
    NSUserDefaults * longitudedef = [NSUserDefaults standardUserDefaults];
    [longitudedef setObject:longitude forKey:@"longitude"];
    
    
}



// Contacts
-(void)contactsToStore{
    
    self.groupOfContacts = [@[] mutableCopy];
    self.phoneNumberArray = [@[] mutableCopy];
    self.contactNameArray = [@[] mutableCopy];
    
    
    [self getallcontacts];
    
    for (CNContact * contact in self.groupOfContacts) {
        
        NSMutableArray * myArr = [[NSMutableArray alloc] init];
        [myArr addObject:contact.givenName];
        
        NSArray * phoneNum = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
        [self.contactNameArray addObjectsFromArray:myArr];
        [self.phoneNumberArray addObjectsFromArray:phoneNum];
    } // for
    
    _FinalContact = _contactNameArray;
    _Finalnumber = _phoneNumberArray;
    
    namecount=[_FinalContact count];
    numbercount=[_Finalnumber count];
    
    
    if (namecount < numbercount) {
        differcount1=numbercount-namecount;
        
        NSLog(@"differcountis %lu",differcount1);
        
    }
    else if (namecount>numbercount){
        
        differcount2=namecount-numbercount;}
    NSLog(@"differcountis %lu",differcount2);
    
    
    
    for (NSUInteger i=0; i<differcount1; i++) {
        
        [_FinalContact addObject:@"NoName"];
        
    }
    
    for (NSUInteger i=0; i<differcount2; i++) {
        
        [_Finalnumber addObject:@"NoNumber"];
        
    }
    
}


-(void)getallcontacts{
    
    if ([CNContactStore class]) {
        CNContactStore * addressBook = [[CNContactStore alloc]init];
        
        NSArray * keysToFetch = @[CNContactGivenNameKey ,
                                  CNContactEmailAddressesKey,
                                  CNContactFamilyNameKey,
                                  CNContactPhoneNumbersKey,
                                  CNContactPostalAddressesKey];
        
        CNContactFetchRequest * fetchRequest = [[CNContactFetchRequest alloc] initWithKeysToFetch:keysToFetch];
        
        [addressBook enumerateContactsWithFetchRequest:fetchRequest error:nil usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
            [self.groupOfContacts addObject:contact];
        }]; // Block
    } // if
} // getallcontacts


// for Posting Contacts
-(void)contactsWebServicesMethod{
    
    
    
    NSString*FinalName=[NSString new];
    NSString*FinalNumber=[NSString new];
    
    NSLog(@"count is%lu",(unsigned long)_FinalContact.count);
    
    for (NSInteger i=0; i<_FinalContact.count; i++) {
        
        if (i == 200) {
            break;
        }
        
        Names=_FinalContact[i];
        Numbers=_Finalnumber[i];
        
        NSLog(@"names are%@",Names);
        
        NSLog(@"numbers are%@",Numbers);
        
        if (Names.length==0) {
            Names=@"Namenotavailable";
        }
        
        Names=[Names stringByReplacingOccurrencesOfString:@"@" withString:@""];
        
        FinalName= [[[[[[[FinalName stringByAppendingString:Names] stringByAppendingString:@"SADDLY!@"]stringByReplacingOccurrencesOfString:@"+" withString:@""] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        if (Numbers.length==0) {
            Numbers=@"numberNotavailable";
        }
        
        FinalNumber=[[[[[[FinalNumber stringByAppendingString:Numbers]stringByAppendingString:@"SADDLY!@"]stringByReplacingOccurrencesOfString:@"+" withString:@""]stringByReplacingOccurrencesOfString:@"*" withString:@"" ]stringByReplacingOccurrencesOfString:@"#" withString:@""]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        
    }
    
    NSLog(@"numbers are%@",FinalNumber);
    NSLog(@"names are%@",FinalName);
    
    
    NSString*user_mob=[@"966" stringByAppendingString:_txtMobNum.text];
    
    
    
    //
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/login/inserMobcontacts1"];
    
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    
    NSString * userId = [@"966" stringByAppendingString:_txtMobNum.text];
    NSString * userPwd = _txtPwd.text;
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
//     Request:contact_name,contact_mob,user_mob,login_status,unique_id,access_Token,from;
    
    NSDictionary * loginDetailsDict =  @{@"contact_name":FinalName,
                                         
                                         @"contact_mob":FinalNumber,
                                         @"from":@"iphone",
                                         @"user_mob":user_mob,
                                         @"login_status":@"iphone",
                                         @"access_Token":a123,
                                         @"unique_id":UUID
                                         
                                         };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            [LoaderClass removeLoader:self.view];
            
            
            NSLog(@"Killer Response is%@",Killer);
            
            
            
            NSLog(@"str Response is%@",str);
            
        });
        
        
    }];
    
    
    [dataTask resume];
}











// Change Language
- (IBAction)btnChangeLanguage:(id)sender {
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
//        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceLeftToRight];
        
        
        [_btnChangeOutlet setTitle:@"عربي" forState:UIControlStateNormal];
        [MCLocalization sharedInstance].language = @"en";

//        _txtMobNum.textAlignment = NSTextAlignmentLeft;
//        _txtPwd.textAlignment = NSTextAlignmentLeft;
//        _txtEmail.textAlignment = NSTextAlignmentLeft;
        
        // Btn Mobile Transformation
        _btnMoboutlet.transform = CGAffineTransformMakeScale(1.0, -1.0);
        _btnMoboutlet.titleLabel.transform = CGAffineTransformMakeScale(1.0, -1.0);
        _btnMoboutlet.imageView.transform = CGAffineTransformMakeScale(1.0, -1.0);
        
        // Btn Email Transformation
        _btnEmailOutlet.transform = CGAffineTransformMakeScale(1.0, -1.0);
        _btnEmailOutlet.titleLabel.transform = CGAffineTransformMakeScale(1.0, -1.0);
        _btnEmailOutlet.imageView.transform = CGAffineTransformMakeScale(1.0, -1.0);
        
        
    } else {
        
//        [[UIView appearance] setSemanticContentAttribute:UISemanticContentAttributeForceRightToLeft];
        
        
        [_btnChangeOutlet setTitle:@"English" forState:UIControlStateNormal];
        [MCLocalization sharedInstance].language = @"ru";

        
//        _txtMobNum.textAlignment = NSTextAlignmentRight;
//        _txtPwd.textAlignment = NSTextAlignmentRight;
//        _txtEmail.textAlignment = NSTextAlignmentRight;
        
        
        // Btn Mobile Transformation
        _btnMoboutlet.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        _btnMoboutlet.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        _btnMoboutlet.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        
        // Btn Email Transformation
        _btnEmailOutlet.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        _btnEmailOutlet.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        _btnEmailOutlet.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        
    }
    
}



//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if ([textField isEqual:_txtMobNum]) {
        
        if([self isNumeric:string]){
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 9;
        } else  {
            return FALSE;
        }
        
    }
//    else if ([textField isEqual:_txtEmail]){
//        if([self isAlphaNumericSpecialCharacters:string])
//            return TRUE;
//        else
//            return FALSE;
//        
//    }
    else if ([textField isEqual:_txtPwd]){
        if([string isEqualToString:@" "]){
            // Returning no here to restrict whitespace
            return NO;
        }
        
    }
    
    return YES;
}


// Entering only numbers not a special Characters
-(BOOL)isNumeric:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}


// Entering only alphaNumeric not a Arabic Characters
-(BOOL)isAlphaNumericSpecialCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._@-"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}




// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = self.view.bounds;
    
    

    
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}










-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}


// When Next Clicked on the Keyboard
-(void)NextClicked{
    
    [self.txtMobNum resignFirstResponder];
    [self.txtPwd becomeFirstResponder];
    
}


// Moving cursor to the next text field 
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    if (textField == self.txtMobNum){
        
        [self.txtMobNum resignFirstResponder];
        [self.txtPwd becomeFirstResponder];
        
    } else if (textField == self.txtPwd) {
        
        [self.txtPwd resignFirstResponder];
        
    }
    return YES;
}




// Selecting Mobile radio button
- (IBAction)btnRadioMobile:(id)sender{
    
    
    _txtMobNum.hidden = NO;
    _txtPwd.hidden = NO;
    if([_btnMoboutlet isSelected]==YES)
    {
        [_btnMoboutlet setSelected:YES];
        [_btnEmailOutlet setSelected:NO];
    }
    else{
        [_btnMoboutlet setSelected:YES];
        [_btnEmailOutlet setSelected:NO];
    }
    _imgEmail.hidden = YES;
    _imgMobile.hidden = NO;
    _lbl966.hidden = NO;
    [_txtMobNum becomeFirstResponder];
    
    [_imgMobile.layer addAnimation:transition forKey:nil];
    
}


// Selecting Email radio button
- (IBAction)btnRadioEmail:(id)sender{
  
    _txtMobNum.hidden = YES;
    _txtPwd.hidden = NO;

    if([_btnEmailOutlet isSelected]==YES)
    {
        [_btnEmailOutlet setSelected:YES];
        [_btnMoboutlet setSelected:NO];
    }
    else{
        [_btnEmailOutlet setSelected:YES];
        [_btnMoboutlet setSelected:NO];
    }
    
    _imgEmail.hidden = NO;
    _imgMobile.hidden = YES;
    _lbl966.hidden = YES;
    
    [_imgEmail.layer addAnimation:transition forKey:nil];
    
}


// Showing & Hiding Password based on button
- (IBAction)btnShowPwd:(UIButton *)sender {
    
    sender.selected  =! sender.selected;
    if (sender.selected)
    {
        _txtPwd.secureTextEntry = NO;
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
    } else {
        _txtPwd.secureTextEntry = YES;
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
    }
    
}


- (IBAction)btnGuest:(id)sender {
    
    
    RCGCVC * guestVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"RCGCVC"];
    [self.navigationController pushViewController:guestVC animated:YES];

    
}




// Moving the view Up & Down
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //move the main view, so that the keyboard does not hide it.
    if  (self.view.frame.origin.y < 0)
    {
        [self animateTextField: textField up: NO];
    }
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = textField.frame.origin.y / 2; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? - movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
} // Moving the view Up & Down




//local IP Addresses
- (void)localIPAddresses {
    
    NSString *publicIP = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"https://icanhazip.com/"] encoding:NSUTF8StringEncoding error:nil];
    
    Ipaddress = [publicIP stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]]; // IP comes with a newline for some reason
    
    if (Ipaddress .length <1) {
        NSString *publicIP = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"https://api.ipify.org?format=json"] encoding:NSUTF8StringEncoding error:nil];
        
        Ipaddress = [publicIP stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
        
    }
    
    if (Ipaddress .length <1) {
        
        NSMutableArray *ipAddresses = [NSMutableArray array] ;
        struct ifaddrs *allInterfaces;
        
        // Get list of all interfaces on the local machine:
        if (getifaddrs(&allInterfaces) == 0) {
            struct ifaddrs *interface;
            
            // For each interface ...
            for (interface = allInterfaces; interface != NULL; interface = interface->ifa_next) {
                unsigned int flags = interface->ifa_flags;
                struct sockaddr *addr = interface->ifa_addr;
                
                // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
                if ((flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING)) {
                    if (addr->sa_family == AF_INET || addr->sa_family == AF_INET6) {
                        
                        // Convert interface address to a human readable string:
                        char host[NI_MAXHOST];
                        getnameinfo(addr, addr->sa_len, host, sizeof(host), NULL, 0, NI_NUMERICHOST);
                        
                        [ipAddresses addObject:[[NSString alloc] initWithUTF8String:host]];
                    }
                }
            }
            
            freeifaddrs(allInterfaces);
        }
        
        
        if (ipAddresses.count < 1) {
            Ipaddress = 0;
        } else {
            Ipaddress =  [ipAddresses objectAtIndex:1];
        }
        
    }
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    [ipaddressdefault setObject:Ipaddress forKey:@"ipaddressdefault"];
    
    NSLog(@"IP address from the method %@",Ipaddress);
    
    
}




// Getting mac address
- (NSString *)getMacAddress
{
    int                 mgmtInfoBase[6];
    char                *msgBuffer = NULL;
    size_t              length;
    unsigned char       macAddress[6];
    struct if_msghdr    *interfaceMsgStruct;
    struct sockaddr_dl  *socketStruct;
    NSString            *errorFlag = NULL;
    
    // Setup the management Information Base (mib)
    mgmtInfoBase[0] = CTL_NET;        // Request network subsystem
    mgmtInfoBase[1] = AF_ROUTE;       // Routing table info
    mgmtInfoBase[2] = 0;
    mgmtInfoBase[3] = AF_LINK;        // Request link layer information
    mgmtInfoBase[4] = NET_RT_IFLIST;  // Request all configured interfaces
    
    // With all configured interfaces requested, get handle index
    if ((mgmtInfoBase[5] = if_nametoindex("en0")) == 0)
        errorFlag = @"if_nametoindex failure";
    else
    {
        // Get the size of the data available (store in len)
        if (sysctl(mgmtInfoBase, 6, NULL, &length, NULL, 0) < 0)
            errorFlag = @"sysctl mgmtInfoBase failure";
        else
        {
            // Alloc memory based on above call
            if ((msgBuffer = malloc(length)) == NULL)
                errorFlag = @"buffer allocation failure";
            else
            {
                // Get system information, store in buffer
                if (sysctl(mgmtInfoBase, 6, msgBuffer, &length, NULL, 0) < 0)
                    errorFlag = @"sysctl msgBuffer failure";
            }
        }
    }
    
    // Befor going any further...
    if (errorFlag != NULL)
    {
        NSLog(@"Error: %@", errorFlag);
        return errorFlag;
    }
    
    // Map msgbuffer to interface message structure
    interfaceMsgStruct = (struct if_msghdr *) msgBuffer;
    
    // Map to link-level socket structure
    socketStruct = (struct sockaddr_dl *) (interfaceMsgStruct + 1);
    
    // Copy link layer address data in socket structure to an array
    memcpy(&macAddress, socketStruct->sdl_data + socketStruct->sdl_nlen, 6);
    
    // Read from char array into a string object, into traditional Mac address format
    NSString *macAddressString = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                                  macAddress[0], macAddress[1], macAddress[2],
                                  macAddress[3], macAddress[4], macAddress[5]];
    NSLog(@"Mac Address: %@", macAddressString);
    
    
    Macaddress=macAddressString;
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    [macaddressDefaults setObject:Macaddress forKey:@"macaddressDefaults"];
    
    // Release the buffer memory
    free(msgBuffer);
    
    
    return macAddressString;
}






-(void)Downloadeddevicedetails{
    
    NSString *token = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", token);
    
    [LoaderClass showLoader:self.view];
    
   
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];

    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/login/insertappdonwloadedetails1"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
   
    
    NSDictionary * loginDetailsDict =  @{@"devicemodel":deviceName,
                                         @"deviceversion":deviceVersion,
                                         @"from":@"iphone",
                                         
                                        };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            [LoaderClass removeLoader:self.view];
            
            
            NSLog(@"Killer Response is%@",Killer);
            
            
            
            NSLog(@"str Response is%@",str);
    
  

                       });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}



//// webservices for Login Action
-(void)loginWebServicesMethod {
    

    name = [@"966" stringByAppendingString:_txtMobNum.text];
    
    
    NSString * pass = [_txtPwd.text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSString * countryCode= [MCLocalization stringForKey:@"DBvalue"];
    
    if ([longitude length]<1) {
        longitude=@"0";
    }
    if ([latitude length]<1) {
        latitude=@"0";
    }
    
    
    [LoaderClass showLoader:self.view];
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/login/loginwithpasswordDetails"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[[@"966" stringByAppendingString:_txtMobNum.text] stringByAppendingString:@":"] stringByAppendingString:_txtPwd.text];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * loginDetailsDict =  @{@"db":countryCode,
                                         @"username":name,
                                         @"password":pass,
                                         @"ipAddress":Ipaddress,
                                         @"from":@"iPhone",
                                         @"longitude":longitude,
                                         @"latitude":latitude,
                                         @"access_token":a123,
                                         @"unique_id":UUID
                                         };
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            [LoaderClass removeLoader:self.view];
            
            loginDetails = Killer;
            
            NSUserDefaults * loginResponseDefaults = [NSUserDefaults standardUserDefaults];
            [loginResponseDefaults setObject:loginDetails forKey:@"loginResponseDefaults"];
            
                        
            if ([[loginDetails valueForKey:@"loginwithpassword"] isEqualToString:@"showpopup"]) {
                
                NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
                [pddefaults setObject:_txtPwd.text forKey:@"pddd"];
                
                
                [self DeviceToken];
                [self contactsToStore];
                [self contactsWebServicesMethod];
                [self Devicedetails];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName setObject:[@"966" stringByAppendingString:_txtMobNum.text] forKey:@"userName"];
                
                
                [self performSegueWithIdentifier:@"togoHome" sender:nil];
                
                
            } else if ([[loginDetails valueForKey:@"loginwithpassword"] isEqualToString:@"wrong username"]) {
                if([_btnEmailOutlet isSelected]==YES) {
                    toastMsg = [MCLocalization stringForKey:@"noAccEmailId"];
                    [self toastMessagemethod];
                } else{
                    toastMsg = [MCLocalization stringForKey:@"noAccMobNum"];
                    [self toastMessagemethod];
                }
            } else if ([[loginDetails valueForKey:@"loginwithpassword"] isEqualToString:@"wrong password"]) {
                toastMsg = [MCLocalization stringForKey:@"wrongPwd"];
                [self toastMessagemethod];
            } else if ([[loginDetails valueForKey:@"loginwithpassword"] isEqualToString:@"account doesn't exist"]) {
                toastMsg = [[[MCLocalization stringForKey:@"noAccMobNum"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
                [self toastMessagemethod];
            } else if ([[loginDetails valueForKey:@"loginwithpassword"] isEqualToString:@"success"]) {
                
                NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
                [pddefaults setObject:_txtPwd.text forKey:@"pddd"];
                
                [self DeviceToken];
                [self contactsToStore];
                [self contactsWebServicesMethod];
                [self Devicedetails];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName setObject:[@"966" stringByAppendingString:_txtMobNum.text] forKey:@"userName"];
                
                [self performSegueWithIdentifier:@"togoHome" sender:nil];
                
            } else if ([[loginDetails valueForKey:@"loginwithpassword"] isEqualToString:@"guestLoginsuccess"]) {
                
                NSUserDefaults * Guestnum = [NSUserDefaults standardUserDefaults];
                [Guestnum setObject:_txtMobNum.text forKey:@"Guestnum"];
                
                GuestOTP * Guestotp  = [self.storyboard instantiateViewControllerWithIdentifier:@"GuestOTP"];
                [self.navigationController pushViewController:Guestotp animated:YES];
                
            }
            
        });
        
    }];
    
    [dataTask resume];
    ////////////
    
}




-(void)checkTokenStatusWebServices {
    
    
    [LoaderClass showLoader:self.view];

    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];

    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];


    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];


    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];


    NSString * encodeMobPwd = [[[@"966" stringByAppendingString:_txtMobNum.text] stringByAppendingString:@":"] stringByAppendingString:_txtPwd.text];

    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);


    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];

   
    NSString * accessToken = @"";
    NSString * tokenType = @"fresh_token";

    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                         @"mac_address":Macaddress,
                                         @"access_token":accessToken,
                                         @"user_mob":[@"966" stringByAppendingString:_txtMobNum.text],
                                         @"from":@"iPhone",
                                         @"unique_id":UUID,
                                         @"device_model":deviceName,
                                         @"genKey":genKey,
                                         @"token_type":tokenType,
                                        };

    NSLog(@"Posting Check token status is %@",checkTokenDict);


    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];


    [request setHTTPBody:postdata];


    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {

        if (!data) {

            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }


        dispatch_async(dispatch_get_main_queue(), ^{


            NSError *deserr;

            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];


            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];


            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);

            [LoaderClass removeLoader:self.view];

            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            

            a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
            
            NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
            [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
            
           
            NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
            [loginName setObject:[@"966" stringByAppendingString:_txtMobNum.text] forKey:@"userName"];
            
            [self loginWebServicesMethod];
            
            
            
        });

    }];


    [dataTask resume];
    ////////////

}
//



- (NSString *)hashed_string:(NSString *)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    uint8_t digest[CC_SHA256_DIGEST_LENGTH];
    
    // This is an iOS5-specific method.
    // It takes in the data, how much data, and then output format, which in this case is an int array.
    CC_SHA256(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    
    // Parse through the CC_SHA256 results (stored inside of digest[]).
    for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    NSLog(@"hashed_string output is %@",output);
    genKey = output;
    
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    [genKeyDefaults setObject:genKey forKey:@"genKeyDefaults"];
    
    return output;
}


-(void)GuestLogin{
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/login/isLoginGuestneedtoshowornot"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
//Request:accessToken,unique_id,from;
    
    
    NSDictionary * loginDetailsDict =  @{
//                                         @"from":@"iPhone",
                                        };
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            [LoaderClass removeLoader:self.view];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            [LoaderClass removeLoader:self.view];
            
            NSLog(@"Guest Details Response: %@",Killer);
            
            if ([[Killer valueForKey:@"isLoginGuestneedtoshowornot"] isEqualToString:@"Show"]) {
                
                _btnGuestOutlet.hidden = NO;
                
            } else {
                
                _btnGuestOutlet.hidden = YES;
                
            }
        });
      
        
    }];
    
    
    [dataTask resume];
}




-(void)Devicedetails {
    
    NSString *token = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", token);
    
    NSString*Number =[@"966" stringByAppendingString:_txtMobNum.text];
    
   

    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    
    NSString*devicemodel=deviceName;
    NSString*deviceversion=deviceVersion;
    NSString * ipAddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
   
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/login/insertdevicedetails1"];
    
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
   
    
    NSString * userId = [@"966" stringByAppendingString:_txtMobNum.text];
    NSString * userPwd = _txtPwd.text;
    
    NSUserDefaults * userid = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * userpwd = [NSUserDefaults standardUserDefaults];

    [userid setObject:userId forKey:@"userId"];
    [userpwd setObject:userPwd forKey:@"userPwd"];

    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
//Request:user_mob,devicemodel,deviceversion,from,user_emailId,ipAddress,access_Tokenunique_id;
    
    
    
    NSDictionary * loginDetailsDict =  @{@"user_mob":Number,
                                         
                                         @"devicemodel":devicemodel,
                                         @"deviceversion":deviceversion,
                                         @"ipAddress":ipAddress,
                                         @"from":@"iPhone",
                                         @"access_Token":a123,
                                         @"unique_id":UUID
                                         
                                         };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            [LoaderClass removeLoader:self.view];
            
            
//            NSLog(@"Killer Response is%@",Killer);
            
            
            
            NSLog(@"str Response is%@",str);
            
        });
        
        
    }];
    
    
    [dataTask resume];
}




//
//- (IBAction)btnLoginOTP:(id)sender {
//    
//    LoginWithOTP * loginOtp  = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginWithOTP"];
//    [self.navigationController pushViewController:loginOtp animated:YES];
//    
//}

- (IBAction)btnForgotPwd:(id)sender {
    
    ForgotPwdVC * forgotPwd  = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPwdVC"];
    [self.navigationController pushViewController:forgotPwd animated:YES];
    
    
}

- (IBAction)btnSignUp:(id)sender {
    
    
    RegistrationVC * registration  = [self.storyboard instantiateViewControllerWithIdentifier:@"RegistrationVC"];
    [self.navigationController pushViewController:registration animated:YES];
    
    
}


- (BOOL)validatePhone:(NSString *)phoneNumber {
    NSString * phoneRegex = @"^((\\+)|(5))[0-9]{8}$";
    NSPredicate * phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phoneNumber];
}


- (IBAction)btnLogin:(id)sender {

    
    [locationManager stopUpdatingLocation];
    [_txtMobNum resignFirstResponder];
    [_txtPwd resignFirstResponder];
    
//    // text fields mobile/email checking
//    if([_btnEmailOutlet isSelected]==YES) {
//        if ([_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
//            toastMsg = [MCLocalization stringForKey:@"pleaseEnterEmail"];
//            [self toastMessagemethod];
//        }  else if (![Validate isValidEmailId:[_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
//            toastMsg = [MCLocalization stringForKey:@"Mailidnotvalid"];
//            [self toastMessagemethod];
//        } else if ([_txtPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
//            toastMsg =[MCLocalization stringForKey:@"PleaseEnterpassword"];
//            [self toastMessagemethod];
//        } else if (_txtPwd.text.length < 6){
//            toastMsg = [MCLocalization stringForKey:@"pwd6char"];
//            [self toastMessagemethod];
//        } else {
//            [self loginWebServicesMethod];
//        }
//        
//    } else {
    
        if ([_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
            toastMsg = [MCLocalization stringForKey:@"pleaseEnterMobile"];
            [self toastMessagemethod];
        } else if (![Validate isValidMobileNumber:[_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
            toastMsg = [MCLocalization stringForKey:@"mobilevalidate"];
            [self toastMessagemethod];
        } else if (![self validatePhone:[_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
            toastMsg = [MCLocalization stringForKey:@"startWith5"];
            [self toastMessagemethod];
        } else  if ([_txtPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
            toastMsg =[MCLocalization stringForKey:@"PleaseEnterpassword"];
            [self toastMessagemethod];
        } else if (_txtPwd.text.length < 6){
            toastMsg = [MCLocalization stringForKey:@"pwd6char"];
            [self toastMessagemethod];
        } else {
            [self checkTokenStatusWebServices];
        }

    
}



-(void)DeviceToken

{
    
    
    NSString *token = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", token);
    
    
    
    NSString*Number =[@"966" stringByAppendingString:_txtMobNum.text];
    NSString * devicetype = @"iphone_SA";
    
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/notificationDetails/insertDeviceTokenDetails"];
    
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    
    NSString * userId = [@"966" stringByAppendingString:_txtMobNum.text];
    NSString * userPwd = _txtPwd.text;
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
//Request:user_mob,devicetoken,devicetype,unique_id,access_Token,from;
    
    
    
    NSDictionary * loginDetailsDict =  @{@"user_mob":Number,
                                         @"devicetype":devicetype,
                                         @"devicetoken":token,
                                         @"ipAddress":Ipaddress,
                                         @"from":@"iphone",
                                         @"access_token":a123,
                                         @"unique_id":UUID
                                         
                                         };
        
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            [LoaderClass removeLoader:self.view];
            
      
            NSLog(@"Killer Response is%@",Killer);

            NSLog(@"str Response is%@",str);
            
        });
        
        
    }];
    
    
    [dataTask resume];
}





-(void)viewDidDisappear:(BOOL)animated{
    
    
    [_btnEmailOutlet setSelected:NO];
    [_btnMoboutlet setSelected:NO];
    _txtMobNum.text = @"";
    _txtPwd.text = @"";
    
    [locationManager stopUpdatingLocation];
    
    noDataLabel.hidden = YES;
    
    
}






@end
