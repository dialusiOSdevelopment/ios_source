//
//  PaymentDetailsVC.h
//  DialuzApp
//
//  Created by Sai krishna on 10/17/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import <PayFortSDK/PayFortSDK.h>
#import "WebServicesMethods.h"
#import "ConnectivityManager.h"
#import "HomeVC.h"
#import "WalletVC.h"
#import "LoaderClass.h"
#import "WalletSuccessVC.h"
#import "SadadViewController.h"
#import "UIView+Toast.h"



#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"




@interface PaymentDetailsVC : UIViewController<PayFortDelegate, NSURLSessionDelegate>{
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    NSDictionary * signatureDetails, * responseDict, * SDKresponsedict, * returnUrlDetails;
    NSString *  deviceId, * signature;
    PayFortController * payFort;
    NSDictionary * paymentGatewayDetailsDict, * SADADresponseDict;
    
    NSDictionary * profileDict;
    
    NSInteger test;
    
     NSString * a123, * methodname;

}
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *mobNum;
@property (strong, nonatomic) IBOutlet UILabel *email;
@property (strong, nonatomic) IBOutlet UILabel *amount;
@property (strong, nonatomic) IBOutlet UIButton *proceedPayName;
@property (strong, nonatomic) IBOutlet UITextField *txtEnterSadadId;


//Names
@property (strong, nonatomic) IBOutlet UILabel *nameName;
@property (strong, nonatomic) IBOutlet UILabel *mobNumName;
@property (strong, nonatomic) IBOutlet UILabel *emailName;
@property (strong, nonatomic) IBOutlet UILabel *amountName;
@property (strong, nonatomic) IBOutlet UILabel *lblEnterSadadName;
@property (strong, nonatomic) IBOutlet UILabel *lblSadadDots;



- (IBAction)btnProceedToPay:(id)sender;

@end
