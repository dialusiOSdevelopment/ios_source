//
//  UpdatePwdVC.h
//  Saddly
//
//  Created by Sai krishna on 8/7/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "WebServicesMethods.h"
#import "ConnectivityManager.h"
#import "Validate.h"
#import "UIView+Toast.h"
#import "LoaderClass.h"
#import "LoginViewController.h"
#import "UpdatePwdOTPVC.h"

@interface UpdatePwdVC : UIViewController <UITextFieldDelegate, NSURLSessionDelegate> {
    
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle,* alertMessage, * toastMsg;
    
    NSDictionary * changePwdDetails;
    NSString * methodname, * a123;
    NSDictionary * profileDict;
    
}



@property (strong, nonatomic) IBOutlet UITextField *txtCurrentPwd;
@property (strong, nonatomic) IBOutlet UITextField *txtNewPwd;
@property (strong, nonatomic) IBOutlet UITextField *txtConfirmPwd;

@property (strong, nonatomic) IBOutlet UIButton *btnOldShowPwdOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnNewShowPwdOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnReEnterNewShowPwdOutlet;



// names
@property (strong, nonatomic) IBOutlet UILabel *lblOldPwd;
@property (strong, nonatomic) IBOutlet UILabel *lblNewPwd;
@property (strong, nonatomic) IBOutlet UILabel *lblConfirmPwd;

@property (strong, nonatomic) IBOutlet UIButton *btnUpdatePwdOutlet
;

- (IBAction)btnUpdatePwd:(id)sender;

- (IBAction)btnOldShowPwd:(id)sender;
- (IBAction)btnNewShowPwd:(id)sender;
- (IBAction)btnReEnterNewShowPwd:(id)sender;




@end
