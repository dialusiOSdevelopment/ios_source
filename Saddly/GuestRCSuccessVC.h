//
//  GuestRCSuccessVC.h
//  Saddly
//
//  Created by Sai krishna on 4/27/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "MCLocalization.h"
#import "UIView+Toast.h"
#import "LoaderClass.h"
#import "ConnectivityManager.h"
#import "WebServicesMethods.h"
#import "Validate.h"

#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
#import "GuestRechargeVC.h"
#import "RegistrationVC.h"
#import "LoginViewController.h"
@interface GuestRCSuccessVC : UIViewController<UIAlertViewDelegate, NSURLSessionDelegate> {
    
    
    
    UIAlertController * alert;
    UIAlertAction * okButton,*Cancel;
    NSString * alertTitle,* alertMessage, * toastMsg;
    
    NSDictionary *  Transfromwallet,*SuccessResponseDict,*WalletSuccessDict, * iqamaDetails;
    
    NSDictionary * profileDict;
    NSString * voucherCode;
    

     NSString * methodname, * a123;
    
    
}

@property (strong, nonatomic) NSString * guestEmailId;


//names
@property (strong, nonatomic) IBOutlet UILabel *TnqTxtname;
@property (strong, nonatomic) IBOutlet UILabel *MobileSuccesName;
@property (strong, nonatomic) IBOutlet UILabel *OperatorSuccessName;
@property (strong, nonatomic) IBOutlet UILabel *RechargeAmountSuccessName;
@property (strong, nonatomic) IBOutlet UIButton *DoneSuccessName;
@property (strong, nonatomic) IBOutlet UILabel *txnStatusName;
@property (strong, nonatomic) IBOutlet UILabel *walletAmountName;
@property (strong, nonatomic) IBOutlet UITextView *txtViewContact;
@property (strong, nonatomic) IBOutlet UITextView *txtViewRefNum;
@property (strong, nonatomic) IBOutlet UILabel *lblVoucherCodeName;
@property (strong, nonatomic) IBOutlet UILabel *lblIqmaNumberName;

@property (strong, nonatomic) IBOutlet UILabel *lblUSSDCode;

@property (strong, nonatomic) IBOutlet UIButton *btnClickToRcOutlet;

@property (strong, nonatomic) IBOutlet UITextField *txtEnterIqamaNumber;
@property (strong, nonatomic) IBOutlet UIButton *btnCopyOutlet;

@property (strong, nonatomic) IBOutlet UIButton *btnVoucherCodeCopyOutlet;

@property (strong, nonatomic) IBOutlet UITextView *txtviewFailureRespMsgs;



//actions
@property (strong, nonatomic) IBOutlet UILabel *MobilenumberSuccessTxt;
@property (strong, nonatomic) IBOutlet UILabel *OperatorSuccessTxt;
@property (strong, nonatomic) IBOutlet UILabel *AmountSucceessTxt;
@property (strong, nonatomic) IBOutlet UILabel *txnStatusSuccessTxt;
@property (strong, nonatomic) IBOutlet UILabel *lblWalletAmount;

@property (strong, nonatomic) IBOutlet UILabel *lblVoucherCode;

- (IBAction)btnClickToRc:(id)sender;
- (IBAction)btnCopy:(id)sender;
- (IBAction)btnVoucherCodeCopy:(id)sender;

- (IBAction)DoneButtonAction:(id)sender;




@end
