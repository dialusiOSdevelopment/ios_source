////
////  PayfromCard.h
////  DialuzApp
////
////  Created by Sai krishna on 11/11/16.
////  Copyright © 2016 Dialuz. All rights reserved.
////
//
#import <UIKit/UIKit.h>
#import "WebServicesMethods.h"
#import "RehargeSuccessVC.h"
#import <PayFortSDK/PayFortSDK.h>
#import "MCLocalization.h"
#import "ConnectivityManager.h"
#import "LoaderClass.h"
#import "MobileRechargeVC.h"
#import "UIView+Toast.h"
#import "RechargeSadadViewController.h"

#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"



//
@interface PayfromCard : UIViewController<PayFortDelegate, NSURLSessionDelegate>{
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle,* alertMessage, * toastMsg;
    PayFortController * payFort;
    NSString*deviceid,*signature;
    NSDictionary * signatureDetails, * responseDict, * SDKresponsedict, * txnDetailsDictCard;
    
    NSDictionary * Walletdetails,*CardTranDetails,*SADADresponseDict;
    
    NSString * methodname, * a123;
    
    NSDictionary * profileDict;
    
    NSInteger test;
    

}



@property (strong, nonatomic) UIActivityIndicatorView * activityView;
@property (strong, nonatomic) UIView *loadingView;
@property (strong, nonatomic) UILabel *loadingLabel;
@property (strong, nonatomic) UIImageView * loadingimage;



//names
@property (strong, nonatomic) IBOutlet UILabel *MobilenumName;
@property (strong, nonatomic) IBOutlet UILabel *OperatorName;
@property (strong, nonatomic) IBOutlet UILabel *RechargeplanName;
@property (strong, nonatomic) IBOutlet UIButton *ProceedtopayCardName;
@property (strong, nonatomic) IBOutlet UILabel *rcAmountName;
@property (strong, nonatomic) IBOutlet UILabel *lblEnterSadadName;


@property (strong, nonatomic) IBOutlet UIImageView *imgOperator;
@property (strong, nonatomic) IBOutlet UILabel *lblFromName;
@property (strong, nonatomic) IBOutlet UILabel *lblOherPaymentModesName;

@property (strong, nonatomic) IBOutlet UILabel *lblDotsSadadId;


//values
@property (strong, nonatomic) IBOutlet UILabel *MobilenumbercardTxt;
@property (strong, nonatomic) IBOutlet UILabel *OperatorCardTxt;
@property (strong, nonatomic) IBOutlet UILabel *PlancardTxt;
@property (strong, nonatomic) IBOutlet UILabel *AmountFromcardTxt;
@property (strong, nonatomic) IBOutlet UILabel *lblRcAmount;
@property (strong, nonatomic) IBOutlet UITextField *txtEnterSadadId;


- (IBAction)Proccedtopaycard:(id)sender;


@end
