//
//  ConnectivityManager.m
//  Spoil
//
//  Created by Cosmic Kayka on 25/04/15.
//  Copyright (c) 2015 Spoil. All rights reserved.
//

#import "ConnectivityManager.h"

@implementation ConnectivityManager

+ (id)sharedManager
{
    static ConnectivityManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    
    return sharedMyManager;
}

#pragma mark -
#pragma mark Reachability Methods

- (BOOL) getInternetStatus:(Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired= [curReach connectionRequired];
    switch (netStatus)
    {
        case NotReachable:
        {
            connectionRequired= NO;
            break;
        }
            
        case ReachableViaWWAN:
        {
            connectionRequired= YES;
            self.isWifi = NO;
            
            break;
        }
        case ReachableViaWiFi:
        {
            connectionRequired= YES;
            self.isWifi = YES;
            break;
        }
    }
    
    if(connectionRequired)
    {
        
    }
    return connectionRequired;
}

- (BOOL) updateInterfaceWithReachability:(Reachability *) curReach
{
    BOOL Status=FALSE;
    if(curReach == internetReach)
    {
        Status = [self getInternetStatus: curReach];
    }
    
    return Status;
}

- (void) reachabilityChanged: (NSNotification* )note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    [self updateInterfaceWithReachability: curReach];
}

-(BOOL) CheckConnectivity
{
    internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    BOOL Status = [self updateInterfaceWithReachability: internetReach];
    return Status;
}

@end
