//
//  WalletSuccessVC.m
//  DialuzApp
//
//  Created by Sai krishna on 1/6/17.
//  Copyright © 2017 Dialuz. All rights reserved.
//

#import "WalletSuccessVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface WalletSuccessVC ()

@end

@implementation WalletSuccessVC

- (void)viewDidLoad {
    
    
    //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Wallet Success Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //

    
    _lblRefNum.hidden=YES;
    
    _lblAmount.adjustsFontSizeToFitWidth=YES;
    _lblName.adjustsFontSizeToFitWidth=YES;
    _lblNumber.adjustsFontSizeToFitWidth=YES;
    _ContactUS.tintAdjustmentMode=YES;
    
    
    _lblCongrats.adjustsFontSizeToFitWidth=YES;
    _lblUSuccAddName.adjustsFontSizeToFitWidth=YES;
    _lblToName.adjustsFontSizeToFitWidth=YES;
    
    
    NSUserDefaults *SuccessResponse = [NSUserDefaults standardUserDefaults];
    
    SuccessResponseDict = [NSKeyedUnarchiver unarchiveObjectWithData:[SuccessResponse objectForKey:@"SuccessResponse"]];
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        NSString*txt=@"في حال وجود أي مشاكل يمكنك استخدام الرقم المشاراليه";
        
        if ([[SuccessResponseDict valueForKey:@"orderId"] length]<1) {
            
            Ref= @"";
        } else{
            Ref= [SuccessResponseDict valueForKey:@"orderId"];
            
        }
        
        NSString*Text1=@"  حال اتصالك بسددلي  ";
        
        
        _ContactUS.text=[[txt stringByAppendingString:Ref]stringByAppendingString:Text1];
        
        _LBLNUMMAil.text=@" للمزيد من الاستفسارات يمكنك التواصل معنا عبر info@saddly.com 920000092";
        
    } else {
        
        NSString*txt=@"In Case of any Issues,You may use Reference No ";
        
        Ref=  [SuccessResponseDict valueForKey:@"orderId"];
        
        NSString*Text1=@" While Contacting Saddly.";
        
        
        _ContactUS.text=[[txt stringByAppendingString:Ref]stringByAppendingString:Text1];
        
        _LBLNUMMAil.text=@"For more Queries reach out to us at info@saddly.com & 92 00000 92.";

        
    }

   
    
    self.title = [MCLocalization stringForKey:@"status"];
    
    NSLog(@"%@",SuccessResponseDict);
    
    NSLog(@"Success Response is%@",SuccessResponseDict);
    
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Wallet"
            action:[SuccessResponseDict valueForKey:@"addedvalueMob"]
            label:[SuccessResponseDict  valueForKey:@"response_message"]
            value:[NSNumber numberWithInteger:[[SuccessResponseDict valueForKey:@"addedValueAmount"] integerValue]]] build]];


    if ([[SuccessResponseDict valueForKey:@"response_message"] isEqualToString:@"AddMoneySuccess"]) {
        
        _imgStatus.image = [UIImage imageNamed:@"success.png"];
        
        _lblCongrats.text = [MCLocalization stringForKey:@"congrats"];
        _lblUSuccAddName.text = [MCLocalization stringForKey:@"uHaveSucAdded"];
        
    } else {
        
        _imgStatus.image = [UIImage imageNamed:@"cancel.png"];
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
            
            _lblCongrats.text = @"فشلت";
            _lblUSuccAddName.text = @"فضلاُ حاول مره اخرى";
            
        } else {
            
            _lblCongrats.text = @"Failed";
            _lblUSuccAddName.text = @"Please try again";
        }
    
        
    }
    
    NSString *amount = [NSString stringWithFormat:@"%@", [SuccessResponseDict valueForKey:@"amount"]];
    
    NSLog(@"Amount is%@",amount);

    _lblAmount.text = [amount stringByAppendingString:@" ﷼"];

    
    _lblRefNum.text = [[MCLocalization stringForKey:@"refNum"] stringByAppendingString:[SuccessResponseDict valueForKey:@"orderId"]];
    _lblRefNum.adjustsFontSizeToFitWidth = YES;

    
    
    
    
//    // Names
//    _lblCongrats.text = [MCLocalization stringForKey:@"congrats"];
//    _lblUSuccAddName.text = [MCLocalization stringForKey:@"uHaveSucAdded"];
//    _lblToName.text = [MCLocalization stringForKey:@"To"];
    
    [_btnBackToHomeOutlet setTitle:[MCLocalization stringForKey:@"backToHome"] forState:UIControlStateNormal];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



// Enabling the Back Button
-(void)viewDidAppear:(BOOL)animated{
    
    
    _lblRefNum.hidden=YES;

    _lblAmount.adjustsFontSizeToFitWidth=YES;
    _lblName.adjustsFontSizeToFitWidth=YES;
    _lblNumber.adjustsFontSizeToFitWidth=YES;
    _lblRefNum.adjustsFontSizeToFitWidth=YES;
    
    
    _lblCongrats.adjustsFontSizeToFitWidth=YES;
    _lblUSuccAddName.adjustsFontSizeToFitWidth=YES;
    _lblToName.adjustsFontSizeToFitWidth=YES;
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
}

 

- (IBAction)btnBackToHome:(id)sender {
    
    NSUserDefaults * addMoneyDefaults = [NSUserDefaults standardUserDefaults];
    [addMoneyDefaults removeObjectForKey:@"addMoneyDefaults"];
    
    [self performSegueWithIdentifier:@"HomeFrmWalletSuccess" sender:nil];


}


@end
