//
//  GuestRCSuccessVC.m
//  Saddly
//
//  Created by Sai krishna on 4/27/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "GuestRCSuccessVC.h"

@interface GuestRCSuccessVC ()

@end

@implementation GuestRCSuccessVC

- (void)viewDidLoad {
    
    
    //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Guest Recharge Success Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //
    
    
    _TnqTxtname.adjustsFontSizeToFitWidth=YES;
    _MobileSuccesName.adjustsFontSizeToFitWidth=YES;
    _OperatorSuccessName.adjustsFontSizeToFitWidth=YES;
    _RechargeAmountSuccessName.adjustsFontSizeToFitWidth=YES;
    _txnStatusName.adjustsFontSizeToFitWidth=YES;
    _walletAmountName.adjustsFontSizeToFitWidth=YES;
    _MobilenumberSuccessTxt.adjustsFontSizeToFitWidth=YES;
    _OperatorSuccessTxt.adjustsFontSizeToFitWidth=YES;
    _AmountSucceessTxt.adjustsFontSizeToFitWidth=YES;
    _txnStatusSuccessTxt.adjustsFontSizeToFitWidth=YES;
    _lblWalletAmount.adjustsFontSizeToFitWidth=YES;
    _lblVoucherCode.adjustsFontSizeToFitWidth = YES;
    _lblVoucherCodeName.adjustsFontSizeToFitWidth = YES;
    _lblIqmaNumberName.adjustsFontSizeToFitWidth = YES;
    _lblUSSDCode.adjustsFontSizeToFitWidth = YES;
    
    
    
    _btnVoucherCodeCopyOutlet.hidden = NO;
    _lblUSSDCode.hidden = YES;
    _btnCopyOutlet.hidden = YES;
    
    _lblVoucherCodeName.hidden = NO;
    _lblVoucherCode.hidden = NO;
    
    
    
    _txtViewRefNum.hidden = NO;
    _txtViewContact.hidden = NO;
    _txtviewFailureRespMsgs.hidden = NO;
    
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];

    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    
    NSUserDefaults *Operator = [NSUserDefaults standardUserDefaults];
    
    NSString*op=   [Operator valueForKey:@"Operator"];
    
    _OperatorSuccessTxt.text=op;
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
    }
    
    
    NSUserDefaults *SuccessResponsecard = [NSUserDefaults standardUserDefaults];
    SuccessResponseDict = [NSKeyedUnarchiver unarchiveObjectWithData:[SuccessResponsecard objectForKey:@"SuccessResponsecard"]];
    

    NSLog(@"SuccessResponseDict: %@",SuccessResponseDict);
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Recharge"
                                                          action:[SuccessResponseDict valueForKey:@"rechargeMob"]
                                                           label:[SuccessResponseDict valueForKey:@"response_message"]
                    //operator:  [SuccessResponseDict valueForKey:@"selectedoperator"]
                                                           value:[NSNumber numberWithInteger:[[SuccessResponseDict valueForKey:@"rechargeAmount"] integerValue]]] build]] ;
    
    

        
        self.title = [MCLocalization stringForKey:@"RcStatus"];
        _OperatorSuccessName.text=[MCLocalization stringForKey:@"Operator"] ;
        
   
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    
    NSUserDefaults * couponcodenum = [NSUserDefaults standardUserDefaults];
    [couponcodenum removeObjectForKey:@"couponcodenum"];
    
    
    _TnqTxtname.text=[MCLocalization stringForKey:@"Tnxtxt"] ;
    _MobileSuccesName.text=[MCLocalization stringForKey:@"MobileNumberCard"] ;
    _RechargeAmountSuccessName.text=[MCLocalization stringForKey:@"RechargeAmountSuccess"] ;
    _txnStatusName.text = [MCLocalization stringForKey:@"txnStatus"];
    _walletAmountName.text = [MCLocalization stringForKey:@"WalletAmount"];
    _txtViewContact.text = [MCLocalization stringForKey:@"dialusContactInfo"];
    _lblVoucherCodeName.text = [MCLocalization stringForKey:@"voucherCode"];
    [_DoneSuccessName setTitle:[MCLocalization stringForKey:@"done"] forState:UIControlStateNormal];
    [_btnClickToRcOutlet setTitle:[MCLocalization stringForKey:@"clicktoRc"] forState:UIControlStateNormal];
    
    
    NSUserDefaults * rcOperator = [NSUserDefaults standardUserDefaults];
    NSString*operator=[NSString stringWithFormat:@"%@",[rcOperator stringForKey:@"rcOperator"]];
    _OperatorSuccessTxt.text=operator;
    
    NSUInteger num = [SuccessResponseDict count];
    
    NSLog(@"number ids %lu",num);
    
    _walletAmountName.hidden=YES;
    _lblWalletAmount.hidden=YES;
   
        
        // From wallet User Defaults
        _MobilenumberSuccessTxt.text = [NSString stringWithFormat:@"%@",[SuccessResponseDict valueForKey:@"rechargeMob"]];
        
        if ([[SuccessResponseDict valueForKey:@"iqmaNumber"] isEqual:[NSNull null]]) {
            
            _txtEnterIqamaNumber.placeholder = [MCLocalization stringForKey:@"enterIqmaNum"];
            
        } else  if ([[SuccessResponseDict valueForKey:@"iqmaNumber"] isEqualToString:@"no iqma number"]) {
            _txtEnterIqamaNumber.placeholder = [MCLocalization stringForKey:@"enterIqmaNum"];
            
        } else {
            
            _txtEnterIqamaNumber.text =  [NSString stringWithFormat:@"%@", [SuccessResponseDict valueForKey:@"iqmaNumber"]];
        }
        
        _AmountSucceessTxt.text = [NSString stringWithFormat:@"%@", [SuccessResponseDict valueForKey:@"rechargeAmount"]];
        
        if ([[SuccessResponseDict valueForKey:@"response_message"] isEqual:[NSNull null]]) {
            
            _txnStatusSuccessTxt.text = [MCLocalization stringForKey:@"failure"];
            
        } else {
            
            _txnStatusSuccessTxt.text = [NSString stringWithFormat:@"%@",[SuccessResponseDict  valueForKey:@"response_message"]];
            
        }
        
        
        _lblWalletAmount.text = [NSString stringWithFormat:@"%@", [SuccessResponseDict  valueForKey:@"walletAmount"]];
        
        _lblVoucherCode.text =  [NSString stringWithFormat:@"%@", [SuccessResponseDict  valueForKey:@"rechargevoucherCode"]];
        
        _txtViewRefNum.text= [NSString stringWithFormat:[[[MCLocalization stringForKey:@"inCaseRefNo"] stringByAppendingString:@" %@ "] stringByAppendingString:[MCLocalization stringForKey:@"whileContactingDialus"]], [SuccessResponseDict  valueForKey:@"orderId"]];
        
        _txtviewFailureRespMsgs.text = [[[MCLocalization stringForKey:@"dialusContactInfo"] stringByAppendingString:@"\n"] stringByAppendingString:_txtViewRefNum.text];
        
        
        NSString*msg=[SuccessResponseDict valueForKey:@"response_message"];
        
        if ([msg isEqual:[NSNull null]]) {
            msg=[MCLocalization stringForKey:@"failure"];
        }
        
        if (![msg isEqualToString:@"Success"]||[msg isEqualToString:@"success"]) {
            
            _txnStatusSuccessTxt.text = [MCLocalization stringForKey:@"failure"];
            _txnStatusSuccessTxt.textColor = [UIColor redColor];
            
            _txtEnterIqamaNumber.hidden = YES;
            _btnClickToRcOutlet.hidden = YES;
            
            _txtViewRefNum.hidden = YES;
            _txtViewContact.hidden = YES;
            _txtviewFailureRespMsgs.hidden = NO;
            
            _btnVoucherCodeCopyOutlet.hidden = YES;
            _lblVoucherCodeName.hidden = YES;
            _lblVoucherCode.hidden = YES;
            
            
        } else {
            
            _txnStatusSuccessTxt.text = [MCLocalization stringForKey:@"success"];
            _txnStatusSuccessTxt.textColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
            
            _lblVoucherCodeName.hidden = NO;
            _lblVoucherCode.hidden = NO;
            
        
                
                _txtEnterIqamaNumber.hidden = NO;
                _btnClickToRcOutlet.hidden = NO;
                
                
                
                _txtViewRefNum.hidden = NO;
                _txtViewContact.hidden = NO;
                _txtviewFailureRespMsgs.hidden = YES;
                
                _btnVoucherCodeCopyOutlet.hidden = NO;
                
            }
            
        
        
    
    
    // total if-else
    
    _txnStatusSuccessTxt.lineBreakMode = NSLineBreakByWordWrapping;
    _txnStatusSuccessTxt.numberOfLines = 2;
    
    if ([_OperatorSuccessTxt.text  isEqualToString:@"Jawwy"] || [_OperatorSuccessTxt.text  isEqualToString:@"Lebara"]) {
        
        _txtEnterIqamaNumber.hidden = NO;
        _btnClickToRcOutlet.hidden = YES;
        _lblUSSDCode.hidden = YES;
        _btnCopyOutlet.hidden = YES;
        
        _txtviewFailureRespMsgs.hidden = YES;
        
        _lblIqmaNumberName.hidden=NO;
        
        _txtViewRefNum.hidden=YES;
        _txtViewContact.hidden=YES;
    }
    else if ([_OperatorSuccessTxt.text  isEqualToString:@"Mobily"]) {
        
        
        if ([[SuccessResponseDict valueForKey:@"planType"]isEqualToString:@"voice"]||[[WalletSuccessDict valueForKey:@"planType"]isEqualToString:@"voice"]) {
            
            voucherCode = [[[[@"*1400*" stringByAppendingString:_lblVoucherCode.text]
                             stringByAppendingString:@"*"] stringByAppendingString:_txtEnterIqamaNumber.text] stringByAppendingString:@"#"];
            
            
            _txtEnterIqamaNumber.hidden = NO;
            _btnClickToRcOutlet.hidden = NO;
            _lblUSSDCode.hidden = YES;
            _btnCopyOutlet.hidden = YES;
            
            _txtviewFailureRespMsgs.hidden = YES;
            
            _lblIqmaNumberName.hidden=NO;
            
            _txtViewRefNum.hidden=NO;
            _txtViewContact.hidden=NO;
            
            
        } else{
            
            _txtEnterIqamaNumber.hidden = YES;
            _btnClickToRcOutlet.hidden = YES;
            _lblUSSDCode.hidden = YES;
            _btnCopyOutlet.hidden = YES;
            
            _txtviewFailureRespMsgs.hidden = NO;
            
            _lblIqmaNumberName.hidden=YES;
            
            _txtViewRefNum.hidden=YES;
            _txtViewContact.hidden=YES;
            
        }
        
    }
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
                            a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                
                if ([methodname isEqualToString:@"GuestRegister"]) {
                    
                    [self GuestRegister];
                    
                }
                
                
                
                
            
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//


-(void)GuestRegister {
    
    
    NSString *token = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", token);
    
    
    NSUserDefaults *Number = [NSUserDefaults standardUserDefaults];
    
    NSString*MobNum =[@"966" stringByAppendingString:[Number valueForKey:@"Number"]];
    
    
    NSString * from = @"iphone_SA";

    NSString*UEmail= _guestEmailId;
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
   NSString* Macaddress=   [macaddressDefaults valueForKey:@"macaddressDefaults"];

    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];

    
    [LoaderClass showLoader:self.view];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/registration/doGuestRegistration"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
//    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
//    
//    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
//    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
//    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
//    
//    
//    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
//    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    NSDictionary * guestRegisDict =  @{@"user_mobile_no":MobNum,
                                       @"user_email_id":UEmail,
                                       @"ipAddress":Ipaddress,
                                       @"from":@"iPhone",
                                       @"db":@"SA",
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],

                                       @"macAddress":Macaddress,
                                     };
    
    NSLog(@"Posting guestRegisDict is %@",guestRegisDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:guestRegisDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSLog(@"No data returned from server, error ocurred: %@", error);
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            

            
            NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            NSError *deserr;
            
            NSDictionary*Response=[NSJSONSerialization
                                   JSONObjectWithData:data
                                   options:kNilOptions
                                   error:&deserr];
            
            NSLog(@"resSrt: %@", Response);


            
            NSMutableDictionary*respdict=[[NSMutableDictionary alloc]init];
            
            [respdict setObject:[Response valueForKey:@"guestStatus"] forKey:@"Message"];
            
            
            //
            
            
            [LoaderClass removeLoader:self.view];
            
            NSLog(@"Status%@",respdict);
            
            
            if ([[Response valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"GuestRegister";
                
                [self checkTokenStatusWebServices];
                
                
            }else if ([[Response valueForKey:@"response_message"]  isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{
            
            
            
            if ([[respdict valueForKey:@"Message"] isEqualToString:@"Success"]) {
                
                
                if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
                    
                    alertMessage = @"تم ارسال اسم المستخدم وكلمة المرور الى  رقم  الجوال . لطفاً قم بتسجيل الدخول للمتابعة";
                    
                } else {
                    
                    alertMessage = @"We Have Send the User ID and Password on Your Mobile Number. Please Login to Continue";
                    
                }
                
                
                alert = [UIAlertController
                         alertControllerWithTitle:alertTitle
                         message:alertMessage
                         preferredStyle:UIAlertControllerStyleAlert];
                
                alert.popoverPresentationController.sourceView = self.view;
                alert.popoverPresentationController.sourceRect = self.view.bounds;
                
                
                okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                                    style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action) {
                                                      
                                                      LoginViewController * rechargesuccess = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                                      
                                                      [self.navigationController pushViewController:rechargesuccess animated:YES];
                                                      
                                                  }];
                
                [alert addAction:okButton];
                [self presentViewController:alert animated:YES completion:nil];
                
            } else if ([[respdict valueForKey:@"Message"] isEqualToString:@"already Registered"]) {
                
                
                if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
                    
                    alertMessage = @"انت مسجل مسبقاً. فضلاً اضغط هنا لتسجيل الدخول";
                    
                } else {
                    
                    alertMessage = @"You are Already Registered. Click Here to Login";
                    
                }
                
                
                alert = [UIAlertController
                         alertControllerWithTitle:alertTitle
                         message:alertMessage
                         preferredStyle:UIAlertControllerStyleAlert];
                
                alert.popoverPresentationController.sourceView = self.view;
                alert.popoverPresentationController.sourceRect = self.view.bounds;
                
                
                okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                                    style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action) {
                                                      
                                                      LoginViewController * rechargesuccess = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                                      
                                                      [self.navigationController pushViewController:rechargesuccess animated:YES];
                                                      
                                                  }];
                
                [alert addAction:okButton];
                [self presentViewController:alert animated:YES completion:nil];
                
                
                
            } else {
                
                
                alertMessage = [[[MCLocalization stringForKey:@"wentWrong"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
                
                
                
                alert = [UIAlertController
                         alertControllerWithTitle:alertTitle
                         message:alertMessage
                         preferredStyle:UIAlertControllerStyleAlert];
                
                alert.popoverPresentationController.sourceView = self.view;
                alert.popoverPresentationController.sourceRect = self.view.bounds;
                
                
                okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                                    style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * action) {
                                                      
                                                      LoginViewController * rechargesuccess = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                                      
                                                      [self.navigationController pushViewController:rechargesuccess animated:YES];
                                                      
                                                  }];
                
                [alert addAction:okButton];
                [self presentViewController:alert animated:YES completion:nil];
                
                
            }
                
            }
    
        });

    }];

    [dataTask resume];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)viewWillAppear:(BOOL)animated{
    
    
    NSUserDefaults *Operator = [NSUserDefaults standardUserDefaults];
    
    NSString*op=   [Operator valueForKey:@"Operator"];
    
    _OperatorSuccessTxt.text=op;
    
    _walletAmountName.hidden=YES;
    _lblWalletAmount.hidden=YES;
    
    
    _TnqTxtname.adjustsFontSizeToFitWidth=YES;
    _MobileSuccesName.adjustsFontSizeToFitWidth=YES;
    _OperatorSuccessName.adjustsFontSizeToFitWidth=YES;
    _RechargeAmountSuccessName.adjustsFontSizeToFitWidth=YES;
    _txnStatusName.adjustsFontSizeToFitWidth=YES;
    _walletAmountName.adjustsFontSizeToFitWidth=YES;
    _MobilenumberSuccessTxt.adjustsFontSizeToFitWidth=YES;
    _OperatorSuccessTxt.adjustsFontSizeToFitWidth=YES;
    _AmountSucceessTxt.adjustsFontSizeToFitWidth=YES;
    _txnStatusSuccessTxt.adjustsFontSizeToFitWidth=YES;
    _lblWalletAmount.adjustsFontSizeToFitWidth=YES;
    
    
    _txnStatusSuccessTxt.lineBreakMode = NSLineBreakByWordWrapping;
    _txnStatusSuccessTxt.numberOfLines = 2;
    
}

// Removing keyboard
-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}

// Returning keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtEnterIqamaNumber) {
        [self.txtEnterIqamaNumber resignFirstResponder];
    }
    return YES;
}


-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
        [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}




// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"Buttonregistername"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          
                                          [self GuestRegister];
                                          
                                          
    }];
    
    Cancel=[UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"Forgetpasswordcancell"]
                                    style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action) {
                                      
                                      LoginViewController * rechargesuccess = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                      
                                      [self.navigationController pushViewController:rechargesuccess animated:YES];
                                      

                                  }];

    
    [alert addAction:Cancel];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}



//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:_txtEnterIqamaNumber]) {
        
        if([self isAlphaNumericSpecialCharacters:string]){
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 16;
        } else  {
            return FALSE;
        }
        
    }
    return YES;
}


// Entering only alphaNumeric not a Arabic Characters
-(BOOL)isAlphaNumericSpecialCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}



// For Iqama number
-(void)iqamaNumServicesMethod{
    
    [LoaderClass showLoader:self.view];
    
   

    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    
    NSString * mobNum =  _MobilenumberSuccessTxt.text;
    NSString * iqama = _txtEnterIqamaNumber.text;
    NSString * ipAddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    NSString * uEmail = [profileDict valueForKey:@"user_email_id"];
    NSString * from = @"iphone";

    
    ////
    
    
    
    ///user_mob,iqmanumber
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://m.app.saddly.com/Recharge/insertIqmaNumberDetails"]];
    
    
    //create the Method "GET" or "POST"
    [request setHTTPMethod:@"POST"];
    
    
    //Pass The String to server(YOU SHOULD GIVE YOUR PARAMETERS INSTEAD OF MY PARAMETERS)
    NSString *userUpdate =[NSString stringWithFormat:@"user_mob=%@&iqmanumber=%@&user_emailId=%@&ipAddress=%@&from=%@",mobNum,iqama,uEmail,ipAddress,from,nil];
    
    
    
    NSString *apiKey  = @"cWZwbWdlcmZkdGV4em5kc2ZnZGdkZzpxZ2h4Y2ZnaHR5bmpta3U=";
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", apiKey];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    //Check The Value what we passed
    NSLog(@"the data Details is: %@", userUpdate);
    
    //Convert the String to Data
    NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    
    //Apply the data to the body
    [request setHTTPBody:data1];
    
    //Create the response and Error
    NSError *err;
    NSURLResponse *response;
    
    
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSString *resSrt = [[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSLog(@"resSrt: %@", resSrt);
    NSError *deserr;
    
    
    
    NSDictionary*Response=[NSJSONSerialization
                           JSONObjectWithData:responseData
                           options:kNilOptions
                           error:&deserr];
    
    NSLog(@"got response = %@", Response);
    
    [LoaderClass removeLoader:self.view];
    
    NSMutableDictionary*respdict=[[NSMutableDictionary alloc]init];
    
    [respdict setObject:resSrt forKey:@"Message"];
    
    NSLog(@"Iqama Response %@",respdict);
    iqamaDetails = respdict;
    
    if ([[iqamaDetails objectForKey:@"Message"] isEqualToString:@"iqama number should be 10 digits"]) {
        toastMsg = [iqamaDetails objectForKey:@"Message"];
        [self toastMessagemethod];
        
    } else if ([[iqamaDetails objectForKey:@"Message"] isEqualToString:@"success"]) {
        
        
        if ([_OperatorSuccessTxt.text  isEqualToString:@"Quicknet"] || [_OperatorSuccessTxt.text  isEqualToString:@"Sawa"]) {
            
            voucherCode = [[[[@"*155*" stringByAppendingString:_lblVoucherCode.text] stringByAppendingString:@"*"] stringByAppendingString:_txtEnterIqamaNumber.text] stringByAppendingString:@"#"];
            
        } else if ([_OperatorSuccessTxt.text  isEqualToString:@"Virgin"]) {
            
            voucherCode = @"*101#";
            
        } else if ([_OperatorSuccessTxt.text  isEqualToString:@"Friendi"]) {
            
            voucherCode = [[[[@"*101*" stringByAppendingString:_lblVoucherCode.text] stringByAppendingString:@"*"] stringByAppendingString:_txtEnterIqamaNumber.text] stringByAppendingString:@"#"];
            
        } else if ([_OperatorSuccessTxt.text  isEqualToString:@"Jawwy"] || [_OperatorSuccessTxt.text  isEqualToString:@"Lebara"]) {
            
            _txtEnterIqamaNumber.hidden = NO;
            _btnClickToRcOutlet.hidden = YES;
            _lblUSSDCode.hidden = YES;
            _btnCopyOutlet.hidden = YES;
            
            _txtviewFailureRespMsgs.hidden = YES;
            
            _lblIqmaNumberName.hidden=NO;
            
            _txtViewRefNum.hidden=YES;
            _txtViewContact.hidden=YES;
            
            
        } else if ([_OperatorSuccessTxt.text  isEqualToString:@"Zain"]) {
            
            voucherCode = [[[[@"*141*" stringByAppendingString:_txtEnterIqamaNumber.text] stringByAppendingString:@"*"] stringByAppendingString:_lblVoucherCode.text] stringByAppendingString:@"#"];
            
        } else if ([_OperatorSuccessTxt.text  isEqualToString:@"Mobily"]) {
            
            if ([[SuccessResponseDict valueForKey:@"planType"]isEqualToString:@"voice"]||[[WalletSuccessDict valueForKey:@"planType"]isEqualToString:@"voice"]) {
                
                voucherCode = [[[[@"*1400*" stringByAppendingString:_lblVoucherCode.text]
                                 stringByAppendingString:@"*"] stringByAppendingString:_txtEnterIqamaNumber.text] stringByAppendingString:@"#"];
                
                _txtEnterIqamaNumber.hidden = NO;
                _btnClickToRcOutlet.hidden = NO;
                _lblUSSDCode.hidden = NO;
                _btnCopyOutlet.hidden = NO;
                
                _txtviewFailureRespMsgs.hidden = YES;
                
                _lblIqmaNumberName.hidden=NO;
                
                _txtViewRefNum.hidden=NO;
                _txtViewContact.hidden=NO;
                
                
            } else {
                
                _txtEnterIqamaNumber.hidden = YES;
                _btnClickToRcOutlet.hidden = YES;
                _lblUSSDCode.hidden = YES;
                _btnCopyOutlet.hidden = YES;
                
                _txtviewFailureRespMsgs.hidden = NO;
                
                _lblIqmaNumberName.hidden=YES;
                
                _txtViewRefNum.hidden=YES;
                _txtViewContact.hidden=YES;
                
            }
            
        }
        
        _lblUSSDCode. text = voucherCode;
        _lblUSSDCode.hidden = NO;
        _btnCopyOutlet.hidden = NO;
        
        
        
    } else  if ([[iqamaDetails objectForKey:@"Message"] isEqualToString:@"failure"]) {
        toastMsg = [iqamaDetails objectForKey:@"Message"];
        [self toastMessagemethod];
    }
    
    
    
}


- (BOOL)isValidMobileNumber:(NSString*)number
{
    number=[number stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([number length] != 10 ) {
        
        return FALSE;
    }
    NSString *Regex = @"^([0-9]*)$";
    NSPredicate *mobileTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex];
    
    return [mobileTest evaluateWithObject:number];
}




- (IBAction)btnClickToRc:(id)sender {
    
    
    // Iqama Number
    if ([_txtEnterIqamaNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg = [MCLocalization stringForKey:@"iqmaNotEmpty"];
        [self toastMessagemethod];
    }
    
    
//    else if (_txtEnterIqamaNumber.text.length != 10) {
//        
//        if (_txtEnterIqamaNumber.text.length != 14) {
//            
//            toastMsg = [MCLocalization stringForKey:@"iqma10or14"];
//            [self toastMessagemethod];
//            
//        }
//        
//    }

    
    else {
        
        [self iqamaNumServicesMethod];
        
        
    }
    
}

- (IBAction)btnCopy:(id)sender {
    
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _lblUSSDCode.text;
    
    toastMsg  = [MCLocalization stringForKey:@"ussdCopied"];
    [self toastMessagemethod];
    
}

- (IBAction)btnVoucherCodeCopy:(id)sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _lblVoucherCode.text;
    
    toastMsg  = [MCLocalization stringForKey:@"voucherCodeCopy"];
    [self toastMessagemethod];
    
}






- (IBAction)DoneButtonAction:(id)sender {
    
    alertMessage= [MCLocalization stringForKey:@"GuestRegistration"];
    [self alertMethod];
    
    NSUserDefaults * kill = [NSUserDefaults standardUserDefaults];
    [kill setObject:@"20" forKey:@"kill"];
    
    NSUserDefaults *SuccessResponse = [NSUserDefaults standardUserDefaults];
    [SuccessResponse removeObjectForKey:@"SuccessResponse"];
    
    NSUserDefaults *SuccessResponsecard = [NSUserDefaults standardUserDefaults];
    [SuccessResponsecard removeObjectForKey:@"SuccessResponsecard"];
    
    
    NSUserDefaults *WalletSuccessResponse = [NSUserDefaults standardUserDefaults];
    [WalletSuccessResponse removeObjectForKey:@"WalletSuccessResponse"];
    
    NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
    [selectedOperator removeObjectForKey:@"selectedOperator"];
    
    NSUserDefaults * recentRcOperatorsDefaults = [NSUserDefaults standardUserDefaults];
    [recentRcOperatorsDefaults removeObjectForKey:@"recentRcOperatorsDefaults"];
    
    
    
}


@end
