//
//  QuickRechargeVC.m
//  Saddly
//
//  Created by Sai krishna on 7/17/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "QuickRechargeVC.h"

@interface QuickRechargeVC ()

@end

@implementation QuickRechargeVC

- (void)viewDidLoad {
    
    self.title = [MCLocalization stringForKey:@"quickRc"];
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    

    _lblUSSDCode.hidden = YES;
    _btnCopyOutlet.hidden = YES;

    
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        operatorNames = @[@"سوى",@"جوي",@"فيرجن",@"فريندلي",@"زين",@"سوا", @"موبايلي",@"ليبارا"];
        
        operatorImages = @[@"Sawaar.png",@"Zainar.png", @"Virginar.png",@"Friendiar.png", @"Jawwyar.png",@"Quicknetar.png",@"Mobilyar.png", @"Leberaar.png"];
        
    } else {
        
        
        operatorNames = @[@"Sawa",@"Zain",@"Virgin",@"Friendi",@"Jawwy",@"Quicknet", @"Mobily",@"Lebara"];
        operatorImages = @[@"sawaen.png",@"zainen.png", @"virginen.png",@"friendien.png", @"jawwyen.png",@"quickneten.png",@"mobilyen.png", @"lebaraen.png"];
        
    }
    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    [tapKeyboardGesture setCancelsTouchesInView:NO];

    
    // Resign First Responder from the mobile text field.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي"  style:UIBarButtonItemStyleDone target:self   action:@selector(NextClicked)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    _txtVoucherCode.inputAccessoryView = keyboardDoneButtonView;
    
    _txtVoucherCode.text = _voucherCodeFrmSuccess;
    
    if ([_iqmaNumberFrmSuccess isEqualToString:@"no iqma number"]) {
        
        _iqmaNumberFrmSuccess=@"";
    }
    _txtIqmaNumber.text = _iqmaNumberFrmSuccess;
    
    
    selOperatorName = _operatorFrmSuccess;
    
    if (selOperatorName.length > 1) {
        
        _lblVoucherCodeName.text = [[selOperatorName stringByAppendingString:@" "] stringByAppendingString:[MCLocalization stringForKey:@"vouchCode"]];
        
    } else {
        selOperatorName = @"";
        _lblVoucherCodeName.text = [MCLocalization stringForKey:@"vouchCode"];
        
    }
    
    
    _lblVoucherCodeName.adjustsFontSizeToFitWidth=YES;
    
    [self.collecOperators reloadData];

    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)removingKeyboardByTap {
    
    [self.view endEditing:YES];
}


// Returning KeyBoard for all text fields
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == self.txtVoucherCode) {
        [self.txtIqmaNumber becomeFirstResponder];
    } else if (textField == self.txtIqmaNumber) {
        [self.txtIqmaNumber resignFirstResponder];
    }
    
    return YES;
}



#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:_collecOperators]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}

-(void)viewWillAppear:(BOOL)animated {
    
    
    _lblUSSDCode.adjustsFontSizeToFitWidth=YES;
    _lblIqmaNumName.adjustsFontSizeToFitWidth=YES;
    _lblVoucherCodeName.adjustsFontSizeToFitWidth=YES;

    _lblSelOperatorName.text = [MCLocalization stringForKey:@"selOperator"];
    _lblVoucherCodeName.text = [MCLocalization stringForKey:@"vouchCode"];
    _lblIqmaNumName.text = [MCLocalization stringForKey:@"iqmaNum"];
    _txtVoucherCode.placeholder = [MCLocalization stringForKey:@"vouchCode"];
    _txtIqmaNumber.placeholder = [MCLocalization stringForKey:@"iqmaNum"];
    
    [_btnRcoutlet setTitle:[MCLocalization stringForKey:@"recharge"] forState:UIControlStateNormal];
    
    
    operatorNames = @[[MCLocalization stringForKey:@"Sawaname"],
                      [MCLocalization stringForKey:@"Zainname"],
                      [MCLocalization stringForKey:@"Virginname"],
                      [MCLocalization stringForKey:@"Friendiname"],
                      [MCLocalization stringForKey:@"Jawwyname"],
                      [MCLocalization stringForKey:@"Quicknetname"],
                      [MCLocalization stringForKey:@"Mobilyname"],
                      [MCLocalization stringForKey:@"Lebaraname"]];

    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
//        operatorNames = @[@"سوى",@"جوي",@"فيرجن",@"فريندلي",@"زين",@"سوا", @"موبايلي",@"ليبارا"];
        
        operatorImages = @[@"Sawaar.png",@"Zainar.png", @"Virginar.png",@"Friendiar.png", @"Jawwyar.png",@"Quicknetar.png",@"Mobilyar.png", @"Leberaar.png"];
        
    } else {
        
//        operatorNames = @[@"Sawa",@"Zain",@"Virgin",@"Friendi",@"Jawwy",@"Quicknet", @"Mobily",@"Lebara"];
        operatorImages = @[@"sawaen.png",@"zainen.png", @"virginen.png",@"friendien.png", @"jawwyen.png",@"quickneten.png",@"mobilyen.png", @"lebaraen.png"];
        
    }
    
    _txtVoucherCode.text = _voucherCodeFrmSuccess;
    _txtIqmaNumber.text = _iqmaNumberFrmSuccess;
    
    
    selOperatorName = _operatorFrmSuccess;
    
    if (selOperatorName.length > 1) {
        
        selOperatorName = _operatorFrmSuccess;
        _lblVoucherCodeName.text = [[selOperatorName stringByAppendingString:@" "] stringByAppendingString:[MCLocalization stringForKey:@"vouchCode"]];
        
    } else {
        
        selOperatorName = @"";
        _lblVoucherCodeName.text = [MCLocalization stringForKey:@"vouchCode"];
        
    }
    
    [self.collecOperators reloadData];
    
}

-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                if ([methodname isEqualToString:@"DirectRecharge"]) {
                    
                    [self DirectRecharge];
                    
                }
                else if ([methodname isEqualToString:@"DirectRecharge"]) {
                    
                    [self DirectRecharge];
                    
                }
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//



            


// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}



//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:_txtIqmaNumber]) {
        
        if([self isAlphaNumericSpecialCharacters:string]){
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 16;
        } else  {
            return FALSE;
        }
    }
    return YES;
}


// Entering only alphaNumeric not a Arabic Characters
-(BOOL)isAlphaNumericSpecialCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}


#pragma mark --
#pragma mark - UICollectionView Delegate Methods


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return operatorNames.count;
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    QuickRcCollectionCell * cell = (QuickRcCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    if (cell == nil) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"QuickRcCollectionCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.imgOperator.image = [UIImage imageNamed:[operatorImages objectAtIndex:indexPath.row]];
    cell.layer.borderColor = [UIColor clearColor].CGColor;
    
    return cell;
    
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    selOperatorName = [operatorNames objectAtIndex:indexPath.row];
    
    _lblVoucherCodeName.adjustsFontSizeToFitWidth=YES;
    _lblVoucherCodeName.text = [[selOperatorName stringByAppendingString:@" "] stringByAppendingString:[MCLocalization stringForKey:@"vouchCode"]];
    
    _lblUSSDCode.text = @"";
    _lblUSSDCode.hidden = YES;
    _btnCopyOutlet.hidden = YES;
    
    _txtVoucherCode.text = @"";

    
}



// For Iqama number
-(void)iqamaNumServicesMethod{
    
    
    
    [LoaderClass showLoader:self.view];
    
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    
    
    
    NSString * mobNum =  [profileDict valueForKey:@"user_mob"];
    NSString * iqama = _txtIqmaNumber.text;
    NSString * ipAddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    NSString * uEmail = [profileDict valueForKey:@"user_email_id"];
    NSString * from = @"iphone";
    
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/insertIqmaNumberDetails"];
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
  
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    NSDictionary * loginDetailsDict =  @{@"user_mob":mobNum,
                                         
                                         @"iqmanumber":iqama,
                                         
                                         @"user_emailId":uEmail,
                                         @"ipAddress":ipAddress,
                                         @"from":from,
                                         @"accessToken":accessToken,
                                         @"unique_id":uniqueid
                                         
                                         };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *deserr;
            
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
            NSString*     resSrt  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            [LoaderClass removeLoader:self.view];
            
            
            NSLog(@"Killer Response is%@",resSrt);
            NSLog(@"Killer Response is%@",Killer);

            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"DirectRecharge";
                
                [self checkTokenStatusWebServices];
                
                
            } else if ([[Killer  valueForKey:@"response_message"]isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else{
    
            
            NSMutableDictionary*respdict=[[NSMutableDictionary alloc]init];
            
            [respdict setObject:[Killer valueForKey:@"insertIqmaNumberDetails"] forKey:@"Message"];
            
            NSLog(@"Iqama Response %@",respdict);
            
            iqamaDetails = respdict;
            
            
            
            
            if ([[iqamaDetails objectForKey:@"Message"] isEqualToString:@"iqama number should be 10 digits"]) {
                toastMsg = [iqamaDetails objectForKey:@"Message"];
                [self toastMessagemethod];
                
            } else if ([[iqamaDetails objectForKey:@"Message"] isEqualToString:@"success"]) {
                
                
                if ([selOperatorName  isEqualToString:[MCLocalization stringForKey:@"Quicknetname"]] || [selOperatorName isEqualToString:[MCLocalization stringForKey:@"Sawaname"]]) {
                    
                    voucherCode = [[[[@"*155*" stringByAppendingString:_txtVoucherCode.text] stringByAppendingString:@"*"] stringByAppendingString:_txtIqmaNumber.text] stringByAppendingString:@"#"];
                    
                } else if ([selOperatorName  isEqualToString:[MCLocalization stringForKey:@"Virginname"]]) {
                    
                    voucherCode = @"*101#";
                    
                } else if ([selOperatorName  isEqualToString:[MCLocalization stringForKey:@"Friendiname"]]) {
                    
                    voucherCode = [[[[@"*101*" stringByAppendingString:_txtVoucherCode.text] stringByAppendingString:@"*"] stringByAppendingString:_txtIqmaNumber.text] stringByAppendingString:@"#"];
                    
                } else if ([selOperatorName  isEqualToString:[MCLocalization stringForKey:@"Jawwyname"]] || [selOperatorName  isEqualToString:[MCLocalization stringForKey:@"Lebaraname"]]) {
                    
                    _lblUSSDCode.hidden = YES;
                    _btnCopyOutlet.hidden = YES;
                    
                } else if ([selOperatorName  isEqualToString:[MCLocalization stringForKey:@"Zainname"]]) {
                    
                    voucherCode = [[[[@"*141*" stringByAppendingString:_txtIqmaNumber.text] stringByAppendingString:@"*"] stringByAppendingString:_txtVoucherCode.text] stringByAppendingString:@"#"];
                    
                } else if ([selOperatorName  isEqualToString:[MCLocalization stringForKey:@"Mobilyname"]]) {
                    
                    
                    voucherCode = [[[[@"*1400*" stringByAppendingString:_txtVoucherCode.text]
                                     stringByAppendingString:@"*"] stringByAppendingString:_txtIqmaNumber.text] stringByAppendingString:@"#"];
                    
                    _lblUSSDCode.hidden = NO;
                    _btnCopyOutlet.hidden = NO;
                    
                    
                }
                
                _lblUSSDCode.text = voucherCode;
                _lblUSSDCode.hidden = NO;
                _btnCopyOutlet.hidden = NO;
                
                
            } else  if ([[iqamaDetails objectForKey:@"Message"] isEqualToString:@"failure"]) {
                
                toastMsg = [iqamaDetails objectForKey:@"Message"];
                [self toastMessagemethod];
            }
            
        

    
            }
    
    
    
    
    
        });
        
        
    }];
    
    
    [dataTask resume];
}








- (IBAction)btnCopy:(id)sender {
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = _lblUSSDCode.text;
    
    toastMsg  = [MCLocalization stringForKey:@"ussdCopied"];
    [self toastMessagemethod];

}



- (IBAction)btnRecharge:(id)sender {
    
    
    if (selOperatorName.length < 1) {
        
        toastMsg = [MCLocalization stringForKey:@"plsSelOperator"];
        [self toastMessagemethod];
        
    } else if ([_txtVoucherCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        
        toastMsg = [MCLocalization stringForKey:@"plsEntVouCode"];
        [self toastMessagemethod];
        
    } else if ([_txtIqmaNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        
        toastMsg =  [MCLocalization stringForKey:@"iqmaNotEmpty"];
        [self toastMessagemethod];
        
    }
    
//    else if (_txtIqmaNumber.text.length != 10) {
//        
//        if (_txtIqmaNumber.text.length != 14) {
//            
//            toastMsg = [MCLocalization stringForKey:@"iqma10or14"];
//            [self toastMessagemethod];
//            
//        }
//        
//    }
    
    else {
        
        [self iqamaNumServicesMethod];
        [self DirectRecharge];

    }
   
}


-(void)DirectRecharge{
    
 
    
    NSString*uemail= [profileDict valueForKey:@"user_email_id"];
    NSString*Umob =   [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/insertDirectRechargeDetails"];
    
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    
    NSDictionary * loginDetailsDict =  @{@"user_emailId":uemail,
                                         
                                         @"operator":selOperatorName,
                                         
                                         @"ipAddress":Ipaddress,
                                         @"iqmaNumber":_txtIqmaNumber.text ,
                                         @"voucherCode":_txtVoucherCode.text,
                                         @"user_mob":Umob,
                                         @"from":@"iphone",
                                         @"accessToken":accessToken,
                                         @"unique_id":uniqueid
                                         
                                         };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
            NSString*     resSrt  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            [LoaderClass removeLoader:self.view];
            
            NSLog(@"Killer Response is%@",resSrt);

            NSLog(@"Killer Response is%@",Killer);
    
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"DirectRecharge";
                
                [self checkTokenStatusWebServices];
                
                
            }else if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }

    
    
        });
        
        
    }];
    
    
    [dataTask resume];
}

    



@end
