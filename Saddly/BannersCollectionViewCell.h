//
//  BannersCollectionViewCell.h
//  Saddly
//
//  Created by Sai krishna on 7/12/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoModel.h"



extern NSString *const kCustomCellIdentifier;

@class PhotoModel;


@interface BannersCollectionViewCell : UICollectionViewCell 



@property (nonatomic) PhotoModel *photoModel;



@property (strong, nonatomic) IBOutlet UIImageView *imgBanner;

@property (strong, nonatomic) IBOutlet UILabel *lblMsg1;
@property (strong, nonatomic) IBOutlet UILabel *lblMsg2;
@property (strong, nonatomic) IBOutlet UILabel *lblUseCodeOutlet;



@end
