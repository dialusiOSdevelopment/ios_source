//
//  LoaderClass.h
//  CKChat
//
//  Created by Cosmic Kayka on 08/01/15.
//  Copyright (c) 2015 Cosmic Kayka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface LoaderClass : NSObject
{
    
}

+(void) showLoader  : (UIView *)CurrentView;
+(void) removeLoader: (UIView *)CurrentView;

+(void) showLoaderOnWindow;
+(void) removeLoaderFromWindow;


@end
