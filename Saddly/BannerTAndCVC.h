//
//  BannerTAndCVC.h
//  Saddly
//
//  Created by Sai krishna on 7/14/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "LoaderClass.h"
#import "ConnectivityManager.h"
#import "HomeVC.h"
#import "MyProfileVC.h"
#import "LoginViewController.h"
#import "FreeSMSVC.h"
#import "OperatorsVC.h"
#import "WalletVC.h"
#import "PayOrSendVC.h"
#import "ChooseLanguage.h"
#import "ContactUSDetailVC.h"
#import "AboutUsVC.h"
#import "DefaultFont.h"
#import "HelpVC.h"
#import "WriteUsVC.h"
#import "ShareViewController.h"
#import "QuickRechargeVC.h"


@interface BannerTAndCVC : UIViewController <UITableViewDelegate, UITableViewDataSource, NSURLSessionDelegate> {
    
    NSArray * tAndCArray;
    NSString * navigation;
    
    NSString * methodname, * a123;
    NSDictionary * profileDict;
    
}


@property (strong,nonatomic) UIImage  * bannerSubImage;
@property (strong,nonatomic) NSString * bannerType;
@property (strong,nonatomic) NSString * bannerNavigateTo;
@property (strong,nonatomic) NSString * bannerTitle;
@property (strong,nonatomic) NSString * bannerButtonColor;
@property (strong,nonatomic) NSString * bannerValidHeader;


@property (strong, nonatomic) IBOutlet UIImageView *imgBanner;

@property (strong, nonatomic) IBOutlet UITableView *tblTAndC;

@property (strong, nonatomic) IBOutlet UIButton *btnBookNowOutlet;

@property (strong, nonatomic) IBOutlet UIScrollView *ScrllView;



- (IBAction)btnBookNow:(id)sender;


@end
