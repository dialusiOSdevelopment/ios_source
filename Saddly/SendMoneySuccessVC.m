//
//  SendMoneySuccessVC.m
//  DialuzApp
//
//  Created by Sai krishna on 12/29/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import "SendMoneySuccessVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface SendMoneySuccessVC ()

@end

@implementation SendMoneySuccessVC

- (void)viewDidLoad {
    
    
    //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Send Money Success Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //
    
    _lblAmount.adjustsFontSizeToFitWidth=YES;
    _lblNumber.adjustsFontSizeToFitWidth=YES;
    _lblName.adjustsFontSizeToFitWidth=YES;
    _lblOrderId.adjustsFontSizeToFitWidth=YES;
    _lblcongratsName.adjustsFontSizeToFitWidth=YES;
    _lblSucSentName.adjustsFontSizeToFitWidth=YES;
    _lblToName.adjustsFontSizeToFitWidth=YES;
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"] isEqualToString:@"AR"]) {
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
    }
    
    self.title = [MCLocalization stringForKey:@"status"];
    
    // Saving Send Money Details Dictionary to NSUserDefaults.
    NSUserDefaults *sendMoneyDetailsDefaults = [NSUserDefaults standardUserDefaults];
    
    sendMoneyDict = [NSKeyedUnarchiver unarchiveObjectWithData:[sendMoneyDetailsDefaults objectForKey:@"sendMoneyDetailsDefaults"]];
    
    
    if ([[sendMoneyDict valueForKey:@"response_message"] isEqualToString:@"Sent"]) {
        _statusImgView.image = [UIImage imageNamed:@"success.png"];
    } else {
        _statusImgView.image = [UIImage imageNamed:@"cancel.png"];
    }
    
    _lblName.text = [[[[sendMoneyDict valueForKey:@"userName"]stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    
    _lblNumber.text = [sendMoneyDict valueForKey:@"targetMob"];

    
    NSString *amount = [NSString stringWithFormat:@"%@", [sendMoneyDict valueForKey:@"amount"]];
    _lblAmount.text = [amount stringByAppendingString:@" ﷼"];

    
    _lblOrderId.text = [[MCLocalization stringForKey:@"orderId"] stringByAppendingString:[sendMoneyDict valueForKey:@"orderId"]];
    _lblOrderId.adjustsFontSizeToFitWidth = YES;

    
    // Names
    _lblcongratsName.text = [MCLocalization stringForKey:@"congrats"];
    _lblSucSentName.text = [MCLocalization stringForKey:@"uHaveSucSent"];
    _lblToName.text = [MCLocalization stringForKey:@"To"];
    
    [_btnBackHomeOutlet setTitle:[MCLocalization stringForKey:@"backToHome"] forState:UIControlStateNormal];

    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// Enabling the Back Button
-(void)viewDidAppear:(BOOL)animated{
    _lblAmount.adjustsFontSizeToFitWidth=YES;
    _lblNumber.adjustsFontSizeToFitWidth=YES;
    _lblName.adjustsFontSizeToFitWidth=YES;
    _lblOrderId.adjustsFontSizeToFitWidth=YES;
    _lblcongratsName.adjustsFontSizeToFitWidth=YES;
    _lblSucSentName.adjustsFontSizeToFitWidth=YES;
    _lblToName.adjustsFontSizeToFitWidth=YES;
    
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
}

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];


        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}


 

- (IBAction)btnBackToHome:(id)sender {
    
    NSUserDefaults * sendMoneyDetailsDefaults = [NSUserDefaults standardUserDefaults];
    [sendMoneyDetailsDefaults removeObjectForKey:@"sendMoneyDetailsDefaults"];
    
    [self performSegueWithIdentifier:@"HomeFrmSendMoneySuccess" sender:nil];

    
}
@end
