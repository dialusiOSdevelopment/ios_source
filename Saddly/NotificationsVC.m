
//
//  NotificationsVC.m
//  Saddly
//
//  Created by Sai krishna on 2/23/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "NotificationsVC.h"

@interface NotificationsVC ()

@end

@implementation NotificationsVC

- (void)viewDidLoad {
    
    self.title = [MCLocalization stringForKey:@"notifications"];
    
    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 




-(void)viewDidAppear:(BOOL)animated{
   
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [[self.tabBarController.tabBar.items objectAtIndex:4] setBadgeValue:nil];
    
    [self NotiWebServicesMethod];
    [self.tblNotifications reloadData];
    
    NSUserDefaults * Notification = [NSUserDefaults standardUserDefaults];
    [Notification removeObjectForKey:@"NotificationDefault"];

}





-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str1  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str1 Response is%@",str1);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
                        
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
            
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"NotiWebServicesMethod"]) {
                    [self NotiWebServicesMethod];
                }
            
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//



// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}



//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}





// Notifications Webservices Method
-(void)NotiWebServicesMethod {
    
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];

    NSString * userMobNum = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"user_mob"]];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    NSString*UEmail= [profileDict valueForKey:@"user_email_id"];

    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];


    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    

    
    [LoaderClass showLoader:self.view];
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/profile/getAllNotificationsDetails"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userMobNum stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * notiDetailsDict =  @{@"usermob":userMobNum,
                                               @"user_emailId":UEmail,
                                               @"ipAddress":Ipaddress,
                                               @"from":@"iPhone",
                                               @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                               @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                               };
    
    NSLog(@"Posting notiDetailsDict is %@",notiDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:notiDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSLog(@"No data returned from server, error ocurred: %@", error);
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{

            
            
            
            NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            NSLog(@"resSrt: %@", resSrt);
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            NSMutableDictionary*respdict=[[NSMutableDictionary alloc]init];
            [respdict setObject:Killer forKey:@"List"];
            
            
            [LoaderClass removeLoader:self.view];
            
            
            NSLog(@"Notifications Details Response %@",Killer);
            
            
            NSLog(@"Notifications Details Response %@",[[Killer valueForKey:@"response_message"]firstObject] );
            
            NSLog(@"Notifications Details Response %@",[[Killer valueForKey:@"totalnotifications"]firstObject]);


            //
            
            notiCount = [Killer valueForKey:@"totalnotifications"];
            
            
            if ([[[[Killer valueForKey:@"response_message"]firstObject] valueForKey:@"response_message" ] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[[[Killer valueForKey:@"response_message"]firstObject] valueForKey:@"response_message" ] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"NotiWebServicesMethod";
                    
                    
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                }
            
            else if (notiCount.count != 0) {
                
                
                notiDateArray=[notiCount valueForKey:@"notification_date"];
                notiMsgArray=[notiCount valueForKey:@"notification_message"];
                notiTypeArray=[notiCount valueForKey:@"notification_type"];
                
                height = 165.0f;
                [self.tblNotifications reloadData];
                
            } else {
                
                noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tblNotifications.bounds.size.width, self.tblNotifications.bounds.size.height)];
                noDataLabel.text             = @"";
                noDataLabel.textColor        = [UIColor whiteColor];
                noDataLabel.textAlignment    = NSTextAlignmentCenter;
                self.tblNotifications.backgroundView = noDataLabel;
                
                
                UIImageView *labelBackground = [[UIImageView alloc] initWithFrame:CGRectMake(self.tblNotifications.bounds.size.width / 2, self.tblNotifications.bounds.size.height / 2, self.tblNotifications.bounds.size.width / 3.5, self.tblNotifications.bounds.size.height / 3.5)];
                
                labelBackground.center = self.tblNotifications.center;
                
                if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
                    
                    labelBackground.image = [UIImage imageNamed:@"no-noti-found-ar.png"];
                    
                } else {
                    
                    labelBackground.image = [UIImage imageNamed:@"no-noti-found-en.png"];
                }
                
                
                [noDataLabel addSubview:labelBackground];
                noDataLabel.backgroundColor = [UIColor clearColor];
                
                
                self.tblNotifications.tableFooterView = [[UIView alloc] init];
                height = 0;
                
            }
            
            [self.tblNotifications reloadData];
            
            }
        });
        
    }];
    
    
    [dataTask resume];
    
    
}



#pragma mark --
#pragma mark - UITableView Delegate Methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return notiMsgArray.count;

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString * cellIdentifier=@"Cell";
    NotiTableViewCell * cell = (NotiTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"NotiTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    
    NSString * notification =[[[[notiMsgArray objectAtIndex:indexPath.row] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];

   
    str = [notiTypeArray objectAtIndex:indexPath.row];
    cell.lblNotification.text = notification;
    cell.lblDateTime.text = [notiDateArray objectAtIndex:indexPath.row];
    
    
    if ([str isEqualToString:@"SendMoney"]) {
        cell.img.image = [UIImage imageNamed:@"send.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"senMon"];
    } else if ([str isEqualToString:@"AddMoney"]) {
        cell.img.image = [UIImage imageNamed:@"addmoney_new.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"moneyAddedWallet"];
    } else if ([str isEqualToString:@"Refund"]) {
        cell.img.image = [UIImage imageNamed:@"refund.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"refund"];
    } else if ([str isEqualToString:@"BonusCash 10 Halal"]) {
        cell.img.image = [UIImage imageNamed:@"addmoney_new.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"bonucash"];
    } else if ([str containsString:@"Zain_VoucherCode"]) {
        cell.img.image = [UIImage imageNamed:@"zain_300.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"recharge"];
    } else if ([str containsString:@"Friendi_VoucherCode"]) {
        cell.img.image = [UIImage imageNamed:@"friendi_300.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"recharge"];
    } else if ([str containsString:@"Jawwy_VoucherCode"]) {
        cell.img.image = [UIImage imageNamed:@"jawwy_300.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"recharge"];
    } else if ([str containsString:@"Mobily_VoucherCode"]) {
        cell.img.image = [UIImage imageNamed:@"mobily_300.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"recharge"];
    } else if ([str containsString:@"STC_VoucherCode"]) {
        cell.img.image = [UIImage imageNamed:@"STC_round.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"recharge"];
    } else if ([str containsString:@"Virgin_VoucherCode"]) {
        cell.img.image = [UIImage imageNamed:@"virgin_300.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"recharge"];
    } else if ([str containsString:@"Sawa_VoucherCode"]) {
        cell.img.image = [UIImage imageNamed:@"sawa_300.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"recharge"];
    } else if ([str containsString:@"Quicknet_VoucherCode"]) {
        cell.img.image = [UIImage imageNamed:@"quicknetr_300.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"recharge"];
    } else if ([str containsString:@"Lebara_VoucherCode"]) {
        cell.img.image = [UIImage imageNamed:@"lebra_300.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"recharge"];
    } else if ([str containsString:@"iTunes_VoucherCode"]) {
        cell.img.image = [UIImage imageNamed:@"iTunes-Card.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"itunesGC"];
    } else if ([str containsString:@"Xbox_VoucherCode"]) {
        cell.img.image = [UIImage imageNamed:@"Xbox-Card.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"xboxGC"];
    } else if ([str containsString:@"Playstation_VoucherCode"]) {
        cell.img.image = [UIImage imageNamed:@"PS-Card.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"playstationGC"];
    } else {
        cell.img.image = [UIImage imageNamed:@"70.png"];
        cell.lblTitle.text = [MCLocalization stringForKey:@"recharge"];
    }

    
    
    if ([str isEqualToString:@"Refund"] || [str isEqualToString:@"BonusCash 10 Halal"]) {
        cell.accessoryType = UITableViewCellAccessoryNone;
    } else {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        [cell.lblTitle setFont:[UIFont fontWithName:@"JFFlat-Regular" size:17.f]];
        [cell.lblNotification setFont:[UIFont fontWithName:@"JFFlat-Regular" size:13.f]];
        [cell.lblDateTime setFont:[UIFont fontWithName:@"JFFlat-Regular" size:17.f]];
        
        if ([str isEqualToString:@"Refund"] || [str isEqualToString:@"BonusCash 10 Halal"]) {
            [cell.lblNotification setFont:[UIFont fontWithName:@"JFFlat-Regular" size:16.f]];
        }
        
    } else {
        
        [cell.lblTitle setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:17.f]];
        [cell.lblNotification setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:13.f]];
        [cell.lblDateTime setFont:[UIFont fontWithName:@"MyriadPro-Regular" size:17.f]];
        
        if ([str isEqualToString:@"Refund"] || [str isEqualToString:@"BonusCash 10 Halal"]) {
            [cell.lblNotification setFont:[UIFont fontWithName:@"JFFlat-Regular" size:16.f]];
        }
    }

    
    
    cell.lblNotification.numberOfLines = 0;
    cell.lblNotification.lineBreakMode = NSLineBreakByWordWrapping;
    cell.lblNotification.adjustsFontSizeToFitWidth = YES;
    
    
    _tblNotifications.alwaysBounceVertical = NO;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.tblNotifications.tableFooterView = [[UIView alloc] init];
    return cell;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    str = [notiTypeArray objectAtIndex:indexPath.row];
    
    NSLog(@"str is: %@",str);

    
    if ([str isEqualToString:@"SendMoney"]) {
        
        SendMoneyVC * sendMoney = [self.storyboard instantiateViewControllerWithIdentifier:@"SendMoneyVC"];
        sendMoney.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:sendMoney animated:YES];
        
    } else if ([str isEqualToString:@"AddMoney"]) {
        
        WalletVC * wallet = [self.storyboard instantiateViewControllerWithIdentifier:@"WalletVC"];
        wallet.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:wallet animated:YES];
        
    } else if ([str containsString:@"_VoucherCode"]) {
        
        OperatorsVC * operators = [self.storyboard instantiateViewControllerWithIdentifier:@"OperatorsVC"];
        operators.hidesBottomBarWhenPushed = YES;
        
        NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
        [btnRcDefaults setObject:@"100" forKey:@"btnRcDefaults"];

        
        [self.navigationController pushViewController:operators animated:YES];
        
    } else if ([str isEqualToString:@"Refund"] || [str isEqualToString:@"BonusCash 10 Halal"]) {
        
        [self presentedViewController];

    } else {
        
        [self presentedViewController];
    }
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return height;
}



@end
