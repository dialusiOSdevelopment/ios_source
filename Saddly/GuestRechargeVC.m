//
//  GuestRechargeVC.m
//  Saddly
//
//  Created by Sai krishna on 4/25/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "GuestRechargeVC.h"

@interface GuestRechargeVC ()

@end

@implementation GuestRechargeVC

- (void)viewDidLoad {
    
    
    self.title = [MCLocalization stringForKey:@"recharge"];

    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];

    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
    }
    
    
    [_btnCreditCardOutlet setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateNormal];
    [_btnCreditCardOutlet setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateSelected];
    
    
    [_btnDebitCardOutlet setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateNormal];
    [_btnDebitCardOutlet setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateSelected];
    
    
    [_btnSadadOutlet setImage:[UIImage imageNamed:@"empty-check-box-black.png"] forState:UIControlStateNormal];
    [_btnSadadOutlet setImage:[UIImage imageNamed:@"accept_t&c-black.png"] forState:UIControlStateSelected];
    
    
    test = 10;
    
    // Names
    _txtMobNum.placeholder = [MCLocalization stringForKey:@"txt_password"];
    
    _lblAfterOfAllSA.adjustsFontSizeToFitWidth = YES;
    _lblLoadingOfAllSA.adjustsFontSizeToFitWidth = YES;
    
    
    _imgOperator.image = _operImage;
    _lblOperator = _operName;
    
    
    [_btnSelOperatorOutlet setTitle:[MCLocalization stringForKey:@"seldOper"] forState:UIControlStateNormal];

    
    [_btnLoadingPlansOutlet setTitle:[MCLocalization stringForKey:@"browsePlans"] forState:UIControlStateNormal];
    [_btnAfterLoadingPlansOutlet setTitle:[MCLocalization stringForKey:@"browsePlans"] forState:UIControlStateNormal];
    
    [_btnCreditCardOutlet setTitle:[MCLocalization stringForKey:@"CREDIT"] forState:UIControlStateNormal];
    [_btnDebitCardOutlet setTitle:[MCLocalization stringForKey:@"DEBIT/ATMCARD"] forState:UIControlStateNormal];
    [_btnSadadOutlet setTitle:[MCLocalization stringForKey:@"sadad"] forState:UIControlStateNormal];
    
    [_btnRechargeOutlet setTitle:[MCLocalization stringForKey:@"recharge"] forState:UIControlStateNormal];


   
    
    NSUserDefaults * mobNumContactsDefaults = [NSUserDefaults standardUserDefaults];
    [mobNumContactsDefaults removeObjectForKey:@"mobNumContactsDefaults"];

    NSUserDefaults * plandefaults = [NSUserDefaults standardUserDefaults];
    [plandefaults removeObjectForKey:@"plandefaults"];
    
    NSUserDefaults * planAmountDefaults = [NSUserDefaults standardUserDefaults];
    [planAmountDefaults removeObjectForKey:@"planAmountDefaults"];
    
    NSUserDefaults * txtMobNumDefaults = [NSUserDefaults standardUserDefaults];
    [txtMobNumDefaults removeObjectForKey:@"txtMobNumDefaults"];


    _lblLoadingOfAllSA.text = [[[[[MCLocalization stringForKey:@"Of"]  stringByAppendingString:@" "] stringByAppendingString:_operName] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]] ;

    
    
    // Returning Keyboard For Number Pad.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(NextClicked)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    _txtMobNum.inputAccessoryView = keyboardDoneButtonView;

    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];

    
    [super viewDidLoad];
    
    
   if ([_txtAmount.text isEqualToString:@""]){
        _btnLoadingPlansOutlet.hidden = NO;
        _txtAmount.hidden = YES;
       _btnAfterLoadingPlansOutlet.hidden=YES;
       _lblLoadingOfAllSA.hidden=NO;
       _lblAfterOfAllSA.hidden=YES;

    } else {
        _btnLoadingPlansOutlet.hidden = YES;
        _btnAfterLoadingPlansOutlet.hidden=NO;
        _txtAmount.hidden = NO;
        _lblLoadingOfAllSA.hidden=YES;
        _lblAfterOfAllSA.hidden=NO;

        
    }

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)NextClicked{
    [self.view endEditing:YES];
}


-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}


-(void)editingChanged:(UITextField * )sender {
    userEnteredNum = sender.text;
}



-(void)viewDidAppear:(BOOL)animated {

    
    NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
    [selectedOperator setObject:_operName forKey:@"selectedOperator"];
    
    NSLog(@"operator is %@",_operName);
    
    NSUserDefaults * guestDefaults = [NSUserDefaults standardUserDefaults];
    [guestDefaults setObject:@"guestDefaults" forKey:@"guestDefaults"];
    
    _lblAfterOfAllSA.adjustsFontSizeToFitWidth = YES;
    _lblLoadingOfAllSA.adjustsFontSizeToFitWidth = YES;

    
    
    NSUserDefaults * kill = [NSUserDefaults standardUserDefaults];
    [kill removeObjectForKey:@"kill"];


    [self checkTokenStatusWebServices];

    
    [_txtMobNum addTarget:self
                   action:@selector(editingChanged:)
         forControlEvents:UIControlEventEditingChanged];
    
    NSUserDefaults * mobNumContactsDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * txtMobNumDefaults = [NSUserDefaults standardUserDefaults];
    
  
    if ([[mobNumContactsDefaults stringForKey:@"mobNumContactsDefaults"] length] >1) {
        _txtMobNum.text = [mobNumContactsDefaults stringForKey:@"mobNumContactsDefaults"];
        
    } else if ([[txtMobNumDefaults stringForKey:@"txtMobNumDefaults"] length] > 1) {
        _txtMobNum.text = [txtMobNumDefaults stringForKey:@"txtMobNumDefaults"];
        
    }
    
    [mobNumContactsDefaults removeObjectForKey:@"mobNumContactsDefaults"];
    [txtMobNumDefaults removeObjectForKey:@"txtMobNumDefaults"];
    

    
     if (test == 10){
         
         _btnLoadingPlansOutlet.hidden = NO;
         _lblLoadingOfAllSA.hidden = NO;
         _lblLoadingPlans.hidden = YES;
         _txtAmount.hidden = YES;
         _lblAfterOfAllSA.hidden = YES;
         _btnAfterLoadingPlansOutlet.hidden = YES;
         
        
    } else {
        
        _btnLoadingPlansOutlet.hidden = YES;
        _lblLoadingOfAllSA.hidden = YES;
        _lblLoadingPlans.hidden = NO;
        _txtAmount.hidden = NO;
        _lblAfterOfAllSA.hidden = NO;
        _btnAfterLoadingPlansOutlet.hidden = NO;
        
    }
    
    
    NSUserDefaults * planAmountDefaults = [NSUserDefaults standardUserDefaults];
    _txtAmount.text =  [planAmountDefaults stringForKey:@"planAmountDefaults"];

    [planAmountDefaults removeObjectForKey:@"planAmountDefaults"];
  
    
    if ([_txtAmount.text isEqualToString:@""]){
        
        _btnLoadingPlansOutlet.hidden = NO;
        _txtAmount.hidden = YES;
        _btnAfterLoadingPlansOutlet.hidden=YES;
        _lblLoadingOfAllSA.hidden=NO;
        _lblAfterOfAllSA.hidden=YES;
        _lblLoadingPlans.hidden = YES;
        
    } else {
        
        _btnLoadingPlansOutlet.hidden = YES;
        _btnAfterLoadingPlansOutlet.hidden=NO;
        _txtAmount.hidden = NO;
        _lblLoadingOfAllSA.hidden=YES;
        _lblAfterOfAllSA.hidden=NO;
        _lblLoadingPlans.hidden = NO;
        
        
    }
    
    _lblAfterOfAllSA.text = [[[[[MCLocalization stringForKey:@"Of"]  stringByAppendingString:@" "] stringByAppendingString:_operName] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]] ;


}





//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if ([textField isEqual:_txtMobNum]) {
        
        if([self isNumeric:string]){
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 9;
        } else  {
            return FALSE;
        }
        
    } else if ([textField isEqual:_txtEmailId]){
        if([self isAlphaNumericSpecialCharacters:string])
            return TRUE;
        else
            return FALSE;
    }
    return YES;
}



// Entering only numbers not a special Characters
-(BOOL)isNumeric:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}




// Entering only alphaNumeric & Special Characters not a Arabic Characters
-(BOOL)isAlphaNumericSpecialCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._@-"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}




// Returning KeyBoard for all text fields
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == self.txtMobNum) {
        [self.txtEmailId becomeFirstResponder];
    } else if (textField == self.txtEmailId) {
        [self.txtEmailId resignFirstResponder];
    }
    
//    [self.view endEditing:YES];
    return YES;
}


-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:15.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
        [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}




// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}



//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}





- (IBAction)btnContacts:(id)sender {
    
    UserContactsVC * contacts = [self.storyboard instantiateViewControllerWithIdentifier:@"UserContactsVC"];
    [self.navigationController pushViewController:contacts animated:YES];
    
}




- (IBAction)btnLoadingBrowsePlans:(id)sender {
    
    NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
    [selectedOperator setObject:_operName forKey:@"selectedOperator"];
    
    BrowsePlansVC * browsePlans = [self.storyboard instantiateViewControllerWithIdentifier:@"BrowsePlansVC"];
    browsePlans.operType = _operType;
    browsePlans.operatorImage = _operImage;
    [self.navigationController pushViewController:browsePlans animated:YES];

}



- (IBAction)btnAfterLoadBrowsePlans:(id)sender {
    
    NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
    [selectedOperator setObject:_operName forKey:@"selectedOperator"];
    
    BrowsePlansVC * browsePlans = [self.storyboard instantiateViewControllerWithIdentifier:@"BrowsePlansVC"];
    browsePlans.operType = _operType;
    browsePlans.operatorImage = _operImage;
    [self.navigationController pushViewController:browsePlans animated:YES];

}





- (IBAction)btnCreditCard:(id)sender {

    if([_btnCreditCardOutlet isSelected]==YES)
    {
        [_btnCreditCardOutlet setSelected:YES];
        [_btnDebitCardOutlet setSelected:NO];
        [_btnSadadOutlet setSelected:NO];

    } else{
        [_btnCreditCardOutlet setSelected:YES];
        [_btnDebitCardOutlet setSelected:NO];
        [_btnSadadOutlet setSelected:NO];

    }
    
    value=@"10";
    NSUserDefaults * paymentModeDefaults = [NSUserDefaults standardUserDefaults];
    [paymentModeDefaults setObject:[MCLocalization stringForKey:@"CREDIT"] forKey:@"paymentModeDefaults"];
    
    Paymnetmode=[MCLocalization stringForKey:@"CREDIT"] ;
    
    
}


- (IBAction)btnDebitCard:(id)sender {
    
    if([_btnDebitCardOutlet isSelected]==YES)
    {
        [_btnCreditCardOutlet setSelected:NO];
        [_btnDebitCardOutlet setSelected:YES];
        [_btnSadadOutlet setSelected:NO];
        
    } else{
        [_btnCreditCardOutlet setSelected:NO];
        [_btnDebitCardOutlet setSelected:YES];
        [_btnSadadOutlet setSelected:NO];
        
    }
    
    value=@"15";
    
    NSUserDefaults * paymentModeDefaults = [NSUserDefaults standardUserDefaults];
    [paymentModeDefaults setObject:[MCLocalization stringForKey:@"DEBIT/ATMCARD"] forKey:@"paymentModeDefaults"];
    
    Paymnetmode=[MCLocalization stringForKey:@"DEBIT/ATMCARD"];
    
    
}





- (IBAction)btnSadad:(id)sender {
    
    if([_btnSadadOutlet isSelected]==YES)
    {
        [_btnCreditCardOutlet setSelected:NO];
        [_btnDebitCardOutlet setSelected:NO];
        [_btnSadadOutlet setSelected:YES];
        
    } else{
        [_btnCreditCardOutlet setSelected:NO];
        [_btnDebitCardOutlet setSelected:NO];
        [_btnSadadOutlet setSelected:YES];
        
    }
    
    value=@"20";
    NSUserDefaults * paymentModeDefaults = [NSUserDefaults standardUserDefaults];
    [paymentModeDefaults setObject:[MCLocalization stringForKey:@"sadad"] forKey:@"paymentModeDefaults"];
    
    Paymnetmode=[MCLocalization stringForKey:@"sadad"] ;
    

    
}




-(void)checkTokenStatusWebServices {
    
    
    NSString * userId = [@"966" stringByAppendingString:_txtMobNum.text];
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    

    
    [LoaderClass showLoader:self.view];
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSString * accessToken = @"";
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":accessToken,
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"fresh_token",
                                       };

    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            [LoaderClass removeLoader:self.view];
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
            
            NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
            [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
            
            
            if ([methodname isEqualToString:@"CardTRansactiondetails"]) {
                [self CardTRansactiondetails];
            }
            
            
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//





-(void)CardTRansactiondetails {
    
    
    NSString * db = [MCLocalization stringForKey:@"DBvalue"];
    NSString*rechargemobileno= [@"966" stringByAppendingString: _txtMobNum.text];
   
    NSUserDefaults * plandefaults = [NSUserDefaults standardUserDefaults];
    NSString*rechargePlan=[plandefaults stringForKey:@"plandefaults"];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*ipAddress= [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    NSUserDefaults * serviceIdDefaults = [NSUserDefaults standardUserDefaults];
    NSString*serviceId=[serviceIdDefaults stringForKey:@"serviceIdDefaults"];
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/insertRechargethrCardPayDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
 
    
    
    NSDictionary * cardPayDetails =  @{@"operator":_lblOperator,
                                       @"user_name":@"Guest",
                                       @"user_mob":@"Guest",
                                       @"user_emailId":_txtEmailId.text,
                                       @"recharge_mob":rechargemobileno,
                                       @"db":db,
                                       @"ipAddress":ipAddress,
                                       @"couponCode":@"UNKNOWN",
                                       @"from":@"iPhone",
                                       @"paidAmount":_txtAmount.text,
                                       @"fromActualCash":@"0",
                                       @"fromBonusCash":@"0",
                                       @"service_id":serviceId,
                                       @"rechargeplan":rechargePlan,
                                       @"payment_method":Paymnetmode,
                                       @"recharge_amount":_txtAmount.text,
                                       @"access_token":a123,
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                       
                                     };
    
    NSLog(@"Posting cardPayDetails is %@",cardPayDetails);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:cardPayDetails options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSLog(@"No data returned from server, error ocurred: %@", error);
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
    
            
            NSError *deserr;
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            NSString*    str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            NSLog(@"str is %@",str);
            NSLog(@"Response is %@",Killer);
            
            [LoaderClass removeLoader:self.view];
            
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"CardTRansactiondetails";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                } else {
                    
                    GuestRCConfirmationVC * guestRCConfirmation = [self.storyboard instantiateViewControllerWithIdentifier:@"GuestRCConfirmationVC"];
                    [self.navigationController pushViewController:guestRCConfirmation animated:YES];
                    
                    
                    NSUserDefaults * plandefaults = [NSUserDefaults standardUserDefaults];
                    
                    
                    guestRCConfirmation.guestMobNum = _txtMobNum.text;
                    guestRCConfirmation.guestRcValue = _txtAmount.text;
                    guestRCConfirmation.guestRcPlan = [plandefaults stringForKey:@"plandefaults"];
                    guestRCConfirmation.guestRCOperator = _lblOperator;
                    guestRCConfirmation.guestOperImage = _imgOperator.image;
                    guestRCConfirmation.guestEmailId = _txtEmailId.text;
                    
                    
                    
                    NSUserDefaults * mobNumContactsDefaults = [NSUserDefaults standardUserDefaults];
                    [mobNumContactsDefaults removeObjectForKey:@"mobNumContactsDefaults"];
                    
                    
                    
                    [plandefaults removeObjectForKey:@"plandefaults"];
                    
                    
                    
                    
                    // Saving profile Details Dictionary to NSUserDefaults.
                    NSUserDefaults *CardTrDefaults = [NSUserDefaults standardUserDefaults];
                    NSData * CardTrData = [NSKeyedArchiver archivedDataWithRootObject:Killer];
                    [CardTrDefaults setObject:CardTrData forKey:@"CardTrDefaults"];
                    
                }
                    
            }
        });
     
    }];
    
    [dataTask resume];

}






- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString * phoneRegex = @"^((5))[0-9]{8}$";
    NSPredicate * phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}



- (IBAction)btnRecharge:(id)sender {

    
    if ([_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg =  [MCLocalization stringForKey:@"Mobilealert"];
        [self toastMessagemethod];
    }
    else if (![Validate isValidMobileNumber:[_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg =   [MCLocalization stringForKey:@"mobilevalidate"];
        [self toastMessagemethod];
    }
    else if (![self validatePhone:[_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg =   [MCLocalization stringForKey:@"startWith5"];
        [self toastMessagemethod];
    }
    // Email Id
    else if ([_txtEmailId.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg = [MCLocalization stringForKey:@"Mailidvalidate"];
        [self toastMessagemethod];
    }
    else if (![Validate isValidEmailId:[_txtEmailId.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg = [MCLocalization stringForKey:@"Mailidnotvalid"];
        [self toastMessagemethod];
    }
    else if ([_txtAmount.text isEqualToString:@""]){
        toastMsg = [MCLocalization stringForKey:@"plsSelPlan"];
        [self toastMessagemethod];
        
    }
    else if ([_btnCreditCardOutlet isSelected] == NO && [_btnDebitCardOutlet isSelected] ==  NO && [_btnSadadOutlet isSelected] == NO){
        
        toastMsg =   [MCLocalization stringForKey:@"plsselPaygateway"];
        [self toastMessagemethod];
        
    }
    else{
        
        
        if  (![Paymnetmode isEqualToString:[MCLocalization stringForKey:@"sadad"]]) {
            
            [self CardTRansactiondetails];
            
        } else{
       
            GuestRCConfirmationVC * guestRCConfirmation = [self.storyboard instantiateViewControllerWithIdentifier:@"GuestRCConfirmationVC"];
            [self.navigationController pushViewController:guestRCConfirmation animated:YES];
            
            
            NSUserDefaults * plandefaults = [NSUserDefaults standardUserDefaults];
            
            
            guestRCConfirmation.guestMobNum = _txtMobNum.text;
            guestRCConfirmation.guestRcValue = _txtAmount.text;
            guestRCConfirmation.guestRcPlan = [plandefaults stringForKey:@"plandefaults"];
            guestRCConfirmation.guestRCOperator = _lblOperator;
            guestRCConfirmation.guestOperImage = _imgOperator.image;
            guestRCConfirmation.guestEmailId = _txtEmailId.text;
            
            
            
            NSUserDefaults * mobNumContactsDefaults = [NSUserDefaults standardUserDefaults];
            [mobNumContactsDefaults removeObjectForKey:@"mobNumContactsDefaults"];
            
            [plandefaults removeObjectForKey:@"plandefaults"];
            
        }

    }
}


-(void)viewWillDisappear:(BOOL)animated{
    
    NSUserDefaults * txtMobNumDefaults = [NSUserDefaults standardUserDefaults];
    [txtMobNumDefaults setObject:_txtMobNum.text forKey:@"txtMobNumDefaults"];
    
    
    _txtAmount.text = @"";
}




@end
