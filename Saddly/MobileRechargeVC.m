//
//  MobileRechargeVC.m
//  Saddly
//
//  Created by Sai krishna on 1/21/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "MobileRechargeVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);



@interface MobileRechargeVC ()

@end

@implementation MobileRechargeVC


- (void)viewDidLoad {
    
    
    
    //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Recharge Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //
    
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];

    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
    }
    
    
    NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
    
   
    if ( [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {

        
        self.title = [MCLocalization stringForKey:@"recharge"];
        
        [_btnloadBrowsePlansOutlet setTitle:[MCLocalization stringForKey:@"browsePlans"] forState:UIControlStateNormal];
        [_btnAfterLoadOperatoOutlet setTitle:[MCLocalization stringForKey:@"browsePlans"] forState:UIControlStateNormal];
        
        [_btnRcOutlet setTitle:[MCLocalization stringForKey:@"recharge"] forState:UIControlStateNormal];
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            
            _lblRecentRcsName.text = @"الرصيد الحالى";
            
        } else {
            
            _lblRecentRcsName.text = @"Recent Recharges";
        }
        
    } else {
    
        self.title = [MCLocalization stringForKey:@"PurchaseGiFtcards"];
        
        
        [_btnloadBrowsePlansOutlet setTitle:[MCLocalization stringForKey:@"browsegiftcards"] forState:UIControlStateNormal];
        [_btnAfterLoadOperatoOutlet setTitle:[MCLocalization stringForKey:@"browsegiftcards"] forState:UIControlStateNormal];
        
        [_btnRcOutlet setTitle:[MCLocalization stringForKey:@"purchase"] forState:UIControlStateNormal];

        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            
            _lblRecentRcsName.text = @"عمليات الشراء الأخيرة";
            
        } else {
            
            _lblRecentRcsName.text = @"Recent Purchases";
        }
        
    }
    
    
    btnOperatorOutlet = _operName;
    _imgOperator.image =  _operImage;
    
    _btnHaveCouponOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _lbl.adjustsFontSizeToFitWidth = YES;
    _lblWalletBalance.adjustsFontSizeToFitWidth=YES;
    _lblOperatorsDiplaying.adjustsFontSizeToFitWidth=YES;
    _lblLoadOperator.adjustsFontSizeToFitWidth=YES;
    _lblRecentRcsName.hidden = NO;

    
    // Showing Balance
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    
    NSString * actualCash = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"actualCash"]];
    
    NSString * bonusCash  = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"bonusCash"]];
    
    
    NSString * actualCashStr, * bonusCashStr;
    
    // Actual & bonus Cash
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ ﷼"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ ﷼"], bonusCash];
    } else {
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ SAR"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ SAR"], bonusCash];
    }
    
    _lblActualCash.text = actualCashStr;
    _lblBonusCash.text = bonusCashStr;

    _lblActualCash.adjustsFontSizeToFitWidth = YES;
    _lblBonusCash.adjustsFontSizeToFitWidth = YES;
    
    [_btnHaveCouponOutlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _lblCouponCodeSuccessFail.hidden = YES;
    
    [self recentsWebServicesMethod];
    [self.tblRecents reloadData];

   

    // Returning Keyboard For Number Pad.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(NextClicked)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    _txtMobNum.inputAccessoryView = keyboardDoneButtonView;
    
    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    [tapKeyboardGesture setCancelsTouchesInView:NO];

    
    
    NSUserDefaults * txtMobNumRecentsDefaults = [NSUserDefaults standardUserDefaults];
    [txtMobNumRecentsDefaults removeObjectForKey:@"txtMobNumRecentsDefaults"];
    
    if ([btnOperatorOutlet isEqualToString:@""]) {
        _btnloadBrowsePlansOutlet.hidden = NO;
        _lblLoadOperator.hidden = NO;
        _lbl.hidden = YES;
        _Amount.hidden = YES;
        _lblOperatorsDiplaying.hidden = YES;
        _btnAfterLoadOperatoOutlet.hidden = YES;
        
    } else {
        
        _btnloadBrowsePlansOutlet.hidden = YES;
        _lblLoadOperator.hidden = YES;
        _lbl.hidden = NO;
        _Amount.hidden = NO;
        _lblOperatorsDiplaying.hidden = NO;
        _btnAfterLoadOperatoOutlet.hidden = NO;
    }
    
    
    // Englis-Arabic Names
    _lblWalletBalance.text = [MCLocalization stringForKey:@"walletBal"];
    _txtMobNum.placeholder = [MCLocalization stringForKey:@"txt_password"];
    
    _lblOperatorsDiplaying.text = [[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:btnOperatorOutlet] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]] ;
    
    _lblLoadOperator.text = [[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:btnOperatorOutlet] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]] ;
    
    
    [_btnHaveCouponOutlet setTitle:[MCLocalization stringForKey:@"haveACouponCode"] forState:UIControlStateNormal];
    
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        _lblCurrentBal.text = [[NSString stringWithFormat:@"%@",[profileDict valueForKey:@"totalCash"]] stringByAppendingString:@" ﷼"];
        
    } else {
        
        _lblCurrentBal.text = [[NSString stringWithFormat:@"%@",[profileDict valueForKey:@"totalCash"]] stringByAppendingString:@" SAR"];
    }

    NSString * trimmedSelectedNumber =[[profileDict valueForKey:@"user_mob"] substringFromIndex:MAX((int)[[profileDict valueForKey:@"user_mob"] length]-9, 0)];
    
    _txtMobNum.text = [[trimmedSelectedNumber componentsSeparatedByCharactersInSet:
                        [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                       componentsJoinedByString:@""];


    NSUserDefaults * mobNumContactsDefaults = [NSUserDefaults standardUserDefaults];
    [mobNumContactsDefaults removeObjectForKey:@"mobNumContactsDefaults"];
    
    NSUserDefaults * plandefaults = [NSUserDefaults standardUserDefaults];
    [plandefaults removeObjectForKey:@"plandefaults"];
    
    NSUserDefaults * recentRcPlanDefaults = [NSUserDefaults standardUserDefaults];
    [recentRcPlanDefaults removeObjectForKey:@"recentRcPlanDefaults"];
    
    NSUserDefaults * txtMobNumDefaults = [NSUserDefaults standardUserDefaults];
    [txtMobNumDefaults removeObjectForKey:@"txtMobNumDefaults"];

    
    if ([_Amount.text intValue] < 100) {
        _btnHaveCouponOutlet.hidden = NO;
        _lblCouponCodeSuccessFail.hidden = YES;
    }
    
    if ([btnOperatorOutlet isEqualToString:@""]) {
        _btnloadBrowsePlansOutlet.hidden = NO;
        _lblLoadOperator.hidden = NO;
        _lbl.hidden = YES;
        _Amount.hidden = YES;
        _lblOperatorsDiplaying.hidden = YES;
        _btnAfterLoadOperatoOutlet.hidden = YES;
        
    } else if ([_Amount.text isEqualToString:@""]){
        _btnloadBrowsePlansOutlet.hidden = NO;
        _lblLoadOperator.hidden = NO;
        _lbl.hidden = YES;
        _Amount.hidden = YES;
        _lblOperatorsDiplaying.hidden = YES;
        _btnAfterLoadOperatoOutlet.hidden = YES;
        
    } else {
        _btnloadBrowsePlansOutlet.hidden = YES;
        _lblLoadOperator.hidden = YES;
        _lbl.hidden = NO;
        _Amount.hidden = NO;
        _lblOperatorsDiplaying.hidden = NO;
        _btnAfterLoadOperatoOutlet.hidden = NO;
    }
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)NextClicked{
    [self.view endEditing:YES];
}


-(void)removingKeyboardByTap {
    
    [self.view endEditing:YES];
}


            
            
            

// Displaying Mobile Number in the text field.
-(void)viewDidAppear:(BOOL)animated{
    
    
    
    // Showing Balance
    NSString * actualCash = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"actualCash"]];
    NSString * bonusCash  = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"bonusCash"]];
    
    
    NSString * actualCashStr, * bonusCashStr;
    
    // Actual & bonus Cash
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ ﷼"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ ﷼"], bonusCash];
    } else {
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ SAR"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ SAR"], bonusCash];
    }
    
    _lblActualCash.text = actualCashStr;
    _lblBonusCash.text = bonusCashStr;
    
    _lblActualCash.adjustsFontSizeToFitWidth = YES;
    _lblBonusCash.adjustsFontSizeToFitWidth = YES;

    
    if ([btnOperatorOutlet isEqualToString:@"iTunes"]) {
        btnOperatorOutlet=[MCLocalization stringForKey:@"itunesGC"];
    }else if ([btnOperatorOutlet isEqualToString:@"Xbox"]){
        btnOperatorOutlet=[MCLocalization stringForKey:@"xboxGC"];
    }else if ([btnOperatorOutlet isEqualToString:@"Playstation"]){
        btnOperatorOutlet=[MCLocalization stringForKey:@"playstationGC"];
    } else if ([btnOperatorOutlet isEqualToString:@"Zain"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            btnOperatorOutlet=@"زين";
            
        }
        else{
            
            btnOperatorOutlet=@"Zain";
        }
    }else if ([btnOperatorOutlet isEqualToString:@"Sawa"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            btnOperatorOutlet=@"سوا"  ;
            
        }
        else{
            btnOperatorOutlet=@"Sawa";
            
        }
    }
    else if ([btnOperatorOutlet isEqualToString:@"Virgin"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            btnOperatorOutlet=@"فيرجن";}
        else{
            
            btnOperatorOutlet=@"Virgin";
        }
    }
    else if ([btnOperatorOutlet isEqualToString:@"Friendi"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            btnOperatorOutlet=@"فريندي";}
        else{
            
            btnOperatorOutlet=@"Friendi";
        }
    }
    else if ([btnOperatorOutlet isEqualToString:@"Jawwy"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            btnOperatorOutlet=@"جوي";}
        else{
            
            btnOperatorOutlet=@"Jawwy";
        }
    }
    else if ([btnOperatorOutlet isEqualToString:@"Quicknet"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            btnOperatorOutlet=@"كويك نت";}
        else{
            
            btnOperatorOutlet=@"Quicknet";
        }
    }
    else if ([btnOperatorOutlet isEqualToString:@"Mobily"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            btnOperatorOutlet=@"موبايلي";}
        else{
            
            btnOperatorOutlet=@"Mobily";
        }
    }
    else if ([btnOperatorOutlet isEqualToString:@"lebara"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            btnOperatorOutlet=@"ليبارا";}
        else{
            
            btnOperatorOutlet=@"lebara";
        }
    }
    
    
    
    
    _btnHaveCouponOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    _lbl.adjustsFontSizeToFitWidth = YES;
    _lblWalletBalance.adjustsFontSizeToFitWidth=YES;
    _lblOperatorsDiplaying.adjustsFontSizeToFitWidth=YES;
    _lblLoadOperator.adjustsFontSizeToFitWidth=YES;
    
    NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
    [selectedOperator setObject:_operName forKey:@"selectedOperator"];
    
    
    
    
    
    // Englis-Arabic Names
    _lblWalletBalance.text = [MCLocalization stringForKey:@"walletBal"];
    _txtMobNum.placeholder = [MCLocalization stringForKey:@"txt_password"];
    
    NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
        
        
        self.title = [MCLocalization stringForKey:@"recharge"];
        
        [_btnloadBrowsePlansOutlet setTitle:[MCLocalization stringForKey:@"browsePlans"] forState:UIControlStateNormal];
        [_btnAfterLoadOperatoOutlet setTitle:[MCLocalization stringForKey:@"browsePlans"] forState:UIControlStateNormal];
        
        [_btnRcOutlet setTitle:[MCLocalization stringForKey:@"recharge"] forState:UIControlStateNormal];
        
    }else{
        
        self.title = [MCLocalization stringForKey:@"PurchaseGiFtcards"];
        
        
        [_btnloadBrowsePlansOutlet setTitle:[MCLocalization stringForKey:@"browsegiftcards"] forState:UIControlStateNormal];
        [_btnAfterLoadOperatoOutlet setTitle:[MCLocalization stringForKey:@"browsegiftcards"] forState:UIControlStateNormal];
        
        [_btnRcOutlet setTitle:[MCLocalization stringForKey:@"purchase"] forState:UIControlStateNormal];
        
    }
    
    
    
    _lblOperatorsDiplaying.text = [[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:btnOperatorOutlet] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]] ;
    
    _lblLoadOperator.text = [[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:btnOperatorOutlet] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]] ;
    
    
    [_txtMobNum addTarget:self
                   action:@selector(editingChanged:)
         forControlEvents:UIControlEventEditingChanged];
    
    NSUserDefaults * mobNumContactsDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * txtMobNumDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * planAmountDefaults = [NSUserDefaults standardUserDefaults];
    _Amount.text =  [planAmountDefaults stringForKey:@"planAmountDefaults"];
    
    
    if ([[mobNumContactsDefaults stringForKey:@"mobNumContactsDefaults"] length] >1) {
        _txtMobNum.text = [mobNumContactsDefaults stringForKey:@"mobNumContactsDefaults"];
        
    } else if ([[txtMobNumDefaults stringForKey:@"txtMobNumDefaults"] length] > 1) {
        _txtMobNum.text = [txtMobNumDefaults stringForKey:@"txtMobNumDefaults"];
        
    }
    
    [planAmountDefaults removeObjectForKey:@"planAmountDefaults"];
    [mobNumContactsDefaults removeObjectForKey:@"mobNumContactsDefaults"];
    [txtMobNumDefaults removeObjectForKey:@"txtMobNumDefaults"];
    
    if ([_Amount.text intValue] < 100) {
        _btnHaveCouponOutlet.hidden = NO;
        _lblCouponCodeSuccessFail.hidden = YES;
    }
    
    if ([btnOperatorOutlet isEqualToString:@""]) {
        _btnloadBrowsePlansOutlet.hidden = NO;
        _lblLoadOperator.hidden = NO;
        _lbl.hidden = YES;
        _Amount.hidden = YES;
        _lblOperatorsDiplaying.hidden = YES;
        _btnAfterLoadOperatoOutlet.hidden = YES;
        
    } else if ([_Amount.text isEqualToString:@""]){
        _btnloadBrowsePlansOutlet.hidden = NO;
        _lblLoadOperator.hidden = NO;
        _lbl.hidden = YES;
        _Amount.hidden = YES;
        _lblOperatorsDiplaying.hidden = YES;
        _btnAfterLoadOperatoOutlet.hidden = YES;
        
    } else {
        _btnloadBrowsePlansOutlet.hidden = YES;
        _lblLoadOperator.hidden = YES;
        _lbl.hidden = NO;
        _Amount.hidden = NO;
        _lblOperatorsDiplaying.hidden = NO;
        _btnAfterLoadOperatoOutlet.hidden = NO;
    }
    
}

-(void)editingChanged:(UITextField * )sender {
    userEnteredNum = sender.text;
}


// Returning KeyBoard for all text fields
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self.view endEditing:YES];
    return YES;
}




#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:_tblRecents]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}


-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:15.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
        [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];

    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}




//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if ([textField isEqual:_txtMobNum]) {
        
        if([self isNumeric:string]){
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 9;
        } else  {
            return FALSE;
        }
        
    }
    return YES;
}



// Entering only numbers not a special Characters
-(BOOL)isNumeric:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}


// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}



//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}


-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"recentsWebServicesMethod"]) {
                    
                    [self recentsWebServicesMethod];
                    
                }
                else if ([methodname isEqualToString:@"couponcodeservices"]) {
                    
                    [self couponcodeservices];
                    
                }
                else if ([methodname isEqualToString:@"recentsWebServicesMethod"]) {
                    
                    [self Walletdetails];
                    
                }
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//




// For Coupon Code
-(void)couponcodeservices{
    
    
    NSUserDefaults * Couponcode = [NSUserDefaults standardUserDefaults];
    NSString*couponCode = [[Couponcode stringForKey:@"Couponecodenum"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
    NSString* operator=   [selectedOperator stringForKey:@"selectedOperator"];
    
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary*   profileDict1 = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    NSString*UEmail= [profileDict1 valueForKey:@"user_email_id"];
    
                      
                      NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
                      
                      
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
                                            NSString*from=@"iphone";

    
    [LoaderClass showLoader:self.view];
    
 
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
 
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/validateCouponDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    

    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    NSLog(@"%@",userId);
    NSLog(@"%@",userPwd);
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];

    
    NSDictionary * loginDetailsDict =  @{@"operator":operator,
                                         
                                         @"rechargeamt":_Amount.text,
                                         @"couponCode":couponCode,
                                         @"user_mob":_txtMobNum.text,
                                         @"user_emailId":UEmail,
                                         @"ipAddress":Ipaddress,
                                         @"from":from,
                                         @"access_token":accessToken,
                                         @"unique_id":uniqueid
                                         
                                         };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
          NSString * str =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Killer Response is%@",[Killer valueForKey:@"rechargeoffers"] );
            
            NSLog(@"str Response is%@",str);
            
            
            
            [LoaderClass removeLoader:self.view];
            
            
            if ([[[Killer valueForKey:@"rechargeoffers"] valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"recentsWebServicesMethod";
                
                [self checkTokenStatusWebServices];
                
                
            }else if ([[[Killer valueForKey:@"rechargeoffers"] valueForKey:@"response_message"]isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{
                
    
        
        NSMutableDictionary * respDict = [[NSMutableDictionary alloc] init];
        [respDict setObject:[Killer valueForKey:@"rechargeoffers"] forKey:@"Message"];
        
        Cpncoderesponse = respDict;
        
        [LoaderClass removeLoader:self.view];
        

    NSUserDefaults * couponcodenum = [NSUserDefaults standardUserDefaults];
    [couponcodenum setObject:cpncode forKey:@"couponcodenum"];
    
    if ([[Cpncoderesponse valueForKey:@"Message"] isEqualToString:@"Coupon is Valid"]) {
        
        
        _lblCouponCodeSuccessFail.hidden = NO;
        _lblCouponCodeSuccessFail.text = [[MCLocalization stringForKey:@"couponApplied"] stringByAppendingString:cpncode];
        _btnHaveCouponOutlet.hidden = YES;
        
    } else if ([[Cpncoderesponse valueForKey:@"Message"] isEqualToString:@"Coupon Valid Once Per Month"]) {
        
        alertMessage = [MCLocalization stringForKey:@"couponvalidonce/month"];
        [self alertMethod];
        
    } else {
        
        _lblCouponCodeSuccessFail.hidden = YES;
        _btnHaveCouponOutlet.hidden = NO;
        toastMsg = [Cpncoderesponse valueForKey:@"Message"];
        [self toastMessagemethod];
        
    }
    
    
            }
        });
        
        
        
    }];
    
    
    [dataTask resume];
    
    
    
    
    
}






// Recent Recharges Webservices Method
-(void)recentsWebServicesMethod{
    
    NSString * userMobNum = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"user_mob"]];
    
    NSString * uemail = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"user_email_id"]];

    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    
    
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSString*from=@"iphone";
                          
///
    
    [LoaderClass showLoader:self.view];
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/getRechargeRecentTransactionsDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    

    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    NSLog(@"%@",userId);
    NSLog(@"%@",userPwd);
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    

    
    NSDictionary * loginDetailsDict =  @{@"user_mob":userMobNum,
                                         
                                         @"operator":btnOperatorOutlet,
                                         @"user_emailId":uemail,
                                         @"ipAddress":Ipaddress,
                                         @"from":from,
                                         @"access_token":accessToken,
                                         @"unique_id":uniqueid
                                         
                                         };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
           
            
            
            NSError *deserr;
            
            
            
          NSDictionary*Response = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
          NSString *resSrt =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Killer Response is%@",Response);
            
            NSLog(@"str Response is%@",resSrt);
            
         
            
            [LoaderClass removeLoader:self.view];
            
            
            if ([[[Response valueForKey:@"response_message"] firstObject] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"recentsWebServicesMethod";
                
                [self checkTokenStatusWebServices];
                
                
            }else if ([[[Response valueForKey:@"response_message"] firstObject] isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{
    
    NSMutableDictionary*respdict=[[NSMutableDictionary alloc]init];
    
    [respdict setObject:Response forKey:@"List"];
    [LoaderClass removeLoader:self.view];
    
    
    rc = [respdict objectForKey:@"List"];
    
    NSLog(@"recentRc Details Response %@",rc);
    recentRechargeDetails = respdict;
    
    if (rc.count != 0) {
        
        Number=[rc valueForKey:@"rechargemob"];
        date=[rc valueForKey:@"date"];
        operators=[rc valueForKey:@"operator"];
        planType=[rc valueForKey:@"planType"];
        rechargePlan=[rc valueForKey:@"rechargePlan"];
        rechargeamt=[rc valueForKey:@"rechargeamt"];
        rechargemob=[rc valueForKey:@"rechargemob"];
        service_id=[rc valueForKey:@"service_id"];
        
        height = 76.0f;
        [self.tblRecents reloadData];
        
    } else {
        
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tblRecents.bounds.size.width, self.tblRecents.bounds.size.height)];
        
        NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
        
        if ( [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
            
            noDataLabel.text = [MCLocalization stringForKey:@"norecRcs"];
            
        } else{
            
            noDataLabel.text = [MCLocalization stringForKey:@"Nopuravailable"];
            
        }
        
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        self.tblRecents.backgroundView = noDataLabel;
        self.tblRecents.tableFooterView = [[UIView alloc] init];
        height = 0;
        
        _lblRecentRcsName.hidden = YES;
    }
            }
        });
        
        
        
    }];
    
    
    [dataTask resume];
    
    
    
    
}





#pragma mark --
#pragma mark - UITableView Delegate Methods


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    if ([rc count] == 0) {
        return 0;
    }
    return [rc count];

    
    
//    if ([rc count] == 0) {
//        return 0;
//    } else if ([rc count] == 1) {
//        return 1;
//    } else if ([rc count] == 2) {
//        return 2;
//    } else {
//        return 3;
//    }
    
}




-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
        
    NSString * cellIdentifier = @"Cell";
    
    RecentsTableViewCell * Cell1 = (RecentsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (Cell1 == nil) {
        
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"RecentsTableViewCell" owner:self options:nil];
        Cell1 = [nib objectAtIndex:0];
    }
    
    Cell1.lblOperatorName.text= [NSString stringWithFormat:@"%@", [operators objectAtIndex:indexPath.row]];
    Cell1.lblNumber.text= [NSString stringWithFormat:@"%@", [Number objectAtIndex:indexPath.row]];
    Cell1.lblRcDate.text= [date objectAtIndex:indexPath.row];
    
    [Cell1.btnRCAmount setTitle:[[NSString stringWithFormat:@"%@", [rechargeamt objectAtIndex:indexPath.row]] stringByAppendingString:@"SAR"]forState:UIControlStateNormal];
        
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        if ([Cell1.lblOperatorName.text isEqualToString:@"Zain"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"Zainar.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Friendi"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"Friendiar.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Jawwy"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"Jawwyar.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Mobily"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"Mobilyar.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"STC"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"STC_round.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Virgin"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"Virginar.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Sawa"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"Sawaar.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Quicknet"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"Quicknetar.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Lebara"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"Leberaar.png"];
        }else if ([Cell1.lblOperatorName.text isEqualToString:@"iTunes"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"itunsar.png"];
        }else if ([Cell1.lblOperatorName.text isEqualToString:@"Xbox"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"xbox-cardar.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Playstation"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"ps-cardar.png"];
        } else {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"70.png"];
        }
        
    } else {
        
        if ([Cell1.lblOperatorName.text isEqualToString:@"Zain"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"zainen.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Friendi"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"friendien.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Jawwy"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"jawwyen.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Mobily"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"mobilyen.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"STC"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"STC_round.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Virgin"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"virginen.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Sawa"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"sawaen.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Quicknet"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"quickneten.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Lebara"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"lebaraen.png"];
        }else if ([Cell1.lblOperatorName.text isEqualToString:@"iTunes"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"itunsen.png"];
        }else if ([Cell1.lblOperatorName.text isEqualToString:@"Xbox"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"Xbox Carden.png"];
        } else if ([Cell1.lblOperatorName.text isEqualToString:@"Playstation"]) {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"ps-carden.png"];
        } else {
            Cell1.imgOperatorLogo.image = [UIImage imageNamed:@"70.png"];
        }
        
    }

    
    
    _tblRecents.alwaysBounceVertical = NO;
    Cell1.backgroundColor =  [UIColor clearColor];
    Cell1.selectionStyle = UITableViewCellSelectionStyleNone;
    self.tblRecents.tableFooterView = [[UIView alloc] init];
    return Cell1;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    _txtMobNum.text = [NSString stringWithFormat:@"%@",[Number objectAtIndex:indexPath.row]];
    _Amount.text = [NSString stringWithFormat:@"%@",[rechargeamt objectAtIndex:indexPath.row]];
    
    NSUserDefaults * txtMobNumRecentsDefaults = [NSUserDefaults standardUserDefaults];
    [txtMobNumRecentsDefaults setObject:_txtMobNum.text forKey:@"txtMobNumRecentsDefaults"];
    
    NSString * recentRcPlan = [rechargePlan objectAtIndex:indexPath.row];
    NSString * serviceId = [service_id objectAtIndex:indexPath.row];

    

    if ([[operators objectAtIndex:indexPath.row] isEqualToString:@"Zain"]) {
        
        btnOperatorOutlet = @"Zain";
        
        _lblOperatorsDiplaying.text = [NSString stringWithFormat:[[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:@"%@"] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]],@"Zain"];
        
        NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
        [selectedOperator setObject:@"Zain" forKey:@"selectedOperator"];
        
    } else if ([[operators objectAtIndex:indexPath.row] isEqualToString:@"Friendi"]) {
        
        btnOperatorOutlet = @"Friendi";
        
        _lblOperatorsDiplaying.text = [NSString stringWithFormat:[[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:@"%@"] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]],@"Friendi"];
        
        NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
        [selectedOperator setObject:@"Friendi" forKey:@"selectedOperator"];
        
    }   if ([[operators objectAtIndex:indexPath.row] isEqualToString:@"Jawwy"]) {
        
        
        btnOperatorOutlet = @"Jawwy";
        
        _lblOperatorsDiplaying.text = [NSString stringWithFormat:[[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:@"%@"] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]],@"Jawwy"];
        
        NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
        [selectedOperator setObject:@"Jawwy" forKey:@"selectedOperator"];
        
    } else if ([[operators objectAtIndex:indexPath.row] isEqualToString:@"Mobily"]) {
        
        btnOperatorOutlet = @"Mobily";
        
        _lblOperatorsDiplaying.text = [NSString stringWithFormat:[[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:@"%@"] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]],@"Mobily"];
        
        NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
        [selectedOperator setObject:@"Mobily" forKey:@"selectedOperator"];
        
    } else if ([[operators objectAtIndex:indexPath.row] isEqualToString:@"STC"]) {
        
        
        btnOperatorOutlet = @"STC";
        
        _lblOperatorsDiplaying.text = [NSString stringWithFormat:[[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:@"%@"] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]],@"STC"];
        
        NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
        [selectedOperator setObject:@"STC" forKey:@"selectedOperator"];
        
    } else if ([[operators objectAtIndex:indexPath.row] isEqualToString:@"Virgin"]) {
        
        btnOperatorOutlet = @"Virgin";
        
        _lblOperatorsDiplaying.text = [NSString stringWithFormat:[[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:@"%@"] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]],@"Virgin"];
        
        NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
        [selectedOperator setObject:@"Virgin" forKey:@"selectedOperator"];
        
    }  else if ([[operators objectAtIndex:indexPath.row] isEqualToString:@"Quicknet"]) {
        
        btnOperatorOutlet = @"Quicknet";
        
        _lblOperatorsDiplaying.text = [NSString stringWithFormat:[[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:@"%@"] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]],@"Quicknet"];
        
        NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
        [selectedOperator setObject:@"Quicknet" forKey:@"selectedOperator"];
        
    } else if ([[operators objectAtIndex:indexPath.row] isEqualToString:@"Sawa"]) {
        
        
        btnOperatorOutlet = @"Sawa";
        
        _lblOperatorsDiplaying.text = [NSString stringWithFormat:[[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:@"%@"] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]],@"Sawa"];
        
        NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
        [selectedOperator setObject:@"Sawa" forKey:@"selectedOperator"];
        
    } else if ([[operators objectAtIndex:indexPath.row] isEqualToString:@"Lebara"]) {
        
        btnOperatorOutlet = @"Lebara";
        
        _lblOperatorsDiplaying.text = [NSString stringWithFormat:[[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:@"%@"] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]],@"Lebara"];
        
        NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
        [selectedOperator setObject:@"Lebara" forKey:@"selectedOperator"];
        
    } else if ([[operators objectAtIndex:indexPath.row] isEqualToString:@"iTunes"]) {
        
        btnOperatorOutlet = @"iTunes";
        
        _lblOperatorsDiplaying.text = [NSString stringWithFormat:[[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:@"%@"] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]],@"iTunes"];
        
        NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
        [selectedOperator setObject:@"iTunes" forKey:@"selectedOperator"];
        
    }  else if ([[operators objectAtIndex:indexPath.row] isEqualToString:@"Google+Play"]) {
        
        btnOperatorOutlet = @"Google Play";
        
        _lblOperatorsDiplaying.text = [NSString stringWithFormat:[[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:@"%@"] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]],@"Google Play"];
        
        NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
        [selectedOperator setObject:@"Google Play" forKey:@"selectedOperator"];
        
    } else if ([[operators objectAtIndex:indexPath.row] isEqualToString:@"Xbox"]) {
        
        
        btnOperatorOutlet = @"Xbox";
        
        _lblOperatorsDiplaying.text = [NSString stringWithFormat:[[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:@"%@"] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]],@"Xbox"];
        
        NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
        [selectedOperator setObject:@"Xbox" forKey:@"selectedOperator"];
        
    } else if ([[operators objectAtIndex:indexPath.row] isEqualToString:@"Playstation"]) {
        
        btnOperatorOutlet = @"Playstation";
        
        _lblOperatorsDiplaying.text = [NSString stringWithFormat:[[[[[MCLocalization stringForKey:@"Of"] stringByAppendingString:@" "] stringByAppendingString:@"%@"] stringByAppendingString:@" - "] stringByAppendingString:[MCLocalization stringForKey:@"saudi"]],@"Playstation"];
        
        NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
        [selectedOperator setObject:@"Playstation" forKey:@"selectedOperator"];
        
    }
    
    _imgOperator.image =  _operImage;
    
    _btnloadBrowsePlansOutlet.hidden = YES;
    _lblLoadOperator.hidden = YES;
    _lbl.hidden = NO;
    _Amount.hidden = NO;
    _lblOperatorsDiplaying.hidden = NO;
    _btnAfterLoadOperatoOutlet.hidden = NO;
    
    
    NSUserDefaults * plandefaults = [NSUserDefaults standardUserDefaults];
    [plandefaults setObject:recentRcPlan forKey:@"plandefaults"];
    
    NSUserDefaults * serviceIdDefaults = [NSUserDefaults standardUserDefaults];
    [serviceIdDefaults setObject:serviceId forKey:@"serviceIdDefaults"];
    

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView==_tblRecents) {
        return 76.0f;
    } else {
        return 0.0f;
    }
}



// Showing Contacts
- (IBAction)btnContacts:(id)sender {
    
    UserContactsVC * contacts = [self.storyboard instantiateViewControllerWithIdentifier:@"UserContactsVC"];
    [self.navigationController pushViewController:contacts animated:YES];
    
}


//showing Recharge Plans after selecting the operator
- (IBAction)btnBrowsePlans:(id)sender {
    
    BrowsePlansVC * browsePlans = [self.storyboard instantiateViewControllerWithIdentifier:@"BrowsePlansVC"];
    browsePlans.operType = _operType;
    browsePlans.operatorImage = _operImage;
    [self.navigationController pushViewController:browsePlans animated:YES];
}

// Showing an alert for a coupon code
- (IBAction)btnHaveACouponCode:(id)sender {
    
    if ([_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg =  [MCLocalization stringForKey:@"Mobilealert"];
        [self toastMessagemethod];
    } else if (![Validate isValidMobileNumber:[_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg =   [MCLocalization stringForKey:@"mobilevalidate"];
        [self toastMessagemethod];
    } else if (![self validatePhone:[_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg =   [MCLocalization stringForKey:@"startWith5"];
        [self toastMessagemethod];
    } else if ([btnOperatorOutlet isEqualToString:@""]){
        toastMsg = [MCLocalization stringForKey:@"plsSelOperator"];
        [self toastMessagemethod];
    } else if ([_Amount.text isEqualToString:@""]){
        toastMsg = [MCLocalization stringForKey:@"plsSelPlan"];
        [self toastMessagemethod];
    } else{
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:[MCLocalization stringForKey:@"couponCode"] message:[MCLocalization stringForKey:@"enterCouponToAvailOffers"] preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField){
            textField.placeholder = [MCLocalization stringForKey:@"enterCouponCode"];
            textField.borderStyle = UITextBorderStyleRoundedRect;
            textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
            
        }];
        
        UIAlertAction * cancelButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"Forgetpasswordcancell"] style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            
            [self presentedViewController];
            
        }];
        
        UIAlertAction * Submitt  = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ForgetpasswordSubmitt"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
            NSArray * textfields = alertController.textFields;
            txtCouponCode = textfields[0];
            
            cpncode=txtCouponCode.text;
            
            NSUserDefaults * Couponcode = [NSUserDefaults standardUserDefaults];
            [Couponcode setObject:cpncode forKey:@"Couponecodenum"];
            
            [self couponcodeservices];
            
        }];
        
        [alertController addAction:cancelButton];
        [alertController addAction:Submitt];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}



- (IBAction)btnBonusCashInfo:(id)sender {
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        alertMessage = @"فقط يمكنك الحصول عليها عند الشحن بميلغ  100 ريال او اكثر";

    } else {

        alertMessage = @"Can be Redeemed only with 100SAR and above Recharges";

    }
    
    [self alertMethod];
    
}


- (IBAction)btnInfo:(id)sender {
    
    NSString * actualCash = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"actualCash"]];
    
    NSString * bonusCash  = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"bonusCash"]];
    
    
    NSString * actualCashStr, * bonusCashStr;
    
    // Actual & bonus Cash Displaying on Alert
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ ﷼"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ ﷼"], bonusCash];
    } else {
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ SAR"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ SAR"], bonusCash];
    }
    
    alert = [UIAlertController alertControllerWithTitle:@"" message:[[actualCashStr stringByAppendingString:@"\n"] stringByAppendingString:bonusCashStr] preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"] style:UIAlertActionStyleDefault  handler:^(UIAlertAction * action) {
        
        [self presentedViewController];
    }];
    
    
    [alert addAction:okButton];
    alert.view.tintColor = [UIColor colorWithRed:0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    [self presentViewController:alert animated:YES completion:nil];
    
}


// While Loading the View
- (IBAction)btnLoadBrowsePlans:(id)sender {
    
    if ([btnOperatorOutlet isEqualToString:@""]) {
        toastMsg = [MCLocalization stringForKey:@"plsSelOperator"];
        [self toastMessagemethod];
    } else {
        BrowsePlansVC * browsePlans = [self.storyboard instantiateViewControllerWithIdentifier:@"BrowsePlansVC"];
        browsePlans.operType = _operType;
        browsePlans.operatorImage = _operImage;
        [self.navigationController pushViewController:browsePlans animated:YES];
    }
}


- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString * phoneRegex = @"^((5))[0-9]{8}$";
    NSPredicate * phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}

// For Operators
-(void)Walletdetails{
    
    
    
    
    NSUserDefaults * profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary * Details = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    NSLog(@"profiledatadetailsare%@",Details);
    
    
    
    NSString*mobileNo=[Details objectForKey:@"user_mob"];
    NSString*rechargeamount =  _Amount.text;
    
    
    NSDictionary*   profileDict1 = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    NSString*UEmail= [profileDict1 valueForKey:@"user_email_id"];
   
                      NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
                      
                      
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
                                            NSString*from=@"iphone";
                                            

    
    NSUserDefaults * rechargeAmountFromTextField = [NSUserDefaults standardUserDefaults];
    [rechargeAmountFromTextField setObject:rechargeamount forKey:@"rechargeAmountFromTextField"];

    
    [LoaderClass showLoader:self.view];
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/checkWalletAmountDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    

    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
//         NSString * userPwd=@"585858";
    
    NSLog(@"%@",userId);
    NSLog(@"%@",userPwd);
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    

    
    NSDictionary * loginDetailsDict =  @{@"userMobileNo":mobileNo,
                                         
                                         @"rechargeamt":_Amount.text,
                                         @"user_emailId":UEmail,
                                         @"ipAddress":Ipaddress,
                                         @"from":from,
                                         @"access_token":accessToken,
                                         @"unique_id":uniqueid
                                         
                                         };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*Response = [NSJSONSerialization
                                     
                                     JSONObjectWithData:data
                                     
                                     options:kNilOptions
                                     
                                     error:&deserr];
            
            
            
            
            
            NSString *resSrt =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Killer Response is%@",Response);
            
            if ([[[Response valueForKey:@"response_message"] firstObject] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"Walletdetails";
                
                [self checkTokenStatusWebServices];
                
                
            }else if ([[[Response valueForKey:@"response_message"] firstObject] isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{

    
    NSMutableDictionary*respdict=[[NSMutableDictionary alloc]init];
    
    [respdict setObject:Response forKey:@"List"];
    
    NSLog(@"walletdetailsAre %@",respdict);
    
    
    NSDictionary*walletdetails;
    
    walletdetails = (NSDictionary *)[[respdict objectForKey:@"List"]firstObject];
    
    NSUserDefaults * rcMobNum = [NSUserDefaults standardUserDefaults];
    [rcMobNum setObject:_txtMobNum.text forKey:@"rcMobNum"];
    
    NSUserDefaults * rcOperator = [NSUserDefaults standardUserDefaults];
    [rcOperator setObject:_operName forKey:@"rcOperator"];
    
    NSUserDefaults * rcAmount = [NSUserDefaults standardUserDefaults];
    [rcAmount setObject:_Amount.text forKey:@"rcAmount"];
    
    
    NSLog(@"walletdetailsare%@",walletdetails);
    
    
    NSUserDefaults *WalletDefaults = [NSUserDefaults standardUserDefaults];
    NSData * WalletdetailsData = [NSKeyedArchiver archivedDataWithRootObject:walletdetails];
    [WalletDefaults setObject:WalletdetailsData forKey:@"WalletDefaults"];
    
    
    RechargeConfirmationVC * rcConfirm = [self.storyboard instantiateViewControllerWithIdentifier:@"RechargeConfirmationVC"];
    [self.navigationController pushViewController:rcConfirm animated:YES];
            }

        });
        
        
        
    }];
    
    
    [dataTask resume];
    
    
    
    
}


// Recharge Button
- (IBAction)btnProceedToRc:(id)sender {
    
    if ([_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg =  [MCLocalization stringForKey:@"Mobilealert"];
        [self toastMessagemethod];
    }
    else if (![Validate isValidMobileNumber:[_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg =   [MCLocalization stringForKey:@"mobilevalidate"];
        [self toastMessagemethod];
    }
    else if (![self validatePhone:[_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg =   [MCLocalization stringForKey:@"startWith5"];
        [self toastMessagemethod];
    }
    else if ([_Amount.text isEqualToString:@""]){
        toastMsg = [MCLocalization stringForKey:@"plsSelPlan"];
        [self toastMessagemethod];
    }
    else{
        [self Walletdetails];
    }
}


-(void)viewWillDisappear:(BOOL)animated{
    
    NSUserDefaults * txtMobNumDefaults = [NSUserDefaults standardUserDefaults];
    [txtMobNumDefaults setObject:_txtMobNum.text forKey:@"txtMobNumDefaults"];
    
}


//
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    
//    static CGFloat previousOffset;
//    CGRect rect = self.view.frame;
//    rect.origin.y += previousOffset - scrollView.contentOffset.y;
//    previousOffset = scrollView.contentOffset.y;
//    self.view.frame = rect;
//    
//}









@end
