//
//  ChooseLanguage.h
//  Saddly
//
//  Created by Sai krishna on 1/24/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "UIView+Toast.h"

@interface ChooseLanguage : UIViewController
{
    NSString* toastMsg;
    
}

@property (strong, nonatomic) IBOutlet UILabel *lblChooseLanguage;

@property (strong, nonatomic) IBOutlet UIButton *btnEngOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnAraboutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnChangeOutlet;

@property (strong, nonatomic) IBOutlet UILabel *lblEnglish;
@property (strong, nonatomic) IBOutlet UILabel *lblArabic;


@property (strong, nonatomic) IBOutlet UILabel *lblBackEnglish;
@property (strong, nonatomic) IBOutlet UILabel *lblBackArabic;


- (IBAction)btnEnglish:(id)sender;
- (IBAction)btnArabic:(id)sender;
- (IBAction)btnChange:(id)sender;



@end
