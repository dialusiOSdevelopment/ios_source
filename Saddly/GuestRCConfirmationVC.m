//
//  GuestRCConfirmationVC.m
//  Saddly
//
//  Created by Sai krishna on 4/26/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "GuestRCConfirmationVC.h"

@interface GuestRCConfirmationVC ()

@end

@implementation GuestRCConfirmationVC

- (void)viewDidLoad {
    
    self.title = [MCLocalization stringForKey:@"RechargeConfirmation"];
   
    
    payFort	= [[PayFortController alloc] initWithEnviroment:KPayFortEnviromentProduction];
    deviceid=[payFort getUDID];
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    // Saving profile Details Dictionary to NSUserDefaults.
    NSUserDefaults *CardTrDefaults = [NSUserDefaults standardUserDefaults];
    CardTranDetails = [NSKeyedUnarchiver unarchiveObjectWithData:[CardTrDefaults objectForKey:@"CardTrDefaults"]];
    NSLog(@"card transaction details are%@",CardTranDetails);
    
    
    
    
    _lblMobNum.adjustsFontSizeToFitWidth = YES;
    _lblOperator.adjustsFontSizeToFitWidth = YES;
    _lblRcPlan.adjustsFontSizeToFitWidth = YES;
    _lblRcAmount.adjustsFontSizeToFitWidth = YES;
    _txtSadadId.adjustsFontSizeToFitWidth = YES;
    
    
    _lblMobNumName.adjustsFontSizeToFitWidth = YES;
    _lblOperatorName.adjustsFontSizeToFitWidth = YES;
    _lblRcPlanname.adjustsFontSizeToFitWidth = YES;
    _lblRcAmountName.adjustsFontSizeToFitWidth = YES;
    _lblEnterSadadIdName.adjustsFontSizeToFitWidth = YES;
    _btnProceedtoPayName.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    
    
    _lblFromName.text=[MCLocalization stringForKey:@"from"];
    _lblMobNumName.text=[MCLocalization stringForKey:@"mobNum"];
    _lblOperatorName.text=[MCLocalization stringForKey:@"operator"];
    _lblRcPlanname.text=[MCLocalization stringForKey:@"rcPlan"];
    _lblRcAmountName.text=[MCLocalization stringForKey:@"rcValue"];
    _lblEnterSadadIdName.text=[MCLocalization stringForKey:@"entSADADID"];
    _txtSadadId.placeholder = [MCLocalization stringForKey:@"sadadid"];
    
    
    
    NSUserDefaults * paymentModeDefaults = [NSUserDefaults standardUserDefaults];
     _lblPaymentMethod.text =  [paymentModeDefaults stringForKey:@"paymentModeDefaults"];
    
    
    if ([_lblPaymentMethod.text isEqualToString:[MCLocalization stringForKey:@"sadad"]]) {
        
        _lblEnterSadadIdName.hidden = NO;
        _txtSadadId.hidden = NO;
        _lblSadadDots.hidden = NO;
    
    } else {
        
        _lblEnterSadadIdName.hidden = YES;
        _txtSadadId.hidden = YES;
        _lblSadadDots.hidden = YES;
        
        [self signatureWebServicesMethod];

        
    }
    
   
    _imgOperator.image = _guestOperImage;
    _lblMobNum.text = _guestMobNum;
    _lblValue.text = [_guestRcValue stringByAppendingString:@" SAR"];
    _lblRcAmount.text = _guestRcValue;
    _lblOperator.text = _guestRCOperator;
    _lblRcPlan.text = _guestRcPlan;

    
    [_btnProceedtoPayName setTitle:[[MCLocalization stringForKey:@"proceedpay"] stringByAppendingString:_lblRcAmount.text] forState:UIControlStateNormal];

    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    

    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}


-(void)viewDidAppear:(BOOL)animated {
    
    if (test == 10) {
        
        [self.navigationController popViewControllerAnimated:NO];
        
    } else {
        
        self.navigationItem.hidesBackButton = NO;
        
    }
    
}




// Returning to Next Text fields
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtSadadId) {
        [self.txtSadadId resignFirstResponder];
    }
    return YES;
}



// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}





//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:_txtSadadId]){
        if([self isAlphaNumericSpecialCharacters:string])
        {
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 12;
            
        }
        else
            return FALSE;
    }
    return YES;
}


// Entering only alphaNumeric & Special Characters not a Arabic Characters
-(BOOL)isAlphaNumericSpecialCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._@-"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}





-(void)checkTokenStatusWebServices {
    
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    [LoaderClass showLoader:self.view];
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSString * accessToken = @"";
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":accessToken,
                                       @"user_mob":_guestMobNum,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"fresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            [LoaderClass removeLoader:self.view];
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
            
            NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
            [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
            
            
            if ([methodname isEqualToString:@"signatureWebServicesMethod"]) {
                [self signatureWebServicesMethod];
            } else if ([methodname isEqualToString:@"returnUrl"]) {
                [self returnUrl];
            } else if ([methodname isEqualToString:@"insertRechargethrCardPayDetails"]) {
                [self Sadadservices];
            }
            
            
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//




//signature
-(void)signatureWebServicesMethod{
    
    [LoaderClass showLoader:self.view];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];

    
    NSString * db = [MCLocalization stringForKey:@"DBvalue"];
    NSString * uMob =   @"Guest";

    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/getTokenSignatureDetails"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];

    
    
    NSDictionary * tokenSignDetailsDict =  @{@"device_id":deviceid,
                                             @"db":db,
                                             @"ipAddress":Ipaddress,
                                             @"from":@"iphone",
                                             @"user_mob":uMob,
                                             @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                             @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                             };
    

    
    NSLog(@"Posting tokenSignDetailsDict is %@",tokenSignDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:tokenSignDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSLog(@"No data returned from server, error ocurred: %@", error);
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
    
    
    
            NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
       
            NSLog(@"resSrt: %@", resSrt);
            NSError *deserr;
            
            
            
            NSDictionary*Response=[NSJSONSerialization
                                   JSONObjectWithData:data
                                   options:kNilOptions
                                   error:&deserr];
            
            
            NSLog(@"got response=%@", Response);
            
            NSLog(@"Signature Response is %@",resSrt);
            
            
            
            
            if ([[Response valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Response valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"signatureWebServicesMethod";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                } else {
            
                    signature = [Response valueForKey:@"Response"];
            
                    [self sendTestJsonCommand];
                }
            }
            
        });
        
    }];
    
    [dataTask resume];
    
}

-(void)sendTestJsonCommand{
    
    
   
    
    NSMutableDictionary * Tokendict = [[NSMutableDictionary alloc]init];
    
    [Tokendict setValue: @"SDK_TOKEN" forKey:@"service_command"];
    
    
    [Tokendict setValue:[payFort getUDID] forKey:@"device_id"];
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        [Tokendict setValue:@"ar" forKey:@"language"];
    } else {
        [Tokendict setValue:@"en" forKey:@"language"];
    }
    
    
    NSString*access_code =[NSString stringWithFormat:@"%@",[CardTranDetails valueForKey:@"access_code"]];
    
    
    
    NSLog(@"sddasdd%@",access_code);
    

    
    [Tokendict setObject:access_code forKey:@"access_code"];
    
    NSString*merchant_identifier =[CardTranDetails valueForKey:@"merchant_identifier"];
    
    
    [Tokendict	setObject:merchant_identifier
                  forKey:@"merchant_identifier"];
    
    [Tokendict	setValue:signature forKey:@"signature"];
    
    NSError * error;
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:Tokendict options:NSJSONWritingPrettyPrinted error:&error];
    
    if (error) {
        NSLog(@"Error (%@), error: %@", Tokendict, error);
        return;
    }
    
#define appService [NSURL \
URLWithString:@"https://paymentservices.payfort.com/FortAPI/paymentApi"]
    
    // Create request object
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:appService];
    
    // Set method, body & content-type
    request.HTTPMethod = @"POST";
    request.HTTPBody = jsonData;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setValue:
     [NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
//    NSString *apiKey  = @"cWZwbWdlcmZkdGV4em5kc2ZnZGdkZzpxZ2h4Y2ZnaHR5bmpta3U=";
//    
//    NSString * authValue = [NSString stringWithFormat:@"Basic %@", apiKey];
//    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
//    
//    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *r, NSData *data, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        NSError *deserr;
        responseDict = [NSJSONSerialization
                        JSONObjectWithData:data
                        options:kNilOptions
                        error:&deserr];
        
        [LoaderClass removeLoader:self.view];
        
        NSLog(@"so, here's the responseDict: %@", responseDict);
        [LoaderClass removeLoader:self.view];
    }];
}


- (void)sdkResult:(id)response{
    
    test = 20;
    
    
    if([response isKindOfClass:[NSDictionary class]]){
        NSDictionary * responseDic = response;
        
        SDKresponsedict = responseDic;
        
        NSLog(@"response SDK..%@",response);
        
        [self returnUrl];
        
        
    } else if (response !=	nil	&&	![response	isEqualToString:@""] &&	![response	isEqualToString:@"nil"])	{
        ///	Invalid	Request	Error	Message
    } else	{
        ///	Unknown	Error	Including	Connectivity	Issues
    }
}



-(void)returnUrl {
    
    
    [LoaderClass showLoader:self.view];

    NSString * a1, * a2, * a3, *a4,* a7,*a8,*a9, *a10,*a11,  *a12,  *a13,  *a14, *a15, *a16, *a17,*a18, *a19, *a20,   *a21;
    
    NSString*merchant_reference=[[SDKresponsedict valueForKey:@"merchant_reference"]stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    if ([merchant_reference length]<1) {
        
        a1 = @"NIL";
        
    } else{
        a1 = merchant_reference;
    }
    
    
    NSUserDefaults * serviceIdDefaults = [NSUserDefaults standardUserDefaults];
    NSString*serviceId=[serviceIdDefaults stringForKey:@"serviceIdDefaults"];
    
    
    if ([serviceId length]<1) {
        a2 = @"NIL";
        
    } else{
        a2 = serviceId;
    }
    
    NSString*response_code=[SDKresponsedict valueForKey:@"response_code"];
    
    
    if ([response_code length]<1) {
        a3 = @"NIL";
        
    } else{
        a3 = response_code;
    }
    
    NSString*response_message=[SDKresponsedict valueForKey:@"response_message"];
    
    if ([response_message length]<1) {
        a4 = @"NIL";
        
    } else{
        a4 = response_message;
    }
    
    
    NSString*Rcamount = _guestRcValue;
    NSString*Plan=_guestRcPlan;
    
   
    
    NSString*authorization_code=[SDKresponsedict valueForKey:@"authorization_code"];
    
    if ([authorization_code length]<1) {
        a7 = @"NIL";
        
    } else{
        a7 = authorization_code;

    }
    
    
    NSString*eci=[SDKresponsedict valueForKey:@"eci"];
    
    if ([eci length]<1) {
        a8 = @"NIL";
        
    } else{
        a8 = eci;
    }
    
    
    NSString*card_number=[SDKresponsedict valueForKey:@"card_number"];
    
    if ([card_number length]<1) {
        a9 = @"NIL";
        
    } else{
        a9 = card_number;
    }
    
    NSString*status=[SDKresponsedict valueForKey:@"status"];
    
    if ([status length]<1) {
        a10 = @"NIL";
        
    } else{
        a10 = status;
    }
    
    NSString*fort_id=[SDKresponsedict valueForKey:@"fort_id"];
    
    
    if ([fort_id length]<1) {
        a11 = @"NIL";
        
    } else{
        a11 = fort_id;
    }
    
    NSString* userName = @"Guest";
    
    if ([userName length]<1) {
        a12 = @"NIL";
        
    } else{
        a12 = userName;
    }
    
    
    NSString*currency=[SDKresponsedict valueForKey:@"currency"];
    
    if ([currency length]<1) {
        a13 = @"NIL";
        
    } else{
        a13 = currency;
    }
    
    
    NSString*customer_ip=[SDKresponsedict valueForKey:@"customer_ip"];
    
    
    if ([customer_ip length]<1) {
        a14 = @"NIL";
        
    } else{
        a14 = customer_ip;
    }
    
    NSString*order_description=[SDKresponsedict valueForKey:@"order_description"];
    
    
    if ([order_description length]<1) {
        a15 = @"NIL";
        
    } else{
        a15 = order_description;
    }
    
    NSString*command=[SDKresponsedict valueForKey:@"command"];
    
    if ([command length]<1) {
        a16 = @"NIL";
        
    } else{
        a16 = command;
    }
    
    
    
    NSString*language=[SDKresponsedict valueForKey:@"language"];
    
    if ([language length]<1) {
        a17 = @"NIL";
        
    } else{
        a17 = language;
    }
    
    
    NSString*expiry_date=[SDKresponsedict valueForKey:@"expiry_date"];
    
    if ([expiry_date length]<1) {
        a18 = @"NIL";
        
    } else{
        a18 = expiry_date;
    }
    
    NSString*couponCode = @"UNKNOWN";
    
    if ([couponCode length]<1) {
        a19 = @"NIL";
        
    }
    else{
        a19 = couponCode;
    }
    
    
    NSString*sdk_token=[SDKresponsedict valueForKey:@"sdk_token"];
    
    
    if ([sdk_token length]<1) {
        a20 = @"NIL";
        
    } else{
        a20 = sdk_token;
    }
    
    NSString*token_name=[SDKresponsedict valueForKey:@"token_name"];
    
    if ([token_name length]<1) {
        a21 = @"NIL";
        
    } else{
        a21 = token_name;
    }
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/insertRechargethrCardPayTransactionsDetails"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSDictionary * cardPayTxnDetailsDict =  @{@"operator":_guestRCOperator,
                                              @"merchant_reference":a1,
                                              @"serviceId":a2,
                                              @"response_code":a3,
                                              @"response_message":a4,
                                              @"amount":Rcamount,
                                              @"rechargePlan":Plan,
                                              @"authorizationCode":a7,
                                              @"eci":a8,
                                              @"cardNumber":a9,
                                              @"status":a10,
                                              @"fortid":a11,
                                              @"customerName":a12,
                                              @"currency":a13,
                                              @"customerIp":a14,
                                              @"orderDescription":a15,
                                              @"command":a16,
                                              @"paymentOption":@"CreditCard",
                                              @"language":a17,
                                              @"expiry_date":a18,
                                              @"coupon_code":a19,
                                              @"sdk_token":a20,
                                              @"token_name":a21,
                                              @"user_mob":@"Guest",
                                              
                                            };
    
    NSLog(@"Posting cardPayTxnDetailsDict is %@",cardPayTxnDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:cardPayTxnDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSLog(@"No data returned from server, error ocurred: %@", error);
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
           
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Response is%@",str);
            
            NSUserDefaults * Couponcode = [NSUserDefaults standardUserDefaults];
            [Couponcode removeObjectForKey:@"Couponecodenum"];
            
            
            NSLog(@"return Url Response is :%@",Killer);
            
            
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"returnUrl";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                } else {
                    
                    
                    NSUserDefaults *SuccessResponsecard = [NSUserDefaults standardUserDefaults];
                    NSData * CardTrData = [NSKeyedArchiver archivedDataWithRootObject:Killer];
                    [SuccessResponsecard setObject:CardTrData forKey:@"SuccessResponsecard"];
                    
                    
                    
                    NSUserDefaults *Operator = [NSUserDefaults standardUserDefaults];
                    [Operator setObject:_guestRCOperator forKey:@"Operator"];
                    
                    
                    NSUserDefaults *Number = [NSUserDefaults standardUserDefaults];
                    [Number setObject:_guestMobNum forKey:@"Number"];
                    
                    
                    [LoaderClass removeLoader:self.view];
                    
                    
                    GuestRCSuccessVC * rechargesuccess = [self.storyboard instantiateViewControllerWithIdentifier:@"GuestRCSuccessVC"];
                    
                    rechargesuccess.guestEmailId = _guestEmailId;
                    [self.navigationController pushViewController:rechargesuccess animated:YES];
                    
                    
                    NSUserDefaults * couponcodenum = [NSUserDefaults standardUserDefaults];
                    [couponcodenum removeObjectForKey:@"couponcodenum"];
                    
                    
                }
            }

            
        });
     
    }];
    
    [dataTask resume];
}




//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}






- (IBAction)btnProceedToPay:(id)sender {
    
    if ([_lblPaymentMethod.text isEqualToString:[MCLocalization stringForKey:@"sadad"]]) {
        
        
        if ([_txtSadadId.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
            
            toastMsg = [MCLocalization stringForKey:@"plsentSadadId"];
            [self toastMessagemethod];
            
        } else if (_txtSadadId.text.length < 6 || _txtSadadId.text.length > 12){
            
            toastMsg = [MCLocalization stringForKey:@"sadad6-12char"];
            [self toastMessagemethod];
            
        } else {
            
            [self Sadadservices];
        }
        
        
        
    } else {
        
        //        if([_AmountFromcardTxt.text intValue] < 10) {
        //
        //            alertMessage = [MCLocalization stringForKey:@"cardbalamount10"];
        //            [self alertMethod];
        //
        //        } else {
        
        payFort.delegate = self;
        //if	you	need	to	switch	on	the	Payfort	Response	page
        payFort.IsShowResponsePage = YES;
        //if	you	need	to	set	custome	Payfort	view
        [payFort setPayFortCustomViewNib:@"PayFortView2"];
        
        //Generate	the	request	dictionary	as	follow
        NSMutableDictionary	* requestDictionary = [[NSMutableDictionary alloc]init];
       
       

//       NSString * email = [CardTranDetails valueForKey:@"customer_email"];

       
        NSString * email = _guestEmailId;
        
        [requestDictionary setValue:[_guestRcValue stringByAppendingString:@"00"] forKey:@"amount"];
        
        NSString*command=[CardTranDetails valueForKey:@"command"];
        
        [requestDictionary setValue:command forKey:@"command"];
        
        [requestDictionary setValue:@"SAR" forKey:@"currency"];
        
        [requestDictionary setValue:email forKey:@"customer_email"];
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            [requestDictionary setValue:@"ar" forKey:@"language"];
        } else {
            [requestDictionary setValue:@"en" forKey:@"language"];
        }
        
        [requestDictionary setValue:[responseDict valueForKey:@"sdk_token"] forKey:@"sdk_token"];
        
        NSString*merchant_reference=[CardTranDetails valueForKey:@"merchant_reference"];
        
        [requestDictionary setValue:merchant_reference forKey:@"merchant_reference"];
        
        NSString*token_name=[CardTranDetails valueForKey:@"token_name"];
        
        [requestDictionary setValue:token_name forKey:@"token_name"];
        
        [requestDictionary setValue:[CardTranDetails valueForKey:@"order_description"] forKey:@"order_description"];
        
        test= 10;
        
        
        NSLog(@"request dictionary is%@",requestDictionary);
        //	Mandatory
        [payFort setPayFortRequest:requestDictionary];
        //make	the	call
        [payFort callPayFort:self];
        
        
        
        // Removing the Back Button on the navigation bar.
        [self.navigationItem setHidesBackButton:YES animated:YES];
        
        //        }
    }

    
    
    
}



-(void)Sadadservices{
    
    
    [LoaderClass showLoader:self.view];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * serviceIdDefaults = [NSUserDefaults standardUserDefaults];

    
    
    NSString* usermob= [@"966" stringByAppendingString:_guestMobNum];
    NSString*rechargemobileno= [@"966" stringByAppendingString:_guestMobNum];
    
    
    NSString*couponCode;
    
    
    if (couponCode.length>1) {
        couponCode = @"Unknown";
        
    } else{
        couponCode = @"Nil";
    }
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/insertRechargethrCardPayDetails"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
//    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
//    
//    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
//    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
//    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
//    
//    
//    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
//    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    
    
    NSDictionary * guestSadadDict =  @{@"operator":_guestRCOperator,
                                     @"user_name":@"Guest",
                                     @"user_mob":usermob,
                                     @"user_emailId":@"info@saddly.com",
                                     @"recharge_mob":rechargemobileno,
                                     @"ipAddress":[ipaddressdefault objectForKey:@"ipaddressdefault"],
                                     @"recharge_amount":_guestRcValue,
                                     @"payment_method":@"SADAD",
                                     @"rechargeplan":_guestRcPlan,
                                     @"service_id":[serviceIdDefaults stringForKey:@"serviceIdDefaults"],
                                     @"fromBonusCash":@"0",
                                     @"fromActualCash":@"0",
                                     @"paidAmount":_guestRcValue,
                                     @"from":@"iPhone",
                                     @"db":[MCLocalization stringForKey:@"DBvalue"],
                                     @"couponCode":couponCode,
                                     @"sadad_username":_txtSadadId.text,
                                    
                                    };
    
    NSLog(@"Posting guestSadadDict is %@",guestSadadDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:guestSadadDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSLog(@"No data returned from server, error ocurred: %@", error);
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            [LoaderClass removeLoader:self.view];
            
            NSError *deserr;
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*  str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Response is%@",Killer);
            
            NSLog(@"Response is%@",str);
            
            [LoaderClass removeLoader:self.view];
            
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"Sadadservices";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                } else {
            
                    
                    NSString*URl=[Killer valueForKey:@"response_message"];
                    
                    NSString*urlstring=[Killer valueForKey:@"transaction_url"];
                    
                    NSLog(@"URL is %@",URl);
                    
                    
                    if (![URl isEqualToString:@"Success"]) {
                        
                        alertMessage= [MCLocalization stringForKey:@"OLPID"];
                        [self alertMethod];
                        
                    }
                    else{
                        
                        NSUserDefaults * UrlDefaults = [NSUserDefaults standardUserDefaults];
                        [UrlDefaults setObject:urlstring forKey:@"url"];
                        
                        
                        NSUserDefaults * Guest = [NSUserDefaults standardUserDefaults];
                        [Guest setObject:@"100" forKey:@"Guest"];
                        
                        
                        GuestSadad * rechargesuccess = [self.storyboard instantiateViewControllerWithIdentifier:@"GuestSadad"];
                        
                        [self.navigationController pushViewController:rechargesuccess animated:YES];
                        
                    }

            
                }
            }
        });
        
    }];
    
    [dataTask resume];

            
    
}






@end
