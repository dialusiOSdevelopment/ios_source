//
//  PayOrSendVC.m
//  Saddly
//
//  Created by Sai krishna on 1/21/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//userPwd

#import "PayOrSendVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface PayOrSendVC ()

@end

@implementation PayOrSendVC



- (void)viewDidLoad {
    
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
    }
    
    
    self.title = [MCLocalization stringForKey:@"pay/send"];
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];

    
    _lblSelAnOption.text = [MCLocalization stringForKey:@"selAnOption"];
    
    arrOptions = @[[MCLocalization stringForKey:@"sendMoneyMobNum"],[MCLocalization stringForKey:@"scanqrcode"],[MCLocalization stringForKey:@"showqrcode"]];
    arrDetailedOptions = @[[MCLocalization stringForKey:@"senMonmobnumorContact"],[MCLocalization stringForKey:@"scanMerchQRcode"],[MCLocalization stringForKey:@"showmechaQRcode"]];
    arrImages = @[@"man2346.png",@"Scan_QR_Code-black.png",@"Show_QR_Code.png"];
    
    
    NSUserDefaults * numQRCodeDefaults = [NSUserDefaults standardUserDefaults];
    [numQRCodeDefaults removeObjectForKey:@"numQRCodeDefaults"];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];


        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}



//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark --
#pragma mark - UITableView Delegate Methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrOptions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@""];
    
    if (cell == nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@""];
    }
    
    cell.textLabel.text = [arrOptions objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = [arrDetailedOptions objectAtIndex:indexPath.row];
    
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    cell.textLabel.adjustsFontSizeToFitWidth =YES;
    cell.detailTextLabel.adjustsFontSizeToFitWidth =YES;
    
    cell.textLabel.textColor = [UIColor blackColor];
    cell.detailTextLabel.textColor = [UIColor blackColor];
    
    cell.imageView.image = [UIImage imageNamed:[arrImages objectAtIndex:indexPath.row]];
    
    _tblPayOrSend.alwaysBounceVertical = NO;
    cell.backgroundColor =  [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.tblPayOrSend.tableFooterView = [[UIView alloc] init];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        SendMoneyVC * sendMoney = [self.storyboard instantiateViewControllerWithIdentifier:@"SendMoneyVC"];
        [self.navigationController pushViewController:sendMoney animated:YES];
        
    } else if (indexPath.row == 1) {
        
        if ([QRCodeReader supportsMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]]) {
            static QRCodeReaderViewController *vc = nil;
            static dispatch_once_t onceToken;
            
            dispatch_once(&onceToken, ^{
                QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
                vc                   = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Cancel" codeReader:reader startScanningAtLoad:YES showSwitchCameraButton:YES showTorchButton:YES];
                vc.modalPresentationStyle = UIModalPresentationFormSheet;
            });
            vc.delegate = self;
            
            [vc setCompletionWithBlock:^(NSString *resultAsString) {
                NSLog(@"Completion with result: %@", resultAsString);
            }];
            
            [self presentViewController:vc animated:YES completion:NULL];
            
        } else {
            
            alertTitle = [MCLocalization stringForKey:@"failure"];
            alertMessage = [MCLocalization stringForKey:@"camNotSupp"];
            [self alertMethod];
            
        }
        
    } else {
        
        ShowQRCodeVC * showQrCode = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowQRCodeVC"];
        [self.navigationController pushViewController:showQrCode animated:YES];
        
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 65.0f;
}


#pragma mark - QRCodeReader Delegate Methods

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result{
    
    [reader stopScanning];
    
    [self dismissViewControllerAnimated:YES completion:^{
    
        output=result;
        
        if ([output integerValue]) {
            NSUserDefaults * numQRCodeDefaults = [NSUserDefaults standardUserDefaults];
            [numQRCodeDefaults setObject:[result stringByReplacingOccurrencesOfString:@"966" withString:@""] forKey:@"numQRCodeDefaults"];
            
            SendMoneyVC * sendMoney = [self.storyboard instantiateViewControllerWithIdentifier:@"SendMoneyVC"];
            [self.navigationController pushViewController:sendMoney animated:YES];
        } else{
            
            toastMsg = [[[MCLocalization stringForKey:@"invalQRCode"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"scanAValidSaddCode"]];
            [self toastMessagemethod];
        }
        
    }];
    
}


- (void)readerDidCancel:(QRCodeReaderViewController *)reader
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}





@end
