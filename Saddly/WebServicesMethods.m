//

#import "WebServicesMethods.h"
#import <UIKit/UIKit.h>


@interface NSMutableURLRequest (IgnoreCertificates)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host;
+ (void)setAllowsAnyHTTPSCertificate:(BOOL)allow forHost:(NSString*)host;
@end

@implementation WebServicesMethods

@synthesize target;
@synthesize action;
@synthesize numberTag;
@synthesize needTag;




- (id)init {
    
    self = [super init];
    responseData = nil;
    return self;
}

-(void) sendURLGetRequestWithURLString:(NSString *) urlString {
    
    NSLog(@"URL getting called is %@",urlString);
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    defaultConfigObject.timeoutIntervalForRequest = 45.0;
    defaultConfigObject.timeoutIntervalForResource = 45.0;
    
    //    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:urlString];
    
    // Create request object
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    // Set method, body & content-type
    request.HTTPMethod = @"GET";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSString *apiKey  = @"cWZwbWdlcmZkdGV4em5kc2ZnZGdkZzpxZ2h4Y2ZnaHR5bmpta3U=";
    
    //    NSString *authStr = [NSString stringWithFormat:@"%@:", apiKey];
    //    NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
    //    NSString *authValue = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedStringWithOptions:NSDataBase64Encoding76CharacterLineLength]];
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", apiKey];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    //    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
    //                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        
        if(error == nil)
        {
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            int code = (int)[httpResponse statusCode];
            
            
            NSString * jsonString = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
            
            NSMutableArray*jsonarray=[[NSMutableArray alloc]init];
            
            
            [jsonarray addObject:data];
            
            
            NSData *unicodedStringData = [jsonString dataUsingEncoding:NSASCIIStringEncoding];
            NSString *decodedString = [[NSString alloc] initWithData:unicodedStringData encoding:NSASCIIStringEncoding];
            
            
            NSData *responseJsonData = [decodedString dataUsingEncoding:NSUTF8StringEncoding];
            
            // Parse the JSON into an Object
            //    id responseObject =  [jsonParser objectWithString:jsonString error:NULL];
            id responseObject = [NSJSONSerialization
                                 JSONObjectWithData:responseJsonData
                                 options:NSJSONReadingMutableContainers
                                 error:NULL];
            
            statusCodeDictionary = [[NSMutableDictionary alloc] init];
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray*Tempw=[[NSArray alloc]initWithArray:responseObject];
                
                [statusCodeDictionary setValue:Tempw forKey:@"List"];
            }
            
            else if ([responseObject isKindOfClass:[NSDictionary class]])
                
            {
                statusCodeDictionary = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
            }
            
            
            else {
                
                NSString *temp = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet ]];
                
                
                [statusCodeDictionary setValue:temp forKey:@"Message"];
            }
            
            
            
            //                                                            if (!responseObject) {
            //                                                                if (![[responseObject allKeys] count]) {
            //                                                                    NSLog(@"get service string %@",jsonString);
            //
            //                                                                    NSString *temp = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet ]];
            //                                                                    //temp = [temp string]
            //                                                                    [statusCodeDictionary setValue:temp  forKey:@"Message"];
            //
            //
            //                                                                }
            //
            //
            //
            //
            //                                                            }
            
            
            [statusCodeDictionary setObject:[NSString stringWithFormat:@"%d",code ] forKey:@"status_code"];
            @try{
                if(self.target != nil && self.action  && [self.target respondsToSelector:self.action]) {
                    
                    [self.target performSelector:self.action withObject:statusCodeDictionary];
                }
            }
            @catch (NSException *exception) {
                NSLog(@"main: Caught %@: %@", [exception name], [exception reason]);
            }
            
        }
        
        
        else{
            NSLog(@"%@", [error localizedDescription]);
            
            [statusCodeDictionary setValue:@"ErrorOccured" forKey:@"List"];
            
            
        }
        
    }];
    
    //[dataTask resume];
    
}


- (void) sendSynchronousPostRequestWithStringForAction:(NSString*)methodName andParameters:(id)parameters andRequestType:(NSString *)requestType {
    
    NSString *urlString = methodName;
    
    NSLog(@"URL getting called is %@",urlString);
    
    
    BOOL firstKey = TRUE;
    NSString *parameterString = @"";
    if ([parameters isKindOfClass:[NSArray class]]) {
        
        NSArray *params=(NSArray*)parameters;
        
        for (int i=0; i<params.count; i++) {
            
            for (NSString *key in params[i])
            {
                if (firstKey == FALSE) parameterString = [parameterString stringByAppendingString:@"&"];
                parameterString = [parameterString stringByAppendingString:key];
                parameterString = [parameterString stringByAppendingString:@"="];
                parameterString = [parameterString stringByAppendingString:[NSString stringWithFormat:@"%@",[[params objectAtIndex:i] objectForKey:key]]];
                
                firstKey = FALSE;
            }
            
        }
        
    }
    else if ([parameters isKindOfClass:[NSDictionary class]]){
        
        for (NSString *key in parameters)
        {
            if (firstKey == FALSE) parameterString = [parameterString stringByAppendingString:@"&"];
            parameterString = [parameterString stringByAppendingString:key];
            parameterString = [parameterString stringByAppendingString:@"="];
            parameterString = [parameterString stringByAppendingString:[NSString stringWithFormat:@"%@",[parameters objectForKey:key]]];
            
            firstKey = FALSE;
        }
        
    }
    
    
    
    // parameterString = [parameters JSONRepresentation];
    
    NSData *jsonData;
    
    if (parameters) {
        jsonData = [NSJSONSerialization dataWithJSONObject:parameters
                                                   options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                     error:NULL];
    }
    
    if (! jsonData) {
        NSLog(@"Got an error:");
    } else {
        parameterString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    //    if (parameters) {
    //        parameterString = [parameters objectForKey:@"userPic"];
    //    }
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:45];
    
    [NSMutableURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[[NSURL URLWithString:[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]] host]];
    
    
    [urlRequest setHTTPMethod:requestType];
    
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    if (parameterString) {
        
        [urlRequest setHTTPBody: [parameterString dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    // Fetch the JSON response
    
    
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForRequest = 45.0;
    configuration.timeoutIntervalForResource = 45.0;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *urlData, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        int code = (int)[httpResponse statusCode];
        NSLog(@"%d is status code",code);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if(error) {
            
            
            @try{
                if(self.target != nil && self.action  && [self.target respondsToSelector:self.action]) {
                    [self.target performSelector:self.action withObject:[error localizedDescription]];
                }
            }
            @catch (NSException *exception) {
                NSLog(@"main: Caught %@: %@", [exception name], [exception reason]);
            }
            return;
            
        }
        
        
        //        NSData *data = [@"{\"id\" : 1}" dataUsingEncoding:NSUTF8StringEncoding];
        //        [NSJSONSerialization JSONObjectWithData:data
        //                                        options:NSJSONWritingPrettyPrinted
        //                                          error:nil];
        
        
        
        
        NSString *jsonString = [[NSString alloc] initWithData:urlData   encoding:NSASCIIStringEncoding];
        NSData *unicodedStringData = [jsonString dataUsingEncoding:NSASCIIStringEncoding];
        NSString *decodedString = [[NSString alloc] initWithData:unicodedStringData encoding:NSASCIIStringEncoding];
        
        
        NSData *responseJsonData = [decodedString dataUsingEncoding:NSUTF8StringEncoding];
        
        // Parse the JSON into an Object
        //    id responseObject =  [jsonParser objectWithString:jsonString error:NULL];
        id responseObject = [NSJSONSerialization
                             JSONObjectWithData:responseJsonData
                             options:NSJSONReadingMutableContainers
                             error:NULL];
        
        
        statusCodeDictionary = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
        if (!responseObject) {
            if (![[responseObject allKeys] count]) {
                [statusCodeDictionary setObject:[jsonString stringByReplacingOccurrencesOfString:@"\"" withString:@""] forKey:@"Message"];
            }
        }
        
        //        unknown_object
        
        
        [statusCodeDictionary setObject:[NSString stringWithFormat:@"%d",code ] forKey:@"status_code"];
        @try{
            if(self.target != nil && self.action  && [self.target respondsToSelector:self.action]) {
                [self.target performSelector:self.action withObject:statusCodeDictionary];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"main: Caught %@: %@", [exception name], [exception reason]);
        }
        
    }];
    
    [postDataTask resume];
    
    
    
    // Construct a String around the Data from the response
    
    
    //    sugeestion code added for temporary
    
    //            NSData *data = [@"{\"id\" : 1}" dataUsingEncoding:NSUTF8StringEncoding];
    //            [NSJSONSerialization JSONObjectWithData:data
    //                                            options:NSJSONWritingPrettyPrinted
    //                                              error:nil];
    //
    
}




#pragma mark ----NSURLSessionAuthentication Methods


- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        //  if([challenge.protectionSpace.host isEqualToString:@"mydomain.com"]){
        NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        //   }
    }
}


- (void) cancelRequest {
    self.target = nil;
    self.action = nil;
}


//
-(void) dealloc {
    //
    self.target = nil;
    self.action = nil;
    responseData = nil;
    //
}


-(void) checkTokenStatusWebServices:(NSString *)urlFromMethod userMobileNumber:(NSString *)userMobParam userPassword:(NSString *)userPasswordParam dictFromMethod:(id)UserDictionary {
    
    NSLog(@"URL getting called is %@",urlFromMethod);
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:urlFromMethod];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userMobParam stringByAppendingString:@":"] stringByAppendingString:userPasswordParam];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
   
//    
//    NSDictionary * checkTokenDict =  @{@"ipaddress":@"183.82.100.179",
//                                 @"mac_address":@"02:00:00:00:00:00",
//                                 @"access_token":@"6c2e46358f7425f6a12663def06d4d993924a3551c9c62c02a40ea8c8652538f",
//                                 @"user_mob":@"585858585",
//                                 @"from":@"iPhone",
//                                 @"unique_id":@"139EE8D1-7CE0-42FE-BBE5-E411AA3B34E4",
//                                 @"device_model":@"iPhone 5s",
//                                 @"genKey":@"4d45584b3b944d7ea2f438b5b8c2e9941f91de49ca401fcb9108415098757958",
//                                 @"token_type":@"fresh_token",
//                                 
//                                 };

    NSDictionary * checkTokenDict =  UserDictionary;

    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            NSString * a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
            
            NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
            [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
            
            
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//







@end
