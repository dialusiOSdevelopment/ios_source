//
//  ContactUSDetailVC.h
//  DialuzApp
//
//  Created by gandhi on 23/06/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MCLocalization.h"
#import <QuartzCore/QuartzCore.h>
#import <MapKit/MapKit.h>




@interface ContactUSDetailVC : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate>

{
    UIAlertController * alert;
    UIAlertAction * okButton;
    UIAlertAction * Callbutton;
    NSString * alertTitle, * alertMessage,*address;
    NSDictionary*addressdict;
    CLLocationCoordinate2D theCoordinate;
    CLLocationManager *currentLocation;
}

@property (strong, nonatomic) IBOutlet UITextView *txtView;

@property (strong, nonatomic) IBOutlet MKMapView *mapview;
@property (strong, nonatomic) IBOutlet UIButton *CntryName;

;

@end
