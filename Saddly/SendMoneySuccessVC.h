//
//  SendMoneySuccessVC.h
//  DialuzApp
//
//  Created by Sai krishna on 12/29/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"



#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"



@interface SendMoneySuccessVC : UIViewController{

    NSDictionary*sendMoneyDict;
}


@property (strong, nonatomic) IBOutlet UIImageView *statusImgView;
@property (strong, nonatomic) IBOutlet UILabel *lblAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblOrderId;
@property (strong, nonatomic) IBOutlet UIButton *btnBackHomeOutlet;


// Names
@property (strong, nonatomic) IBOutlet UILabel *lblcongratsName;
@property (strong, nonatomic) IBOutlet UILabel *lblSucSentName;
@property (strong, nonatomic) IBOutlet UILabel *lblToName;

- (IBAction)btnBackToHome:(id)sender;

@end
