//
//  PayFromWalletVC.h
//  DialuzApp
//
//  Created by Sai krishna on 11/11/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServicesMethods.h"
#import "RehargeSuccessVC.h"
#import "MCLocalization.h"
#import "ConnectivityManager.h"
#import "LoaderClass.h"
#import "PayfromCard.h"

#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"




@interface PayFromWalletVC : UIViewController <NSURLSessionDelegate>{
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle,* alertMessage;
    NSDictionary * txnDetailsDict;
    NSDictionary * Walletdetails,*responseDict;
    
    NSString*Str;
    NSString *base64Decoded;
    
    NSData*mutableData;
    
    NSString * methodname, * a123;
    NSDictionary * profileDict;
    
    
    
    
}



@property (strong, nonatomic) UIActivityIndicatorView * activityView;
@property (strong, nonatomic) UIView *loadingView;
@property (strong, nonatomic) UILabel *loadingLabel;
@property (strong, nonatomic) UIImageView * loadingimage;

//names

@property (strong, nonatomic) IBOutlet UILabel *Namemobilenumber;
@property (strong, nonatomic) IBOutlet UILabel *NameOperator;
@property (strong, nonatomic) IBOutlet UILabel *nameRechargeAmount;
@property (strong, nonatomic) IBOutlet UILabel *NameRemainingWallet;
@property (strong, nonatomic) IBOutlet UILabel *NameRechargeplan;
@property (strong, nonatomic) IBOutlet UILabel *fromWalletName;
@property (strong, nonatomic) IBOutlet UIButton *btnProceedToPayOutlet;

@property (strong, nonatomic) IBOutlet UIImageView *imgOperator;
@property (strong, nonatomic) IBOutlet UILabel *lblFromName;
@property (strong, nonatomic) IBOutlet UILabel *lblWalletName;



//values
@property (strong, nonatomic) IBOutlet UILabel *mobilenumberTxt;
@property (strong, nonatomic) IBOutlet UILabel *OperatorTxt;
@property (strong, nonatomic) IBOutlet UILabel *PlanTxt;
@property (strong, nonatomic) IBOutlet UILabel *AmountFromwalletTxt;
@property (strong, nonatomic) IBOutlet UILabel *lblRcAmount;

@property (strong, nonatomic) IBOutlet UILabel *lblRemainingWallet;


- (IBAction)ProceedtopayQuickpay:(id)sender;

@end
