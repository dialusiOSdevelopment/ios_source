//
//  UserContactsVC.m
//  Saddly
//
//  Created by Sai krishna on 1/19/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "UserContactsVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface UserContactsVC ()

@end

@implementation UserContactsVC

- (void)viewDidLoad {
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
    }
    
    self.title = [MCLocalization stringForKey:@"PhoneBook"];
    _searchBarContacts.placeholder = [MCLocalization stringForKey:@"searchByName"];
    
    self.groupOfContacts = [@[] mutableCopy];
    self.phoneNumberArray = [@[] mutableCopy];
    self.contactNameArray = [@[] mutableCopy];
    
    
    [self getallcontacts];
    
    for (CNContact * contact in self.groupOfContacts) {
        
        NSMutableArray * myArr = [[NSMutableArray alloc] init];
        [myArr addObject:contact.givenName];
        
        NSArray * phoneNum = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
        [self.contactNameArray addObjectsFromArray:myArr];
        [self.phoneNumberArray addObjectsFromArray:phoneNum];
    } // for
    
    _FinalContact = _contactNameArray;
    _Finalnumber = _phoneNumberArray;
    
    _finalcontacts = [NSMutableArray new];
    
    
    if (_FinalContact.count <1 || _Finalnumber.count<1) {
        
        NSLog(@"No contacts");
        
        _tblContacts.hidden = YES;
        _searchBarContacts.hidden = YES;
        
        
        UILabel * noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tblContacts.bounds.size.width, self.tblContacts.bounds.size.height)];
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        [self.view addSubview:noDataLabel];
        noDataLabel.numberOfLines = 2;
        

        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
            
            noDataLabel.text = @"عفواً !! لاتوجد أرقم متاحه";
            
        } else {
            
            noDataLabel.text = @"Sorry!! No Contacts are Available";
        }
        
                
    }  else{
        
        _tblContacts.hidden = NO;
        _searchBarContacts.hidden = NO;
        
        
        for (NSInteger i=0; i<_FinalContact.count; i++) {
            
            NSMutableDictionary * finaldata = [NSMutableDictionary new];
            finaldata[@"Finalcontact"] = _FinalContact[i];
            finaldata[@"Finalnumber"] = _Finalnumber[i];
            
            [_finalcontacts addObject:finaldata];
            searchedcontacts = _finalcontacts;
        }
        
        NSSortDescriptor * nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Finalcontact"  ascending:YES];
        NSSortDescriptor * numDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Finalnumber"  ascending:YES];
        NSArray * sortDescriptors = @[nameDescriptor, numDescriptor];
        
        searchedcontacts = [searchedcontacts sortedArrayUsingDescriptors:sortDescriptors];
        
    }
        
    
    
        
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];


        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}

 



-(void)getallcontacts{
    
    if ([CNContactStore class]) {
        CNContactStore * addressBook = [[CNContactStore alloc]init];
        
        NSArray * keysToFetch = @[CNContactGivenNameKey ,
                                  CNContactEmailAddressesKey,
                                  CNContactFamilyNameKey,
                                  CNContactPhoneNumbersKey,
                                  CNContactPostalAddressesKey];
        
        CNContactFetchRequest * fetchRequest = [[CNContactFetchRequest alloc] initWithKeysToFetch:keysToFetch];
        
        [addressBook enumerateContactsWithFetchRequest:fetchRequest error:nil usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
            [self.groupOfContacts addObject:contact];
        }]; // Block
    } // if
} // getallcontacts



//Dismissing Keyboard for SearchBar
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [_searchBarContacts resignFirstResponder];
}


// Returning Search bar, when search text is empty.
- (void)searchBarTextDidBeginEditing:(UISearchBar *) bar
{
    UITextField *searchBarTextField = nil;
    NSArray *views = ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f) ? bar.subviews : [[bar.subviews objectAtIndex:0] subviews];
    for (UIView *subview in views)
    {
        if ([subview isKindOfClass:[UITextField class]])
        {
            searchBarTextField = (UITextField *)subview;
            break;
        }
    }
    searchBarTextField.enablesReturnKeyAutomatically = NO;
}


#pragma mark --
#pragma mark - UITableView Delegate Methods



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = searchedcontacts.count;
    return count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString * cellIdentifier=@"Cell";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [[searchedcontacts objectAtIndex:indexPath.row] valueForKey:@"Finalcontact"];
    cell.detailTextLabel.text = [[searchedcontacts objectAtIndex:indexPath.row] valueForKey:@"Finalnumber"];
    
    cell.textLabel.textColor = [UIColor blackColor];
    cell.detailTextLabel.textColor = [UIColor blackColor];
    
    cell.backgroundColor =  [UIColor whiteColor];
    _tblContacts.alwaysBounceVertical = NO;
    self.tblContacts.tableFooterView = [[UIView alloc] init];
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableString * selectedNumber = [[searchedcontacts objectAtIndex:indexPath.row]valueForKey:@"Finalnumber"];
    
    NSString * trimmedSelectedNumber =[selectedNumber substringFromIndex:MAX((int)[selectedNumber length]-9, 0)];
    
    NSString * newString = [[trimmedSelectedNumber componentsSeparatedByCharactersInSet:
                             [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                            componentsJoinedByString:@""];
    
    NSUserDefaults * mobNumContactsDefaults = [NSUserDefaults standardUserDefaults];
    [mobNumContactsDefaults setObject:newString forKey:@"mobNumContactsDefaults"];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


// searching the contacts names.
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if (searchText.length > 0) {
        searchText = [searchText stringByReplacingOccurrencesOfString:@" "   withString: @" "];
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"Finalcontact CONTAINS [cd] %@", searchText];
        
        searchedcontacts = [searchedcontacts filteredArrayUsingPredicate:predicate];
        [_tblContacts reloadData];
    }
    else{
        searchedcontacts = _finalcontacts ;
        [_tblContacts reloadData];
    }
}



@end
