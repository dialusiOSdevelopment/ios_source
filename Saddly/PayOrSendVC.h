//
//  PayOrSendVC.h
//  Saddly
//
//  Created by Sai krishna on 1/21/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SendMoneyVC.h"
#import "QRCodeReaderDelegate.h"
#import "QRCodeReaderViewController.h"
#import "QRCodeReader.h"
#import "ShowQRCodeVC.h"
#import "MCLocalization.h"
#import "ConnectivityManager.h"
#import "WebServicesMethods.h"
#import "LoaderClass.h"
#import "UIView+Toast.h"

@interface PayOrSendVC : UIViewController <UITableViewDataSource,UITableViewDelegate,QRCodeReaderDelegate>{
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    
    NSString*output;
    
    NSArray * arrOptions, * arrDetailedOptions, * arrImages;
    
}


@property (strong, nonatomic) IBOutlet UILabel *lblSelAnOption;
@property (strong, nonatomic) IBOutlet UITableView *tblPayOrSend;






@end
