//
//  RCGCVC.h
//  Saddly
//
//  Created by Sai krishna on 6/17/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "RCGCOperators.h"

@interface RCGCVC : UIViewController {
    
    NSUserDefaults * btnRcDefaults;
}






@property (strong, nonatomic) IBOutlet UIButton *btnRcOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnGiftCardsOutlet;

- (IBAction)btnRecharge:(id)sender;
- (IBAction)btnGiftCards:(id)sender;


@end
