//
//  RehargeSuccessVC.h
//  DialuzApp
//
//  Created by Sai krishna on 11/11/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "UIView+Toast.h"
#import "LoaderClass.h"
#import "ConnectivityManager.h"
#import "WebServicesMethods.h"
#import "Validate.h"

#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
#import "QuickRechargeVC.h"



@interface RehargeSuccessVC : UIViewController{
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle,* alertMessage, * toastMsg;
    
    NSDictionary *  Transfromwallet,*SuccessResponseDict,*WalletSuccessDict, * iqamaDetails;
    
    NSDictionary * profileDict;
    NSString * voucherCode;

    
}


@property (strong, nonatomic) NSString * txtViewRefNum;
@property (strong, nonatomic) NSString * OperatorSuccessTxt;
@property (strong, nonatomic) NSString *txtEnterIqamaNumber;


@property (strong, nonatomic) IBOutlet UILabel *TnqTxtname;
@property (strong, nonatomic) IBOutlet UIButton *DoneSuccessName;
@property (strong, nonatomic) IBOutlet UITextView *txtViewContact;
@property (strong, nonatomic) IBOutlet UILabel *lblVoucherCodeName;

@property (strong, nonatomic) IBOutlet UIButton *btnVoucherCodeCopyOutlet;
@property (strong, nonatomic) IBOutlet UITextView *txtviewFailureRespMsgs;
@property (strong, nonatomic) IBOutlet UIView *viewTxns;
@property (strong, nonatomic) IBOutlet UIButton *btnClickToRcOutlet;



@property (strong, nonatomic) IBOutlet UILabel *txnStatusName;
@property (strong, nonatomic) IBOutlet UILabel *walletAmountName;
@property (strong, nonatomic) IBOutlet UILabel *MobileSuccesName;
@property (strong, nonatomic) IBOutlet UILabel *OperatorSuccessName;
@property (strong, nonatomic) IBOutlet UILabel *RechargeAmountSuccessName;
@property (strong, nonatomic) IBOutlet UILabel *lblUSSDCode;
@property (strong, nonatomic) IBOutlet UILabel *lblIqmaNumberName;
@property (strong, nonatomic) IBOutlet UIButton *btnCopyOutlet;
@property (strong, nonatomic) IBOutlet UILabel *lblWalletAmount;




//actions
@property (strong, nonatomic) IBOutlet UILabel *MobilenumberSuccessTxt;
@property (strong, nonatomic) IBOutlet UILabel *AmountSucceessTxt;
@property (strong, nonatomic) IBOutlet UILabel *txnStatusSuccessTxt;


@property (strong, nonatomic) IBOutlet UILabel *lblVoucherCode;
@property (strong, nonatomic) IBOutlet UIImageView *imgOperator;

- (IBAction)btnClickToRc:(id)sender;
- (IBAction)btnCopy:(id)sender;
- (IBAction)btnVoucherCodeCopy:(id)sender;
- (IBAction)DoneButtonAction:(id)sender;



@property (strong, nonatomic) IBOutlet UIImageView *imgSuccessFailOutlet;



@end
