//
//  ForgotPwdMobNumVC.m
//  Saddly
//
//  Created by Sai krishna on 8/6/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "ForgotPwdMobNumVC.h"

@interface ForgotPwdMobNumVC ()

@end

@implementation ForgotPwdMobNumVC

- (void)viewDidLoad {
    
    
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated {
    
    _txtPwd.placeholder = [MCLocalization stringForKey:@"Regpassword"];
    _txtConfirmPwd.placeholder = [MCLocalization stringForKey:@"confirmpass"];
    _btnSendOutlet.titleLabel.text = [MCLocalization stringForKey:@"sendName"];
    
    
    _txtPwd.adjustsFontSizeToFitWidth = YES;
    _txtConfirmPwd.adjustsFontSizeToFitWidth = YES;
    _btnSendOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    
}


-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}



// Returning to Next Text fields
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtPwd) {
        [self.txtConfirmPwd becomeFirstResponder];
    } else if (textField == self.txtConfirmPwd) {
        [self.txtConfirmPwd resignFirstResponder];
    }
    
    return YES;
}




// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = self.view.bounds;
    
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}





-(void) checkTokenStatusWebServices {
    
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":_forgotMobnum,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                if ([methodname isEqualToString:@"ForgetPasswordOTPVeri"]) {
                    [self ForgetPasswordOTPVeri];
                   
                }
                
               
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//





-(void)ForgetPasswordOTPVeri {
    
    NSString*userMobNum= _forgotMobnum;
    NSString*db= [MCLocalization stringForKey:@"DBvalue"];;
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];

    
    [LoaderClass showLoader:self.view];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/forgotPassword/forgotpasswordOTPVerification"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSDictionary * forPwdOTPDetailsDict =  @{@"usermob":userMobNum,
                                             @"db":db,
                                             @"from":@"iPhone",
                                             @"ipAddress":Ipaddress,
                                             @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                             @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                             };
    
    NSLog(@"Posting forPwdOTPDetailsDict is %@",forPwdOTPDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:forPwdOTPDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            NSError *deserr;
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"str  Response is%@",str);
            NSLog(@"Killer  Response is :%@",Killer);
            
            
            [LoaderClass removeLoader:self.view];
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"ForgetPasswordOTPVeri";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                } else if ([[Killer valueForKey:@"response"] isEqualToString:@"Success"]) {
                  
                    
                    ForgotPwdOTPVC * forOTP = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPwdOTPVC"];
                    forOTP.forOTPPwd = _txtConfirmPwd.text;
                    forOTP.forgotMobnum = _forgotMobnum;
                    [self.navigationController pushViewController:forOTP animated:YES];
                    
                    
                } else if ([[Killer valueForKey:@"response"] isEqualToString:@"failure"]) {
                   
                    
                    toastMsg = [[[MCLocalization stringForKey:@"wentWrong"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
                    [self toastMessagemethod];
                    
                } else if ([[Killer valueForKey:@"response"] isEqualToString:@"some thing went wrong"]) {
                    
                    toastMsg = [[[MCLocalization stringForKey:@"wentWrong"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
                    [self toastMessagemethod];
                    
                }
                
            }
            
        });
        
    }];
    
    [dataTask resume];
    
}





- (IBAction)btnShowPwd:(UIButton * )sender {
    
    sender.selected  =! sender.selected;
    if (sender.selected)
    {
        _txtPwd.secureTextEntry = NO;
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
    } else {
        _txtPwd.secureTextEntry = YES;
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
    }
    
}


- (IBAction)btnShowConfirmPwd:(UIButton * )sender {
    
    sender.selected  =! sender.selected;
    if (sender.selected)
    {
        _txtConfirmPwd.secureTextEntry = NO;
        [_btnShowConfirmPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        [_btnShowConfirmPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
    } else {
        _txtConfirmPwd.secureTextEntry = YES;
        [_btnShowConfirmPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        [_btnShowConfirmPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
    }

    
}

- (IBAction)btnSend:(id)sender {
    
    // Validating the Password for the repeated characters
    NSCountedSet* characterCounts = [[NSCountedSet alloc] init];
    // This ensures that we deal with all unicode code points correctly
    [_txtConfirmPwd.text enumerateSubstringsInRange:NSMakeRange(0, [_txtConfirmPwd.text length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
        [characterCounts addObject:substring];
    }];
    NSString* highestCountCharacterSequence = nil;
    NSUInteger highestCharacterCount = 0;
    for (NSString* characterSequence in characterCounts) {
        NSUInteger currentCount = [characterCounts countForObject:characterSequence];
        if (currentCount > highestCharacterCount) {
            highestCountCharacterSequence = characterSequence;
            highestCharacterCount = currentCount;
        }
    }
    NSLog(@"Highest Character Count in _txtPassword is %@ with count of %lu", highestCountCharacterSequence, (unsigned long)highestCharacterCount);
    
    

    
    // Password
     if ([_txtPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg = [MCLocalization stringForKey:@"Passwordvalidate"];
        [self toastMessagemethod];
    } else if (_txtPwd.text.length < 8){
        toastMsg = [MCLocalization stringForKey:@"pwd8char"];
        [self toastMessagemethod];
    }
    // Confirm password
    else if ([_txtConfirmPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg =   [MCLocalization stringForKey:@"confirmpass"];
        [self toastMessagemethod];
    } else if (_txtConfirmPwd.text.length < 8){
        toastMsg = [MCLocalization stringForKey:@"cnfrmpwd8char"];
        [self toastMessagemethod];
    }
    // Comparising passwords
    else if (![_txtConfirmPwd.text isEqualToString:_txtPwd.text]) {
        toastMsg = [MCLocalization stringForKey:@"Passmatch"];
        [self toastMessagemethod];
    }
    // Not entering the Repeated characters
    else if (highestCharacterCount > 1) {
        toastMsg = [MCLocalization stringForKey:@"repeatNotAllow"];
        [self toastMessagemethod];
    }
    else {
        [self ForgetPasswordOTPVeri];
    }


    
    
}


@end
