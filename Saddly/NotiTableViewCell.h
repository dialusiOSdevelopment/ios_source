//
//  NotiTableViewCell.h
//  Saddly
//
//  Created by Sai krishna on 5/6/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotiTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *img;


@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblNotification;
@property (strong, nonatomic) IBOutlet UILabel *lblDateTime;




@end
