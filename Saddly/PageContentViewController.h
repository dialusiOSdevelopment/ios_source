//
//  PageContentViewController.h
//  Saddly
//
//  Created by Sai krishna on 2/7/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageContentViewController : UIViewController


@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;
@property (strong, nonatomic) IBOutlet UILabel *lblScreenData;


@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;


@end
