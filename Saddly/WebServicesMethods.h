//
//  WebServicesMethods.h
//  Tarushi
//
//  Created by Saketh Ashvapuram on 03/12/15.
//  Copyright (c) 2015 self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface WebServicesMethods : NSObject <NSURLSessionDataDelegate> {
    
    int numberTag;
    BOOL needTag;
    id target;
    SEL action;
    id responseString;
    
    NSURLResponse *urlResponse;
    NSMutableData *responseData;
    NSURLConnection *asyncConnection;
    NSMutableDictionary *statusCodeDictionary;
}

@property (nonatomic, readwrite, retain) id target;
@property (nonatomic, readwrite, assign) SEL action;
@property (nonatomic, assign) int numberTag;
@property (nonatomic, assign) BOOL needTag;


-(void) sendURLGetRequestWithURLString:(NSString *) urlString;

- (void) sendSynchronousPostRequestWithStringForAction:(NSString*)methodName andParameters:(id)parameters andRequestType:(NSString *)requestType;


-(void) checkTokenStatusWebServices:(NSString *)urlFromMethod userMobileNumber:(NSString *)userMobParam userPassword:(NSString *)userPasswordParam dictFromMethod:(id)UserDictionary;


@end
