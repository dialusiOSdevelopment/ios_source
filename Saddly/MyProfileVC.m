//
//  MyProfileVC.m
//  Saddly
//
//  Created by Sai krishna on 1/19/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "MyProfileVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface MyProfileVC ()

@end

@implementation MyProfileVC

- (void)viewDidLoad {
    
    
    self.title = [MCLocalization stringForKey:@"myprofile"];
    
//    profileOptions = @[[MCLocalization stringForKey:@"changePwd"],[MCLocalization stringForKey:@"changeEmail"],[MCLocalization stringForKey:@"myRcs"]];

    profileOptions = @[[MCLocalization stringForKey:@"updateProfle"],[MCLocalization stringForKey:@"updatePwd"],[MCLocalization stringForKey:@"myRcs"]];
    profileOptionsImages = @[@"update-Profile.png",@"change-Password.png",@"my_txns.png"];

    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
   
    
    if ([[profileDict valueForKey:@"gender"] isEqual:[NSNull null]] || [[profileDict valueForKey:@"gender"] isEqualToString:@"null"]){
        
        _imgProfileImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"appicon_green.png"]];
        
    } else if ([[profileDict valueForKey:@"gender"] isEqualToString:@"male"]) {
        _imgProfileImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"man.png"]];
            
    } else if ([[profileDict valueForKey:@"gender"] isEqualToString:@"female"]) {
            
        _imgProfileImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"female.png"]];
            
    } else {
            
        _imgProfileImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"appicon_green.png"]];
    }
        

    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (IBAction)btnChangeProfilePic:(id)sender {
    
    
}


#pragma mark --
#pragma mark - UITableView Delegate Methods


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [profileOptions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@""];
    
    if (cell == nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
    }
    
    cell.textLabel.text = [profileOptions objectAtIndex:indexPath.row];
    cell.imageView.image = [UIImage imageNamed:[profileOptionsImages objectAtIndex:indexPath.row]];
    cell.backgroundColor =  [UIColor colorWithRed:235.0/255.0 green:236.0/255.0 blue:236.0/255.0 alpha:1.0];
    
    _tblOptions.alwaysBounceVertical = NO;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    self.tblOptions.tableFooterView = [[UIView alloc] init];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        
        UpdataProfileVC * updateProf = [self.storyboard instantiateViewControllerWithIdentifier:@"UpdataProfileVC"];
        [self.navigationController pushViewController:updateProf animated:YES];

    } else if (indexPath.row == 1) {
        
        UpdatePwdVC * updatePass = [self.storyboard instantiateViewControllerWithIdentifier:@"UpdatePwdVC"];
        [self.navigationController pushViewController:updatePass animated:YES];
        
    } else if (indexPath.row == 2) {
        
        RechargeHistoryVC * rcHistory = [self.storyboard instantiateViewControllerWithIdentifier:@"RechargeHistoryVC"];
        [self.navigationController pushViewController:rcHistory animated:YES];
        
    }
}





@end
