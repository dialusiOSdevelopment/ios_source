//
//  FAQuestionsVC.h
//  Saddly
//
//  Created by Sai krishna on 3/9/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "WebServicesMethods.h"
#import "ConnectivityManager.h"
#import "LoaderClass.h"
#import "LoginViewController.h"

@interface FAQuestionsVC : UIViewController <UITableViewDelegate, UITableViewDataSource, NSURLSessionDelegate> {
    
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;

    
    NSMutableArray *arrayforBool;
    NSMutableArray *arrayfaq;
    NSArray * enSectionTitleArray, * enFaqAns, * arSectionTitleArray, * arFaqAns;
    
    UIImageView *headerImage;
    
    NSString * methodname, * a123;
    
}


@property (strong, nonatomic) IBOutlet UITableView *tblFaqs;
@end
