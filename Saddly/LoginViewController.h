//
//  LoginViewController.h
//  Saddly
//
//  Created by Sai krishna on 1/18/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>
#import <CoreLocation/CoreLocation.h>
#import "MCLocalization.h"
#import "Validate.h"
#import "ConnectivityManager.h"
#import "UIView+Toast.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <netdb.h>
#import "HomeVC.h"
#import "RegistrationVC.h"
#import "ForgotPwdVC.h"
#include "LoaderClass.h"
#include "WebServicesMethods.h"
#import "DefaultFont.h"


#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
#import <sys/utsname.h>
#import "GuestOTP.h"
#import "RCGCVC.h"


// for Getting Mac address
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>

#import "KeychainItemWrapper.h"
#import <CommonCrypto/CommonDigest.h>



@interface LoginViewController : UIViewController <CNContactViewControllerDelegate, UIPopoverPresentationControllerDelegate, UITextFieldDelegate, CLLocationManagerDelegate,NSURLSessionDelegate>{
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    NSString * Ipaddress,*name;

    NSUInteger namecount , numbercount,differcount1,differcount2;

    
    NSDictionary * loginDetails, * forgotPasswordDetails, * getOTPDetails;
    NSDictionary * profileDict;

    CLLocationManager *locationManager;
    
    NSString * latitude, * longitude;
    
    NSString* Names, * Numbers;
    
    CATransition *transition;

    NSString * deviceName, * deviceVersion;
    
    UILabel * noDataLabel;
    NSString * Macaddress;
    NSString * UUID, * genKey;
    NSString * a123;
    
}




@property (nonatomic, strong) NSMutableArray * FinalContact, * Finalnumber;
@property (nonatomic, strong) NSMutableArray * finalcontacts;

@property (strong, nonatomic) NSMutableArray * groupOfContacts;
@property (strong, nonatomic) NSMutableArray * contactNameArray;
@property (strong, nonatomic) NSMutableArray * phoneNumberArray;



@property (strong, nonatomic) IBOutlet UIButton *btnChangeOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnMoboutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnEmailOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnLoginOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnShowPwdOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnLoginOTPOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnForgotPwdOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnSignupOutlet;

@property (strong, nonatomic) IBOutlet UIImageView *imgEmail;
@property (strong, nonatomic) IBOutlet UIImageView *imgMobile;

@property (strong, nonatomic) IBOutlet UILabel *lbl966;
@property (strong, nonatomic) IBOutlet UITextField *txtMobNum;
@property (strong, nonatomic) IBOutlet UITextField *txtPwd;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;

@property (strong, nonatomic) IBOutlet UIButton *btnGuestOutlet;


- (IBAction)btnChangeLanguage:(id)sender;
- (IBAction)btnRadioMobile:(id)sender;
- (IBAction)btnRadioEmail:(id)sender;
- (IBAction)btnLogin:(id)sender;

- (IBAction)btnShowPwd:(id)sender;

- (IBAction)btnGuest:(id)sender;

- (IBAction)btnLoginOTP:(id)sender;
- (IBAction)btnForgotPwd:(id)sender;
- (IBAction)btnSignUp:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *lblMonEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblPwd;


// Names
@property (strong, nonatomic) IBOutlet UILabel *lblWelcomeText;
@property (strong, nonatomic) IBOutlet UILabel *lblDontHaveAnAcc;

@end
