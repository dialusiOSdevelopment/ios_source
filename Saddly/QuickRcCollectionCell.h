//
//  QuickRcCollectionCell.h
//  Saddly
//
//  Created by Sai krishna on 7/17/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuickRcCollectionCell : UICollectionViewCell <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> {
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle,* alertMessage, * toastMsg;
    
}


@property (strong, nonatomic) IBOutlet UIImageView *imgOperator;


@end
