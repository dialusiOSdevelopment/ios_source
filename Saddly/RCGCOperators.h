//
//  RCGCOperators.h
//  Saddly
//
//  Created by Sai krishna on 6/17/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "RCGCCollectionViewCell.h"
#import "GuestSelectPackageVC.h"
#import "LoaderClass.h"
#import "UIView+Toast.h"
#import "ConnectivityManager.h"
#import "WebServicesMethods.h"
#import "LoginViewController.h"


@interface RCGCOperators : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, NSURLSessionDelegate> {
    
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    
    NSArray * operatorNames, * operatorImages, * operatorNamesGiftCards , * showingOperators, * operatorNamesAR;
    NSDictionary * operatorDetails;
    NSString * methodname, * a123;
    
}

@property (strong, nonatomic) IBOutlet UIImageView *imgOperatorbanner;


@property (strong, nonatomic) IBOutlet UILabel *lblSelOperatorName;
@property (strong, nonatomic) IBOutlet UICollectionView *collecOperators;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewOperaotrs;

@end
