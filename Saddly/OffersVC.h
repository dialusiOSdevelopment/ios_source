//
//  OffersVC.h
//  Saddly
//
//  Created by Sai krishna on 1/21/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoaderClass.h"
#import "WebServicesMethods.h"
#import "ConnectivityManager.h"
#import "UIView+Toast.h"
#import "MCLocalization.h"
#import "OperatorsVC.h"
#import "ShareViewController.h"


@interface OffersVC : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, NSURLSessionDelegate > {
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    
    NSDictionary * offerDetails, * profileDict;
    
    
    NSArray * offerNames;
    NSUInteger height;

     NSString * methodname, * a123;
    
}

@property (strong, nonatomic) IBOutlet UICollectionView *collecOffers;


@end
