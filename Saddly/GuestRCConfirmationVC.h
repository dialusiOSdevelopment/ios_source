//
//  GuestRCConfirmationVC.h
//  Saddly
//
//  Created by Sai krishna on 4/26/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PayFortSDK/PayFortSDK.h>

#import "MCLocalization.h"
#import "LoaderClass.h"
#import "UIView+Toast.h"
#import "Validate.h"
#import "WebServicesMethods.h"
#import "ConnectivityManager.h"
#import "LoaderClass.h"
#import "GuestRCSuccessVC.h"
#import "RechargeSadadViewController.h"
#import "GuestSadad.h"
#import "LoginViewController.h"


@interface GuestRCConfirmationVC : UIViewController <PayFortDelegate, NSURLSessionDelegate>{
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle,* alertMessage, * toastMsg;
    PayFortController * payFort;
NSString*deviceid,*signature;
    NSDictionary * signatureDetails, * responseDict, * SDKresponsedict, * txnDetailsDictCard;
    
    NSDictionary * Walletdetails,*CardTranDetails,*SADADresponseDict;
    
    NSDictionary * profileDict;
    
    NSInteger test;

    NSString * a123, * methodname;
    
}


@property (strong, nonatomic) NSString * guestMobNum;
@property (strong, nonatomic) NSString * guestRcPlan;
@property (strong, nonatomic) NSString * guestRcValue;
@property (strong, nonatomic) NSString * guestRCOperator;
@property (strong, nonatomic) NSString * guestEmailId;
@property (strong, nonatomic) UIImage * guestOperImage;




@property (strong, nonatomic) IBOutlet UILabel *lblValue;
@property (strong, nonatomic) IBOutlet UILabel *lblPaymentMethod;
@property (strong, nonatomic) IBOutlet UILabel *lblMobNum;
@property (strong, nonatomic) IBOutlet UILabel *lblOperator;
@property (strong, nonatomic) IBOutlet UILabel *lblRcPlan;
@property (strong, nonatomic) IBOutlet UILabel *lblRcAmount;
@property (strong, nonatomic) IBOutlet UITextField *txtSadadId;


@property (strong, nonatomic) IBOutlet UIImageView *imgOperator;
@property (strong, nonatomic) IBOutlet UILabel *lblSadadDots;





// Names

@property (strong, nonatomic) IBOutlet UILabel *lblFromName;
@property (strong, nonatomic) IBOutlet UILabel *lblMobNumName;
@property (strong, nonatomic) IBOutlet UILabel *lblOperatorName;
@property (strong, nonatomic) IBOutlet UILabel *lblRcPlanname;
@property (strong, nonatomic) IBOutlet UILabel *lblRcAmountName;
@property (strong, nonatomic) IBOutlet UILabel *lblEnterSadadIdName;
@property (strong, nonatomic) IBOutlet UIButton *btnProceedtoPayName;


- (IBAction)btnProceedToPay:(id)sender;




@end
