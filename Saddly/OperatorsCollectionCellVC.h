//
//  OperatorsCollectionCellVC.h
//  Saddly
//
//  Created by Sai krishna on 2/1/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OperatorsCollectionCellVC : UICollectionViewCell


@property (strong, nonatomic) IBOutlet UIImageView *imgOperator;

@property (strong, nonatomic) IBOutlet UILabel *lblOperator;


@end
