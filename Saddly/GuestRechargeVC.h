//
//  GuestRechargeVC.h
//  Saddly
//
//  Created by Sai krishna on 4/25/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MCLocalization.h"
#import "LoaderClass.h"
#import "UIView+Toast.h"
#import "Validate.h"

#import "BrowsePlansVC.h"
#import "UserContactsVC.h"
#import "GuestRCConfirmationVC.h"
#import "LoginViewController.h"


@interface GuestRechargeVC : UIViewController <NSURLSessionDelegate>  {
    
    UIAlertController * alert;
    UIAlertAction * okButton;
    NSString * alertTitle,* alertMessage, * toastMsg;
    
    NSString * userEnteredNum;
    
    NSInteger test;
    NSArray * showingOperators;
    NSDictionary * operatorDetails;

    
    NSString*value,*Paymnetmode, *a123, * methodname;
    

    
    
}


@property (strong, nonatomic) NSString * operName;
@property (strong, nonatomic) UIImage * operImage;
@property (strong,nonatomic) NSString  * operType;

@property (strong, nonatomic) NSString * lblOperator;


@property (strong, nonatomic) IBOutlet UIImageView *imgOperator;
@property (strong, nonatomic) IBOutlet UIButton *btnSelOperatorOutlet;


@property (strong, nonatomic) IBOutlet UITextField *txtMobNum;
@property (strong, nonatomic) IBOutlet UITextField *txtEmailId;
@property (strong, nonatomic) IBOutlet UITextField *txtAmount;


@property (strong, nonatomic) IBOutlet UILabel *lblLoadingOfAllSA;
@property (strong, nonatomic) IBOutlet UILabel *lblAfterOfAllSA;
@property (strong, nonatomic) IBOutlet UILabel *lblLoadingPlans;


@property (strong, nonatomic) IBOutlet UIButton *btnLoadingPlansOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnAfterLoadingPlansOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnRechargeOutlet;


- (IBAction)btnLoadingBrowsePlans:(id)sender;
- (IBAction)btnAfterLoadBrowsePlans:(id)sender;


@property (strong, nonatomic) IBOutlet UIButton *btnCreditCardOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnDebitCardOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnSadadOutlet;






- (IBAction)btnCreditCard:(id)sender;
- (IBAction)btnDebitCard:(id)sender;
- (IBAction)btnSadad:(id)sender;


//- (IBAction)btnOperator:(id)sender;




- (IBAction)btnContacts:(id)sender;
- (IBAction)btnRecharge:(id)sender;


@end
