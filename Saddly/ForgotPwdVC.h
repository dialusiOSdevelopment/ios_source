//
//  ForgotPwdVC.h
//  Saddly
//
//  Created by Sai krishna on 1/20/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "WebServicesMethods.h"
#import "LoaderClass.h"
#import "ConnectivityManager.h"
#import "UIView+Toast.h"
#import "Validate.h"
#import "LoginViewController.h"
#import "ForgotPwdMobNumVC.h"



@interface ForgotPwdVC : UIViewController <NSURLSessionDelegate> {

    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    NSString*name;
    
    NSDictionary  * forgotPasswordDetails;
    
    NSString * methodname, * a123;

}
@property (strong, nonatomic) IBOutlet UILabel *lblGreetings;
@property (strong, nonatomic) IBOutlet UILabel *lblMobNumName;
@property (strong, nonatomic) IBOutlet UITextField *txtMobNum;
@property (strong, nonatomic) IBOutlet UITextView *txtViewMsg;
@property (strong, nonatomic) IBOutlet UIButton *btnSendOutlet;




- (IBAction)btnSend:(id)sender;



@end
