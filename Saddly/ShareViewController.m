//
//  ShareViewController.m
//  Saddly
//
//  Created by gandhi on 16/02/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "ShareViewController.h"

@interface ShareViewController ()

@end

@implementation ShareViewController

- (void)viewDidLoad {
    
    self.title=[MCLocalization stringForKey:@"shareTitle"];
    
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
  
    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    

    
    [self Refferalservices];
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        _imgShare.image = [UIImage imageNamed:@"ShareTheApp_ar-black.png"];
        [_btnShareOutlet setImage:[UIImage imageNamed:@"sharebutton_ar.png"] forState:UIControlStateNormal];
        
    } else {
        
        _imgShare.image = [UIImage imageNamed:@"ShareTheApp_en-black.png"];
        [_btnShareOutlet setImage:[UIImage imageNamed:@"sharebutton_en.png"] forState:UIControlStateNormal];
    }
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated{
    
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    [self Refferalservices];
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){

        _imgShare.image = [UIImage imageNamed:@"ShareTheApp_ar-black.png"];
        [_btnShareOutlet setImage:[UIImage imageNamed:@"sharebutton_ar.png"] forState:UIControlStateNormal];
        
    } else {
        
        _imgShare.image = [UIImage imageNamed:@"ShareTheApp_en-black.png"];
        [_btnShareOutlet setImage:[UIImage imageNamed:@"sharebutton_en.png"] forState:UIControlStateNormal];
    }
    
}


-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
            
            NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
            [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
            
            
            if ([methodname isEqualToString:@"Refferalservices"]) {
                [self Refferalservices];
                
            }
            
            
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//





-(void)Refferalservices{
    
   
    NSString * db;
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        db = @"AR";
        
    } else {
        db = @"SA";
    }
    
   
    NSString * userMobNum = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"user_mob"]];
    ///mobileno
    
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId =[profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    NSString * UEmail= [profileDict valueForKey:@"user_email_id"];

    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];

    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    
    
    [LoaderClass showLoader:self.view];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/login/getReferalCodeDetails"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
        
    NSDictionary * sendMsgsDict =  @{@"mobileno":userMobNum,
                                     @"user_emailId":UEmail,
                                     @"ipAddress":Ipaddress,
                                     @"from":@"iPhone",
                                     @"db":db,
                                     @"accessToken":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                     @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                     };
    
    NSLog(@"Posting sendMsgsDict is %@",sendMsgsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:sendMsgsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSLog(@"No data returned from server, error ocurred: %@", error);
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            NSLog(@"resSrt: %@", resSrt);
            NSError *deserr;
            
            
            [LoaderClass removeLoader:self.view];
            
            NSDictionary*Response=[NSJSONSerialization
                                   JSONObjectWithData:data
                                   options:kNilOptions
                                   error:&deserr];
            
            
            NSLog(@"got response=%@", Response);
            
            if ([[Response valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }  else  {
                
                
                if ([[Response valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"Refferalservices";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                } else {
                    
                    
                    refDict = Response;
                    NSLog(@"refDict is: %@",refDict);
                    
                    NSLog(@"refferal code is: %@",[refDict valueForKey:@"ReferalCode"]);
                    NSLog(@"refferal Message is: %@",[refDict valueForKey:@"message"]);
                    
                    
                    _txtReferralCode.text = [NSString stringWithFormat:@"%@", [refDict valueForKey:@"ReferalCode"]];
                }
                
              
            }
            
        });
        
    }];
    
    [dataTask resume];
    
}






- (IBAction)btnShare:(id)sender{

    NSString * tShare = [NSString stringWithFormat:@"%@",[refDict valueForKey:@"message"]];
    
    
    NSString * textToShare = [[[tShare stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];

    
    NSURL * myWebsite = [NSURL URLWithString:@"https://goo.gl/ygB0UC"];
    
    NSArray * objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController * activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    activityVC.popoverPresentationController.sourceView = self.view;
    activityVC.popoverPresentationController.sourceRect = CGRectMake( CGRectGetMidX(self.btnShareOutlet.frame), CGRectGetMidY(self.btnShareOutlet.frame), 0, 0);

    
    NSArray * excludeActivities = @[UIActivityTypeAirDrop,
                                    UIActivityTypePrint,
                                    UIActivityTypeAssignToContact,
                                    UIActivityTypeSaveToCameraRoll,
                                    UIActivityTypeAddToReadingList,
                                    UIActivityTypePostToFlickr,
                                    UIActivityTypePostToVimeo];
    
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];

    
}







@end
