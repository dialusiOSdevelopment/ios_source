//
//  InsufficientWalBalVC.h
//  DialuzApp
//
//  Created by Sai krishna on 12/30/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebServicesMethods.h"
#import "MCLocalization.h"
#import "UIView+Toast.h"
#import "LoaderClass.h"
#import "ConnectivityManager.h"
#import <PayFortSDK/PayFortSDK.h>
#import "SendMoneySuccessVC.h"
#import "SendMoneySadadViewController.h"
#import "LoginViewController.h"


@interface InsufficientWalBalVC : UIViewController<PayFortDelegate, NSURLSessionDelegate>{
    
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg,*a123,*methodname;
    
    NSString * deviceid, * signature;
    PayFortController * payFort;
    NSDictionary * profileDict;
    
    NSDictionary * paymentGatewayDetails, * signatureDetails, * responseDict, * SDKresponsedict, * txnDetailsDictCard, * returnUrlDetails,*SADADresponseDict;
    
    NSInteger test;
    
    NSDictionary*valid;
    NSString * pass123;
    
}

@property (strong, nonatomic) IBOutlet UILabel *lblTotAmoSent;
@property (strong, nonatomic) IBOutlet UILabel *lblActualCash;
@property (strong, nonatomic) IBOutlet UILabel *lblPayBalAmount;
@property (strong, nonatomic) IBOutlet UIButton *btnPayNowOutlet;
@property (strong, nonatomic) IBOutlet UITextField *txtPayment;

@property (strong, nonatomic) IBOutlet UITextField *txtEnterSadadId;

- (IBAction)DEBITATMCARD:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *DEBITATMNAME;


// Names
@property (strong, nonatomic) IBOutlet UILabel *lblActCashName;
@property (strong, nonatomic) IBOutlet UILabel *lblPayBalAmoName;
@property (strong, nonatomic) IBOutlet UILabel *lblSelPaymGateway;
@property (strong, nonatomic) IBOutlet UIButton *btnCreditDebOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnSadadOutlet;




- (IBAction)btnCheckBox:(id)sender;

- (IBAction)btnPayNow:(id)sender;

@end
