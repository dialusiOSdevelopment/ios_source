//
//  ConnectivityManager.h
//  Spoil
//
//  Created by Cosmic Kayka on 25/04/15.
//  Copyright (c) 2015 Spoil. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface ConnectivityManager : NSObject
{
    Reachability *internetReach;
}

@property (nonatomic, assign) BOOL isWifi;

+ (id)sharedManager;
-(BOOL) CheckConnectivity;

@end
