//
//  WalletVC.m
//  Saddly
//
//  Created by Sai krishna on 1/20/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "WalletVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface WalletVC ()

@end

@implementation WalletVC

- (void)viewDidLoad {
    
 //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Wallet Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //
    
    _walletBalName.adjustsFontSizeToFitWidth = YES;
    sadadButton.hidden = YES;



    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
        
    }

    
    self.title = [MCLocalization stringForKey:@"SlidearrWllt"];
    
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];

    
    
    
    
    NSString * actualCash = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"actualCash"]];
    
    NSString * bonusCash  = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"bonusCash"]];
    
    
    NSString * actualCashStr, * bonusCashStr;
    
    // Actual & bonus Cash
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ ﷼"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ ﷼"], bonusCash];
    } else {
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ SAR"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ SAR"], bonusCash];
    }
    
    _lblActualCash.text = actualCashStr;
    _lblBonusCash.text = bonusCashStr;
    
    _lblActualCash.adjustsFontSizeToFitWidth = YES;
    _lblBonusCash.adjustsFontSizeToFitWidth = YES;
    
    // For Key board is not appearing for this TextField 
    UIView* dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    _txtPayment.inputView = dummyView;
    
    
//    // Offer with % and With string Additional cash
//    NSUserDefaults * uOfferDefaults = [NSUserDefaults standardUserDefaults];
//    NSString * offer = [uOfferDefaults stringForKey:@"uOffer"];
//    
//    if ([offer isEqualToString:@"0"]) {
//        _noOffer.hidden = NO;
//        _lblOffer.hidden = YES;
//        _lblOne.hidden = YES;
//        _lblTwo.hidden = YES;
//        _lblOfferTwo.hidden = YES;
//        
//    } else{
//        _noOffer.hidden = YES;
//        _lblOne.hidden = NO;
//        _lblTwo.hidden = NO;
//        
//        NSString * addCashStr = [[offer stringByAppendingString:@"%"] stringByAppendingString:[MCLocalization stringForKey:@"additionalcash"]];
//        _lblOffer.text = addCashStr;
//        _lblOfferTwo.text = [offer stringByAppendingString:@"%"];
//        
//    }
    
    // Returning Keyboard For Number Pad.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(NextClicked)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    _enterAmount.inputAccessoryView = keyboardDoneButtonView;
    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        _lblCurrentBal.text = [[NSString stringWithFormat:@"%@",[profileDict valueForKey:@"totalCash"]] stringByAppendingString:@" ﷼"];
        
    } else {
        
        _lblCurrentBal.text = [[NSString stringWithFormat:@"%@",[profileDict valueForKey:@"totalCash"]] stringByAppendingString:@" SAR"];
    }



    
    _lblCouponCodeSuccess.hidden = YES;
    _btnCouponCodeOutlet.hidden = NO;

    
    
    //Names in English/Arabic
    _walletBalName.text = [MCLocalization stringForKey:@"walletBal"];
    _amountName.text = [MCLocalization stringForKey:@"amount"];
    _payWIthName.text = [MCLocalization stringForKey:@"paywith"];
    _offersForYouName.text = [MCLocalization stringForKey:@"offersForYou"];
    _noOffer.text = [MCLocalization stringForKey:@"nooffers"];
    _lblOne.text = [MCLocalization stringForKey:@"AddMoneyWallet"];
    _lblTwo.text = [MCLocalization stringForKey:@"tcApplyName"];
    _enterAmountName.placeholder=[MCLocalization stringForKey:@"EnterAmount"];
    _txtPayment.placeholder=[MCLocalization stringForKey:@"selpaymentMethod"];
    [_btnCouponCodeOutlet setTitle:[MCLocalization stringForKey:@"haveACouponCode"] forState:UIControlStateNormal];

    
    _btnCouponCodeOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;
    [_btnCouponCodeOutlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];


    
    [_addMoneyName setTitle:[MCLocalization stringForKey:@"addmoney"] forState:UIControlStateNormal];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(void)NextClicked{
    [self.view endEditing:YES];
}

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];


        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}


-(void)viewWillAppear:(BOOL)animated{

    // Showing Balance
    NSString * actualCash = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"actualCash"]];
    NSString * bonusCash  = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"bonusCash"]];
    
    
    NSString * actualCashStr, * bonusCashStr;
    
    // Actual & bonus Cash
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ ﷼"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ ﷼"], bonusCash];
    } else {
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ SAR"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ SAR"], bonusCash];
    }
    
    _lblActualCash.text = actualCashStr;
    _lblBonusCash.text = bonusCashStr;
    
    _lblActualCash.adjustsFontSizeToFitWidth = YES;
    _lblBonusCash.adjustsFontSizeToFitWidth = YES;
    

    
    _lblCouponCodeSuccess.hidden = YES;
    _btnCouponCodeOutlet.hidden = NO;

    _walletBalName.adjustsFontSizeToFitWidth = YES;
    _btnCouponCodeOutlet.titleLabel.adjustsFontSizeToFitWidth = YES;


    if ([_txtPayment.text isEqualToString:[MCLocalization stringForKey:@"sadad"]]) {
        sadadButton.hidden = NO;

    } else {
        sadadButton.hidden = YES;

    }

}

-(void)removingKeyboardByTap {
    
    [self.view endEditing:YES];
}




-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSLog(@"Killer Response is%@",Killer);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
            
            NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
            [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
            
            
            if ([methodname isEqualToString:@"paymentGatewaywebServicesMethod"]) {
                
                [self paymentGatewaywebServicesMethod];
                
            }

            
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//





// Returning KeyBoard
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [_enterAmount resignFirstResponder];
    return YES;
}


// Entering only numbers not a special Characters
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if([self isNumeric:string]){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return TRUE && newLength <= 5;
    } else  {
        return FALSE;
    }
}


// Entering only numbers not a special Characters
-(BOOL)isNumeric:(NSString*)inputString{
    
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
} // Entering only numbers not a special Characters


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}

// webservices for Paymeny Gateway
-(void)paymentGatewaywebServicesMethod{
    
    NSString * db = [MCLocalization stringForKey:@"DBvalue"];
    
    
    NSString*username=[[[[profileDict valueForKey:@"user_name"] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    
    NSString*mobile = [profileDict valueForKey:@"user_mob"];
    
    
    NSString * amount = _enterAmount.text;
    
    
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString * ipAddress =  [ipaddressdefault stringForKey:@"ipaddressdefault"];
    NSUserDefaults * Couponcode = [NSUserDefaults standardUserDefaults];
    
    
    NSString*cpn;
    
    if ([cpn length] <1) {
        
        cpn=@"Nil";
    } else{
        cpn=  [Couponcode valueForKey:@"Couponecodenum"];
        
    }
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/addMoney/addMoneyToWalletDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
 
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    //     NSString * userPwd=@"585858";
    
    NSLog(@"%@",userId);
    NSLog(@"%@",userPwd);
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    

    
    
    
    NSDictionary * OpDict =  @{
                               @"db":db,
                               @"username":username,
                               @"userMob":mobile,
                               @"amount":amount,
                               @"paymentMethod":_txtPayment.text,
                               @"ipAddress":ipAddress,
                               @"login_status":@"iphone_SA",
                               @"order_description":@"AddMoney",
                               @"couponCode":cpn,
                               @"from":@"iphone",
                               @"user_emailId":[profileDict valueForKey:@"user_email_id"],
                               @"access_token":[accessTokenDefaults valueForKey:@"accessTokenDefaults"],
                               @"unique_id":uniqueid,
                               @"sadad_username":@"",
                               @"order_id":@"",
                               };
    
    
    NSLog(@"Posting loginDetailsDict is %@",OpDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:OpDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    addmoneywalletdict = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
            NSString*  resSrt  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Killer Response is%@",addmoneywalletdict);
            
            NSLog(@"str Response is%@",resSrt);
            
            if ([[[addmoneywalletdict valueForKey:@"addMoneytowalletStatus" ] valueForKey:@"response_message" ] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"paymentGatewaywebServicesMethod";
                
                [self checkTokenStatusWebServices];
                
                
            } else if ([[[addmoneywalletdict valueForKey:@"addMoneytowalletStatus" ]  valueForKey:@"response_message" ] isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                

                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{
            
            NSDictionary*Killer=[addmoneywalletdict valueForKey:@"addMoneytowalletStatus"];
            
            [LoaderClass removeLoader:self.view];
            
            
            // Saving profile Details Dictionary to NSUserDefaults.
            NSUserDefaults *paymentGatewayDetailsDefaults = [NSUserDefaults standardUserDefaults];
            NSData * CardTrData = [NSKeyedArchiver archivedDataWithRootObject:Killer];
            [paymentGatewayDetailsDefaults setObject:CardTrData forKey:@"paymentGatewayDetailsDefaults"];
            
            
            
            NSUserDefaults * walletAmountDefaults = [NSUserDefaults standardUserDefaults];
            [walletAmountDefaults setObject:_enterAmount.text forKey:@"amount"];
            
            PaymentDetailsVC * paymentDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentDetailsVC"];
            [self.navigationController pushViewController:paymentDetails animated:YES];
            }
        });
        
        
        
    }];
    
    
    [dataTask resume];
    
    
    
    
    
}
    
    
    
//    
//#define appService [NSURL \
//URLWithString:@""]
//

   
// For Coupon Code
-(void)couponcodeservices{
   
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    
    NSDictionary*   profileDict1 = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    NSString*UEmail= [profileDict1 valueForKey:@"user_email_id"];
    
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    
    
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSString*from=@"iphone";
    

    
    NSString*mob=[@"966" stringByAppendingString:[profileDict valueForKey:@"user_mob"]];
    
    

    ///
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/validateCouponDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    
    
    NSString * userId = [profileDict1 valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    //     NSString * userPwd=@"585858";
    
    NSLog(@"%@",userId);
    NSLog(@"%@",userPwd);
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    

    
    
    NSDictionary * OpDict =  @{
                               @"operator":@"",
                               @"rechargeamt":_enterAmount.text,
                               @"couponCode":cpncode,
                               @"user_mob":mob,
                               @"user_emailId":UEmail,
                               @"ipAddress":Ipaddress,
                               @"from":from,
                               @"access_token":accessToken,
                               @"unique_id":uniqueid
                               
                               };
    
    
    NSLog(@"Posting loginDetailsDict is %@",OpDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:OpDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
            NSString*  str1  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Killer Response is%@",Killer);
            
            NSLog(@"str Response is%@",str1);
            
            
            
            [LoaderClass removeLoader:self.view];
    
//            NSError *deserr;
        
            
            if ([[Killer valueForKey:@"response_message" ] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"paymentGatewaywebServicesMethod";
                
                [self checkTokenStatusWebServices];
                
                
            } else if ([[Killer  valueForKey:@"response_message" ] isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{
                

            
        NSUserDefaults * trasactionid = [NSUserDefaults standardUserDefaults];
        
        [trasactionid setObject:str1 forKey:@"trasactionid"];
        
        NSLog(@"Response is%@",str1);
        
        [LoaderClass removeLoader:self.view];
        
        
        NSMutableDictionary*respdict=[[NSMutableDictionary alloc]init];
        
        [respdict setObject:str1 forKey:@"Message"];
        
        NSUserDefaults * couponcodenum = [NSUserDefaults standardUserDefaults];
        [couponcodenum setObject:cpncode forKey:@"couponcodenum"];
        
        Cpncoderesponse=respdict;
        
        if ([[Cpncoderesponse valueForKey:@"Message"] isEqualToString:@"Coupon is Valid"]) {
            
            NSUserDefaults * Couponcode = [NSUserDefaults standardUserDefaults];
            
            _lblCouponCodeSuccess.hidden = NO;
            _lblCouponCodeSuccess.text = [[MCLocalization stringForKey:@"couponApplied"] stringByAppendingString:[Couponcode stringForKey:@"Couponecodenum"]];
            _btnCouponCodeOutlet.hidden = YES;
            
        } else if ([[Cpncoderesponse valueForKey:@"Message"] isEqualToString:@"Coupon Valid Once Per Month"]) {
            
            _lblCouponCodeSuccess.hidden = YES;
            _btnCouponCodeOutlet.hidden = NO;
            alertMessage = [MCLocalization stringForKey:@"couponvalidonce/month"];
            [self alertMethod];
            
        } else {
            
            _lblCouponCodeSuccess.hidden = YES;
            _btnCouponCodeOutlet.hidden = NO;
            toastMsg = [Cpncoderesponse valueForKey:@"Message"];
            [self toastMessagemethod];
            
        }
        
            }
        
        });
        
        
        
    }];
    
    
    [dataTask resume];
    
    
    
    
    
}


// Selecting the payment gateqway method on UIAlertAction
- (IBAction)selectpaymentMethod:(id)sender {
    
    alert = [UIAlertController alertControllerWithTitle:nil message:[MCLocalization stringForKey:@"selPaymentGateway"] preferredStyle:UIAlertControllerStyleActionSheet];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = CGRectMake( CGRectGetMidX(self.txtPayment.frame), CGRectGetMidY(self.txtPayment.frame), 0, 0);
    

    
    UIAlertAction * firstAction = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"CREDIT"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        _txtPayment.text = [MCLocalization stringForKey:@"CREDIT"];
        sadadButton.hidden = YES;

    }];
    UIAlertAction * thirdaction = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"sadad"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
    
        
        UIImage* needHelp = [UIImage imageNamed:@"sadad-logo.png"];
        CGRect frameimg = CGRectMake(0, 0, 50, 30);
        sadadButton = [[UIButton alloc] initWithFrame:frameimg];
        [sadadButton setBackgroundImage:needHelp forState:UIControlStateNormal];
        [sadadButton setShowsTouchWhenHighlighted:NO];
        sadadButton.hidden = NO;
        
        UIBarButtonItem * sadadbarButton = [[UIBarButtonItem alloc] initWithCustomView:sadadButton];
        self.navigationItem.rightBarButtonItem = sadadbarButton;

        
      _txtPayment.text = [MCLocalization stringForKey:@"sadad"];
//        
//        alertTitle = [MCLocalization stringForKey:@"sadadNotSupporting"];
//        alertMessage = [MCLocalization stringForKey:@"chooseCredit/Debit"];
//        [self alertMethod];
        
    }];
    UIAlertAction * secondAction = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"DEBIT/ATMCARD"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        _txtPayment.text = [MCLocalization stringForKey:@"DEBIT/ATMCARD"];
        sadadButton.hidden = YES;

    }];
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"CANCEL"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [self presentedViewController];
        sadadButton.hidden = YES;

    }];
    
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    [alert addAction:thirdaction];

    [alert addAction:cancelAction];
    alert.view.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    [self presentViewController:alert animated:YES completion:nil];
    
}



- (IBAction)btnCouponCode:(id)sender {
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:[MCLocalization stringForKey:@"couponCode"] message:[MCLocalization stringForKey:@"enterCouponToAvailOffers"] preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField){
        
        textField.placeholder = [MCLocalization stringForKey:@"enterCouponCode"];
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
        
    }];
    
    UIAlertAction * cancelButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"Forgetpasswordcancell"] style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        
        [self presentedViewController];
        
    }];
    
    UIAlertAction * Submitt  = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ForgetpasswordSubmitt"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        NSArray * textfields = alertController.textFields;
        txtCouponCode = textfields[0];
        
        cpncode=txtCouponCode.text;
        
        NSUserDefaults * Couponcode = [NSUserDefaults standardUserDefaults];
        [Couponcode setObject:cpncode forKey:@"Couponecodenum"];
        
        [self couponcodeservices];
        
    }];
    
    [alertController addAction:cancelButton];
    [alertController addAction:Submitt];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


- (IBAction)btnBonusCashInfo:(id)sender {
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        alertMessage = @"فقط يمكنك الحصول عليها عند الشحن بميلغ  100 ريال او اكثر";
        
    } else {
        
        alertMessage = @"Can be Redeemed only with 100SAR and above Recharges";
        
    }
    
    [self alertMethod];
    
}



- (IBAction)btnInfo:(id)sender {

    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    NSString * actualCash = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"actualCash"]];
    
    NSString * bonusCash  = [NSString stringWithFormat:@"%@",[profileDict valueForKey:@"bonusCash"]];
    
    
    NSString * actualCashStr, * bonusCashStr;
    
    // Actual & bonus Cash Displaying on Alert
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ ﷼"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ ﷼"], bonusCash];
    } else {
        actualCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"actualCash"] stringByAppendingString:@"%@ SAR"],actualCash];
        bonusCashStr = [NSString stringWithFormat:[[MCLocalization stringForKey:@"bonusCash"] stringByAppendingString:@"%@ SAR"], bonusCash];
    }
    
    
    alert = [UIAlertController alertControllerWithTitle:@"" message:[[actualCashStr stringByAppendingString:@"\n"] stringByAppendingString:bonusCashStr] preferredStyle:UIAlertControllerStyleAlert];
    
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"] style:UIAlertActionStyleDefault  handler:^(UIAlertAction * action) {
        
        [self presentedViewController];
    }];
    
    
    [alert addAction:okButton];
    alert.view.tintColor = [UIColor colorWithRed:0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    [self presentViewController:alert animated:YES completion:nil];
    
}


// Adding +50, +100, +200 to textfield
- (IBAction)addAmount:(id)sender {
    
    
    if ([_enterAmount.text intValue] < 99800) {
        
        if ([sender tag] == 0) {
            int result = [_enterAmount.text intValue] + 50;
            _enterAmount.text = [NSString stringWithFormat:@"%d", result];
        }
        else if ([sender tag] == 1) {
            int result = [_enterAmount.text intValue] + 100;
            _enterAmount.text = [NSString stringWithFormat:@"%d", result];
        }
        else if ([sender tag] == 2) {
            int result = [_enterAmount.text intValue] + 200;
            _enterAmount.text = [NSString stringWithFormat:@"%d", result];
        }

    } else  if ([_enterAmount.text intValue] < 99900) {
        
        if ([sender tag] == 0) {
            int result = [_enterAmount.text intValue] + 50;
            _enterAmount.text = [NSString stringWithFormat:@"%d", result];
        }
        else if ([sender tag] == 1) {
            int result = [_enterAmount.text intValue] + 100;
            _enterAmount.text = [NSString stringWithFormat:@"%d", result];
        }
        
    } else  if ([_enterAmount.text intValue] < 99950) {
        
        if ([sender tag] == 0) {
            int result = [_enterAmount.text intValue] + 50;
            _enterAmount.text = [NSString stringWithFormat:@"%d", result];
        }
        
    }
    
}


// Adding Money to Wallet
- (IBAction)btnAddMoney:(id)sender {
    
    NSUserDefaults * AmntDefaults = [NSUserDefaults standardUserDefaults];
    
    
    [AmntDefaults setObject:_enterAmount.text forKey:@"AmntDefaults"];
    
    
    NSUserDefaults * Paymentmethod = [NSUserDefaults standardUserDefaults];
    [Paymentmethod setObject:_txtPayment.text forKey:@"Paymentmethod"];
    

    
    if ([_enterAmount.text isEqualToString:@""]) {
        toastMsg = [MCLocalization stringForKey:@"plsenteramount"];
        [self toastMessagemethod];
    } else if ([_enterAmount.text intValue] < 10){
        toastMsg = [MCLocalization stringForKey:@"amount10"];
        [self toastMessagemethod];
    } else if ([_txtPayment.text isEqualToString: @""]){
        toastMsg = [MCLocalization stringForKey:@"plsselPaygateway"];
        [self toastMessagemethod];
    } else{
        
        if ([_txtPayment.text isEqualToString:[MCLocalization stringForKey:@"CREDIT"]]||[_txtPayment.text isEqualToString:[MCLocalization stringForKey:@"DEBIT/ATMCARD"]]) {
            
            [self paymentGatewaywebServicesMethod];
            
        } else {
            
            NSUserDefaults * walletAmountDefaults = [NSUserDefaults standardUserDefaults];
            [walletAmountDefaults setObject:_enterAmount.text forKey:@"amount"];

            
            PaymentDetailsVC * paymentDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentDetailsVC"];
            [self.navigationController pushViewController:paymentDetails animated:YES];
        }
    }
}



@end
