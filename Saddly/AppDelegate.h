//
//  AppDelegate.h
//  Saddly
//
//  Created by Sai krishna on 1/18/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import <UserNotifications/UserNotifications.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "GAI.h"


//#import <Cordova/CDVViewController.h>



//#import "DefaultFont.h"

//@implementation UILabel (Helper)
//- (void)setSubstituteFontName:(NSString *)name UI_APPEARANCE_SELECTOR {
//    self.font = [UIFont fontWithName:name size:self.font.pointSize]; }
//@end


@import FirebaseCore;
@import FirebaseInstanceID;
@import FirebaseMessaging;

@interface AppDelegate:UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate,FIRMessagingDelegate>




@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) id<GAITracker> tracker;

@property (strong, nonatomic)  UINavigationController  *navigationController;
+(AppDelegate*)sharedAppdelegate;


@property (strong, nonatomic) NSString *strDeviceToken;


@end

