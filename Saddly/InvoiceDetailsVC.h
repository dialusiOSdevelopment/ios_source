//
//  InvoiceDetailsVC.h
//  Saddly
//
//  Created by Sai krishna on 5/12/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransactionDetailsVC.h"
#import "LoaderClass.h"

@interface InvoiceDetailsVC : UIViewController <UIDocumentInteractionControllerDelegate> {
    
    
    NSString * documentDirectoryFilename;
    NSDictionary * profileDict;
    
    UILabel * noDataLabel;
}



@property (strong, nonatomic) UIDocumentInteractionController * documentInteractionController;


@property (strong, nonatomic) NSString * inOrderId;
@property (strong, nonatomic) NSString * inDate;
@property (strong, nonatomic) NSString * inRcMobNum;
@property (strong, nonatomic) NSString * inOperator;
@property (strong, nonatomic) NSString * inRcAmount;
@property (strong, nonatomic) NSString * inValueFromWallet;
@property (strong, nonatomic) NSString * inValueFromCard;
@property (strong, nonatomic) NSString * inTotal;





@property (strong, nonatomic) IBOutlet UIButton *btnSaddlyImage;
@property (strong, nonatomic) IBOutlet UILabel *lblOrderNum;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblUserMobNum;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;

@property (strong, nonatomic) IBOutlet UILabel *lblRcMobNum;
@property (strong, nonatomic) IBOutlet UILabel *lblRcAmount;

@property (strong, nonatomic) IBOutlet UILabel *lblValueFrmWallet;

@property (strong, nonatomic) IBOutlet UIButton *btnTrustName;




// Names
@property (strong, nonatomic) IBOutlet UILabel *lblTxnReceiptName;
@property (strong, nonatomic) IBOutlet UILabel *lblValueFrmWalletName;
@property (strong, nonatomic) IBOutlet UILabel *lblValueFrmCardName;
@property (strong, nonatomic) IBOutlet UILabel *lblValueFromCard;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalName;
@property (strong, nonatomic) IBOutlet UILabel *lblTotal;





@end

