//
//  SadadViewController.h
//  DialuzApp
//
//  Created by gandhi on 29/12/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "WebServicesMethods.h"
#import "MCLocalization.h"
#import "HomeVC.h"
#import "WalletVC.h"
#import "LoaderClass.h"


#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"



@interface SadadViewController : UIViewController<UIWebViewDelegate,NSURLConnectionDelegate>{

    BOOL _authenticated;
    NSURLConnection*con;
    NSURLRequest*req;
    NSURL *url;
    NSDictionary* responseDict;
    NSURLRequest *mainrequest;
    
    NSURLConnection *_urlConnection;
    NSURLRequest *_request;
}

@property (strong, nonatomic) IBOutlet UIButton *btnDoneOutlet;


@property (strong, nonatomic) IBOutlet UIWebView *WebView;


@end
