//
//  FreeSMSVC.m
//  DialuzApp
//
//  Created by gandhi on 28/06/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import "FreeSMSVC.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface FreeSMSVC ()

@end

@implementation FreeSMSVC



- (void)viewDidLoad {
    
    //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Free SMS Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //

    
    
    _charactersLeftMsg.adjustsFontSizeToFitWidth=YES;
    _dailySmsCount.adjustsFontSizeToFitWidth=YES;
    _btnMsgToFrndsName.titleLabel.adjustsFontSizeToFitWidth=YES;
    _msgsTodayName.adjustsFontSizeToFitWidth=YES;
    _mobNumName.adjustsFontSizeToFitWidth=YES;
    _msgName.adjustsFontSizeToFitWidth=YES;
    _charLeftName.adjustsFontSizeToFitWidth=YES;
    
    
//    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
//    
//    //  [myView setDefaultFontFamily:@"JFFlat-Regular" andSubViews:YES andColor:nil];
//    
//    [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
//    
//    
//    } else {
//    // [myView setDefaultFontFamily:@"MyriadPro-Regular" andSubViews:YES andColor:nil];
//    
//        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
//    
//    
//    }
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
   
    self.title = [MCLocalization stringForKey:@"SlidearrSMS"];
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [self smsCountWebServicesMethod];
   [self smsHistWebServicesMethod];
    
    
    //Names
    _btnMsgToFrndsName.titleLabel.text = [MCLocalization stringForKey:@"msgToFrnds"];
    _msgsTodayName.text = [MCLocalization stringForKey:@"msgToday"];
    _mobNumName.text = [MCLocalization stringForKey:@"mobNum"];
    _msgName.text = [MCLocalization stringForKey:@"msgName"];
    _charLeftName.text = [MCLocalization stringForKey:@"charLeft"];
    _txtMobNum.placeholder=[MCLocalization stringForKey:@"enterrecipient"];
    
    [_sendName setTitle:[MCLocalization stringForKey:@"sendName"] forState:UIControlStateNormal];
  
    
    NSUserDefaults * mobNumContactsDefaults = [NSUserDefaults standardUserDefaults];
    [mobNumContactsDefaults removeObjectForKey:@"mobNumContactsDefaults"];
    
    // Returning Keyboard For Number Pad.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(NextClicked)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    _txtMobNum.inputAccessoryView = keyboardDoneButtonView;
    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
//    if ([view isKindOfClass:[UIButton class]])
//    {
//        UIButton *lbl = (UIButton *)view;
//        
//         [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
//        
//        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
//
//    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}


-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}

-(void)NextClicked{
    [_txtMobNum resignFirstResponder];
    [_txtViewMsg becomeFirstResponder];
}



-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];

    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
            
            NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
            [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
            
            
            if ([methodname isEqualToString:@"smsWebServicesMethod"]) {
                [self smsWebServicesMethod];
            }
          else  if ([methodname isEqualToString:@"smsHistWebServicesMethod"]) {
                [self smsHistWebServicesMethod];
            }
            else if ([methodname isEqualToString:@"smsCountWebServicesMethod"]) {
                [self smsCountWebServicesMethod];
            }

            
            
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//



// Displaying Mobile Number in the text field.
-(void)viewDidAppear:(BOOL)animated{
    
    _charactersLeftMsg.adjustsFontSizeToFitWidth=YES;
    _dailySmsCount.adjustsFontSizeToFitWidth=YES;
    _btnMsgToFrndsName.titleLabel.adjustsFontSizeToFitWidth=YES;
    _msgsTodayName.adjustsFontSizeToFitWidth=YES;
    _mobNumName.adjustsFontSizeToFitWidth=YES;
    _msgName.adjustsFontSizeToFitWidth=YES;
    _charLeftName.adjustsFontSizeToFitWidth=YES;
    
    NSUserDefaults * mobNumContactsDefaults = [NSUserDefaults standardUserDefaults];
    _txtMobNum.text = [[mobNumContactsDefaults stringForKey:@"mobNumContactsDefaults"] stringByReplacingOccurrencesOfString:@"+966" withString:@""];
}

//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:_txtMobNum]) {
        
        if([self isNumeric:string]){
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 9;
        } else  {
            return FALSE;
        }
    }
    return YES;
}

// Entering only numbers not a special Characters
-(BOOL)isNumeric:(NSString*)inputString{
    
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}


// Returning KeyBoard
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == self.txtMobNum) {
        
        [self.txtViewMsg becomeFirstResponder];
    } else if ([textField isEqual:self.txtViewMsg]) {
        [self.txtViewMsg resignFirstResponder];
    }
    return YES;
}




// Moving the view Up & Down for the textfield
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //move the main view, so that the keyboard does not hide it.
    if  (self.view.frame.origin.y < 0)
    {
        [self animateTextField: textField up: NO];
    }
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = textField.frame.origin.y / 2; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
} // Moving the view Up & Down for the textfield


// Moving the view Up & Down for the textview
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateTextView: YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextView:NO];
}

- (void) animateTextView:(BOOL) up
{
    const int movementDistance = 80.0f; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    int movement= movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
} // Moving the view Up & Down for the textview



// Free SMS Webservices Method
-(void)smsWebServicesMethod{


    NSString * db = [MCLocalization stringForKey:@"DBvalue"];
    NSString * userId =[profileDict valueForKey:@"user_mob"];
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    NSString * mobNum = [@"966" stringByAppendingString:_txtMobNum.text];
    NSString * messages = _txtViewMsg.text;
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    

    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/profile/sendSmsToFriends"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
       
    
    NSDictionary * sendMsgsDict =  @{@"usermob":userId,
                                         @"sentMob":mobNum,
                                         @"message":messages,
                                         @"from":@"iPhone",
                                         @"db":db,
                                         @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                         @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                         };
    
    NSLog(@"Posting sendMsgsDict is %@",sendMsgsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:sendMsgsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSLog(@"No data returned from server, error ocurred: %@", error);
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError*deserr;
            
            NSDictionary *    smsstatus = [NSJSONSerialization
                                      JSONObjectWithData:data
                                      options:kNilOptions
                                      error:&deserr];
            
            NSString * str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSDictionary*Killer=[smsstatus valueForKey:@"sendsmsstatus"];
            
            NSLog(@"Free SMS Details Response is: %@",Killer);

            [LoaderClass removeLoader:self.view];
            
            [freeSmsDetails setValue:str forKey:@"Message"];
            
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"smsWebServicesMethod";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                }
                
                else{  if ([[Killer valueForKey:@"response_message"]  isEqualToString:@"sms sent"]) {
                
                toastMsg = [MCLocalization stringForKey:@"msgsentsuccessfully"];
                [self toastMessagemethod];
                
                [self smsHistWebServicesMethod];
                [self smsCountWebServicesMethod];
                
                //            _dailySmsCount.text = [NSString stringWithFormat:@"%d",[_dailySmsCount.text intValue]-1];
                _txtMobNum.text = @"";
                _txtViewMsg.text = @"";
                
            } else if  ([[Killer valueForKey:@"response_message"]  isEqualToString:@"sms not sent"]) {
                toastMsg = [MCLocalization stringForKey:@"msgNotSent"];
                [self toastMessagemethod];
                
            } else if  ([[Killer valueForKey:@"response_message"]  isEqualToString:@"limit excedded"]) {
                toastMsg = [MCLocalization stringForKey:@"msgLimitExc"];
                [self toastMessagemethod];
            }  else {
                toastMsg = [MCLocalization stringForKey:@"msgsendingfailed"];
                [self toastMessagemethod];
            }

                }
            }
         });
        
     }];
    
    [dataTask resume];
}


// Free SMS Webservices Method
-(void)smsCountWebServicesMethod {
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    NSString * UEmail= [profileDict valueForKey:@"user_email_id"];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSString*from=@"iphone";
    

    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    

    
    [LoaderClass showLoader:self.view];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/profile/getSMSCOUNT2"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * smsCountDetailsDict =  @{@"usermob":userId,
                                            @"user_emailId":UEmail,
                                            @"ipAddress":Ipaddress,
                                            @"from":from,
                                            @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                            @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                         };
    
    NSLog(@"Posting smsCountDetailsDict is %@",smsCountDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:smsCountDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError*deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];

            
            NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            
            NSLog(@"resSrt: %@", resSrt);
            
            
            
            [LoaderClass removeLoader:self.view];
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"smsHistWebServicesMethod";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                }
                
                else{
            
            NSMutableDictionary*respdict=[[NSMutableDictionary alloc]init];
            
            
            [respdict setObject:[Killer valueForKey:@"smsCount"] forKey:@"Message"];
            
//            NSLog(@"sms Count Details Response %@",respdict);
//            smsCountDetails = respdict;
            
            _dailySmsCount.text = [NSString stringWithFormat:@"%@",  [Killer valueForKey:@"smsCount"]];
            _charactersLeftMsg.text = @"60";
                }
            }
        });
    }];
    
    [dataTask resume];
    
}




//  SMS History Webservices Method
-(void)smsHistWebServicesMethod {
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    NSString * UEmail= [profileDict valueForKey:@"user_email_id"];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    
    
    [LoaderClass showLoader:self.view];
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/profile/getSMSHistoryDetails"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    

    
    NSDictionary * smsHistDetailsDict =  @{@"usermob":userId,
                                         @"user_emaiId":UEmail,
                                         @"ipAddress":Ipaddress,
                                         @"from":@"iPhone",
                                         @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                         @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"]
                                         };
    
    NSLog(@"Posting smsHistDetailsDict is %@",smsHistDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:smsHistDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            NSLog(@"resSrt: %@", resSrt);
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
//            NSMutableDictionary*respdict=[[NSMutableDictionary alloc]init];
//            [respdict setObject:Killer forKey:@"List"];
            
            [LoaderClass removeLoader:self.view];
            
            
//            NSArray * rc = [respdict objectForKey:@"List"];
            NSLog(@"sms History Response %@",Killer);
            
            NSArray * rc = [Killer valueForKey:@"smshistory"];
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"smsHistWebServicesMethod";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                }
                
                else   if (rc.count != 0) {
                
                message=[rc valueForKey:@"messages"];
                mobile=[rc valueForKey:@"sentmobile"];
                smsDate=[rc valueForKey:@"date"];
                
                height = 92.0f;
                [self.tblSMSHistory reloadData];
                
            } else {
                
                UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tblSMSHistory.bounds.size.width, self.tblSMSHistory.bounds.size.height)];
                noDataLabel.text             = [MCLocalization stringForKey:@"noSentMsgs"];
                noDataLabel.textColor        = [UIColor blackColor];
                noDataLabel.textAlignment    = NSTextAlignmentCenter;
                self.tblSMSHistory.backgroundView = noDataLabel;
                self.tblSMSHistory.tableFooterView = [[UIView alloc] init];
                height = 0;
            }
            }
        
            
           });
    
    }];
    
    [dataTask resume];
}




 // Characters Length in TextView
-(void)textViewDidChange:(UITextView *)textView{
    _charactersLeftMsg.text = [NSString stringWithFormat:@"%lu",60-_txtViewMsg.text.length];
}



// TextView Delagate Method
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text length] == 0) {
        if([_txtViewMsg.text length] != 0)
        {
            return YES;
        }
    } else if([[_txtViewMsg text] length] > 59) {
        return NO;
    }
    
    // Dismissing Keyboard for textview
    if([text isEqualToString:@"\n"]) {
        [_txtViewMsg resignFirstResponder];
        return NO;
    }
    return YES;
}



#pragma mark --
#pragma mark - UITableView Delegate Methods


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([mobile count] == 0) {
        return 0;
    }
    return [mobile count];
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString * cellIdentifier=@"Cell";
    UserSendSMSCell * cell= (UserSendSMSCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"UserSendSMSCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
   
    
    cell.lblMobNum.adjustsFontSizeToFitWidth=YES;
    cell.lblTime.adjustsFontSizeToFitWidth=YES;
    
    cell.lblMobNum.text = [NSString stringWithFormat:@"%@", [mobile objectAtIndex:indexPath.row]];
    cell.lblTime.text = [NSString stringWithFormat:@"%@", [smsDate objectAtIndex:indexPath.row]];
    cell.txtViewSentMsg.text = [NSString stringWithFormat:@"%@", [[[[message objectAtIndex:indexPath.row] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "]];
    cell.backgroundColor =  [UIColor whiteColor];

    _tblSMSHistory.alwaysBounceVertical = NO;
    self.tblSMSHistory.tableFooterView = [[UIView alloc] init];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return height;
}



 

// alert method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}


- (IBAction)btnContacts:(id)sender {
    
    
    UserContactsVC * contacts = [self.storyboard instantiateViewControllerWithIdentifier:@"UserContactsVC"];
    [self.navigationController pushViewController:contacts animated:YES];
    
}


- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString * phoneRegex = @"^((5))[0-9]{8}$";
    NSPredicate * phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}


- (IBAction)btnFreeSmsSend:(id)sender {
    
    // Recipient Mobile Number
    if ([_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg = [MCLocalization stringForKey:@"pleaseEnterMobile"];
        [self toastMessagemethod];
    } else if (![Validate isValidMobileNumber:[_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg = [MCLocalization stringForKey:@"mobilevalidate"];
        [self toastMessagemethod];
    } else if (![self validatePhone:[_txtMobNum.text stringByTrimmingCharactersInSet:[NSCharacterSet 	whitespaceCharacterSet]]]){
        toastMsg = [MCLocalization stringForKey:@"startWith5"];
        [self toastMessagemethod];
    } else  if ([_txtViewMsg.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        toastMsg = [MCLocalization stringForKey:@"entermsg"];
        [self toastMessagemethod];
    }
    else if ([_dailySmsCount.text intValue]-1 == -1) {
        alertMessage = [MCLocalization stringForKey:@"10SmsPerDay"];
        [self alertMethod];
    }
    else if ([_dailySmsCount.text isEqualToString:@"0"]) {
        alertMessage = [MCLocalization stringForKey:@"10SmsPerDay"];
        [self alertMethod];
   }

    else {
        [self smsWebServicesMethod];
    }
}



@end
