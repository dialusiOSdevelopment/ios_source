//
//  RCGCVC.m
//  Saddly
//
//  Created by Sai krishna on 6/17/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "RCGCVC.h"


@interface RCGCVC ()

@end

@implementation RCGCVC

- (void)viewDidLoad {
    
    self.title = [MCLocalization stringForKey:@"guest"];
    
    [_btnRcOutlet setTitle:[MCLocalization stringForKey:@"recharge"] forState:UIControlStateNormal];
    [_btnGiftCardsOutlet setTitle:[MCLocalization stringForKey:@"GiftCards"] forState:UIControlStateNormal];
    
    [super viewDidLoad];
    
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

 


- (IBAction)btnRecharge:(id)sender {
    
    btnRcDefaults = [NSUserDefaults standardUserDefaults];
    [btnRcDefaults setObject:@"100" forKey:@"btnRcDefaults"];
    
    RCGCOperators * guestRC = [self.storyboard instantiateViewControllerWithIdentifier:@"RCGCOperators"];
    [self.navigationController pushViewController:guestRC animated:YES];
    
}



- (IBAction)btnGiftCards:(id)sender {
    
    btnRcDefaults = [NSUserDefaults standardUserDefaults];
    [btnRcDefaults setObject:@"200" forKey:@"btnRcDefaults"];
    
    RCGCOperators * guestGC = [self.storyboard instantiateViewControllerWithIdentifier:@"RCGCOperators"];
    [self.navigationController pushViewController:guestGC animated:YES];
    
}






@end
