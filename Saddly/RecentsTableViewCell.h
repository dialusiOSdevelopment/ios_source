//
//  RecentsTableViewCell.h
//  Saddly
//
//  Created by Sai krishna on 1/30/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecentsTableViewCell : UITableViewCell




@property (strong, nonatomic) IBOutlet UIImageView *imgOperatorLogo;
@property (strong, nonatomic) IBOutlet UILabel *lblOperatorName;
@property (strong, nonatomic) IBOutlet UILabel *lblNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblRcDate;
@property (strong, nonatomic) IBOutlet UIButton *btnRCAmount;



@end
