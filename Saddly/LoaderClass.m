//
//  LoaderClass.h
//  CKChat
//
//  Created by Cosmic Kayka on 08/01/15.
//  Copyright (c) 2015 Cosmic Kayka. All rights reserved.


#import "LoaderClass.h"
#import "AppDelegate.h"

@implementation LoaderClass

+(void) showLoader  : (UIView *)CurrentView
{
	CurrentView.userInteractionEnabled = NO;
    
	//AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    //appDelegate.window.userInteractionEnabled=NO;
    
    UIView *LoadingView=[[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];//CGRectMake(110, 200, 49, 49)];
    //LoadingView.center=CurrentView.center;
    LoadingView.backgroundColor= [UIColor clearColor];//[UIColor colorWithWhite:0.0 alpha:0.2];
    LoadingView.layer.cornerRadius=8.0f;
	LoadingView.tag=12345789;
	[CurrentView addSubview:LoadingView];
	[CurrentView bringSubviewToFront:LoadingView];
    
    
    
    UIView *LoadingViewback=[[UIView alloc] initWithFrame:CGRectMake(110, 200, 49, 49)];
    LoadingViewback.center=LoadingView.center;
    LoadingViewback.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.8];
    LoadingViewback.layer.cornerRadius=8.0f;
    LoadingViewback.tag=5789;
    [LoadingView addSubview:LoadingViewback];
    
    UIActivityIndicatorView *acticity=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    acticity.frame=CGRectMake(12, 12, 25, 25);
    [acticity startAnimating];
    //acticity.center=LoadingViewback.center;
    acticity.hidesWhenStopped = NO;
    [LoadingViewback addSubview:acticity];
    acticity=nil;
    
    LoadingView.clipsToBounds=YES;
    LoadingView=nil;
    
}

+(void) removeLoader: (UIView *)CurrentView
{
    //AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    //appDelegate.window.userInteractionEnabled=YES;

	CurrentView.userInteractionEnabled=YES;
	
	UIView *TempView=(UIView *)[CurrentView viewWithTag:12345789];
//    UIView *TempView1=(UIView *)[TempView viewWithTag:5789];
//    [TempView1 removeFromSuperview];
	[TempView removeFromSuperview];
   
	TempView=nil;
}

#pragma mark -
#pragma mark Loader on Window

+(void) showLoaderOnWindow
{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.window.userInteractionEnabled=NO;

    UIView *LoadingView=[[UIView alloc] initWithFrame:CGRectMake(110, 200, 49, 49)];
    LoadingView.center=appDelegate.window.center;
    LoadingView.backgroundColor=[UIColor colorWithWhite:0.0 alpha:0.5];
    LoadingView.layer.cornerRadius=8.0f;
	LoadingView.tag=12345789;
    
    [appDelegate.window addSubview:LoadingView];
    [appDelegate.window bringSubviewToFront:LoadingView];
    
    UIActivityIndicatorView *acticity=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    acticity.frame=CGRectMake(12, 12, 25, 25);
    [acticity startAnimating];
    [LoadingView addSubview:acticity];
    acticity=nil;
    
    LoadingView.clipsToBounds=YES;
    LoadingView=nil;
}

+(void) removeLoaderFromWindow
{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.window.userInteractionEnabled=YES;

    appDelegate.window.userInteractionEnabled=YES;

    UIView *TempView=(UIView *)[appDelegate.window viewWithTag:12345789];
    [TempView removeFromSuperview];
    TempView=nil;
}



@end
