//
//  UpdateProfileOTP.h
//  Saddly
//
//  Created by Sai krishna on 7/15/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoaderClass.h"
#import "ConnectivityManager.h"
#import "UIView+Toast.h"
#import "Validate.h"
#import "LoginViewController.h"
#import "UpdateDetails.h"
#import "MCLocalization.h"



@interface UpdateProfileOTP : UIViewController <NSURLSessionDelegate> {
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    NSString *name,*a123,*methodname;
    
    NSDictionary  * getOTPDetails, * loginWithOTPDetails;
    
    CLLocationManager *locationManager;
    
    NSString * latitude, * longitude;
    
    NSString * deviceName, * deviceVersion,*response1, * otpFromServer;
    
    NSDictionary * profileDict;
    
}

@property (strong, nonatomic) NSString * OTPName;
@property (strong, nonatomic) NSString * OTPMobNum;
@property (strong, nonatomic) NSString * OTPEmail;
@property (strong, nonatomic) NSString * OTPPwd;







@property (strong, nonatomic) IBOutlet UIButton *btnShowPwdOutlet;

- (IBAction)btnShowPwd:(id)sender;





@property (strong, nonatomic) IBOutlet UITextField *OTP;
@property (strong, nonatomic) IBOutlet UIButton * btnSubmitOutlet;
@property (strong, nonatomic) IBOutlet UITextView *TxtActivationCode;
@property (strong, nonatomic) IBOutlet UILabel *Greetingtxt;
@property (strong, nonatomic) IBOutlet UILabel *LblActivationcode;
- (IBAction)Submit:(id)sender;

@end

