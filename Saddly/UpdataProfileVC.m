//
//  UpdataProfileVC.m
//  Saddly
//
//  Created by Sai krishna on 7/15/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "UpdataProfileVC.h"

@interface UpdataProfileVC ()

@end

@implementation UpdataProfileVC

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    
    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.title = [MCLocalization stringForKey:@"updateProfle"];
    
    [_TxtUserName setPlaceholder:[MCLocalization stringForKey:@"RegName"]];
    [_TxtEmailid setPlaceholder:[MCLocalization stringForKey:@"RegMAilid"]];
    [_btnDoneOutlet setTitle:[MCLocalization stringForKey:@"update"] forState:UIControlStateNormal];

    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];

    
    _TxtUserName.text = [[[[profileDict valueForKey:@"user_name"] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    _TxtMobNum.text = [profileDict valueForKey:@"user_mob"];
    _TxtEmailid.text = [profileDict valueForKey:@"user_email_id"];
    
    
    
}


-(void)NextClicked{
    [self.view endEditing:YES];
}

-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



// Returning to Next Text fields
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.TxtUserName) {
        [self.TxtEmailid becomeFirstResponder];
    } else if (textField == self.TxtEmailid) {
        [self.TxtEmailid resignFirstResponder];
    }
    return YES;
}


//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if ([textField isEqual:_TxtEmailid]){
        
        if([self isAlphaNumericSpecialCharacters:string])
            return TRUE;
        else
            return FALSE;
        
    } else if ([textField isEqual:_TxtUserName]){
        
        if([self isAlphaNumericWithSpace:string]) {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 40;
        } else
            return FALSE;
        
    }
    
    return YES;
}





// Entering only alphaNumeric & Special Characters not a Arabic Characters
-(BOOL)isAlphaNumericSpecialCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._@-"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}

// Entering all Except special Characters
-(BOOL)isAlphaNumericWithSpace:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzأ ب ت ث ح خ  د ه ع غ ف ق ث ص ض ذ ط ك م ن ت ل ي س ش ظ ز و ة ى لا ر ؤ ء ئ "] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}

// Entering all Except special Characters
-(BOOL)isSpace:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@" "] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}

// Entering all Except special Characters
-(BOOL)isCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzأ ب ت ث ح خ  د ه ع غ ف ق ث ص ض ذ ط ك م ن ت ل ي س ش ظ ز و ة ى لا ر ؤ ء ئ "] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}






// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = self.view.bounds;
    
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}




- (IBAction)btnNameEdit:(id)sender {
    
    [_TxtUserName becomeFirstResponder];
    
}
-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"getOTPForProfileServices"]) {
                    
                    [self getOTPForProfileServices];
                    
                }
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//


- (IBAction)btnEmailEdit:(id)sender {
    
    [_TxtEmailid becomeFirstResponder];
    
}






-(void)getOTPForProfileServices{
    
    
    [LoaderClass showLoader:self.view];
    
    NSString*db;
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
db=@"AR";
    } else {
        db=@"SA";
        
    }
    
    
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    
    
    
    ///
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/profile/getOTPForProfile"];
    
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    
    NSDictionary * loginDetailsDict =  @{@"user_mob":[profileDict valueForKey:@"user_mob"] ,
                                         
                                         @"from":@"iphone",
                                         
                                         @"ipaddress":[ipaddressdefault stringForKey:@"ipaddressdefault"],
                                    
                                         @"access_token":accessToken,
                                         @"unique_id":uniqueid
                                         
                                         };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
            NSString*     resSrt  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            [LoaderClass removeLoader:self.view];
            
            NSLog(@"Killer Response is%@",resSrt);
            
            NSLog(@"Killer Response is%@",Killer);
            
            
            
            if ([[Killer valueForKey:@"response_message" ] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"getOTPForProfileServices";
                
                [self checkTokenStatusWebServices];
                
                
            }else if ([[Killer valueForKey:@"response_message" ] isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            
    
        
        otpFromServer  =[Killer valueForKey:@"response_message" ];
        
        NSLog(@"get OTP Profile Services Response is: %@",otpFromServer);
        
         [LoaderClass removeLoader:self.view];
        
        if ([otpFromServer isEqualToString:@"email already exist"]) {
            
            toastMsg=otpFromServer;
            [self toastMessagemethod];
            
        }else{
            
            NSUserDefaults * Otp = [NSUserDefaults standardUserDefaults];
            [Otp setValue:otpFromServer forKey:@"OTP"];
            
            
            UpdateProfileOTP * updateProfOTP = [self.storyboard instantiateViewControllerWithIdentifier:@"UpdateProfileOTP"];
            
            updateProfOTP.OTPName = _TxtUserName.text;
            updateProfOTP.OTPMobNum = _TxtMobNum.text;
            updateProfOTP.OTPEmail= _TxtEmailid.text;
            
            [self.navigationController pushViewController:updateProfOTP animated:YES];
            
        }
        

        
        
    });
     
     
     }];
            
            
            [dataTask resume];
        }





- (IBAction)Done:(id)sender {
    
    if ([_TxtUserName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].
        length == 0){
        toastMsg = [MCLocalization stringForKey:@"Namealert"];
        [self toastMessagemethod];
    }
    // Email id
    else if ([_TxtEmailid.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
        toastMsg = [MCLocalization stringForKey:@"Mailidvalidate"];
        [self toastMessagemethod];
    }
    else if (![Validate isValidEmailId:[_TxtEmailid.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]){
        toastMsg = [MCLocalization stringForKey:@"Mailidnotvalid"];
        [self toastMessagemethod];
    }
    
    else{
        
        [self getOTPForProfileServices];
        
    }
    
}



@end
