//
//  ShareViewController.h
//  Saddly
//
//  Created by gandhi on 16/02/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import "WebServicesMethods.h"
#import "ConnectivityManager.h"
#import "LoaderClass.h"
#import "LoginViewController.h"


@interface ShareViewController : UIViewController <NSURLSessionDelegate> {
    
    NSDictionary * refDict;
     NSString * methodname, * a123;
    NSDictionary * profileDict;
    
}




@property (strong, nonatomic) IBOutlet UIImageView *imgShare;
@property (strong, nonatomic) IBOutlet UITextField *txtReferralCode;
@property (strong, nonatomic) IBOutlet UIButton *btnShareOutlet;



- (IBAction)btnShare:(id)sender;


@end
