//
//  VoiceTableViewCell.h
//  DialuzApp
//
//  Created by Sai krishna on 11/10/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoiceTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblVoice;
@property (strong, nonatomic) IBOutlet UIButton *btnVoiceOutlet;

@end
