//
//  WriteUsSuccessVC.h
//  Saddly
//
//  Created by Sai krishna on 6/20/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"

@interface WriteUsSuccessVC : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *TransactText;

@property (strong, nonatomic) IBOutlet UILabel *TransactID;
@property (strong, nonatomic) IBOutlet UILabel *contacttext;

@property (strong, nonatomic) IBOutlet UIButton *DoneBttn;
- (IBAction)DoneBttn:(id)sender;


@end
