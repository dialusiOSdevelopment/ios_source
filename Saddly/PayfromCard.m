//
//  PayfromCard.m
//  DialuzApp
//
//  Created by Sai krishna on 11/11/16.
//  Copyright © 2016 Dialuz. All rights reserved.
//

#import "PayfromCard.h"

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);


@interface PayfromCard ()

@end

@implementation PayfromCard


- (void)viewDidLoad {
    
    
    //
    
    [[GAI sharedInstance].defaultTracker set:kGAIScreenName
                                       value:@"Pay From Card Screen"];
    
    [[GAI sharedInstance].defaultTracker
     send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
    }
    
    
    // Setting Vertical line.
    UIView *borderBottom = [[UIView alloc] initWithFrame:CGRectMake(0.0, 100.0, self.view.frame.size.width, 1.5)];
    borderBottom.backgroundColor = [UIColor blackColor];
    [self.view addSubview:borderBottom];
    
    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self 	action:@selector(removingKeyboardByTap)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    
    _MobilenumName.adjustsFontSizeToFitWidth=YES;
    _OperatorName.adjustsFontSizeToFitWidth=YES;
    _RechargeplanName.adjustsFontSizeToFitWidth=YES;
    _rcAmountName.adjustsFontSizeToFitWidth=YES;
    _ProceedtopayCardName.titleLabel.adjustsFontSizeToFitWidth=YES;
    _lblEnterSadadName.adjustsFontSizeToFitWidth = YES;
    _txtEnterSadadId.adjustsFontSizeToFitWidth = YES;
    
    _MobilenumbercardTxt.adjustsFontSizeToFitWidth=YES;
    _OperatorCardTxt.adjustsFontSizeToFitWidth=YES;
    _AmountFromcardTxt.adjustsFontSizeToFitWidth=YES;
    _PlancardTxt.adjustsFontSizeToFitWidth=YES;
    _lblRcAmount.adjustsFontSizeToFitWidth=YES;
    
    
    self.title = [MCLocalization stringForKey:@"PayfromCard"] ;
    
    
    NSUserDefaults * otherPaymentModesDefaults = [NSUserDefaults standardUserDefaults];
    _lblOherPaymentModesName.text = [otherPaymentModesDefaults stringForKey:@"otherPaymentModesDefaults"];
    
    _lblFromName.text = [MCLocalization stringForKey:@"from"];
    
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    // Saving profile Details Dictionary to NSUserDefaults.
    NSUserDefaults *CardTrDefaults = [NSUserDefaults standardUserDefaults];
    
    
    CardTranDetails = [NSKeyedUnarchiver unarchiveObjectWithData:[CardTrDefaults objectForKey:@"CardTrDefaults"]];
    
    
    NSLog(@"card transaction details are%@",CardTranDetails);
    
    _MobilenumName.text=[MCLocalization stringForKey:@"numbername"] ;
    _RechargeplanName.text=[MCLocalization stringForKey:@"planname"];
    _rcAmountName.text = [MCLocalization stringForKey:@"Amount12"];
    
    
    
    NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
        
        _OperatorName.text=[MCLocalization stringForKey:@"operator"] ;
        
    } else {
        
        _OperatorName.text=[MCLocalization stringForKey:@"store"] ;
       
    }
    
    
    _lblEnterSadadName.text = [MCLocalization stringForKey:@"entersadadid"];
    _txtEnterSadadId.placeholder = [MCLocalization stringForKey:@"sadadid"];
    
    
    
    NSUserDefaults * WalletDefaults = [NSUserDefaults standardUserDefaults];
    
    Walletdetails=    [NSKeyedUnarchiver unarchiveObjectWithData:[WalletDefaults objectForKey:@"WalletDefaults"]];
    
    
    NSUserDefaults * amountFromPayBalAmountDefaults = [NSUserDefaults standardUserDefaults];

   
    _AmountFromcardTxt.text = [amountFromPayBalAmountDefaults stringForKey:@"amountFromPayBalAmountDefaults"];

    
    NSUserDefaults * rcAmount = [NSUserDefaults standardUserDefaults];
    _lblRcAmount.text = [rcAmount stringForKey:@"rcAmount"];
    
    [_ProceedtopayCardName setTitle:[[MCLocalization stringForKey:@"proceedpay"] stringByAppendingString:_AmountFromcardTxt.text]  forState:UIControlStateNormal];
    
    
    NSUserDefaults * rcMobNum = [NSUserDefaults standardUserDefaults];
    _MobilenumbercardTxt.text= [NSString stringWithFormat:@"%@",[rcMobNum stringForKey:@"rcMobNum"]];
    
    NSUserDefaults * rcOperator = [NSUserDefaults standardUserDefaults];
    NSString*operator=[NSString stringWithFormat:@"%@",[rcOperator stringForKey:@"rcOperator"]];

    NSLog(@"Operator %@",operator);
    
    NSUserDefaults * plandefaults = [NSUserDefaults standardUserDefaults];
    _PlancardTxt.text=[NSString stringWithFormat:@"%@",[[plandefaults stringForKey:@"plandefaults"]stringByReplacingOccurrencesOfString:@" " withString:@""]];
    
    payFort	= [[PayFortController alloc] initWithEnviroment:KPayFortEnviromentProduction];
    deviceid=[payFort getUDID];
    
    NSUserDefaults * sadadefaults = [NSUserDefaults standardUserDefaults];
    
    if ([[sadadefaults valueForKey:@"sadadefaults"] isEqualToString:@"100"]) {
        
        _txtEnterSadadId.hidden=NO;
        _lblEnterSadadName.hidden=NO;
        _lblDotsSadadId.hidden = NO;
        
        UIImage* needHelp = [UIImage imageNamed:@"sadad-logo.png"];
        CGRect frameimg = CGRectMake(0, 0, 50, 30);
        UIButton * contactButton = [[UIButton alloc] initWithFrame:frameimg];
        [contactButton setBackgroundImage:needHelp forState:UIControlStateNormal];
        [contactButton setShowsTouchWhenHighlighted:NO];
        contactButton.hidden = NO;
        
        UIBarButtonItem * sadadButton = [[UIBarButtonItem alloc] initWithCustomView:contactButton];
        self.navigationItem.rightBarButtonItem = sadadButton;
        
    } else {
        
        _txtEnterSadadId.hidden=YES;
        _lblEnterSadadName.hidden=YES;
        _lblDotsSadadId.hidden = YES;
        
       
        [self signatureWebServicesMethod];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    
    
    _MobilenumName.adjustsFontSizeToFitWidth=YES;
    _OperatorName.adjustsFontSizeToFitWidth=YES;
    _RechargeplanName.adjustsFontSizeToFitWidth=YES;
    _rcAmountName.adjustsFontSizeToFitWidth=YES;
    _ProceedtopayCardName.titleLabel.adjustsFontSizeToFitWidth=YES;
    
    _MobilenumbercardTxt.adjustsFontSizeToFitWidth=YES;
    _OperatorCardTxt.adjustsFontSizeToFitWidth=YES;
    _AmountFromcardTxt.adjustsFontSizeToFitWidth=YES;
    _PlancardTxt.adjustsFontSizeToFitWidth=YES;
    _lblRcAmount.adjustsFontSizeToFitWidth=YES;
}


-(void)removingKeyboardByTap {
    [self.view endEditing:YES];
}


// Enabling the Back Button
-(void)viewDidAppear:(BOOL)animated{
    
    NSUserDefaults * rcOperator = [NSUserDefaults standardUserDefaults];
    NSString*operator=[NSString stringWithFormat:@"%@",[rcOperator stringForKey:@"rcOperator"]];
    
    NSLog(@"%@",operator);
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        if ([operator isEqualToString:@"Zain"]) {
            _imgOperator.image = [UIImage imageNamed:@"Zainar.png"];
        } else if ([operator isEqualToString:@"Friendi"]) {
            _imgOperator.image = [UIImage imageNamed:@"Friendiar.png"];
        } else if ([operator isEqualToString:@"Jawwy"]) {
            _imgOperator.image = [UIImage imageNamed:@"Jawwyar.png"];
        } else if ([operator isEqualToString:@"Mobily"]) {
            _imgOperator.image = [UIImage imageNamed:@"Mobilyar.png"];
        } else if ([operator isEqualToString:@"STC"]) {
            _imgOperator.image = [UIImage imageNamed:@"STC_round.png"];
        } else if ([operator isEqualToString:@"Virgin"]) {
            _imgOperator.image = [UIImage imageNamed:@"Virginar.png"];
        } else if ([operator isEqualToString:@"Sawa"]) {
            _imgOperator.image = [UIImage imageNamed:@"Sawaar.png"];
        } else if ([operator isEqualToString:@"Quicknet"]) {
            _imgOperator.image = [UIImage imageNamed:@"Quicknetar.png"];
        } else if ([operator isEqualToString:@"Lebara"]) {
            _imgOperator.image = [UIImage imageNamed:@"Leberaar.png"];
        }else if ([operator isEqualToString:@"iTunes"]) {
            _imgOperator.image = [UIImage imageNamed:@"itunsar.png"];
        }else if ([operator isEqualToString:@"Xbox"]) {
            _imgOperator.image = [UIImage imageNamed:@"xbox-cardar.png"];
        } else if ([operator isEqualToString:@"Playstation"]) {
            _imgOperator.image = [UIImage imageNamed:@"ps-cardar.png"];
        } else {
            _imgOperator.image = [UIImage imageNamed:@"appicon_green.png"];
        }
        
    } else {
      
        if ([operator isEqualToString:@"Zain"]) {
            _imgOperator.image = [UIImage imageNamed:@"zainen.png"];
        } else if ([operator isEqualToString:@"Friendi"]) {
            _imgOperator.image = [UIImage imageNamed:@"friendien.png"];
        } else if ([operator isEqualToString:@"Jawwy"]) {
            _imgOperator.image = [UIImage imageNamed:@"jawwyen.png"];
        } else if ([operator isEqualToString:@"Mobily"]) {
            _imgOperator.image = [UIImage imageNamed:@"mobilyen.png"];
        } else if ([operator isEqualToString:@"STC"]) {
            _imgOperator.image = [UIImage imageNamed:@"STC_round.png"];
        } else if ([operator isEqualToString:@"Virgin"]) {
            _imgOperator.image = [UIImage imageNamed:@"virginen.png"];
        } else if ([operator isEqualToString:@"Sawa"]) {
            _imgOperator.image = [UIImage imageNamed:@"sawaen.png"];
        } else if ([operator isEqualToString:@"Quicknet"]) {
            _imgOperator.image = [UIImage imageNamed:@"quickneten.png"];
        } else if ([operator isEqualToString:@"Lebara"]) {
            _imgOperator.image = [UIImage imageNamed:@"lebaraen.png"];
        }else if ([operator isEqualToString:@"iTunes"]) {
            _imgOperator.image = [UIImage imageNamed:@"itunsen.png"];
            
        }else if ([operator isEqualToString:@"Xbox"]) {
            _imgOperator.image = [UIImage imageNamed:@"Xbox Carden.png"];
            
        } else if ([operator isEqualToString:@"Playstation"]) {
            _imgOperator.image = [UIImage imageNamed:@"ps-carden.png"];
            
        } else {
            _imgOperator.image = [UIImage imageNamed:@"appicon_green.png"];
        }
        
    }
    
    
    
    if ([operator isEqualToString:@"iTunes"]) {
        _OperatorCardTxt.text=[MCLocalization stringForKey:@"itunesGC"];
    }else if ([operator isEqualToString:@"Xbox"]){
        _OperatorCardTxt.text=[MCLocalization stringForKey:@"xboxGC"];
    }else if ([operator isEqualToString:@"Playstation"]){
        _OperatorCardTxt.text=[MCLocalization stringForKey:@"playstationGC"];
    }else if ([operator isEqualToString:@"Zain"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorCardTxt.text=@"زين";
            
        }
        else{
            
            _OperatorCardTxt.text=@"Zain";
        }
    }else if ([operator isEqualToString:@"Sawa"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorCardTxt.text=@"سوا"  ;
            
        }
        else{
            _OperatorCardTxt.text=@"Sawa";
            
        }
    }
    else if ([operator isEqualToString:@"Virgin"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorCardTxt.text=@"فيرجن";}
        else{
            
            _OperatorCardTxt.text=@"Virgin";
        }
    }
    else if ([operator isEqualToString:@"Friendi"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorCardTxt.text=@"فريندي";}
        else{
            
            _OperatorCardTxt.text=@"Friendi";
        }
    }
    else if ([operator isEqualToString:@"Jawwy"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorCardTxt.text=@"جوي";}
        else{
            
            _OperatorCardTxt.text=@"Jawwy";
        }
    }
    else if ([operator isEqualToString:@"Quicknet"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorCardTxt.text=@"كويك نت";}
        else{
            
            _OperatorCardTxt.text=@"Quicknet";
        }
    }
    else if ([operator isEqualToString:@"Mobily"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorCardTxt.text=@"موبايلي";}
        else{
            
            _OperatorCardTxt.text=@"Mobily";
        }
    }
    else if ([operator isEqualToString:@"lebara"]){
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            _OperatorCardTxt.text=@"ليبارا";}
        else{
            
            _OperatorCardTxt.text=@"lebara";
        }
    }
    
    [self loaderWithOperator];
   
    
    if (test == 10) {
        
        [self performSegueWithIdentifier:@"HomePayFrmCard" sender:nil];
        
    } else{
        
        self.navigationItem.hidesBackButton = NO;
    }
    
    
}









- (void)loaderWithOperator {
    
    
    _loadingView = [[UIView alloc] initWithFrame:CGRectMake(110, 200, 250, 200)];
    _loadingView.center=self.view.center;
    _loadingView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    _loadingView.clipsToBounds = YES;
    _loadingView.layer.cornerRadius = 10.0;
    
    
    
    _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _activityView.frame = CGRectMake(10 , 130, _activityView.bounds.size.width, _activityView.bounds.size.height);
    [_loadingView addSubview:_activityView];
    
    
    
    _loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, 100, self.loadingView.frame.size.width, 100)];
    _loadingLabel.backgroundColor = [UIColor clearColor];
    _loadingLabel.textColor = [UIColor whiteColor];
    _loadingLabel.textAlignment = NSTextAlignmentLeft;
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        _loadingLabel.text = @"عملية الدفع جارية... يرجى الانتظار";
    } else {
        _loadingLabel.text = @"Payment is Processing...\nPlease Wait....";
    }
    
    _loadingLabel.numberOfLines=0;
    [_loadingView addSubview:_loadingLabel];
    
    
    
    
    _loadingimage=[[UIImageView alloc]initWithFrame:CGRectMake(75, 20, 100, 100)];
    _loadingimage.image=_imgOperator.image;
    [_loadingView addSubview:_loadingimage];
    
    
}



// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}


//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}


-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
        [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}




//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:_txtEnterSadadId]){
        
        
        if([self isAlphaNumericSpecialCharacters:string]) {
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 12;
            
        } else
            return FALSE;
    }
    return YES;
}


// Entering only alphaNumeric & Special Characters not a Arabic Characters
-(BOOL)isAlphaNumericSpecialCharacters:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._@-"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}




-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"signatureWebServicesMethod"]) {
                    
                    [self signatureWebServicesMethod];
                    
                }
                else if ([methodname isEqualToString:@"Sadadservices"]) {
                    
                    [self Sadadservices];
                    
                }
                else if ([methodname isEqualToString:@"returnUrl"]) {
                    
                    [self returnUrl];
                    
                }
                
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//


-(void)signatureWebServicesMethod{
    
    [LoaderClass showLoader:self.view];
    
    
    NSString * db = [MCLocalization stringForKey:@"DBvalue"];
    
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    
    
    NSDictionary*   profileDict1 = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    NSString*UEmail= [profileDict1 valueForKey:@"user_email_id"];
    NSString*uMob=   [profileDict1 valueForKey:@"user_mob"];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    NSString*from=@"iphone";
    
    
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/getTokenSignatureDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    //         NSString * userPwd=@"585858";
    
    NSLog(@"%@",userId);
    NSLog(@"%@",userPwd);
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];

    
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    
    NSDictionary * loginDetailsDict =  @{@"device_id":deviceid,
                                         @"db":db,
                                         @"from":from,
                                         @"ipAddress":Ipaddress,
                                         @"user_mob":uMob,
                                         @"user_emailId":UEmail,
                                         @"access_token":accessToken,
                                         @"unique_id":uniqueid
                                         
                                         };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*Response = [NSJSONSerialization
                                     
                                     JSONObjectWithData:data
                                     
                                     options:kNilOptions
                                     
                                     error:&deserr];
            
            
            
            
            
            NSString *resSrt =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Killer Response is%@",[Response valueForKey:@"Response"]);
            
            
            if ([[Response valueForKey:@"Response"] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"signatureWebServicesMethod";
                
                [self checkTokenStatusWebServices];
                
                
            }else if ([[Response valueForKey:@"Response"] isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{
            
            NSLog(@"str Response is%@",resSrt);
   
    
    NSLog(@"Signature Response is %@",resSrt);
    
    signature = [Response valueForKey:@"Response"];
    
    [self sendTestJsonCommand];
    

            }
                       });
        
        
        
    }];
    
    
    [dataTask resume];
    
    
    
    
}




-(void)sendTestJsonCommand{
    
    NSMutableDictionary * Tokendict = [[NSMutableDictionary alloc]init];
    
    [Tokendict setValue: @"SDK_TOKEN" forKey:@"service_command"];
    
    
    [Tokendict setValue:[payFort getUDID] forKey:@"device_id"];
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
        [Tokendict setValue:@"ar" forKey:@"language"];
    } else {
        [Tokendict setValue:@"en" forKey:@"language"];
    }
    
    
    
    NSString*access_code =[NSString stringWithFormat:@"%@",[CardTranDetails valueForKey:@"access_code"]];
    
    
    
    NSLog(@"sddasdd%@",access_code);
    
    
    
    [Tokendict setObject:access_code forKey:@"access_code"];
    
    NSString*merchant_identifier =[CardTranDetails valueForKey:@"merchant_identifier"];
    
    
    [Tokendict	setObject:merchant_identifier
                  forKey:@"merchant_identifier"];
    
    [Tokendict	setValue:signature forKey:@"signature"];
    //
    
    NSError * error;
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:Tokendict options:NSJSONWritingPrettyPrinted error:&error];
    
    if (error) {
        NSLog(@"Error (%@), error: %@", Tokendict, error);
        return;
    }
    
#define appService [NSURL \
URLWithString:@"https://paymentservices.payfort.com/FortAPI/paymentApi"]
    
    // Create request object
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:appService];
    
    // Set method, body & content-type
    request.HTTPMethod = @"POST";
    request.HTTPBody = jsonData;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setValue:
     [NSString stringWithFormat:@"%lu",(unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    NSString *apiKey  = @"cWZwbWdlcmZkdGV4em5kc2ZnZGdkZzpxZ2h4Y2ZnaHR5bmpta3U=";
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", apiKey];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *r, NSData *data, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        NSError *deserr;
        responseDict = [NSJSONSerialization
                        JSONObjectWithData:data
                        options:kNilOptions
                        error:&deserr];
        
        
        [LoaderClass removeLoader:self.view];
        
        NSLog(@"so, here's the responseDict: %@", responseDict);
        
    }];
}



- (void)sdkResult:(id)response{
    
    test = 20;
    
    
    if([response isKindOfClass:[NSDictionary class]]){
        NSDictionary * responseDic = response;
        
        SDKresponsedict = responseDic;
        
        NSLog(@"response SDK..%@",response);
        
        [self returnUrl];
        
        
    } else if (response !=	nil	&&	![response	isEqualToString:@""] &&	![response	isEqualToString:@"nil"])	{
        ///	Invalid	Request	Error	Message
    } else	{
        ///	Unknown	Error	Including	Connectivity	Issues
    }
}



-(void)Sadadservices{
    
    
    NSUserDefaults * selectedOperator = [NSUserDefaults standardUserDefaults];
    NSString* operator=   [selectedOperator stringForKey:@"selectedOperator"];
    
    [LoaderClass showLoader:self.view];
    
    NSLog(@"Jaffa fello%@",operator);
    
    NSString* userName= [[[[profileDict valueForKey:@"user_name"] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    
    
    
    NSLog(@"Jaffa fello%@",userName);
   
    NSString* usermob=  [profileDict valueForKey:@"user_mob"];
    
    NSLog(@"Jaffa fello%@",usermob);
   
    NSString*email=[[profileDict valueForKey:@"user_email_id"]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSLog(@"Jaffa fello%@",email);
    
    
    NSUserDefaults * rcMobNum = [NSUserDefaults standardUserDefaults];
    NSString*rechargemobileno= [@"966" stringByAppendingString:[rcMobNum stringForKey:@"rcMobNum"]];
    
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*ipAddress= [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    NSString* rechargeamount=[Walletdetails valueForKey:@"rechargeAmount"];
    
    
    NSUserDefaults * plandefaults = [NSUserDefaults standardUserDefaults];
    
    NSString*rechargePlan=[plandefaults stringForKey:@"plandefaults"];
    
    
    NSUserDefaults * serviceIdDefaults = [NSUserDefaults standardUserDefaults];
    NSString*serviceId=[serviceIdDefaults stringForKey:@"serviceIdDefaults"];
    
    
    
    NSUserDefaults*actualcashDefaults=[NSUserDefaults standardUserDefaults];
    NSString * fromActualCash=   [actualcashDefaults valueForKey:@"fromActualCash"];
    
    
    NSUserDefaults*BonuscashDefaults=[NSUserDefaults standardUserDefaults];
    NSString * fromBonusCash=    [BonuscashDefaults valueForKey:@"fromBonusCash"];
    
    
    
    NSUserDefaults*paidamountdefaults=[NSUserDefaults standardUserDefaults];
    NSString*Paidamount=  [paidamountdefaults valueForKey:@"Paidamount"];
    
    
    
    
    
    NSString * from = @"iPhone";


    
    NSString * db = [MCLocalization stringForKey:@"DBvalue"];
    
    
    NSUserDefaults * Couponcode = [NSUserDefaults standardUserDefaults];
    
    
    NSString*couponCode;
    if (couponCode.length>1) {
        
        couponCode=[Couponcode stringForKey:@"Couponecodenum"];
    }else{
        
       couponCode=@"Nil";
    }
    
    [LoaderClass showLoader:self.view];
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/insertRechargethrCardPayDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    //         NSString * userPwd=@"585858";
    
    NSLog(@"%@",userId);
    NSLog(@"%@",userPwd);
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];

    
    
    
    NSDictionary * loginDetailsDict =  @{@"operator":operator,
                                         @"user_name":userName,
                                         @"user_mob":usermob,
                                         @"user_emailId":email,
                                         @"recharge_mob":rechargemobileno,
                                         @"recharge_amount":rechargeamount,
                                         @"ipAddress":ipAddress,
                                         @"payment_method":@"SADAD",
                                         @"rechargeplan":rechargePlan,
                                         @"service_id":serviceId,
                                         @"fromBonusCash":fromBonusCash,
                                         @"fromActualCash":fromActualCash,
                                         @"paidAmount":Paidamount,
                                         @"from":from,
                                         @"db":db,
                                         @"couponCode":couponCode,
                                         @"access_token":accessToken,
                                         @"sadad_username":_txtEnterSadadId.text,
                                         @"unique_id":uniqueid,
                                         
                                         };
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *deserr;
            
            
            NSDictionary*Killer = [NSJSONSerialization
                                   
                                   JSONObjectWithData:data
                                   
                                   options:kNilOptions
                                   
                                   error:&deserr];
            
            
            NSString*  str =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Killer Response is%@",Killer);
            
            NSLog(@"str Response is%@",str);
            
            
            
            [LoaderClass removeLoader:self.view];
            
        
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"Sadadservices";
                
                [self checkTokenStatusWebServices];
                
                
            }else if ([[Killer valueForKey:@"response_message"]isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{
        
        NSString*URl=[Killer valueForKey:@"response_message"];
        
         NSString*URlstring=[Killer valueForKey:@"transaction_url"];
        NSLog(@"URL is %@",URl);
        
        
        
            
            NSUserDefaults * UrlDefaults = [NSUserDefaults standardUserDefaults];
            [UrlDefaults setObject:URlstring forKey:@"url"];
            
            
            RechargeSadadViewController * rechargesuccess = [self.storyboard instantiateViewControllerWithIdentifier:@"RechargeSadadViewController"];
            
            [self.navigationController pushViewController:rechargesuccess animated:YES];
     
            }
        });
        
        
        
    }];
    
    
    [dataTask resume];
    
    
    
    
}

// Returning to Next Text fields
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtEnterSadadId) {
        [self.txtEnterSadadId resignFirstResponder];
    }
    return YES;
}






- (IBAction)Proccedtopaycard:(id)sender {
    
    NSUserDefaults * sadadefaults = [NSUserDefaults standardUserDefaults];
    
    
    if ([[sadadefaults valueForKey:@"sadadefaults"] isEqualToString:@"100"]) {
        
        if ([_txtEnterSadadId.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0){
            
            toastMsg = [MCLocalization stringForKey:@"plsentSadadId"];
            [self toastMessagemethod];
            
        } else if (_txtEnterSadadId.text.length < 6 || _txtEnterSadadId.text.length > 12){
            
            toastMsg = [MCLocalization stringForKey:@"sadad6-12char"];
            [self toastMessagemethod];
            
        } else {
            
            [self Sadadservices];
            
        }
        
    } else{
        
//        if([_AmountFromcardTxt.text intValue] < 10) {
//            
//            alertMessage = [MCLocalization stringForKey:@"cardbalamount10"];
//            [self alertMethod];
//            
//        } else {
        
            payFort.delegate = self;
            //if	you	need	to	switch	on	the	Payfort	Response	page
            payFort.IsShowResponsePage = YES;
            //if	you	need	to	set	custome	Payfort	view
            [payFort setPayFortCustomViewNib:@"PayFortView2"];
            
            //Generate	the	request	dictionary	as	follow
            NSMutableDictionary	* requestDictionary = [[NSMutableDictionary alloc]init];
            
            
            NSString * email = [CardTranDetails valueForKey:@"customer_email"];
            
            
            [requestDictionary setValue:[_AmountFromcardTxt.text stringByAppendingString:@"00"] forKey:@"amount"];
            
            NSString*command=[CardTranDetails valueForKey:@"command"];
            
            [requestDictionary setValue:command forKey:@"command"];
            
            [requestDictionary setValue:@"SAR" forKey:@"currency"];
            
            [requestDictionary setValue:email forKey:@"customer_email"];
            
            if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
                [requestDictionary setValue:@"ar" forKey:@"language"];
            } else {
                [requestDictionary setValue:@"en" forKey:@"language"];
            }
            
            [requestDictionary setValue:[responseDict valueForKey:@"sdk_token"] forKey:@"sdk_token"];
            
            NSString*merchant_reference=[CardTranDetails valueForKey:@"merchant_reference"];
            
            [requestDictionary setValue:merchant_reference forKey:@"merchant_reference"];
            
            NSString*token_name=[CardTranDetails valueForKey:@"token_name"];
            
            [requestDictionary setValue:token_name forKey:@"token_name"];
            
            [requestDictionary setValue:[CardTranDetails valueForKey:@"order_description"] forKey:@"order_description"];
            
            test= 10;
            
            
            NSLog(@"request dictionary is%@",requestDictionary);
            //	Mandatory
            [payFort setPayFortRequest:requestDictionary];
            //make	the	call
            [payFort callPayFort:self];
            
            
            
            // Removing the Back Button on the navigation bar.
            [self.navigationItem setHidesBackButton:YES animated:YES];
            
//        }
    }
}


-(void)returnUrl {
    
    
    [self.view addSubview:_loadingView];
    [_activityView startAnimating];

    
    NSUserDefaults * rcOperator = [NSUserDefaults standardUserDefaults];
    NSString*operator;
    
    NSLog(@"%@",operator);
    
    
    if ([[rcOperator stringForKey:@"rcOperator"] length]<1) {
        
        operator=@"Nil";
    } else{
        
        operator=[NSString stringWithFormat:@"%@",[rcOperator stringForKey:@"rcOperator"]];
        
    }
    
    
    NSString*merchant_reference;
    
    
    
    if ([[SDKresponsedict valueForKey:@"merchant_reference"] length]<1) {
        merchant_reference=@"Nil";
        
    } else{
        merchant_reference=[[SDKresponsedict valueForKey:@"merchant_reference"]stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }
    
    
    
    NSUserDefaults * serviceIdDefaults = [NSUserDefaults standardUserDefaults];
    NSString*serviceId;
    
    
    if ([[serviceIdDefaults stringForKey:@"serviceIdDefaults"] length]<1) {
        serviceId=@"Nil";
        
    } else{
        serviceId=[serviceIdDefaults stringForKey:@"serviceIdDefaults"];
    }
    
    
    NSString*response_code;
    
    
    if ([[SDKresponsedict valueForKey:@"response_code"] length]<1) {
        
response_code=@"Nil";
    } else{
        response_code=[SDKresponsedict valueForKey:@"response_code"];
    }
    
    NSString*response_message;
    
    
    if ([[SDKresponsedict valueForKey:@"response_message"] length]<1) {
        
response_message=@"Nil";
    } else{
        response_message=[SDKresponsedict valueForKey:@"response_message"];
    }
    
    
    NSUserDefaults * rcAmount = [NSUserDefaults standardUserDefaults];
    NSString*Rcamount;
    
    if ([[rcAmount stringForKey:@"rcAmount"] length]<1) {
        
Rcamount=@"Nil";
    } else{
        
        Rcamount= [rcAmount stringForKey:@"rcAmount"];
    }
    
    
    
    NSUserDefaults * plandefaults = [NSUserDefaults standardUserDefaults];
    NSString*Plan;
    
    
    if ([[plandefaults stringForKey:@"plandefaults"] length]<1) {
      Plan=@"Nil";

    } else{
     Plan=[NSString stringWithFormat:@"%@",[[plandefaults stringForKey:@"plandefaults"]stringByReplacingOccurrencesOfString:@" " withString:@""]];
    }
    
    
    
    NSString*authorization_code;
    
    if ([[SDKresponsedict valueForKey:@"authorization_code"] length]<1) {
        
authorization_code=@"Nil";
    } else{
        
        authorization_code =[SDKresponsedict valueForKey:@"authorization_code"];


    }
    
    
    NSString*eci;
    
    
    if ([[SDKresponsedict valueForKey:@"eci"] length]<1) {
        
eci=@"Nil";
        
    } else{
        eci=[SDKresponsedict valueForKey:@"eci"];

    }
    
    
    NSString*card_number;
    
    if ([[SDKresponsedict valueForKey:@"card_number"] length]<1) {
        
        card_number=@"Nil";
        
    } else{
        card_number   =[SDKresponsedict valueForKey:@"card_number"];

    }
    
    
    
  
    
    NSString*status;
    
    if ([[SDKresponsedict valueForKey:@"status"] length]<1) {
        
status=@"Nil";
    } else{
        status=[SDKresponsedict valueForKey:@"status"];
    }
    
    NSString*fort_id;
    
    
    if ([[SDKresponsedict valueForKey:@"fort_id"] length]<1) {
        fort_id=@"Nil";
        
    } else{
        fort_id=[SDKresponsedict valueForKey:@"fort_id"];
    }
    
    NSString* userName;
    
    if ([[profileDict valueForKey:@"user_name"] length]<1) {
        
userName=@"Nil";
        
    } else{
      userName= [[[[profileDict valueForKey:@"user_name"] stringByRemovingPercentEncoding] componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@" "];
    }
    
    
    NSString*currency;
    
    if ([[SDKresponsedict valueForKey:@"currency"] length]<1) {
        
currency=@"Nil";
        
    } else{
        currency=[SDKresponsedict valueForKey:@"currency"];
    }
 
    
    NSString*customer_ip;
    
    
    if ([[SDKresponsedict valueForKey:@"customer_ip"] length]<1) {
        customer_ip=@"Nil";
        
    } else{
        customer_ip=[SDKresponsedict valueForKey:@"customer_ip"];
    }
    
    NSString*order_description;
    

    
    if ([[SDKresponsedict valueForKey:@"order_description"] length]<1) {
        
order_description=@"Nil";
    } else{
        order_description=[SDKresponsedict valueForKey:@"order_description"];
    }
    
    NSString*command;
    if ([[SDKresponsedict valueForKey:@"command"] length]<1) {
        
command=@"Nil";
    } else{
        command=[SDKresponsedict valueForKey:@"command"];

    }
    
    
    
    
    NSString*language;
    
    if ([[SDKresponsedict valueForKey:@"language"] length]<1) {
        
language=@"Nil";
    } else{
      language  =[SDKresponsedict valueForKey:@"language"];
    }
    
    
    NSString*expiry_date;
    
    if ([[SDKresponsedict valueForKey:@"expiry_date"] length]<1) {
        
expiry_date=@"Nil";
    } else{
      

expiry_date=[SDKresponsedict valueForKey:@"expiry_date"];

    }
    
    NSUserDefaults * Couponcode = [NSUserDefaults standardUserDefaults];
    NSString*couponCode;
    
    if ([[Couponcode stringForKey:@"Couponecodenum"] length]<1) {
couponCode=@"Nil";
        
    }
    else{
     couponCode    = [Couponcode stringForKey:@"Couponecodenum"];
    }
    
    
    NSString*sdk_token;
    
    
    if ([[SDKresponsedict valueForKey:@"sdk_token"] length]<1) {
        
sdk_token=@"Nil";
    } else{
        sdk_token=[SDKresponsedict valueForKey:@"sdk_token"];
    }
    
    NSString*token_name;
    
    if ([[SDKresponsedict valueForKey:@"token_name"] length]<1) {
        
token_name=@"Nil";
    } else{
        token_name=[SDKresponsedict valueForKey:@"token_name"];
    }
    
    
    
    ////
    
    
    [LoaderClass showLoader:self.view];
    
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/insertRechargethrCardPayTransactionsDetails"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    //         NSString * userPwd=@"585858";
    
    NSLog(@"%@",userId);
    NSLog(@"%@",userPwd);
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];


    
    NSDictionary * loginDetailsDict =  @{@"operator":operator,
                                         @"merchant_reference":merchant_reference,
                                         @"serviceId":serviceId,
                                         @"response_code":response_code,
                                         @"response_message":response_message,
                                         @"amount":Rcamount,
                                         @"rechargePlan":Plan,
                                         @"authorizationCode":authorization_code,
                                         @"eci":eci,
                                         @"cardNumber":card_number,
                                         @"status":status,
                                         @"fortid":fort_id,
                                         @"customerName":userName,
                                         @"currency":currency,
                                         @"customerIp":customer_ip,
                                         @"orderDescription":order_description,
                                         @"command":command,
                                         @"paymentOption":@"CreditCard",
                                         @"language":language,
                                         @"expiry_date":language,
                                         @"coupon_code":expiry_date,
                                         @"sdk_token":sdk_token,
                                         @"token_name":token_name,
                                         @"access_token":accessToken,
                                         @"unique_id":uniqueid,
                                         @"serviceId":serviceId,
                                         @"user_mob":userId
                                         };
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSError *deserr;
            
            
            NSDictionary*Killer = [NSJSONSerialization
                                   
                                   JSONObjectWithData:data
                                   
                                   options:kNilOptions
                                   
                                   error:&deserr];
            
            
            NSString*  str =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            
            NSLog(@"Killer Response is%@",Killer);
            
            NSLog(@"str Response is%@",str);
            
            
            if ([[Killer valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"returnUrl";
                
                [self checkTokenStatusWebServices];
                
                
            }else if ([[Killer valueForKey:@"response_message"]isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            
            else{
                

            
            
            
            [LoaderClass removeLoader:self.view];
            
        NSUserDefaults * Couponcode = [NSUserDefaults standardUserDefaults];
        
        [Couponcode removeObjectForKey:@"Couponecodenum"];
        
        
        
        NSLog(@"return Url Response is :%@",Killer);
        
        NSUserDefaults *SuccessResponsecard = [NSUserDefaults standardUserDefaults];
        NSData * CardTrData = [NSKeyedArchiver archivedDataWithRootObject:Killer];
        [SuccessResponsecard setObject:CardTrData forKey:@"SuccessResponsecard"];
        
        
        [_activityView stopAnimating];
        [_loadingView removeFromSuperview];


        
        RehargeSuccessVC * rechargesuccess = [self.storyboard instantiateViewControllerWithIdentifier:@"RehargeSuccessVC"];
        [self.navigationController pushViewController:rechargesuccess animated:YES];
        
        NSUserDefaults * couponcodenum = [NSUserDefaults standardUserDefaults];
        [couponcodenum removeObjectForKey:@"couponcodenum"];
        
            }
        });
        
        
        
    }];
    
    
    [dataTask resume];
    
    
    
    
}




@end
