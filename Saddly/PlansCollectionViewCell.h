//
//  PlansCollectionViewCell.h
//  Saddly
//
//  Created by Sai krishna on 6/28/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlansCollectionViewCell : UICollectionViewCell




@property (strong, nonatomic) IBOutlet UIImageView *imgOperator;
@property (strong, nonatomic) IBOutlet UILabel *lblAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblPlan;



@end
