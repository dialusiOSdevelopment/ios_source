//
//  ShowQRCodeVC.h
//  DialuzApp
//
//  Created by Sai krishna on 1/2/17.
//  Copyright © 2017 Dialuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+MDQRCode.h"
#import "MCLocalization.h"
#import <Photos/Photos.h>



@interface ShowQRCodeVC : UIViewController{


   NSString*killer;
    
    UIImage *img;
    
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    NSMutableData *pdfData;

}

@property (strong, nonatomic) IBOutlet UIButton *SharebttnName;
@property (strong, nonatomic) IBOutlet UIButton *DnldBttnName;
@property (strong, nonatomic) IBOutlet UIButton *PrintBttnName;

@property (strong, nonatomic) IBOutlet UIImageView *imgDialusLogo;

@property (strong, nonatomic) IBOutlet UIImageView *imgQRCode;

@property (retain)UIDocumentInteractionController *docController;

@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lbluserMobNum;

// Names
@property (strong, nonatomic) IBOutlet UILabel *lblNameScanDialusCode;
@property (strong, nonatomic) IBOutlet UILabel *lblNameEntMobNum;

@property (strong, nonatomic) IBOutlet UIView *aView;


- (IBAction)DownloadQrcode:(id)sender;

- (IBAction)ShareQrcode:(id)sender;

- (IBAction)PrintQRcode:(id)sender;

@end
