//
//  UpdateProfileOTP.m
//  Saddly
//
//  Created by Sai krishna on 7/15/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "UpdateProfileOTP.h"

@interface UpdateProfileOTP ()

@end

@implementation UpdateProfileOTP

- (void)viewDidLoad {
    
    
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
        _Greetingtxt.text=@" رائع، شكرا لك";
        _LblActivationcode.text=@" كود التفعيل";
        _OTP.placeholder=@"كود التفعيل";
        _TxtActivationCode.text=@"فضلاً أدخل رمز التحقق الذي ارسل  لرقم جوالك لتحديث البيانات";
        
        [_btnSubmitOutlet setTitle:@"تسجيل" forState:UIControlStateNormal];
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
        
        
        _Greetingtxt.text=@"Wonderful,Thankyou";
        _LblActivationcode.text=@"Activation Code";
        _OTP.placeholder=@"Activation Code";
        _TxtActivationCode.text=@"Please enter the activation code recieved on your mobile number to update the details";
        
        [_btnSubmitOutlet setTitle:@"Done" forState:UIControlStateNormal];
        
    }
    
    
    // Fetching the data from the profileDetailsDefaults.
    NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
    profileDict = [NSKeyedUnarchiver unarchiveObjectWithData:[profileDetailsDefaults objectForKey:@"profileDetailsDefaults"]];
    
    
    
    
    self.title = [MCLocalization stringForKey:@"verification"];
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    // Returning Keyboard For Number Pad.
    UIToolbar * keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Next/التالي"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(NextClicked)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    doneButton.tintColor = [UIColor colorWithRed:0.0/255.0 green:172.0/255.0 blue:236.0/255.0 alpha:1.0];
    _OTP.inputAccessoryView = keyboardDoneButtonView;
    
    
    // tapping on view, Removing the Keyboard.
    UITapGestureRecognizer * tapKeyboardGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(NextClicked)];
    tapKeyboardGesture.numberOfTapsRequired = 1;
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapKeyboardGesture];
    
    
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)NextClicked{
    [self.view endEditing:YES];
}


-(void) checkTokenStatusWebServices {
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":userId,
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"getUpdateProfileServices"]) {
                    
                    [self getUpdateProfileServices];
                    
                }
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//



// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    alert.popoverPresentationController.sourceView = self.view;
    alert.popoverPresentationController.sourceRect = self.view.bounds;
    
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}



-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
        [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
        
        
        
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}


//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}






- (IBAction)btnShowPwd:(UIButton *)sender {
    
    sender.selected  =! sender.selected;
    
    if (sender.selected) {
        
        _OTP.secureTextEntry = NO;
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        
    } else {
        
        _OTP.secureTextEntry = YES;
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black.png"] forState:UIControlStateNormal];
        [_btnShowPwdOutlet setBackgroundImage:[UIImage imageNamed:@"dark-eye-black-hide.png"] forState:UIControlStateSelected];
        
    }
    
}


//Setting the range of the TextField
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if ([textField isEqual:_OTP]) {
        
        if([self isNumeric:string]){
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return TRUE && newLength <= 4;
        } else  {
            return FALSE;
        }
        
    }
    return YES;
}





// Entering only numbers not a special Characters
-(BOOL)isNumeric:(NSString*)inputString
{
    NSCharacterSet *cs=[[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    NSString *filtered;
    filtered = [[inputString componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [inputString isEqualToString:filtered];
}


-(void)Validation{
    
    NSUserDefaults * loginResponseDefaults = [NSUserDefaults standardUserDefaults];
    [loginResponseDefaults setObject:getOTPDetails forKey:@"loginResponseDefaults"];
    
    
    if ([[getOTPDetails objectForKey:@"Message"] isEqualToString:@"account doesn't exist"]) {
        
        alertTitle = [MCLocalization stringForKey:@"getOTP"];
        alertMessage = [[[MCLocalization stringForKey:@"noAccMobNum"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
        [self alertMethod];
        
    } else if ([[getOTPDetails objectForKey:@"Message"] isEqualToString:@"Something Went wrong"]) {
        
        alertTitle = [MCLocalization stringForKey:@"getOTP"];
        alertMessage = [[[MCLocalization stringForKey:@"wentWrong"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
        [self alertMethod];
        
    } else if ([[getOTPDetails objectForKey:@"Message"] isEqualToString:@"Please Enter the Mobile Number with 9 digits starting with 5"]) {
        
        alertTitle =  [MCLocalization stringForKey:@"getOTP"];
        alertMessage = [MCLocalization stringForKey:@"mobilevalidate"];;
        [self alertMethod];
        
        
    }  else {
        
        
        if ([_OTP.text isEqualToString:[getOTPDetails objectForKey:@"Message"]]) {
            
            UpdateDetails * updatedetails  = [self.storyboard instantiateViewControllerWithIdentifier:@"UpdateDetails"];
            [self.navigationController pushViewController:updatedetails animated:YES];
            
            
        } else {
            
            alertTitle=[MCLocalization stringForKey:@"incorrOTP"] ;
            alertMessage=[MCLocalization stringForKey:@"ValidOTP"];
            
            [self alertMethod];
            
        }
    }
}



-(void)getUpdateProfileServices{
    
    
    [LoaderClass showLoader:self.view];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    
    
                          
    NSString*from=@"iphone";
                          
    
///
    
    
    
    ///
    
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/profile/updateProfile"];
    
    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    
    request.HTTPMethod = @"POST";
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSString*uniqueid=   [uuidDefaults stringForKey:@"uuidDefaults"];
    
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSString*accessToken=   [accessTokenDefaults valueForKey:@"accessTokenDefaults"];
    
    
    
    NSString * userId = [profileDict valueForKey:@"user_mob"];
    
    NSUserDefaults*pddefaults=[NSUserDefaults standardUserDefaults];
    
    NSString * userPwd = [pddefaults stringForKey:@"pddd"];
    
    NSString * encodeMobPwd = [[userId stringByAppendingString:@":"] stringByAppendingString:userPwd];
    
    NSData * nsdata123 = [encodeMobPwd dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64encodeMobPwd = [nsdata123 base64EncodedStringWithOptions:0];
    NSLog(@"base 64 Encoded Mob Num & Pwd:%@",base64encodeMobPwd);
    
    
    NSString * authValue = [NSString stringWithFormat:@"Basic %@", base64encodeMobPwd];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
 
    
    NSDictionary * loginDetailsDict =  @{@"user_name":_OTPName ,
                                         
                                         @"user_mob":_OTPMobNum,
                                         
                                         @"user_email_id":_OTPEmail,
                                         @"otp":_OTP.text,
                                         @"ipAddress":[ipaddressdefault objectForKey:@"ipaddressdefault"],
                                         @"from":from,
                                         @"access_token":accessToken,
                                         @"unique_id":uniqueid
                                         
                                         };
    
    
    
    NSLog(@"Posting loginDetailsDict is %@",loginDetailsDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:loginDetailsDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        
        if (!data) {
            
            
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            
            NSLog(@"%@",userErrorText);
            
            return;
            
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            
            
            NSError *deserr;
            
            
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       
                                       JSONObjectWithData:data
                                       
                                       options:kNilOptions
                                       
                                       error:&deserr];
            
            
            
            
            
            NSString*     resSrt  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            [LoaderClass removeLoader:self.view];
            
            NSLog(@"Killer Response is%@",resSrt);
            
            NSLog(@"Killer Response is%@",Killer);
            
            
            
            if ([[Killer valueForKey:@"response_message" ] isEqualToString:@"TokenExpired"]) {
                
                methodname=@"getUpdateProfileServices";
                
                [self checkTokenStatusWebServices];
                
                
            }else if ([[Killer valueForKey:@"response_message" ] isEqualToString:@"userUnauthorized"]){
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            }
            else{

            
                
                NSString * otpFromServer2  =[Killer valueForKey:@"response_message"];
                
                NSLog(@"Update Profile Response is: %@",otpFromServer2);
                
                [LoaderClass removeLoader:self.view];
                
                
                
                if ([otpFromServer2 isEqualToString:@"success"]) {
                    
                    [self performSegueWithIdentifier:@"UpdateProfileOTPHome" sender:nil];
                    
                } else if ([otpFromServer2 isEqualToString:@"email alredy exist"]) {
                    
                    
                    toastMsg = [[[MCLocalization stringForKey:@"emailalreadytaken"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"Chooseanotherone"]];
                    [self toastMessagemethod];
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                } else if ([otpFromServer2 isEqualToString:@"invalid otp"]) {
                    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"SA"]) {
                        toastMsg = @"Pleasen enter correct otp";
                        [self toastMessagemethod];
                        
                    }else{
                        toastMsg=@" يرجى إدخال كلمة مرور صحيحة";
                        
                        [self toastMessagemethod];
                    }
                }else if ([otpFromServer2 isEqualToString:@"otp expired"]) {
                    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"SA"]) {
                        toastMsg = @"Otp is expired";
                        [self toastMessagemethod];
                        
                    }else{
                        toastMsg=@"كلمة المرور المؤقته غير متاحه";
                        [self toastMessagemethod];
                    }
                }
                    else {
                        
                        alertMessage = [[[MCLocalization stringForKey:@"wentWrong"] stringByAppendingString:@"\n"] stringByAppendingString:[MCLocalization stringForKey:@"plsTryAgain"]];
                        [self alertMethod];
                        
                    }
                
    
            }
        });
     
     }];
            
            
            [dataTask resume];
            ////////////
            
        }



- (IBAction)Submit:(id)sender {
    
    
        [self getUpdateProfileServices];
        
    }


    
//    [self Validation];
//}




@end
