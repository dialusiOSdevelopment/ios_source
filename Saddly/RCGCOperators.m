//
//  RCGCOperators.m
//  Saddly
//
//  Created by Sai krishna on 6/17/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import "RCGCOperators.h"

@interface RCGCOperators ()

@end

@implementation RCGCOperators

- (void)viewDidLoad {
    
    
    // Navigation Bar
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:29.0/255.0 green:142.0/255.0 blue:80.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
        
        [self setFontFamily:@"JFFlat-Regular" forView:self.view andSubViews:YES];
        
    } else {
        
        [self setFontFamily:@"MyriadPro-Regular" forView:self.view andSubViews:YES];
    }
    
    _scrollViewOperaotrs.scrollEnabled =YES;
    
    NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"] || [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"guestOperator"]) {
        
        _scrollViewOperaotrs.scrollEnabled =YES;

        self.title = [MCLocalization stringForKey:@"operator"];
        
        [self operatorsServicesMethod];
        _lblSelOperatorName.text = [MCLocalization stringForKey:@"selOperator"];
        
        operatorNames = @[@"Sawa",@"Zain",@"Virgin",@"Friendi",@"Jawwy",@"Quicknet", @"Mobily",@"Lebara"];
        
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]){
            
            //            operatorNamesAR = @[@"سوى",@"زين",@"فيرجن",@"فريندلي",@"جاوي",@"كويكنت", @"موبايلي",@"ليبارا"];
            
            operatorNamesAR = @[@"سوى",@"جوي",@"فيرجن",@"فريندلي",@"زين",@"سوا", @"موبايلي",@"ليبارا"];
            
        } else {
            
            operatorNamesAR = @[@"Sawa",@"Zain",@"Virgin",@"Friendi",@"Jawwy",@"Quicknet", @"Mobily",@"Lebara"];
            
        }
        

        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {

            operatorImages = @[@"Sawaar.png",@"Zainar.png", @"Virginar.png",@"Friendiar.png", @"Jawwyar.png",@"Quicknetar.png",@"Mobilyar.png", @"Leberaar.png"];

        } else {
            
            operatorImages = @[@"sawaen.png",@"zainen.png", @"virginen.png",@"friendien.png", @"jawwyen.png",@"quickneten.png",@"mobilyen.png", @"lebaraen.png"];

        }
        
    } else {
        
        _scrollViewOperaotrs.scrollEnabled =NO;

        self.title = [MCLocalization stringForKey:@"giftCards"];
        _lblSelOperatorName.text = [MCLocalization stringForKey:@"selAnOption"];
        
        operatorNames = @[@"iTunes",@"Xbox",@"Playstation"];
        operatorNamesGiftCards = @[[MCLocalization stringForKey:@"itunesGC"],[MCLocalization stringForKey:@"xboxGC"],[MCLocalization stringForKey:@"playstationGC"]];
        
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
            
            operatorImages = @[@"itunsar.png",@"xbox-cardar.png",@"ps-cardar.png"];
            
        } else {
            
            operatorImages = @[@"itunsen.png",@"Xbox Carden.png",@"ps-carden.png"];

        }
    }
    
    
    // When Lebara is Not in Live
    if ([[operatorDetails valueForKey:@"Lebara"]isEqualToString:@"remove"]) {
        
        operatorNames = @[@"Sawa",@"Zain",@"Virgin",@"Friendi",@"Jawwy",@"Quicknet", @"Mobily"];
        
        if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
            
            operatorImages = @[@"Sawaar.png",@"Zainar.png", @"Virginar.png",@"Friendiar.png", @"Jawwyar.png",@"Quicknetar.png",@"Mobilyar.png", @"Leberaar.png"];
            
        } else {
            
            operatorImages = @[@"sawaen.png",@"zainen.png", @"virginen.png",@"friendien.png", @"jawwyen.png",@"quickneten.png",@"mobilyen.png", @"lebaraen.png"];
            
        }
        
    }
    
    if ([[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
        
        _imgOperatorbanner.image = [UIImage imageNamed:@"operators-banner3.jpg"];
        
    } else {
        
        _imgOperatorbanner.image = [UIImage imageNamed:@"operators-banner-GC.jpg"];
        
    }

    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated {
    
    NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
    

    
    NSUserDefaults * operNameDefaults = [NSUserDefaults standardUserDefaults];
    [operNameDefaults removeObjectForKey:@"operNameDefaults"];
    
    NSUserDefaults * operImageDefaults = [NSUserDefaults standardUserDefaults];
    [operImageDefaults removeObjectForKey:@"operImageDefaults"];
    
    if ([[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
        
        _imgOperatorbanner.image = [UIImage imageNamed:@"operators-banner3.jpg"];
        
    } else {
        
        _imgOperatorbanner.image = [UIImage imageNamed:@"operators-banner-GC.jpg"];
        
    }

    
    [self.collecOperators reloadData];
    
}


-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
    }
    
    if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *lbl = (UIButton *)view;
        
        [lbl.titleLabel  setFont:[UIFont fontWithName:fontFamily size:19.f]];
        
        [lbl.titleLabel setFont:[UIFont boldSystemFontOfSize:13.f]];
        
    }
    
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
        [lbl setFont:[UIFont fontWithName:fontFamily size:17.f]];
        
    }
    
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}



// Alert Method
-(void)alertMethod{
    
    alert = [UIAlertController
             alertControllerWithTitle:alertTitle
             message:alertMessage
             preferredStyle:UIAlertControllerStyleAlert];
    
    okButton = [UIAlertAction actionWithTitle:[MCLocalization stringForKey:@"ok"]
                                        style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [self presentedViewController];
                                      }];
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}



//toast messages method
-(void)toastMessagemethod{
    
    CSToastStyle * style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.messageColor = [UIColor whiteColor];
    style.backgroundColor = [UIColor blackColor];
    
    [self.view makeToast:toastMsg
                duration:2.0
                position:CSToastPositionCenter
                   style:style];
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:YES];
    [CSToastManager setQueueEnabled:YES];
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


// For Operators
-(void)operatorsServicesMethod{
  
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    
    // Setup the request with URL
    NSURL *url = [NSURL URLWithString:@"https://m.app.saddly.com/doRecharge/getOperatorsDetails1"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    NSString*from=@"iphone";
    
    
    NSDictionary * operatorDict =  @{@"ipAddress":Ipaddress,
                                     @"from":from,
                                    
                                    };
    
    NSLog(@"Posting operatorDict is %@",operatorDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:operatorDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSLog(@"No data returned from server, error ocurred: %@", error);
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            

            NSString *resSrt = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            
            NSLog(@"resSrt: %@", resSrt);
            NSError *deserr;
            
            
            
            NSDictionary*Response=[NSJSONSerialization
                                   JSONObjectWithData:data
                                   options:kNilOptions
                                   error:&deserr];
            
            
            NSLog(@"got response=%@", Response);
            
            
            operatorDetails = Response;
            
            //    allKeys : Lebara, zain
            //    allValues: show,  don't show
            
            
            
            
            if ([[operatorDetails valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                
                if ([[operatorDetails valueForKey:@"response_message"] isEqualToString:@"TokenExpired"]) {
                    
                    methodname=@"operatorsServicesMethod";
                    
                    NSLog( @"Method Name %@",methodname);
                    [self checkTokenStatusWebServices];
                    
                }
                
                // When Lebara is Not in Live
                else  if ([[operatorDetails valueForKey:@"Lebara"]isEqualToString:@"remove"]) {
                    
                    operatorNames = @[@"Sawa",@"Zain",@"Virgin",@"Friendi",@"Jawwy",@"Quicknet", @"Mobily"];
                    
                    
                    if ([[MCLocalization stringForKey:@"DBvalue"]isEqualToString:@"AR"]) {
                        
                        operatorImages = @[@"Sawaar.png",@"Zainar.png", @"Virginar.png",@"Friendiar.png", @"Jawwyar.png",@"Quicknetar.png",@"Mobilyar.png", @"Leberaar.png"];
                        
                    } else {
                        
                        operatorImages = @[@"sawaen.png",@"zainen.png", @"virginen.png",@"friendien.png", @"jawwyen.png",@"quickneten.png",@"mobilyen.png", @"lebaraen.png"];
                        
                    }
                    
                }
                
                
                showingOperators = [operatorDetails allKeysForObject:@"show"];
                NSLog( @"showingOperators: %@",showingOperators);
                
                [self.collecOperators reloadData];
                          
            }
        });
        
    }];
    
    [dataTask resume];

    
}


   

-(void) checkTokenStatusWebServices {
    
    
    
    NSUserDefaults * macaddressDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * uuidDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * deviceNameDefaults = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * genKeyDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUserDefaults * ipaddressdefault = [NSUserDefaults standardUserDefaults];
    NSString*Ipaddress = [ipaddressdefault objectForKey:@"ipaddressdefault"];
    
    
    
    // Create the URLSession on the default configuration
    NSURLSessionConfiguration *defaultSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession * defaultSession = [NSURLSession sessionWithConfiguration:defaultSessionConfiguration delegate:self delegateQueue:Nil];
    
    
    // Setup the request with URL
    NSURL * url = [NSURL URLWithString:@"https://m.app.saddly.com/authentication/checkTokenStatus"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSDictionary * checkTokenDict =  @{@"ipaddress":Ipaddress,
                                       @"mac_address":[macaddressDefaults stringForKey:@"macaddressDefaults"],
                                       @"access_token":[accessTokenDefaults stringForKey:@"accessTokenDefaults"],
                                       @"user_mob":@"Guest",
                                       @"from":@"iPhone",
                                       @"unique_id":[uuidDefaults stringForKey:@"uuidDefaults"],
                                       @"device_model":[deviceNameDefaults stringForKey:@"deviceNameDefaults"],
                                       @"genKey":[genKeyDefaults stringForKey:@"genKeyDefaults"],
                                       @"token_type":@"refresh_token",
                                       };
    
    
    NSLog(@"Posting Check token status is %@",checkTokenDict);
    
    
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:checkTokenDict options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [request setHTTPBody:postdata];
    
    
    // Create dataTask
    NSURLSessionDataTask *dataTask = [defaultSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (!data) {
            
            NSString *userErrorText = [NSString stringWithFormat: @"Error communicating with Server: %@", error.localizedDescription];
            NSLog(@"%@",userErrorText);
            return;
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            NSError *deserr;
            
            NSDictionary*    Killer = [NSJSONSerialization
                                       JSONObjectWithData:data
                                       options:kNilOptions
                                       error:&deserr];
            
            
            NSString*     str  =[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
            
            
            NSLog(@"Killer Response is%@",Killer);
            NSLog(@"str Response is%@",str);
            
            
            NSDictionary * tokenStatusDict = [Killer valueForKey:@"respone"];
            
            
            
            if ([[tokenStatusDict valueForKey:@"response_message"] isEqualToString:@"userUnauthorized"]) {
                
                NSUserDefaults *profileDetailsDefaults = [NSUserDefaults standardUserDefaults];
                [profileDetailsDefaults removeObjectForKey:@"profileDetailsDefaults"];
                
                NSUserDefaults * loginName = [NSUserDefaults standardUserDefaults];
                [loginName removeObjectForKey:@"userName"];
                
                
                LoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self.navigationController pushViewController:login animated:YES];
                
            } else  {
                
                a123 = [NSString stringWithFormat:@"%@",[tokenStatusDict valueForKey:@"access_token"]];
                
                NSUserDefaults * accessTokenDefaults = [NSUserDefaults standardUserDefaults];
                [accessTokenDefaults setObject:a123 forKey:@"accessTokenDefaults"];
                
                
                if ([methodname isEqualToString:@"operatorsServicesMethod"]) {
                    [self operatorsServicesMethod];
                }
                
            }
        });
        
    }];
    
    
    [dataTask resume];
    ////////////
    
}
//


                       
                       


#pragma mark --
#pragma mark - UICollectionView Delegate Methods


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return operatorNames.count;
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    RCGCCollectionViewCell * cell = (RCGCCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    if (cell == nil) {
        NSArray * nib = [[NSBundle mainBundle] loadNibNamed:@"RCGCCollectionViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.imgOperator.image = [UIImage imageNamed:[operatorImages objectAtIndex:indexPath.row]];
    
    NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"] || [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"guestOperator"]) {
        
        cell.lblOperator.text = [operatorNamesAR objectAtIndex:indexPath.row];
    } else {
        
        cell.lblOperator.text = [operatorNamesGiftCards objectAtIndex:indexPath.row];
        
    }
    cell.lblOperator.adjustsFontSizeToFitWidth = YES;
    cell.layer.borderColor = [UIColor clearColor].CGColor;
    
    return cell;
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSUserDefaults * btnRcDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"100"]) {
        
        NSString * selectedItem = [operatorNames objectAtIndex:indexPath.row];
        
        BOOL isObjectThere = [showingOperators containsObject:selectedItem];
        
        if (isObjectThere == true) {
            
            
            GuestSelectPackageVC * guestSelPack =[self.storyboard instantiateViewControllerWithIdentifier:@"GuestSelectPackageVC"];
            
            guestSelPack.operName = [operatorNames objectAtIndex:indexPath.row];
            guestSelPack.operImage = [UIImage imageNamed:[operatorImages objectAtIndex:indexPath.row]];
            
            [self.navigationController pushViewController:guestSelPack animated:YES];
            
        } else {
            
            alertMessage = [MCLocalization stringForKey:@"launchsoon"];
            [self alertMethod];
            
        }
        
    } else  if ( [[btnRcDefaults stringForKey:@"btnRcDefaults"] isEqualToString:@"guestOperator"]) {
        
        
        NSString * selectedItem = [operatorNames objectAtIndex:indexPath.row];
        
        BOOL isObjectThere = [showingOperators containsObject:selectedItem];
        
        if (isObjectThere == true) {
            
            NSUserDefaults * operNameDefaults = [NSUserDefaults standardUserDefaults];
            [operNameDefaults setObject:selectedItem forKey:@"operNameDefaults"];
            
            NSUserDefaults * operImageDefaults = [NSUserDefaults standardUserDefaults];
            [operImageDefaults setObject:[operatorImages objectAtIndex:indexPath.row] forKey:@"operImageDefaults"];
            
            [self.navigationController popViewControllerAnimated:YES];
            
        } else {
            
            alertMessage = [MCLocalization stringForKey:@"launchsoon"];
            [self alertMethod];
            
        }
        
    } else {
        
        GuestSelectPackageVC * guestSelPack =[self.storyboard instantiateViewControllerWithIdentifier:@"GuestSelectPackageVC"];
        
        guestSelPack.operName = [operatorNames objectAtIndex:indexPath.row];
        guestSelPack.operImage = [UIImage imageNamed:[operatorImages objectAtIndex:indexPath.row]];
        
        [self.navigationController pushViewController:guestSelPack animated:YES];
        
    }
    
    
}


@end
