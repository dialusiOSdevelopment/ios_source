//
//  HelpVC.h
//  Saddly
//
//  Created by Sai krishna on 3/3/17.
//  Copyright © 2017 mullangi gandhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLocalization.h"
#import <CoreMotion/CoreMotion.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <GooglePlus/GooglePlus.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import "FAQuestionsVC.h"
#import "SocialMediaViewController.h"
#import "WriteUsVC.h"


//@interface MyViewController : UIViewController <GPPSignInDelegate>


@interface HelpVC : UIViewController <NSURLSessionDelegate> {
    
    
    
    UIAlertController * alert;
    UIAlertAction* okButton;
    NSString * alertTitle, * alertMessage, * toastMsg;
    
    
    NSDictionary * socialMediaDetails, * profileDict;
    NSString * methodname, * a123;
    
}



@property (strong, nonatomic) IBOutlet UIButton *btnFAQsOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnDemoVideoOutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnfollowUsOnoutlet;
@property (strong, nonatomic) IBOutlet UIButton *btnWriteUsOutlet;


- (IBAction)btnFAQs:(id)sender;
- (IBAction)btnDemoVideo:(id)sender;
- (IBAction)btnFollowUsOn:(id)sender;
- (IBAction)btnWriteUs:(id)sender;






@end
